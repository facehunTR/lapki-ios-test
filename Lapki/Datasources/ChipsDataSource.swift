//
//  ChipsDataSource.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 08/04/2019.
//  Copyright © 2019 MEGAKOM, OOO. All rights reserved.
//

import UIKit


struct Chips{
    let filterType:String
    var image:UIImage?
    let chipText:String
    init(type:String? = nil , text:String? = nil) {
        self.filterType = type ?? ""
        self.chipText = text ?? ""
    }
}


class ChipsDataSource: NSObject,UICollectionViewDataSource {
    var chipsArray = [Chips]()
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return chipsArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:ChipsCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChipsCell", for: indexPath) as! ChipsCell
        cell.setParams(filter: chipsArray[indexPath.row].chipText, image: chipsArray[indexPath.row].image,index:indexPath)
        return cell
    }

    enum FilterTypes:String,CaseIterable{
        case distanceTo = "dist"
        case category = "category"
        case type = "type"
        case subType = "subtype"
        case subsubType = "subsubtype"
        case byAgeMin = "age__gt"
        case byAgeMax = "age__lt"
        case priceMin = "price__gt"
        case priceMax = "price__lt"
        case search = "search"
        case ordering = "ordering"
        case country = "country"
        }
    
    func chekIfContains(type:String) -> Bool{
        for each in chipsArray{
            if each.filterType == type{
                return true
            }
        }
        return false
    }
    
    func addFilter(key:String,value:String){
        var chip = Chips()
        func chipInit(value:String,image:UIImage? = nil){
            chip = Chips.init(type: key, text: value)
            if value != ""{
            chip.image = image
            chipsArray.append(chip)
            }
        }
        switch key {
        case FilterTypes.distanceTo.rawValue:
            chipInit(value: value)
        case FilterTypes.byAgeMin.rawValue:
            chipInit(value:"От " + value )
        case FilterTypes.byAgeMax.rawValue:
            chipInit(value: "До " + value)
        case FilterTypes.priceMin.rawValue:
            chipInit(value:"От " + value + " Р" )
        case FilterTypes.priceMax.rawValue:
            chipInit(value:"До " + value + " Р" )
        case FilterTypes.category.rawValue,FilterTypes.type.rawValue:
            chipInit(value: value,image:getPojoImage(name:value))
        case FilterTypes.subType.rawValue,FilterTypes.subsubType.rawValue:
            chipInit(value:value)
        case FilterTypes.search.rawValue:
            chipInit(value: value)
        case FilterTypes.country.rawValue:
            let val = Countries.init(rawValue: value)
            chipInit(value: val!.origin,image: val!.flag)
        case FilterTypes.ordering.rawValue:
            if value == "-dist" {
                chipInit(value: "По геолокации")
            }
            else if value == "-date_uplift" {
                chipInit(value: "По дате публикации")
            }
        default:
            break
        }
        
    }
    
    
    func getOrderOfParameter(parameterKey:String) -> Int{
        switch parameterKey {
        case FilterTypes.distanceTo.rawValue:
            return 0
        case FilterTypes.category.rawValue:
            return 1
        case FilterTypes.type.rawValue:
            return 2
        case FilterTypes.subType.rawValue:
            return 3
        case FilterTypes.subsubType.rawValue:
            return 4
        case FilterTypes.priceMin.rawValue:
            return 5
        case FilterTypes.priceMax.rawValue:
            return 6
        case FilterTypes.byAgeMin.rawValue:
            return 7
        case FilterTypes.byAgeMax.rawValue:
            return 8
        case FilterTypes.search.rawValue:
            return 9
        case FilterTypes.ordering.rawValue:
            return 10
        case FilterTypes.country.rawValue:
            return 11
        default:
            return Int.max
        }
    }
    func cc(parameters:[String:Any?]?){
        guard let parameters = parameters?.sorted(by:{return (getOrderOfParameter(parameterKey: $0.key))<getOrderOfParameter(parameterKey: $1.key)}) else{
            return
        }
        dump(parameters)
        chipsArray.removeAll()
        for each in parameters{
            if let int:Int = each.value as? Int{
                var val = ""
                if each.key == FilterTypes.distanceTo.rawValue{
                    val = int.toStringKm()
                }
                else if each.key == FilterTypes.byAgeMin.rawValue || each.key == FilterTypes.byAgeMax.rawValue{
                    val = int.daysToMonthsAndYears()
                }
                else if each.key == FilterTypes.priceMin.rawValue || each.key == FilterTypes.priceMax.rawValue{
                    
                    val = String(int - 1)
                    
                }
                else{
                    val = String(int)
                }
                
                addFilter(key:each.key,value:val)
            }
            else if let str:String = each.value as? String{
                addFilter(key:each.key,value: str)
            }
        }

    }
    
}

