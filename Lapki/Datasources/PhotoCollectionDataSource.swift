
//  Lapki
//
//  Created by Yuriy Yashchenko on 20/02/2019.
//  Copyright © 2019 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import MaterialComponents
import AudioToolbox
import DKImagePickerController

class PhotoCollectionDataSource: NSObject,UICollectionViewDataSource {
    var photoImages:[UIImage] = []
    var photoImageViews:[UIImageView] = []
    var cellArray:[PhotoCell] = []
    var mode = "edit"
    let imagePickerController = DKImagePickerController()
        var photoCollection:UICollectionView = UICollectionView.init(frame: CGRect.zero, collectionViewLayout: UICollectionViewLayout.init())
    func numberOfSections(in collectionView: UICollectionView) -> Int {
       
        if photoCollection != collectionView {
            photoCollection = collectionView

        }
        
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView
            .dequeueReusableCell(withReuseIdentifier: "photoCell", for: indexPath) as! PhotoCell
        cell.layer.masksToBounds = false
        photoImageViews.append(cell.photo) 
        cell.isSelectable = false
        cell.cornerRadius = 8
        cell.inkView.inkColor = UIColor.inkColors.orange
        cell.setShadowElevation(ShadowElevation(rawValue: 1.0), for: .normal)
        cell.setShadowColor(UIColor.black, for: .normal)
        cell.photoButton.addTarget(self, action: #selector(imageClicked(sender:)), for: .touchUpInside)
        cell.clearButton.addTarget(self, action: #selector(clearImage(sender:)), for: .touchUpInside)
        cell.photoButton.tag = indexPath.row
        cell.clearButton.tag = indexPath.row
        if indexPath.row + 1 <= photoImages.count  {
            
            cell.photo.image = photoImages[indexPath.row]
            cell.photo.layer.masksToBounds = true
            cell.photo.layer.cornerRadius = 8
            cell.photo.contentMode = .scaleAspectFill
            cell.clearButton.isHidden = false
        }
        else{
            cell.clearButton.isHidden = true
            cell.photo.image = UIImage(named: "camera_orange_big")
            cell.photo.contentMode = .center
        }
      
        
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        if indexPath.row + 1 > photoImages.count{
            return false
        }
        else{
        return true
        }
    }
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        if destinationIndexPath.row + 1 <= photoImages.count{
            photoImages.swapAt(sourceIndexPath.row, destinationIndexPath.row)
        }
        else{
            collectionView.moveItem(at:destinationIndexPath , to: sourceIndexPath)

        }
        
    }
    @objc func clearImage(sender:UIButton){
        
        photoImages.remove(at: sender.tag)
        
        photoCollection.reloadData()
    }

    @objc func imageClicked(sender: UIButton) {
        var imagesCanAdd = 0
        imagesCanAdd = 5 - sender.tag
        imagePickerController.didSelectAssets = {(assets:[DKAsset])in
            if !assets.isEmpty{
                for asset in assets{
                    asset.fetchOriginalImage(completeBlock: { (image, info) in
                        
                        let indexI = sender.tag + assets.firstIndex{$0 === asset}!
                        
                        if self.photoImages.count >= indexI + 1{
                            
                            self.photoImages[indexI] = (image ?? nil)!
                        }
                        else {
                            self.photoImages.append((image ?? nil)!)
                        }
                        
                        self.photoCollection.reloadData()
                    })
                }
                
            }
            self.imagePickerController.deselectAll()
        }
        imagePickerController.sourceType = .both
        imagePickerController.showsCancelButton = true
        imagePickerController.view.tintColor = UIColor.lapkiColors.lostOrange
        imagePickerController.maxSelectableCount = imagesCanAdd
        imagePickerController.showsEmptyAlbums = false
        imagePickerController.assetType = .allPhotos
        UIApplication.topViewController()!.present(imagePickerController, animated:true, completion: nil)
    }
    
}


    
    

    

