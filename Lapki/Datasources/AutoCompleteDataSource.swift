//
//  AutoCompleteDataSource.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 28/02/2019.
//  Copyright © 2019 MEGAKOM, OOO. All rights reserved.
//

import UIKit

class AutoCompleteDataSource: NSObject,UITableViewDataSource {
    var dadataSourceArray = [Suggestion]()
    var nominatimArray = GeoDatas()

    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isUserInRussia{
        return dadataSourceArray.count
        }
        else{
            return nominatimArray.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:AutoCompleteCell = tableView.dequeueReusableCell(withIdentifier: "SearchCell", for: indexPath) as! AutoCompleteCell
        cell.selectionStyle = .none
        if isUserInRussia{
            cell.autoCompleteCellLabel.text = dadataSourceArray[indexPath.row].editedValue
        }
        else{
            cell.autoCompleteCellLabel.text = self.nominatimArray[indexPath.row].address!.editedValue
        }
        
        return cell
    }
}
