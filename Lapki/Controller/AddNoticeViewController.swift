//
//  AddNoticeViewController
//  Lapki
//
//  Created by Yuriy Yashchenko on 04/12/2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import MaterialComponents.MDCButton
import MaterialComponents.MaterialTextFields
import MaterialComponents.MDCMultilineTextField
import MaterialComponents.MaterialBottomSheet
import RangeSeekSlider
import Toast_Swift
import SearchTextField
import AudioToolbox
import DKImagePickerController
    
    


class AddNoticeViewController: UIViewController,UICollectionViewDelegate {
    
   
    var indicator: UIActivityIndicatorView?
    let dataSource = PhotoCollectionDataSource()
   
    let impact = UIImpactFeedbackGenerator(style: .light)
    
    var pojo: Pojo!
    var parentView: UIViewController!
    var endOfList: CompletionHandler?
    var category: String?
    var type: String?
    var subType: String? {
        didSet {
            subTypeLabel.text = subType
        }
    }
    
    @IBOutlet weak var collectionView: PhotoCollectionView!
    @IBAction func subTypeClicked(_ sender: Any) {
//        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//        let pickerViewController = storyBoard.instantiateViewController(withIdentifier: "PickerViewController") as! MyPickerViewBottomController
//        pickerViewController.data = subTypes
//        pickerViewController.selected = subType
//        pickerViewController.parentView = self
//        pickerViewController.outPutName = "subType"
//        let bottomSheet = MDCBottomSheetController(contentViewController: pickerViewController)
//        self.present(bottomSheet, animated: true, completion: nil)
    }
    
    @IBOutlet weak var subTypeLabel: UILabel!
    var subTypes: [String?] = [String]()
    
    @IBOutlet weak var bottomHeight: NSLayoutConstraint!
    
    @IBOutlet weak var navBar: MyNavigationBar!
    
    @IBOutlet weak var categoriesHeader: UILabel!
    @IBOutlet weak var categoriesBody: UILabel!
    @IBOutlet weak var categoriesIcon: UIImageView!
    @IBOutlet weak var categoriesCard: MDCCard!
    @IBOutlet weak var subTypeCard: MDCCard!
    @IBOutlet weak var subSubTF: SearchTextField!
    @IBOutlet weak var subSubTypeCard: MDCCard!
    @IBOutlet var cardToPickerConstraint: NSLayoutConstraint!
    @IBOutlet var cardToTFConstraint: NSLayoutConstraint!
    @IBOutlet weak var priceTF: MDCTextField!
    @IBOutlet weak var priceIcon: UIImageView!
    @IBOutlet weak var priceCard: MDCCard!
    @IBOutlet var noticeStackView: UIStackView!
    @IBOutlet weak var noticeScrollView: UIScrollView!
    
    @IBOutlet weak var photoScrollView: UIScrollView!
    @IBOutlet var photoStackView: UIStackView!
    var allTextFieldsControllers = [MDCTextInputControllerOutlined]()
    var allTextFields = [MDCTextField]()
    
    var allTextAreaControllers = [MDCTextInputControllerOutlinedTextArea]()
    var allTextAreas = [MDCMultilineTextField]()
    @IBOutlet weak var photoCard: UIView!
    @IBOutlet weak var photoCard1: MDCCard!
    @IBOutlet weak var photoCard2: MDCCard!
    @IBOutlet weak var photoCard3: MDCCard!
    @IBOutlet weak var photoCard4: MDCCard!
    @IBOutlet weak var photoCard5: MDCCard!
    
    var photoCards = [MDCCard]()
    var photoButtons = [MDCButton]()
    @IBOutlet weak var photoImage1: UIImageView!
    @IBOutlet weak var photoImage2: UIImageView!
    @IBOutlet weak var photoImage3: UIImageView!
    @IBOutlet weak var photoImage4: UIImageView!
    @IBOutlet weak var photoImage5: UIImageView!
    
    var photoImages = [UIImageView]()
    var titleTF: MDCMultilineTextField?
    var descriptionTF: MDCMultilineTextField?
    @IBOutlet weak var titleCard: MDCCard!
    @IBOutlet weak var titleIcon: UIImageView!
    @IBOutlet weak var descriptionCard: MDCCard!
    @IBOutlet weak var descriptionIcon: UIImageView!
    
    @IBOutlet weak var genderLabel: UILabel!
    
    var genders = ["Укажите пол","Самец","Самка","Не определено"]
    @IBOutlet weak var genderCard: MDCCard!
    
    @IBAction func ageCardClicked(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let agePickerViewController = storyBoard.instantiateViewController(withIdentifier: "AgePickerViewController") as! AgePickerViewController
        agePickerViewController.parentView = self
        agePickerViewController.year = ageYear
        agePickerViewController.month = ageMonth
        agePickerViewController.week = ageWeek
        let bottomSheet = MDCBottomSheetController(contentViewController: agePickerViewController)
        self.present(bottomSheet, animated: true, completion: nil)
    }
    @IBOutlet weak var ageCard: MDCCard!
    var gender :String?{
        didSet {
            genderLabel.text = gender
        }
    }
    
    
    @IBAction func genderCardClicked(_ sender: Any) {
//        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//        let pickerViewController = storyBoard.instantiateViewController(withIdentifier: "PickerViewController") as! MyPickerViewBottomController
//        pickerViewController.data = genders
//        pickerViewController.selected = gender
//        pickerViewController.parentView = self
//        pickerViewController.outPutName = "gender"
//        let bottomSheet = MDCBottomSheetController(contentViewController: pickerViewController)
//        self.present(bottomSheet, animated: true, completion: nil)
    }
    
    let imagePickerController = DKImagePickerController()
    
    var ageYear: Int = 0
    var ageMonth: Int = 0
    var ageWeek: Int = 0
    
    @IBAction func categoriesCardClicked(_ sender: MDCCard) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let categoriesNavigationController = storyBoard.instantiateViewController(withIdentifier: "CategoriesNavigationController") as! CategoriesNavigationController
        categoriesNavigationController.parentView = self
        categoriesNavigationController.pojo = pojo
        categoriesNavigationController.checkedType = type
        categoriesNavigationController.checkedCategory = category
        self.present(categoriesNavigationController, animated: true, completion: nil)
    }
    
    
    @IBOutlet weak var ageLabel: UILabel!
    
    var photos: [UIImage?] = [nil,nil,nil,nil,nil]
    
    
    @IBOutlet weak var addressCard: MDCCard!
    
    @IBOutlet weak var addressText: UILabel!
    var lat: String?
    var lng: String?
    var location_address: String?
    var first = true
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        collectionView.dataSource = dataSource
//        if #available(iOS 11.0, *) {
//            collectionView.dragDelegate = self
//        }
        gender = self.genders[0]
        initScrollStackView()
      
        initToolbar()
        initCategoriesView()
        initSubTypeView()
        initPriceView()
        initTitleView()
        initDescriptionView()
        initAddressButton()
        viewReorder()
        collectionView.collectionViewSet()
    }
    
    func getYearString(_ ageYear: Int) -> String?{
        switch (ageYear % 10){
        case let x where x<1:
            return nil
        case 1:
            return " \(ageYear) год"
        case 2,3,4:
            return " \(ageYear) года"
        default:
            return " \(ageYear) лет"
        }
    }
    func getMonthString(_ ageMonth: Int) -> String?{
        switch (ageMonth){
        case let x where (x<1 || x>12):
            return nil
        case 1:
            return " \(ageMonth) месяц"
        case 2,3,4:
            return " \(ageMonth) месяца"
        default:
            return " \(ageMonth) месяцев"
        }
    }
    func getWeekString(_ ageWeek: Int) -> String?{
        guard ageWeek > 0, ageWeek < 4 else {return nil}
        var weekString: String!
        if ageWeek == 1 {
            weekString = " \(ageWeek) неделя"
        }else{
            weekString = " \(ageWeek) недели"
        }
        if (ageYear > 0 || ageMonth > 0) {
            weekString = " и" + weekString
        }
        return weekString
    }
    func ageFormat(){
        if (ageYear + ageMonth + ageWeek) == 0 {
            self.ageLabel.text = "Укажите возраст"
            return
        }
        let yearString = getYearString(ageYear)
        let monthString = getMonthString(ageMonth)
        let weekString = getWeekString(ageWeek)
        self.ageLabel.text = "Возраст: \(yearString ?? "")\(monthString ?? "")\(weekString ?? "")"
    }
    
    @objc func keyboardWillShow(notification: NSNotification){
        guard let userInfo = notification.userInfo else {return}
        let window = UIApplication.shared.keyWindow
        guard let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {return}
        if #available(iOS 11.0, *) {
            bottomHeight.constant = keyboardSize.cgRectValue.height - 44 - (window?.safeAreaInsets.bottom ?? 0)
        } else {
            bottomHeight.constant = keyboardSize.cgRectValue.height - 44
        }
    }
    @objc func keyboardWillHide(notification: NSNotification){
        bottomHeight.constant = 0
    }
    func initAddressButton() {
        let gestureAdd = UITapGestureRecognizer(target: self, action: #selector(addressCardClicked(_:)))
        gestureAdd.delegate = self
        if !(addressCard.gestureRecognizers?.contains(gestureAdd) ?? false){
            addressCard.addGestureRecognizer(gestureAdd)
        }
        addressText.text = location_address ?? "Укажите адрес"
    }
    @objc func addressCardClicked(_ sender: Any){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let addNoticeMapViewController = storyBoard.instantiateViewController(withIdentifier: "AddNoticeMapViewController") as! AddNoticeMapViewController
        addNoticeMapViewController.parentView = self
        self.present(addNoticeMapViewController, animated: true, completion: nil)
    }
  
    func viewReorder() {
        guard let category = category, let type = type else {
            return
        }
        allTextFieldsControllers[0].placeholderText = (category.contains("Потеря")) ?  "Вознаграждение, руб" : "Стоимость, руб"
        
        priceCard.isHidden = category.contains(oneOf: ["Вязка","Находка","Мероприятия"])
        
        genderCard.isHidden = category.contains(oneOf: ["Зоотовары","Мероприятия","Услуги","Для бизнеса"])
        
        ageCard.isHidden = category.contains(oneOf: ["Находка","Зоотовары","Мероприятия","Услуги","Для бизнеса"])
        
        subTypeCard.isHidden = (category.contains(oneOf: ["Зоотовары","Находка","Услуги","Для бизнеса","Мероприятия"]) || type.contains("Собаки"))
        
        subSubTypeCard.isHidden = !type.contains("Собаки")
    }
    
    
    func initPhotoScrollStackView(){
       
        photoImages = [photoImage1,photoImage2,photoImage3,photoImage4,photoImage5]
        
        for card in photoCards {
            let button = MDCButton(frame: CGRect(x: 70, y: 0, width: 30, height: 30))
            //            button.translatesAutoresizingMaskIntoConstraints = false
            button.contentEdgeInsets = .zero
            button.imageEdgeInsets = .zero
            button.setBackgroundColor(UIColor.white.withAlphaComponent(0))
            button.setImage(UIImage(named: "clear_filter_button"), for: .normal)
            button.contentMode = .scaleAspectFit
            button.inkColor = UIColor.inkColors.orange
           // button.addTarget(self, action: #selector(ageCardClicked(sender:)), for: .touchUpInside)
            photoButtons.append(button)
            card.addSubview(button)
            button.isHidden = true
            card.heightAnchor.constraint(equalToConstant: 100).isActive = true
            card.widthAnchor.constraint(equalToConstant: 100).isActive = true
            button.trailingAnchor.constraint(equalTo: card.trailingAnchor).isActive = true
            button.topAnchor.constraint(equalTo: card.topAnchor).isActive = true
            
            
        }
    }
    @available(iOS 11.0, *)
    func collectionView(_ collectionView: UICollectionView, shouldSpringLoadItemAt indexPath: IndexPath, with context: UISpringLoadedInteractionContext) -> Bool {
        if dataSource.photoImages.count <= indexPath.row{
            return false
        }
        else{
            return true
        }
    }
    
    @objc func imageClicked(sender: UIGestureRecognizer){
        for index in 0...photoCards.count-1{
            if photoCards[index].gestureRecognizers?.contains(sender) ?? false   {
                imagePickerController.didSelectAssets = { (assets: [DKAsset]) in
                    if !assets.isEmpty{
                        for i in index...assets.count+index-1 {
                            let indicator = UIActivityIndicatorView(style: .gray)
                            indicator.color = UIColor.lapkiColors.lostOrange
                            indicator.center = self.photoImages[i%self.photoImages.count].center
                            indicator.startAnimating()
                            self.photoImages[i%self.photoImages.count].addSubview(indicator)
                            assets[i-index].fetchOriginalImage(options: nil , completeBlock: { (image, info) in
                                self.photoImages[i%self.photoImages.count].contentMode = .scaleAspectFill
                                self.photoImages[i%self.photoImages.count].clipsToBounds = true
                                UIView.transition(with: self.photoImages[i%self.photoImages.count],
                                                  duration: 0.75,
                                                  options: .transitionCrossDissolve,
                                                  animations: { self.photoImages[i%self.photoImages.count].image = image },
                                                  completion: {_ in
                                                    indicator.removeFromSuperview()
                                })
                                self.photos[i%self.photoImages.count] = image
                                self.photoButtons[i%self.photoImages.count].isHidden = false
                            })
                        }
                    }
                }
                imagePickerController.sourceType = .both
                imagePickerController.maxSelectableCount = 5
                imagePickerController.showsEmptyAlbums = false
                present(imagePickerController, animated: true, completion: nil)
                //                presentBottomSheet(imageView:photoImages[index])
            }
        }
    }
    func presentBottomSheet(imageView: UIImageView){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let bottomSheetController = storyBoard.instantiateViewController(withIdentifier: "BottomSheetController")
        let bottomSheet = MDCBottomSheetController(contentViewController: bottomSheetController)
        bottomSheet.preferredContentSize = CGSize(width: self.view.frame.size.width, height: 136)
        present(bottomSheet, animated: true, completion: nil)
    }
    func initTitleView() {
        if titleTF == nil {
            titleTF = MDCMultilineTextField()
            titleCard.addSubview(titleTF!)
            titleTF!.translatesAutoresizingMaskIntoConstraints = false
            titleTF!.textView!.autocapitalizationType = .none
            titleTF!.textView!.keyboardType = .default
            titleTF!.font = UIFont.LapkiFonts.LatoRegular
            
            let titleTFController = MDCTextInputControllerOutlinedTextArea(textInput: titleTF)
            allTextAreaControllers.append(titleTFController)
            allTextAreas.append(titleTF!)
            titleTF!.textView!.delegate = self
            titleTFController.floatingPlaceholderNormalColor = UIColor.gray
            titleTFController.floatingPlaceholderActiveColor = UIColor.lapkiColors.orange
            titleTFController.normalColor = UIColor.gray
            titleTFController.activeColor = UIColor.lapkiColors.orange
            titleTFController.placeholderText = "Заголовок объявления"
            titleTFController.helperText = ""
            titleTFController.characterCountMax = 50
            titleTFController.minimumLines = 1
            titleTFController.textInputFont = UIFont.LapkiFonts.LatoRegular
            titleTFController.inlinePlaceholderFont = UIFont.LapkiFonts.LatoRegular
            titleTFController.leadingUnderlineLabelFont = UIFont.LapkiFonts.LatoRegular
            titleTFController.trailingUnderlineLabelFont = UIFont.LapkiFonts.LatoRegular
            titleTF!.minimumLines = 1
            titleIcon.image = UIImage(named: "label_gray_icon")
            titleCard.inkView.inkColor = UIColor.black.withAlphaComponent(0.0)
            self.titleTF!.leadingAnchor.constraint(equalTo: self.titleIcon.trailingAnchor,constant: 0).isActive = true
            self.titleTF!.trailingAnchor.constraint(equalTo: self.titleCard.trailingAnchor,constant: -11).isActive = true
            self.titleTF!.topAnchor.constraint(equalTo: self.titleCard.topAnchor,constant: 8).isActive = true
            self.titleTF!.bottomAnchor.constraint(equalTo: self.titleCard.bottomAnchor).isActive = true
        }
    }
    func initDescriptionView() {
        if descriptionTF == nil {
            
            descriptionTF = MDCMultilineTextField()
            descriptionCard.addSubview(descriptionTF!)
            descriptionTF!.translatesAutoresizingMaskIntoConstraints = false
            descriptionTF!.textView!.autocapitalizationType = .none
            descriptionTF!.textView!.keyboardType = .default
            descriptionTF!.font = UIFont.LapkiFonts.LatoRegular
            let descriptionTFController = MDCTextInputControllerOutlinedTextArea(textInput: descriptionTF)
            allTextAreaControllers.append(descriptionTFController)
            allTextAreas.append(descriptionTF!)
            descriptionTF!.textView!.delegate = self
            descriptionTFController.floatingPlaceholderNormalColor = UIColor.gray
            descriptionTFController.floatingPlaceholderActiveColor = UIColor.lapkiColors.orange
            descriptionTFController.normalColor = UIColor.gray
            descriptionTFController.activeColor = UIColor.lapkiColors.orange
            descriptionTFController.placeholderText = "Описание объявления"
            descriptionTFController.helperText = ""
            descriptionTFController.characterCountMax = 3000
            descriptionTFController.minimumLines = 1
            descriptionTFController.textInputFont = UIFont.LapkiFonts.LatoRegular
            descriptionTFController.inlinePlaceholderFont = UIFont.LapkiFonts.LatoRegular
            descriptionTFController.leadingUnderlineLabelFont = UIFont.LapkiFonts.LatoRegular
            descriptionTFController.trailingUnderlineLabelFont = UIFont.LapkiFonts.LatoRegular
            descriptionTF!.minimumLines = 1
            descriptionIcon.image = UIImage(named: "description_gray_icon")
            descriptionCard.inkView.inkColor = UIColor.black.withAlphaComponent(0.0)
            self.descriptionTF!.leadingAnchor.constraint(equalTo: self.descriptionIcon.trailingAnchor,constant: 0).isActive = true
            self.descriptionTF!.trailingAnchor.constraint(equalTo: self.descriptionCard.trailingAnchor,constant: -11).isActive = true
            self.descriptionTF!.topAnchor.constraint(equalTo: self.descriptionCard.topAnchor,constant: 8).isActive = true
            self.descriptionTF!.bottomAnchor.constraint(equalTo: self.descriptionCard.bottomAnchor).isActive = true
        }
        
    }
    func initSubTypeView(){
//        subType = nil
//        subSubTF.backgroundColor = UIColor.white
//        subSubTF.theme.bgColor = UIColor.white
//        subSubTF.theme.font = UIFont.LapkiFonts.LatoRegular
//        subSubTF.font = UIFont.LapkiFonts.LatoRegular
//        if (type != nil && category != nil &&
//            !category!.contains(oneOf: ["Находка","Услуги","Для бизнеса","Мероприятия"])){
//            if (pojo.getSubs()![pojo.getNames().firstIndex(of: category)!]?.getSubs()![(pojo.getSubs()![pojo.getNames().firstIndex(of: category)!]?.getNames().firstIndex(of: type))!]?.getSubs()) != nil{
//                subTypes = (pojo.getSubs()![pojo.getNames().firstIndex(of: category)!]?.getSubs()![(pojo.getSubs()![pojo.getNames().firstIndex(of: category)!]?.getNames().firstIndex(of: type))!]?.getNames())!
//                subTypeCard.isHidden = false
//                subType = subTypes[0]
////                subTypeLabel.text = subTypes[0]
//                subSubTF.text = ""
//                subSubTypeCard.isHidden = true
//            }
//            else{
//                subTypes = (pojo.getSubs()![pojo.getNames().firstIndex(of: category)!]?.getSubs()![(pojo.getSubs()![pojo.getNames().firstIndex(of: category)!]?.getNames().firstIndex(of: type))!]?.getNames())!
//                subTypeCard.isHidden = true
//                subSubTypeCard.isHidden = false
//                subSubTF.filterStrings(subTypes as! [String])
//                subSubTF.placeholder = "Укажите породу"
//            }
//        }else{
//            subTypeCard.isHidden = true
//            subSubTypeCard.isHidden = true
//        }
        
    }
    
    func initScrollStackView(){
        if !self.noticeScrollView.subviews.contains(self.noticeStackView){
            self.noticeScrollView.addSubview(self.noticeStackView)
            self.noticeStackView.translatesAutoresizingMaskIntoConstraints = false
            self.noticeStackView.leadingAnchor.constraint(equalTo: self.noticeScrollView.leadingAnchor).isActive = true
            self.noticeStackView.trailingAnchor.constraint(equalTo: self.noticeScrollView.trailingAnchor).isActive = true
            self.noticeStackView.topAnchor.constraint(equalTo: self.noticeScrollView.topAnchor).isActive = true
            self.noticeStackView.bottomAnchor.constraint(equalTo: self.noticeScrollView.bottomAnchor).isActive = true
            self.noticeStackView.widthAnchor.constraint(equalTo: self.noticeScrollView.widthAnchor).isActive = true
        }
    }
    func initPriceView() {
        if priceCard.inkView.alpha != 0.0 {
            priceTF.translatesAutoresizingMaskIntoConstraints = false
            priceTF.font = UIFont.LapkiFonts.LatoRegular
            let priceTFController = MDCTextInputControllerOutlined(textInput: priceTF)
            allTextFieldsControllers.append(priceTFController)
            priceTF.delegate = self
            priceTFController.floatingPlaceholderNormalColor = UIColor.gray
            priceTFController.floatingPlaceholderActiveColor = UIColor.lapkiColors.orange
            priceTFController.normalColor = UIColor.gray
            priceTFController.activeColor = UIColor.lapkiColors.orange
            priceTFController.placeholderText = "Стоимость, руб"
            priceTFController.helperText = ""
            priceTFController.textInputFont = UIFont.LapkiFonts.LatoRegular
            priceTFController.inlinePlaceholderFont = UIFont.LapkiFonts.LatoRegular
            priceTFController.leadingUnderlineLabelFont = UIFont.LapkiFonts.LatoRegular
            priceTFController.trailingUnderlineLabelFont = UIFont.LapkiFonts.LatoRegular
            priceTF.sizeToFit()
            priceCard.inkView.alpha = 0.0
        }else {
            for controller in allTextFieldsControllers {
                controller.textInput?.text = nil
            }
        }
        
        
    }
    
    func initCategoriesView(){
        categoriesCard.inkView.inkColor = UIColor.inkColors.orange
        
        categoriesIcon.image = UIImage(named: "categories_gray_icon")
        
//        if type != nil && category != nil {
//            categoriesHeader.text = pojo.getNames()[0]?.replacingOccurrences(of: "Укажите", with: "Изменить")
//            categoriesBody.text = category! + "/" + type!
//            if pojo.getSubs()![pojo.getNames().firstIndex(of: category!)!]!.getNames().contains("Кошки") {
//                categoriesIcon.image = UIImage.animalsImages[pojo.getSubs()![pojo.getNames().firstIndex(of: category!)!]!.getNames().firstIndex(of: type)!]
//            }
//        }else if category != nil {
//            categoriesHeader.text = pojo.getNames()[0]?.replacingOccurrences(of: "Укажите", with: "Изменить")
//            categoriesBody.text = category!
//            categoriesIcon.image = UIImage.categoriesImages[pojo.getNames().firstIndex(of: category)!+1]
//        }else {
//            categoriesHeader.text = pojo.getNames()[0]
//            categoriesBody.text = "Выбраны все категории"
//        }
    }
    func initToolbar(){
        if navBar.leftBarButtonItem == nil{
            navBar.leftBarButtonItem = MyBarButtonItem(frame: CGRect(x: 0, y: 0, width: 44, height: 44), imageName: "back_orange",action: #selector(backButtonClick(_:)))
            navBar.rightBarButtonItem = MyBarButtonItem(frame: CGRect(x: 0, y: 0, width: 44, height: 44), title: "Опубликовать",action: #selector(acceptButtonClick(_:)))
        }
    }
    
   
    
    
    @objc func backButtonClick(_ sender: Any){
        let alertController = createAlertController(title: "Вы уверены что хотите выйти?", message: nil)
        let action = MDCAlertAction(title:"ДА"){ (action) in
            self.dismiss(animated: true, completion: nil)
        }
        let action2 = MDCAlertAction(title:"НЕТ"){ (action) in
            alertController.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(action)
        alertController.addAction(action2)
        self.present(alertController, animated:true, completion:nil)
//        self.dismiss(animated: true,completion: nil)
    }
    @objc func acceptButtonClick(_ sender: Any){
        self.navBar.rightBarButtonItem?.isEnabled = false
        indicator = UIActivityIndicatorView(style:.whiteLarge)
        indicator!.backgroundColor = UIColor.white
        indicator!.color = UIColor.lapkiColors.lostOrange
        indicator!.center = self.view.center
        indicator!.startAnimating()
        self.view.addSubview(indicator!)
        var blank = false
        for controller in allTextAreaControllers {
            if (controller.textInput?.text?.isEmpty ?? true) && !(controller.textInput?.isHidden ?? true){
                controller.setErrorText("Обязательное поле", errorAccessibilityValue: nil)
                blank = true
            }
            if (controller.errorText != nil){
                let animation = CABasicAnimation(keyPath: "position")
                animation.duration = 0.07
                animation.repeatCount = 4
                animation.autoreverses = true
                animation.fromValue = NSValue(cgPoint: CGPoint(x: (controller.textInput?.center.x)! - 10, y: (controller.textInput?.center.y)!))
                animation.toValue = NSValue(cgPoint: CGPoint(x: (controller.textInput?.center.x)! + 10, y: (controller.textInput?.center.y)!))
                controller.textInput?.layer.add(animation, forKey: "position")
            }
        }
        if blank {
            navBar.rightBarButtonItem?.isEnabled = true
            indicator?.removeFromSuperview()
            return
        }
        var photoBlank = true
        if dataSource.photoImages.count != 0{
            photoBlank = false
        }
        if photoBlank{
            self.view.makeToast("Добавьте не менее одной фотографии", position: .top)
            navBar.rightBarButtonItem?.isEnabled = true
            
            indicator?.removeFromSuperview()
            return
        }
        if type?.isEmpty ?? true {
            self.view.makeToast("Указана неверная категория объявления", position: .top)
            navBar.rightBarButtonItem?.isEnabled = true
            
            indicator?.removeFromSuperview()
            return
        }
        var parameters = [String:String]()
        parameters["title"] = titleTF?.text ?? nil
        parameters["description"] = descriptionTF?.text ?? nil
        if !(gender?.contains("Укажите") ?? true){
            parameters["gender"] = gender
        }
        if !priceTF.isHidden {
            parameters["price"] = priceTF?.text
        }
        if !parameters.keys.contains("price"){
            parameters["price"] = "-1"
        }else if parameters["price"] == nil{
            parameters["price"] = "-1"
        }else if parameters["price"]!.isEmpty {
            parameters["price"] = "-1"
        }
        
        if location_address == nil{
            self.view.makeToast("Укажите адрес", position: .top)
            navBar.rightBarButtonItem?.isEnabled = true
            indicator?.removeFromSuperview()
            return
        }
        parameters["location_address"] = location_address
        parameters["lat"] = lat
        parameters["lng"] = lng
        parameters["old_week"] = "\(ageWeek)"
        parameters["old_month"] = "\(ageMonth)"
        parameters["old_year"] = "\(ageYear)"
        parameters["category"] = category
        parameters["type"] = type
        
        if !(subType?.contains("Укажите") ?? true){
            parameters["subtype"] = subType
        }
        parameters["subsubtype"] = subSubTF?.text ?? nil
//        Network.postNotice(images: dataSource.photoImages, parameters: parameters, responseHandler: weakCompletion(of: self, onResult: AddNoticeViewController.postSuccess, onError: AddNoticeViewController.postError))
    }
    func postSuccess(_ result: Notice){
            indicator?.removeFromSuperview()
            NotificationCenter.default.post(name: NSNotification.Name("userUpdated"), object: nil)
            self.openConfirmNoticeAndDismiss()
    }
    func postError(_ error: Error){
        indicator?.removeFromSuperview()
        makeToast("Произошла ошибка при публикации объявления", position: .top)
        self.navigationItem.rightBarButtonItem?.isEnabled = true
    }
    func openConfirmNoticeAndDismiss(){
        self.dismiss(animated: true) {
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "ConfirmNoticeViewController") as? ConfirmNoticeViewController
            UIApplication.topViewController()?.present(vc!, animated: true, completion: nil)
        }
    }
}

extension AddNoticeViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        
        return false
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        if textField == priceTF{
            let maxLength = 8
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    
}
extension AddNoticeViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        print("print1")
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView == titleTF!.textView {
            allTextAreaControllers[allTextAreas.firstIndex(of: titleTF!)!].setErrorText(nil,errorAccessibilityValue: nil)
            let maxLength = 50
            let currentString: NSString = titleTF!.textView!.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: text) as NSString
            return newString.length <= maxLength
        }
        if textView == descriptionTF!.textView {
            allTextAreaControllers[allTextAreas.firstIndex(of: descriptionTF!)!].setErrorText(nil,errorAccessibilityValue: nil)
            let maxLength = 3000
            let currentString: NSString = descriptionTF!.textView!.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: text) as NSString
            return newString.length <= maxLength
        }
        return true
    }
}
extension AddNoticeViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}








