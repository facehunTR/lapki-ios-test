//
//  CategoriesNavigationController.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 30/11/2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//

import UIKit

class CategoriesNavigationController: UINavigationController {
    
     var pojo: Pojo!
     var checkedType: String?
    var defaultType: String?
    var checkedCategory: String?
    var defaultCategory: String?
    var parentView: UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.defaultType = self.checkedType
        self.defaultCategory = self.checkedCategory
        self.checkedCategory = nil
        self.checkedType = nil
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let categoriesViewController = storyBoard.instantiateViewController(withIdentifier: "CategoriesViewController") as? CategoriesViewController
        categoriesViewController!.pojo = pojo
        categoriesViewController!.checkedType = checkedType
        categoriesViewController!.checkedCategory = checkedCategory
        categoriesViewController?.endOfList = {success in
            if let homePageViewController = self.parentView as? HomePageViewController {
                homePageViewController.parameters["type"] = self.checkedType
                homePageViewController.parameters["category"] = self.checkedCategory
                self.dismiss(animated: true, completion: {homePageViewController.refresh(self)})
            }else if let filterViewController = self.parentView as? FilterViewController {
                filterViewController.checkedType = self.checkedType
                filterViewController.checkedCategory = self.checkedCategory
                self.dismiss(animated: true, completion: {filterViewController.initCategoriesView()
                    filterViewController.initSubTypeView()
                })
            }else if let addNoticeViewController = self.parentView as? AddNoticeViewController {
                if addNoticeViewController.isViewLoaded{
                    if self.checkedType == nil{
                        addNoticeViewController.type = self.defaultType
                        addNoticeViewController.category = self.defaultCategory
                        self.dismiss(animated: true, completion: {addNoticeViewController.viewDidLoad()
                        })
                    }else{
                        addNoticeViewController.type = self.checkedType
                        addNoticeViewController.category = self.checkedCategory
                        self.dismiss(animated: true, completion: {addNoticeViewController.viewDidLoad()
                        })
                    }
                }else{
                    if self.checkedType == nil {
                        addNoticeViewController.type = nil
                        addNoticeViewController.category = nil
                        self.dismiss(animated: true, completion: nil)
                    }else{
                        addNoticeViewController.type = self.checkedType
                        addNoticeViewController.category = self.checkedCategory
                       
                        self.dismiss(animated: false, completion: {
                            UIApplication.topViewController()?.present(addNoticeViewController, animated: true, completion: nil)
                        })
                    }
                }
                
            } else if let editNoticeViewController = self.parentView as? EditNoticeViewController {
                if editNoticeViewController.isViewLoaded{
                    if self.checkedType == nil{
                        editNoticeViewController.type = nil
                        editNoticeViewController.category = nil
                        self.dismiss(animated: true, completion: {editNoticeViewController.viewDidLoad()
                        })
                    }
                    editNoticeViewController.type = self.checkedType
                    editNoticeViewController.category = self.checkedCategory
                    self.dismiss(animated: true, completion: {editNoticeViewController.viewDidLoad()
                    })
                }else{
                    if self.checkedType == nil {
                        editNoticeViewController.type = nil
                        editNoticeViewController.category = nil
                        self.dismiss(animated: true, completion: nil)
                    }
                    editNoticeViewController.type = self.checkedType
                    editNoticeViewController.category = self.checkedCategory
                    self.present(editNoticeViewController, animated: true, completion: nil)
                }
                
            }
        }
        initRootViewController(vc: categoriesViewController!, transitionType: CATransitionType.push.rawValue)
        // Do any additional setup after loading the view.
    }
    func initRootViewController(vc: UIViewController, transitionType type: String, duration: CFTimeInterval = 0.3) {
        self.addTransition(transitionType: type, duration: duration)
        self.viewControllers.removeAll()
        self.pushViewController(vc, animated: false)
        self.popToRootViewController(animated: false)
    }
    private func addTransition(transitionType type: String, duration: CFTimeInterval = 0.3) {
        let transition = CATransition()
        transition.duration = duration
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType(rawValue: type)
        self.view.layer.add(transition, forKey: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
