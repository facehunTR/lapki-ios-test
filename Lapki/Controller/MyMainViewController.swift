//
//  ViewController.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 23.10.2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import MaterialComponents.MDCBottomNavigationBar
import SQLite3
import Toast_Swift
import StoreKit
class MyMainViewController: UIViewController {
    
    
    
    @IBOutlet weak var botNavBar: MDCBottomNavigationBar!
    
    var myPageViewController: MyPageViewController!
    var loginViewController: LoginViewController!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var Categories: Pojo!
    var notice: Notice?
    override func viewDidLoad() {
        super.viewDidLoad()
        let fileURL = Bundle.main.url(forResource: "serializable_db", withExtension: "pj")
        if let data = try? Data(contentsOf: fileURL!) {
            do {
                Categories = try JSONDecoder().decode(Pojo.self, from: data)
            }catch{
                print(error)
            }
        }
        navBarInit()
        StoreReviewHelper.checkAndAskForReview()
        // Do any additional setup after loading the view, typically from a nib.
    }
    func navBarInit(){
        self.botNavBar.delegate = self
        self.botNavBar.selectedItemTintColor = UIColor.lapkiColors.orange
        self.botNavBar.unselectedItemTintColor = UIColor.lapkiColors.lightGray
        var items = [UITabBarItem]()
        items.append(UITabBarItem(title: "", image: UIImage(named: "home")?.withRenderingMode(.alwaysTemplate), tag: 0))
        items.append(UITabBarItem(title: "", image: UIImage(named: "heart")?.withRenderingMode(.alwaysTemplate), tag: 1))
        items.append(UITabBarItem(title: "", image: UIImage(named: "plus")?.withRenderingMode(.alwaysTemplate), tag: 2))
        items.append(UITabBarItem(title: "", image: UIImage(named: "lapkisearch")?.withRenderingMode(.alwaysTemplate), tag: 3))
        items.append(UITabBarItem(title: "", image: UIImage(named: "settings")?.withRenderingMode(.alwaysTemplate), tag: 4))
        self.botNavBar.items = items
        self.botNavBar.elevation = .none
        self.botNavBar.titleVisibility = .always
        self.botNavBar.itemsContentVerticalMargin = 8
        let size = self.botNavBar.sizeThatFits(view.bounds.size)
        let bottomNavBarFrame = CGRect(x: 0,
                                       y: view.bounds.height - size.height,
                                       width: size.width,
                                       height: 44)
        self.botNavBar.frame = bottomNavBarFrame
        self.botNavBar.contentMode = .scaleAspectFit
        self.botNavBar.sizeToFit()
        self.botNavBar.selectedItem = self.botNavBar.items[0]
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "PageViewSegue") {
            self.myPageViewController = segue.destination as? MyPageViewController
            self.myPageViewController.botNavBar = self.botNavBar
            self.myPageViewController.mainViewController = self
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        if notice != nil {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let noticeDetailViewController = storyBoard.instantiateViewController(withIdentifier: "NoticeDetailViewController") as! NoticeDetailViewController
            noticeDetailViewController.notice = self.notice
            noticeDetailViewController.parentView = self
            noticeDetailViewController.myMainViewController = self.myPageViewController.mainViewController
            self.present(noticeDetailViewController, animated: true, completion: {self.notice = nil})
        }
    }
    func HomeBarButtonClicked(_ sender: Any) {
        myPageViewController.changePageTo(index: 0)
    }
    func FavoriteBarButtonClicked(_ sender: Any) {
        myPageViewController.changePageTo(index: 1)
    }
    func AddNoticeBarButtonClicked(_ sender: Any) {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        if MyStaticUser.id == 0 {
            loginViewController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
            loginViewController.mainViewController = self
            self.present(loginViewController, animated: true, completion: nil)
        }
        else if !MyStaticUser.verificationPhone{
            myPageViewController.changePageTo(index: 3)
            makeToast("Для публикации объявлений нужно подтвердить номер телефона",position: .top)
        }
        else {
            let addNoticeViewController = storyBoard.instantiateViewController(withIdentifier: "AddNoticeViewController") as? AddNoticeViewController
            addNoticeViewController!.pojo = Categories
            addNoticeViewController!.parentView = self
            let categoriesNavigationController = storyBoard.instantiateViewController(withIdentifier: "CategoriesNavigationController") as! CategoriesNavigationController
            categoriesNavigationController.parentView = addNoticeViewController
            categoriesNavigationController.pojo = Categories
            categoriesNavigationController.checkedType = nil
            categoriesNavigationController.checkedCategory = nil
            self.present(categoriesNavigationController, animated: true, completion: nil)
        }
    }
    func LapkiSearchBarButtonClicked(_ sender: Any) {
        myPageViewController.changePageTo(index: 2)
    }
    func SettingsBarButtonClicked(_ sender: Any) {
        myPageViewController.changePageTo(index: 3)
    }
}
extension MyMainViewController: MDCBottomNavigationBarDelegate {
    func bottomNavigationBar(_ bottomNavigationBar: MDCBottomNavigationBar, shouldSelect item: UITabBarItem) -> Bool {
        if item.tag == 2 {
            self.AddNoticeBarButtonClicked(item)
            return false
        }
        return true
    }
    func bottomNavigationBar(_ bottomNavigationBar: MDCBottomNavigationBar, didSelect item: UITabBarItem) {
        switch item.tag {
        case 0:
            self.HomeBarButtonClicked(item)
            return
        case 1:
            self.FavoriteBarButtonClicked(item)
            return
        case 3:
            self.LapkiSearchBarButtonClicked(item)
            return
        case 4:
            self.SettingsBarButtonClicked(item)
            return
        default:
            return
        }
    }
}








