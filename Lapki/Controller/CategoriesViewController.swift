//
//  CategoriesViewController.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 29/11/2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import MaterialComponents.MDCButton


class CategoriesViewController: UIViewController {
    var checkedType: String?
    var checkedCategory: String?
    var isInternal: Bool = false
    var categories: [String] = [String]()
    var pojo: Pojo!
    var endOfList: CompletionHandler?
    var k = 1
    var adUrl: String?
    @IBOutlet weak var myTableView: UITableView!
    
    @IBOutlet weak var footerImage: UIImageView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var navBar: MyNavigationBar!
    var ads: Ads?
    override func viewDidLoad() {
        super.viewDidLoad()
        initToolbar()
        
        myTableView.delegate = self
        myTableView.dataSource = self
        initFooter()
        if let navPar = self.navigationController as? CategoriesNavigationController {
            if navPar.parentView is HomePageViewController {
                if checkedCategory != nil {
                    categories.append("Все объявления этой категории")
                    k = 0
                }else{
                    categories.append("Все объявления")
                    k = 0
                }
            }
        }
        
        for index in 1...pojo.names.count-1{
            categories.append(pojo.names[index]!)
        }
        // Do any additional setup after loading the view.
    }
    func initFooter(){
        if footerView.gestureRecognizers?.isEmpty ?? true {
            let gesture = UITapGestureRecognizer(target: self, action: #selector(self.footerClicked(_:)))
            gesture.delegate = self
            footerView.addGestureRecognizer(gesture)
        }
        
        //Первый запуск(или все элементы удалены)
        if ads == nil || ads!.isEmpty{
            ads = getAds(category: self.checkedCategory) ?? getAds(category: "Social")
            ads!.sort(by: { return $0.number > $1.number})
            //Высота футера = ширина экрана / 3
        }
        //Расчет суммы коефициентов
        let coefSum = ads!.map({return $0.coefficient}).reduce(0,+)
        guard coefSum != 0  else{
            return
        }
        let random = Int.random(in: 0...1000)
        //расчет 1 доли в рандоме
        let k = 1000/coefSum
        
        var coefs = [Int]()
        for (index, element) in ads!.enumerated(){
            if index == 0{
                coefs.append(0)
            }
            coefs.append(coefs.last! + element.coefficient*k)
        }
        coefs[coefs.count-1] = 1000
        //Поиск значений которые меньше рандома
        let less = coefs.filter({return $0 < random})
        // Поиск индекса объявления которое выпало
        let result = coefs.firstIndex(of: less.last!)
        adUrl = ads![result!].url
        myTableView.tableFooterView = footerView
        let image = ads![result!].metr_image ?? ads![result!].image ?? ""
        footerImage.kf.setImage(with: URL(string: image),options: [.forceRefresh])
    }
    @objc func footerClicked(_ sender: Any?){
        guard let url = URL(string: adUrl ?? "")else {
            return
        }
        UIApplication.shared.open(url, completionHandler: {success in
            if success {
                self.ads?.removeAll(where: {return $0.url == self.adUrl})
                self.initFooter()
            }
        })
    }

    
    func initToolbar(){
        if navBar.leftBarButtonItem == nil{
            navBar.leftBarButtonItem = MyBarButtonItem(frame: CGRect(x: 0, y: 0, width: 44, height: 44), imageName: "back_orange",action: #selector(backButtonClick(_:)))
            navBar.title = pojo.names[0]
        }
    }
    @objc func backButtonClick(_ sender: Any){
        if let parentC = self.navigationController as? CategoriesNavigationController {
            parentC.checkedType = nil
            parentC.checkedCategory = nil
            endOfList?(true)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension CategoriesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "categoriesCell", for: indexPath) as? CategoriesTableViewCell  else {
            fatalError("The dequeued cell is not an instance of CategoriesTableViewCell.")
        }
        cell.categoriesLabel.text = categories[indexPath.row+k]
        cell.iconImage.image = getPojoImage(name: categories[indexPath.row+k])
        if categories[indexPath.row] == checkedCategory || categories[indexPath.row] == checkedType {
            cell.setSelected(true, animated: true)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !isInternal{
            if categories[indexPath.row].contains("Все объявления"){
                if let parentC = self.navigationController as? CategoriesNavigationController {
                    if categories[indexPath.row].contains("этой"){
                        parentC.checkedType = nil
                        endOfList?(true)
                        return
                    }
                    parentC.checkedType = nil
                    parentC.checkedCategory = nil
                    endOfList?(true)
                    return
                }
            }else{
                checkedCategory = pojo.names[indexPath.row+k]
            }
            if let parentC = self.navigationController as? CategoriesNavigationController {
                parentC.checkedCategory = checkedCategory
            }
            let subPojo = pojo.subs?[indexPath.row+k]
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let categoriesViewController = storyBoard.instantiateViewController(withIdentifier: "CategoriesViewController") as? CategoriesViewController
           
            categoriesViewController!.pojo = subPojo
            categoriesViewController!.checkedType = checkedType
            categoriesViewController!.checkedCategory = checkedCategory
            categoriesViewController!.isInternal = true
            categoriesViewController!.endOfList = endOfList
             guard var viewControllers = self.navigationController?.viewControllers else { return }
            viewControllers.removeLast()
            viewControllers.append(categoriesViewController!)
            self.navigationController?.setViewControllers(viewControllers, animated: true)
        }else{
            if categories[indexPath.row].contains("Все объявления"){
                checkedType = nil
            }else{
                checkedType = pojo.names[indexPath.row+k]
            }
            if let parentC = self.navigationController as? CategoriesNavigationController {
                parentC.checkedType = checkedType
                endOfList!(true)
                return
            }
        }
    }
}
extension CategoriesViewController: UIGestureRecognizerDelegate{
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
