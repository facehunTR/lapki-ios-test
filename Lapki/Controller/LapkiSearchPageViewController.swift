//
//  LapkiSearchPageViewController.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 24/10/2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import MapKit
import MaterialComponents.MaterialButtons
import CoreLocation
class LapkiSearchPageViewController: UIViewController,PageObservation {

    var tileRenderer: MKTileOverlayRenderer!
    @IBOutlet weak var mapView: MKMapView!
    var myPageViewController : MyPageViewController!
    func getParentPageViewController(parentRef: MyPageViewController) {
        myPageViewController = parentRef
    }
    var notices: Notices?
    var userLocation: MKUserLocation?
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        mapView.showsUserLocation = true
        setupTileOverlay()
        Network.getSocialNotices(responseHandler: weakCompletion(of: self, onResult: LapkiSearchPageViewController.setSocialMarkers, onError: LapkiSearchPageViewController.socialNoticesError))
    
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    var region: MKCoordinateRegion?
    
    @IBAction func userLocationButtonClicked(_ sender: Any){
        if userLocation != nil {
            let mapRegion = MKCoordinateRegion(center: userLocation!.coordinate, latitudinalMeters: 500, longitudinalMeters: 500)
            
            self.mapView.setRegion(mapRegion, animated: true)
        }else if lastUserLocation != nil{
            let mapRegion = MKCoordinateRegion(center: userLocation!.coordinate, latitudinalMeters: 500, longitudinalMeters: 500)
            self.mapView.setRegion(mapRegion, animated: true)
        }else{
            self.mapView.setCenter(MyStaticUser.defaultLocation, animated: true)
        }
    }
    
    @IBAction func plusClicked(_ sender: Any){
        if region == nil {
            region = mapView.region
        }
        region!.center = mapView.region.center
        region!.span.latitudeDelta /= 2.0
        region!.span.longitudeDelta /= 2.0
        mapView.setRegion(region!, animated: true)
    }
    @IBAction func minusClicked(_ sender: Any){
        if region == nil {
            region = mapView.region
        }
        region!.center = mapView.region.center
        region!.span.latitudeDelta = min(region!.span.latitudeDelta * 2.0, 180.0)
        region!.span.longitudeDelta = min(region!.span.longitudeDelta * 2.0, 180.0)
        mapView.setRegion(region!, animated: true)
    }
    func setSocialMarkers(_ notices: Notices){
        for notice in notices {
            let annotation = MKPointAnnotation()
            let centerCoordinate = CLLocationCoordinate2D(latitude: notice.lat, longitude:notice.lng)
            annotation.coordinate = centerCoordinate
            annotation.accessibilityHint = "\(notice.id)"
            self.mapView.addAnnotation(annotation)
        }
        self.notices = notices
    }
    func socialNoticesError(_ error: Error){
        self.handleResponseError(error)
        self.notices = nil
        self.mapView.removeAnnotations(self.mapView.annotations)
    }
    @objc func leftEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            if myPageViewController.currentIndex>0{
                myPageViewController.changePageTo(index: myPageViewController.currentIndex-1)
            }
        }
    }
    @objc func rightEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            if myPageViewController.currentIndex<myPageViewController.pages.count{
                myPageViewController.changePageTo(index: myPageViewController.currentIndex+1)
            }
        }
    }
    
    func setupTileOverlay(){
        let overlay = MyMKTileOverlay()
        overlay.canReplaceMapContent = true
        for overlay in mapView.overlays {
            mapView.removeOverlay(overlay)
        }
        mapView.addOverlay(overlay)
        tileRenderer = MKTileOverlayRenderer(tileOverlay: overlay)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension LapkiSearchPageViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
extension LapkiSearchPageViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        guard let tileOverlay = overlay as? MKTileOverlay else {
            return MKOverlayRenderer()
        }
        
        return MKTileOverlayRenderer(tileOverlay: tileOverlay)
    }
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        if self.userLocation == nil {
            let mapRegion = MKCoordinateRegion(center: userLocation.coordinate, latitudinalMeters: 500, longitudinalMeters: 500)
            self.mapView.setRegion(mapRegion, animated: true)
        }
        self.userLocation = userLocation
    }
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation  {
            return nil
        }
        let annotationIdentifier = "Identifier"
        var annotationView: MKAnnotationView?
        //TO-DO Проверить как работает dequeue
        if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
            annotationView = dequeuedAnnotationView
            annotationView!.annotation = annotation
        }
        else {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView!.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        if let pointAnnotation = annotation as? MKPointAnnotation{
            pointAnnotation.title = ""
            if let notice = self.notices?.first(where: {return String($0.id) == pointAnnotation.accessibilityHint}) {
                annotationView!.canShowCallout = false
                if notice.category == "Потеряшка"{
                    annotationView!.image = (notice.price > 0) ? UIImage(named: "lapki_notice_money") : UIImage(named: "lapki_notice_default")
                }else{
                    annotationView!.image = UIImage(named: "lapki_notice_found")
                }
                annotationView!.centerOffset = CGPoint(x: 0, y: -annotationView!.image!.size.height / 2)
                annotationView!.accessibilityHint = pointAnnotation.accessibilityHint
            }
        }
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let id = view.accessibilityHint else {
            return
        }
        guard let notice = notices?.first(where: {return String($0.id) == id}) else{
            return
        }
        mapView.deselectAnnotation(view.annotation, animated: false)
        
        self.performSegue(withIdentifier: "toNotice", sender: notice)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? NewNoticeDetailViewController{
            vc.notice = sender as? Notice
            
        }
    }
}
