//
//  NoticeDetailViewController.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 10/12/2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import ImageSlideshow
import MaterialComponents.MaterialButtons
import MessageUI

class NoticeDetailViewController: UIViewController {
    
    
    @IBOutlet weak var upliftCard: MDCCard!
    var wasUplifted: Bool?
    @IBOutlet weak var errorIcon: UIImageView!
    @IBOutlet weak var statisticCard: MDCCard!
    var parentView: UIViewController!
    var myMainViewController: MyMainViewController!
    @IBOutlet var titleToPrice: NSLayoutConstraint!
    @IBOutlet var titleToTop: NSLayoutConstraint!
    @IBOutlet weak var navBar: MyNavigationBar!
    @IBOutlet weak var subSubTypeLabel: UILabel!
    @IBOutlet weak var subSubTypeCard: MDCCard!
    @IBOutlet weak var subTypeLabel: UILabel!
    @IBOutlet weak var subTypeCard: MDCCard!
    @IBOutlet weak var descriptionCard: MDCCard!
    @IBOutlet weak var typeCard: MDCCard!
    @IBOutlet weak var categoryCard: MDCCard!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var noticeDescriptionTF: UILabel!
    @IBOutlet weak var addressCard: MDCCard!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var noticeTitleTF: UILabel!
    @IBOutlet weak var sendButton: MDCButton!
    @IBOutlet weak var callButton: MDCButton!
    @IBOutlet weak var noticePriceTF: UILabel!
    @IBOutlet var stackView: UIStackView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageSlideShow: ImageSlideshow!
    @IBOutlet var titleCard: MDCCard!
    @IBOutlet var priceToTop: NSLayoutConstraint!
    @IBOutlet weak var statusCard: MDCCard!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var rejectionReasonLabel: UILabel!
    var owner: MyUser?
    @IBOutlet weak var ageCard: MDCCard!
    
    @IBOutlet weak var upliftButton: MyButton!
    @IBOutlet weak var genderCard: MDCCard!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var viewsLabel: UILabel!
    var notice: Notice! {
        didSet{
            //Notice Date
            if let date: Date = DateFormatter().date(fromPythonDate: self.notice.date){
                self.statisticCard.isHidden = false
                let dateFormatter = DateFormatter()
                dateFormatter.locale = Locale(identifier: "ru_RU")
                dateFormatter.dateFormat = "dd MMMM YYYY"
                self.dateLabel.text = "Дата публикации: \(dateFormatter.string(from: date))"
            }else{
                self.statisticCard.isHidden = true
            }
            //Notice Views
            if let views: Int = self.notice.views {
                self.statisticCard.isHidden = false
                self.viewsLabel.text = "Просмотров: \(views)"
            }else{
                self.statisticCard.isHidden = true
            }
            self.statisticCard.sizeToFit()
            //Notice Age
            if self.notice.age > 0 {
                self.ageCard.isHidden = false
                var yearString, monthString, weekString: String?
                if self.notice.oldYear > 0 {
                    switch (self.notice.oldYear % 10){
                    case 1:
                        yearString = " \(self.notice.oldYear) год"
                        break
                    case 2,3,4:
                        yearString = " \(self.notice.oldYear) года"
                        break
                    default:
                        yearString = " \(self.notice.oldYear) лет"
                        break
                    }
                }
                if self.notice.oldMonth > 0 {
                    switch (self.notice.oldMonth){
                    case 1:
                        monthString = " \(self.notice.oldMonth) месяц"
                        break
                    case 2,3,4:
                        monthString = " \(self.notice.oldMonth) месяца"
                        break
                    default:
                        monthString = " \(self.notice.oldMonth) месяцев"
                        break
                    }
                }
                if self.notice.oldWeek > 0 {
                    if self.notice.oldWeek == 1 {
                        weekString = " \(self.notice.oldWeek) неделя"
                    }else{
                        weekString = " \(self.notice.oldWeek) недели"
                    }
                    if (self.notice.oldYear > 0 || self.notice.oldMonth > 0) {
                        weekString = " и" + weekString!
                    }
                }
                self.ageLabel.text = "Возраст: \(yearString ?? "")\(monthString ?? "")\(weekString ?? "")"
                self.ageCard.sizeToFit()
            }else {
                self.ageCard.isHidden = true
            }
            //Notice gender
            if !(self.notice.gender?.isEmpty ?? true) {
                self.genderCard.isHidden = false
                self.genderLabel.text = "Пол: \(self.notice.gender!)"
            }else{
                self.genderCard.isHidden = true
            }
            //Notice uplift button
            
            
            
            if MyStaticUser.id == notice.owner && notice.status.contains("active") {
                upliftCard.inkView.inkColor = UIColor.clear
                self.upliftCard.sizeToFit()
            }
            else if notice.category.contains("Потеря") || notice.category.contains("Находка"){
                print("PRICE:",notice.price)
                if notice.status.contains("active") && notice.price <= 0{
                    upliftCard.inkView.inkColor = UIColor.clear
                    var string = ""
                    if notice.category.contains("Потеря"){
                        string = "Помочь найти питомца"
                    }else{
                        string = "Помочь найти хозяина"
                    }
                    upliftButton.setTitle(string, for: .normal)
                    self.upliftCard.sizeToFit()
                }else{
                    self.upliftCard.removeFromSuperview()
                }
                
            }
                
            else{
                self.upliftCard.removeFromSuperview()
            }
            //Notice status
            if statusCard != nil{
                statusCard.inkView.inkColor = UIColor.white.withAlphaComponent(0)
                if notice.status.elementsEqual("active"){
                    statusCard.backgroundColor = UIColor.white
                    statusCard.removeFromSuperview()
                    errorIcon.removeFromSuperview()
                }else {
                    errorIcon.tintColor = UIColor.red
                    statusLabel.textColor = UIColor.black
                    rejectionReasonLabel.textColor = UIColor.black
                    statusCard.backgroundColor = UIColor.red.withAlphaComponent(0.40)
                    if notice.status.elementsEqual("rejected"){
                        statusLabel.text = "Не одобрено"
                        
                    }else if notice.status.elementsEqual("moderation"){
                        statusLabel.text = "Ждёт одобрения"
                        
                        errorIcon.removeFromSuperview()
                    }else if notice.status.elementsEqual("deleted"){
                        statusLabel.text = "В архиве"
                        
                        errorIcon.removeFromSuperview()
                    }
                    if !(notice.reasonRejection?.isEmpty ?? true) {
                        rejectionReasonLabel.text = notice.reasonRejection
                    }else{
                        rejectionReasonLabel.removeFromSuperview()
                    }
                }
            }
            //Notice address
            addressLabel.text = notice.locationAddress
            addressLabel.sizeToFit()
            addressCard.inkView.inkColor = UIColor.inkColors.orange
            let gesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(addressCardClicked(_:)))
            gesture.delegate = self
            addressCard.addGestureRecognizer(gesture)
            //Notice category
            categoryCard.inkView.inkColor = UIColor.white.withAlphaComponent(0)
            if !notice.category.isEmpty{
                categoryLabel.text = notice.category
                categoryLabel.sizeToFit()
            }else {
                categoryCard.isHidden = true
            }
            //Notice type
            typeCard.inkView.inkColor = UIColor.white.withAlphaComponent(0)
            if !notice.type.isEmpty {
                typeLabel.text = notice.type
                typeLabel.sizeToFit()
            }else {
                typeCard.isHidden = true
            }
            //Call and Send buttons
            if notice.owner == MyStaticUser.id {
                callButton.removeFromSuperview()
                sendButton.removeFromSuperview()
            }
            //Notice subtype
            subTypeCard.inkView.inkColor = UIColor.white.withAlphaComponent(0)
            if notice.subtype != nil && !notice.subtype!.isEmpty{
                subTypeLabel.text = notice.subtype!
                subTypeLabel.sizeToFit()
            }else {
                subTypeCard.isHidden = true
            }
            //Notice subSubType
            subSubTypeCard.inkView.inkColor = UIColor.white.withAlphaComponent(0)
            if notice.subsubtype != nil && !notice.subsubtype!.isEmpty{
                subSubTypeLabel.text = notice.subsubtype!
                subSubTypeLabel.sizeToFit()
            }else {
                subSubTypeCard.isHidden = true
            }
            //Notice Title
            titleCard.inkView.inkColor = UIColor.white.withAlphaComponent(0)
            noticeTitleTF.text = notice.title
            //Notice Price
            noticePriceTF.isHidden = false
            if notice.price == -1 {
                noticePriceTF.removeFromSuperview()
            }else if notice.price == 0 {
                noticePriceTF.text = "Бесплатно"
            }else if notice.price > 0 {
                noticePriceTF.text = "\(notice.price) Р"
            }
            noticePriceTF.sizeToFit()
            //Notice CallButton
            //Notice description
            descriptionCard.inkView.inkColor = UIColor.white.withAlphaComponent(0)
            noticeDescriptionTF.text = notice.description
            noticeDescriptionTF.sizeToFit()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initScrollStackView()
        initToolbar()
        initSlideView()
        Network.getUserData(id:"\(notice!.owner)",responseHandler: weakCompletion(of: self, onResult: NoticeDetailViewController.setOwner, onError: NoticeDetailViewController.handleResponseError))
        Network.noticeViewed(id: self.notice.id)
    }
   
    @IBAction func upliftBtnClick(_ sender: AnyObject){
        guard MyStaticUser.id != 0 else{
            self.view.makeToast("Доступно только авторизированным пользователям", position: .top)
            return
        }
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let upliftViewController = storyBoard.instantiateViewController(withIdentifier: "UpliftViewController") as! UpliftViewController
        upliftViewController.parentView = self
        upliftViewController.notice = self.notice
        self.wasUplifted = true
        self.present(upliftViewController, animated: true, completion: nil)
    }
    func initToolbar(){
        if navBar.leftBarButtonItem == nil{
            navBar.leftBarButtonItem = MyBarButtonItem(frame: CGRect(x: 0, y: 0, width: 44, height: 44), imageName: "back_orange",action: #selector(backButtonClick(_:)))
            if notice.owner == MyStaticUser.id {
                navBar.rightBarButtonItem = MyBarButtonItem(frame: CGRect(x: 0, y: 0, width: 44, height: 44), title: "Редактировать",action: #selector(acceptButtonClick(_:)))
            }
            navBar.layoutIfNeeded()
        }
    }
    @objc func acceptButtonClick(_ sender: Any){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let editNoticeViewController = storyBoard.instantiateViewController(withIdentifier: "EditNoticeViewController") as? EditNoticeViewController
        editNoticeViewController!.notice = try! JSONDecoder().decode(EditNotice.self, from: JSONEncoder().encode(self.notice))
        editNoticeViewController!.parentView = self
        editNoticeViewController!.pojo = self.myMainViewController.Categories
        self.present(editNoticeViewController!, animated: true, completion: nil)
    }
    @objc func backButtonClick(_ sender: Any){
            self.dismiss(animated: true, completion: nil)
    }
    
    func setOwner(_ owner: MyUser){
        self.owner = owner
    }
    @IBAction func callButtonClick(_ sender: Any){
        guard let phonenumber = self.owner?.phonenumber else {return}
        if let url = URL(string: "tel://\(phonenumber)"),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler:nil)
        } else {
            print("Ошибка при звонке! \(phonenumber)")
        }
    }
    @IBAction func sendButtonClicked(_ sender: Any){
        guard let phonenumber = self.owner?.phonenumber else {return}
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = ""
            controller.recipients = [phonenumber]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    @objc func addressCardClicked(_ sender: Any){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let noticeDetailMapViewController = storyBoard.instantiateViewController(withIdentifier: "NoticeDetailMapViewController") as? NoticeDetailMapViewController
        noticeDetailMapViewController!.notice = self.notice
        self.present(noticeDetailMapViewController!, animated: true, completion: nil)
    }
    
    func initScrollStackView(){
        if !self.scrollView.subviews.contains(self.stackView){
            self.scrollView.addSubview(self.stackView)
            self.stackView.translatesAutoresizingMaskIntoConstraints = false
            self.stackView.leadingAnchor.constraint(equalTo: self.scrollView.leadingAnchor).isActive = true
            self.stackView.trailingAnchor.constraint(equalTo: self.scrollView.trailingAnchor).isActive = true
            self.stackView.topAnchor.constraint(equalTo: self.scrollView.topAnchor).isActive = true
            self.stackView.bottomAnchor.constraint(equalTo: self.scrollView.bottomAnchor).isActive = true
            self.stackView.widthAnchor.constraint(equalTo: self.scrollView.widthAnchor).isActive = true
        }
    }
    func initSlideView(){
        var sources = [InputSource]()
        if notice.image1 != nil && KingfisherSource(urlString: notice.image1!) != nil {
            sources.append(KingfisherSource(urlString: notice.image1!)!)
        }
        if notice.image2 != nil && KingfisherSource(urlString: notice.image2!) != nil{
            sources.append(KingfisherSource(urlString: notice.image2!)!)
        }
        if notice.image3 != nil && KingfisherSource(urlString: notice.image3!) != nil {
            sources.append(KingfisherSource(urlString: notice.image3!)!)
        }
        if notice.image4 != nil && KingfisherSource(urlString: notice.image4!) != nil {
            sources.append(KingfisherSource(urlString: notice.image4!)!)
        }
        if notice.image5 != nil && KingfisherSource(urlString: notice.image5!) != nil {
            sources.append(KingfisherSource(urlString: notice.image5!)!)
        }
        imageSlideShow.setImageInputs(sources)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(imageClicked(_:)))
        
        imageSlideShow.activityIndicator = DefaultActivityIndicator(style: .white, color: UIColor.lapkiColors.lostOrange)
        imageSlideShow.sd_setShowActivityIndicatorView(true)
        imageSlideShow.addGestureRecognizer(gesture)
        imageSlideShow.backgroundColor = UIColor.black
    }
    @objc func imageClicked(_ sender: Any){
        imageSlideShow.presentFullScreenController(from: self)
    }
}
extension NoticeDetailViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
extension NoticeDetailViewController: MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
    }
}


