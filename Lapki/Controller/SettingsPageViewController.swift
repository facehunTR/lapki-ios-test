//
//  SettingsPageViewController.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 24/10/2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialCards
import MaterialComponents.MaterialDialogs
import Kingfisher
import DKImagePickerController
import MessageUI
import ImageSlideshow

class SettingsPageViewController: UIViewController,PageObservation, MFMailComposeViewControllerDelegate {
    
    
    @IBOutlet weak var lapkiLabel: UILabel!
    @IBOutlet weak var bonusLapkiCard: MDCCard!
    var dialogTransitionController: MDCDialogTransitionController!
    @IBOutlet weak var SignOutButton: MDCButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet var stackView: UIStackView!
    @IBOutlet weak var userImageCard: MDCCard!
    var lapki: Int? {
        didSet{
            let L = (self.lapki ?? 0)/100
            let IntL = Int(L)
            self.lapkiLabel.text = "На вашем счету: \(IntL) бонусов"
        }
    }
    func lapkiUpdate(lapki: LapkiResponse){
        self.lapki = lapki.count
        }
    let imagePickerController = DKImagePickerController()
    var loginViewController: LoginViewController!
    var myPageViewController : MyPageViewController!
    func getParentPageViewController(parentRef: MyPageViewController) {
        myPageViewController = parentRef
    }
    
    @IBOutlet weak var bonusIcon: UIImageView!
    @IBAction func bonusLapkiCardClicked(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let bonusViewController = storyBoard.instantiateViewController(withIdentifier: "BonusViewController") as! BonusViewController
        bonusViewController.parentView = self
        self.present(bonusViewController, animated: true, completion: nil)
    }
    
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var phoneNumberCard: MDCCard!
    @IBAction func phoneNumberClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "toTelConfirm", sender: nil)
    }

    
    
    @IBOutlet weak var UserFirstLastNameEditButton: MDCButton!
    
    
    @IBOutlet weak var userInfoCard: MDCCard!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userFirstLastName: UILabel!

    @IBOutlet weak var addressCard: MDCCard!
    @IBOutlet weak var addressLabel: UILabel!
    @IBAction func addressChangeClicked(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let addNoticeMapViewController = storyBoard.instantiateViewController(withIdentifier: "AddNoticeMapViewController") as! AddNoticeMapViewController
        addNoticeMapViewController.parentView = self
        self.present(addNoticeMapViewController, animated: true, completion: nil)
    }
    
    @IBOutlet weak var emailCard: MDCCard!
    @IBOutlet weak var emailLabel: UILabel!
    
    @IBAction func emailChangeClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "toEmail", sender: nil)
    }
    
    @IBOutlet weak var policyCard: MDCCard!
    @IBOutlet weak var licenseCard: MDCCard!
    @IBOutlet weak var supportCard: MDCCard!
    
    @IBAction func supportClicked(_ sender: Any) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["support@lapki.com"])
            present(mail, animated: true)
        } else {
            self.view.makeToast("Не удается открыть приложение \"Почта\", e-mail поддержки - support@lapki.com")
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    @IBAction func licenseClicked(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let myWebViewController = storyBoard.instantiateViewController(withIdentifier: "MyWebViewController") as? MyWebViewController
        myWebViewController!.urlString = "https://www.lapki.com/license/"
        myWebViewController!.titleString = "Лицензионное соглашение"
        self.present(myWebViewController!, animated: true, completion: nil)
    }
    @IBAction func policyClicked(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let myWebViewController = storyBoard.instantiateViewController(withIdentifier: "MyWebViewController") as? MyWebViewController
        myWebViewController!.urlString = "https://www.lapki.com/privacy/"
        myWebViewController!.titleString = "Политика конфиденциальности"
        self.present(myWebViewController!, animated: true, completion: nil)
    }
    @IBOutlet weak var versionLabel: UILabel!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Network.getLapki(responseHandler: weakCompletion(of: self, onResult: SettingsPageViewController.lapkiUpdate, onError: SettingsPageViewController.handleResponseError))
    }
    var user: MyUser?
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let leftEdgePan = UIScreenEdgePanGestureRecognizer(target:self, action:#selector(leftEdgeSwiped))
        leftEdgePan.edges = .left
        view.addGestureRecognizer(leftEdgePan)
        let rightEdgePan = UIScreenEdgePanGestureRecognizer(target:self, action:#selector(rightEdgeSwiped))
        rightEdgePan.edges = .right
        view.addGestureRecognizer(rightEdgePan)
        dialogTransitionController = MDCDialogTransitionController()
        addressCard.inkView.inkColor = UIColor.inkColors.orange
        emailCard.inkView.inkColor = UIColor.inkColors.orange
        policyCard.inkView.inkColor = UIColor.inkColors.orange
        licenseCard.inkView.inkColor = UIColor.inkColors.orange
        let gesture = UITapGestureRecognizer(target: self, action: #selector(SettingsPageViewController.licenseClicked(_:)))
        gesture.delegate = self
        licenseCard.addGestureRecognizer(gesture)
        let gesture2 = UITapGestureRecognizer(target: self, action: #selector(SettingsPageViewController.policyClicked(_:)))
        gesture2.delegate = self
        policyCard.addGestureRecognizer(gesture2)
        let gesture3 = UITapGestureRecognizer(target: self, action: #selector(SettingsPageViewController.supportClicked(_:)))
        gesture3.delegate = self
        supportCard.addGestureRecognizer(gesture3)
        supportCard.inkView.inkColor = UIColor.inkColors.orange
        initScrollStackView()
        initUserInfoCard()
        initPhoneCard()
        let buildNumber: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
        let appVersionString: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        let versionAndBuildNumber: String = "\(appVersionString) (\(buildNumber))"
        versionLabel.text = "Версия: \(versionAndBuildNumber)"
        self.stackView.axis = .vertical
        self.stackView.alignment = .fill
        self.stackView.spacing = 3.0
        self.stackView.layoutIfNeeded()
        self.SignOutButton.addTarget(self, action: #selector(SignOutButtonPressed(_:)), for: .touchUpInside)
        userUpdated()
        NotificationCenter.default.addObserver(self, selector: #selector(SettingsPageViewController.userUpdated), name: NSNotification.Name("userUpdated"), object: nil)
        NotificationCenter.default.addObserver(self,selector:#selector(SettingsPageViewController.callTelConfirm), name:NSNotification.Name("toTelConfirm"),object:nil)
        Network.getLapki(responseHandler: weakCompletion(of: self, onResult: SettingsPageViewController.lapkiUpdate, onError: SettingsPageViewController.handleResponseError))
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        NotificationCenter.default.removeObserver(Notification.Name(rawValue: "toTelConfirm"))
        
    }
    @objc func callTelConfirm(){
        self.performSegue(withIdentifier: "toTelConfirm", sender: nil)
    }
    func phoneNumberUpdate(){
        phoneNumberLabel.text = (MyStaticUser.phonenumber.isEmpty) ? "Не указан" : "+\(MyStaticUser.phonenumber)"
        phoneNumberLabel.textColor = (MyStaticUser.verificationPhone) ? UIColor.green : UIColor.red
    }
    func userImageUpdate(){
        let maxsize = max(userImageView.bounds.width, userImageView.bounds.height)*3
        let minsize = min(userImageView.bounds.width, userImageView.bounds.height)
        let processor = ResizingImageProcessor(referenceSize: CGSize(width: maxsize, height: maxsize), mode: .aspectFill).append(another: CroppingImageProcessor(size: CGSize(width:minsize*3,height: minsize*3 ), anchor: CGPoint(x: 0.5,y: 0.5))).append(another: RoundCornerImageProcessor(cornerRadius: minsize))
        var urlAvatar: Resource?
        urlAvatar = ((!MyStaticUser.avatar.isEmpty) ? URL(string: MyStaticUser.avatar) : URL(string: MyStaticUser.urlAvatar))
        userImageView.contentMode = .center
        userImageView.kf.setImage(with: urlAvatar, placeholder: UIImage(named: "camera_orange_big"), options: [.processor(processor),.cacheOriginalImage]){ result in
            switch (result) {
            case .success( _):
                self.userImageView.contentMode = .scaleAspectFit
                break
            case .failure(let error):
                print(error.localizedDescription)
                break
            }
        }
    }
    @objc func userUpdated(){
        let isLogined = (MyStaticUser.id != 0)
        bonusLapkiCard.isHidden = !isLogined
        userInfoCard.isHidden = !isLogined
        phoneNumberCard.isHidden = !isLogined
        emailCard.isHidden = !isLogined
        addressCard.isHidden = !isLogined
        SignOutButton.setTitle(!isLogined ? "ВОЙТИ" : "ВЫЙТИ", for: .normal)
        bonusIcon.tintColor = UIColor.lapkiColors.orange
        if (isLogined){
            if (self.user?.id != MyStaticUser.id){
                userImageUpdate()
                userFirstLastName.text = MyStaticUser.firstName + " " + MyStaticUser.lastName
                phoneNumberUpdate()
                emailLabel.text = (MyStaticUser.email.isEmpty) ? "Не указан" : MyStaticUser.email
                addressLabel.text = (!MyStaticUser.defAddress.isEmpty) ? MyStaticUser.defAddress : "Не указан"
            }else{
                if (self.user?.avatar != MyStaticUser.avatar) || (self.user?.urlAvatar != MyStaticUser.urlAvatar) {
                    userImageUpdate()
                }
                if (self.user?.firstName != MyStaticUser.firstName) || (self.user?.lastName != MyStaticUser.lastName) {
                    userFirstLastName.text = MyStaticUser.firstName + " " + MyStaticUser.lastName
                }
                if (self.user?.phonenumber != MyStaticUser.phonenumber) || (self.user?.verificationPhone != MyStaticUser.verificationPhone) {
                    phoneNumberUpdate()
                }
                if self.user?.email != MyStaticUser.email {
                    emailLabel.text = (MyStaticUser.email.isEmpty) ? "Не указан" : MyStaticUser.email
                }
                if self.user?.defAddress != MyStaticUser.defAddress {
                    addressLabel.text = (!MyStaticUser.defAddress.isEmpty) ? MyStaticUser.defAddress : "Не указан"
                }
            }
        }
        SignOutButton.titleLabel?.sizeToFit()
        self.user = MyStaticUser().toMyUser()
    }

    func initUserInfoCard(){
        userInfoCard.inkView.inkColor = UIColor.white.withAlphaComponent(0)
        userImageCard.inkView.inkColor = UIColor.inkColors.orange
        userImageCard.setShadowColor(UIColor.clear, for: .normal)
        userImageCard.cornerRadius = userImageCard.bounds.width/2
        let gesture = UITapGestureRecognizer(target: self, action: #selector(userImageChange(_:)))
        gesture.delegate = self
        userImageCard.addGestureRecognizer(gesture)
        SignOutButton.backgroundColor = UIColor.white
        SignOutButton.setBorderWidth(1.0, for: .normal)
        SignOutButton.setBorderColor(UIColor.lapkiColors.lightGray, for: .normal)
        SignOutButton.inkColor = UIColor.inkColors.orange
        SignOutButton.tintColor = UIColor.lapkiColors.orange
        UserFirstLastNameEditButton.setImage(UIImage(named: "ic_first_last_name")?.withRenderingMode(.alwaysTemplate), for: UIControl.State.normal)
        UserFirstLastNameEditButton.setImageTintColor(UIColor.lapkiColors.orange, for: .normal)
        UserFirstLastNameEditButton.contentEdgeInsets = .zero
        UserFirstLastNameEditButton.imageEdgeInsets = .zero
        UserFirstLastNameEditButton.tintColor = UIColor.lapkiColors.orange
        UserFirstLastNameEditButton.inkColor = UIColor.inkColors.orange
    }
    func initPhoneCard(){
        phoneNumberCard.inkView.inkColor = UIColor.inkColors.orange
    }
    
    func userImageChangeSuccess(_ user: MyUser){
        userToStatic(user)
        makeToast("Фотография успешно изменена", position: .top)
    }
    func userImageChangeFailure(_ error: Error){
        if let error = error as? ResponseError {
            switch error{
            case .detailReturned(let detail):
                makeToast(detail, position: .top)
                break
            default:
                break
            }
        }
        self.handleResponseError(error)
    }
    @objc func userImageChange(_ sender: UIView){
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let galleryAction = UIAlertAction(title: "Выбрать с галереи", style: .default) { (action) in
            self.imagePickerController.didSelectAssets = { (assets: [DKAsset]) in
                if !assets.isEmpty{
                    assets[0].fetchOriginalImage(options: nil , completeBlock: { (image, info) in
                        Network.patchUserAvatar(image: image!, responseHandler: weakCompletion(of: self, onResult: SettingsPageViewController.userImageChangeSuccess, onError: SettingsPageViewController.userImageChangeFailure))
                    })
                }
            }
            self.imagePickerController.sourceType = .both
            self.imagePickerController.maxSelectableCount = 1
            self.imagePickerController.showsEmptyAlbums = false
            self.present(self.imagePickerController, animated: true, completion: nil)
            print("didPress report abuse")
        }
        
        let showPhotoAction = UIAlertAction(title: "Показать фотку", style: .default) { (action) in
            
            let fullScreenController = FullScreenSlideshowViewController()
            let slideShowTrasitionDelegate = ZoomAnimatedTransitioningDelegate(imageView: self.userImageView, slideshowController: fullScreenController)
            fullScreenController.transitioningDelegate = slideShowTrasitionDelegate
            
            
            //            let imageSlideShow = ImageSlideshow()
            //
            //            self.userImageView.addSubview(imageSlideShow)
            //            imageSlideShow.activityIndicator = DefaultActivityIndicator(style: .white, color: UIColor.lapkiColors.lostOrange)
            //            imageSlideShow.sd_setShowActivityIndicatorView(true)
            //            imageSlideShow.backgroundColor = UIColor.black
            //            imageSlideShow.draggingEnabled = true
            var source = [InputSource]()
            if self.user?.avatar != ""{
                source.append(KingfisherSource(urlString: (self.user?.avatar)!) ?? "" as! InputSource)
            }
            else if self.user?.urlAvatar != ""{
                source.append(KingfisherSource(urlString: (self.user?.urlAvatar)!)!)
            }
            //            imageSlideShow.setImageInputs(source)
            //            imageSlideShow.presentFullScreenController(from: self)
//            fullScreenController.inputs = source
//            self.present(fullScreenController,animated:true,completion:nil)
//
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            print("didPress cancel")
        }
        if user?.avatar != "" || user?.urlAvatar != "" {
            actionSheet.addAction(showPhotoAction)
        }
        actionSheet.addAction(galleryAction)
        actionSheet.addAction(cancelAction)
        self.present(actionSheet, animated: true, completion: nil)
    }
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        self.navigationController?.isNavigationBarHidden = false
        self.tabBarController?.tabBar.isHidden = false
        sender.view?.removeFromSuperview()
    }
    func initScrollStackView(){
        if !self.scrollView.subviews.contains(self.stackView){
            self.scrollView.addSubview(self.stackView)
            self.stackView.translatesAutoresizingMaskIntoConstraints = false
            self.stackView.leadingAnchor.constraint(equalTo: self.scrollView.leadingAnchor).isActive = true
            self.stackView.trailingAnchor.constraint(equalTo: self.scrollView.trailingAnchor).isActive = true
            self.stackView.topAnchor.constraint(equalTo: self.scrollView.topAnchor).isActive = true
            self.stackView.bottomAnchor.constraint(equalTo: self.scrollView.bottomAnchor).isActive = true
            self.stackView.widthAnchor.constraint(equalTo: self.scrollView.widthAnchor).isActive = true
        }
    }
    @objc func leftEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            if myPageViewController.currentIndex>0{
                myPageViewController.changePageTo(index: myPageViewController.currentIndex-1)
            }
        }
    }
    @objc func rightEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            if myPageViewController.currentIndex<myPageViewController.pages.count{
                myPageViewController.changePageTo(index: myPageViewController.currentIndex+1)
            }
        }
    }
    @objc func SignOutButtonPressed(_ sender: AnyObject){
        if self.user?.id == 0 {
            //            refreshMyUser(completionHandler: userRefreshHandler)
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            loginViewController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
            loginViewController.mainViewController = myPageViewController.mainViewController
            self.present(loginViewController, animated: true, completion: nil)
        }else{
            do {
                self.user = nil
                clearMyUser()
                try clearTokenAndId()
                Network.getCurrentUserData({(_,_) in})
            }catch{
                debugPrint(error)
            }
        }
    }
    
   
 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! MyDialogViewController
        var header = ""
        var placeholder = ""
        
//        func postParameters(Parameters:[String:String],completion:@escaping(Bool?,DataResponse<Any>?) -> Void )  {
//            
//            patchUserData(parameters: Parameters, responseHandler: {response in
//                if response.result.isSuccess {
//                    completion(true,response)
//                }else{
//                    completion(false,nil)
//                }
//            })
//        }
        
        if segue.identifier == "toNameSurname"{
//            vc.mode = "toNameSurname"
//            vc.placeholderText  = "Имя"
//            vc.secondPlaceholderText = "Фамилия"
//            vc.inputType = .default
//            vc.headerText = "Введите Имя и Фамилию"
//            vc.textFieldString = MyStaticUser.firstName
//            vc.secondTextFieldString = MyStaticUser.lastName
//
//            vc.rightButtonAction = {success in
//
//                postParameters(Parameters: ["first_name":vc.textField?.text ?? "","last_name":vc.secondTextField?.text ?? ""], completion: { (cool, data) in
//                    if cool! {
//                        vc.dismiss(animated: true, completion: nil)
//                    }
//                })
//
//            }
            
        }
//        if segue.identifier == "toTelConfirm"{
//            vc.mode = "toTelConfirm"
//            vc.textFieldString = MyStaticUser.phonenumber
//            vc.placeholderText = ""
//            vc.inputType = UIKeyboardType.phonePad
//            vc.headerText = "Введите номер телефона"
//            vc.isSecondTextFieldisHidden = true
//            vc.isTimerLabelHidden = true
//            vc.rightButtonTitle = "Подтвердить"
//            vc.rightButtonAction = {success in
//
//                if vc.textField.text!.count < 11 {
//                    vc.view.makeToast("Номер телефона указан неверно",position:.top)
//                }else{
//                    var text = vc.textField.text!
//                    for a in text {
//                        if a == "+" || a == " " || a == "(" || a == ")"{
//                            text.remove(at: text.firstIndex(of: a)!)
//                        }
//                    }
//
//                    postParameters(Parameters:["phonenumber":text], completion: { (cool, response) in
//                        if cool == true{
//                            if response?.response?.statusCode == 202{
//
//                                if let dict = response?.result.value as? [String:Any?]{
//                                    if let detail = dict["detail"] as? String{
//                                        vc.view.makeToast(detail,position:.top)
//
//                                    }
//
//                                }
//                            }
//                            else if response?.response?.statusCode == 200{
//
//                                let alertController = createAlertController(title: "Обратите внимание", message: "Сейчас вам поступит звонок.\nИспользуйте последние четыре цифры номера телефона в качестве кода подтверждения.\nЕсли звонок не поступил нажмите \"Отправить код повторно\" для отправки SMS с кодом")
//                                let action = MDCAlertAction(title:"ПОНЯТНО") { (action) in
//                                    genVerificationCode(call: true,responseHandler: {response in
//                                        if let dict = response.result.value as? [String:Any?]{
//                                            if response.response?.statusCode == 202{
//                                                if let detail = dict["detail"] as? String{
//                                                    vc.view.makeToast(detail,position:.top)
//
//                                                }
//                                            }
//                                            else if response.response?.statusCode == 200{
//                                                self.performSegue(withIdentifier: "toConfirm", sender: nil)
//                                            }
//                                        }
//
//                                    })
//                                }
//
//                                vc.dismiss(animated: true, completion: {
//                                    alertController.addAction(action)
//                                    self.present(alertController, animated:true, completion:nil)
//                                })
//                            }
//                        }
//                    })
//                }
//            }
//        }
//        if segue.identifier == "toConfirm"{
//            vc.mode = "toConfirm"
//            vc.isTimerLabelHidden = false
//            vc.isSecondTextFieldisHidden = true
//            vc.placeholderText = ""
//            vc.inputType = UIKeyboardType.numberPad
//            vc.headerText = "Введите код подтверждения"
//            vc.codeRepeatAction = {success in
//                genVerificationCode(responseHandler: {response in
//                    if let dict = response.result.value as? [String:Any?]{
//                        if let detail = dict["detail"] as? String{
//                            vc.view.makeToast(detail,position:.top)
//                        }
//                    }
//                    // print(response)
//                    vc.timerRestart()
//                })
//                
//            }
//            vc.rightButtonAction = {success in
//                
//                numberVerification(code: vc.textField.text!, responseHandler: {response in
//                    if response.response?.statusCode == 400 {
//                        vc.view.makeToast("Неверный код подтверждения",position:.top)
//                    }else{
//                        createOrder(order_reason:"info",ad: 0,byBonuses: nil,responseHandler: {response in
//                            if response.response?.statusCode == 200 {
//                                //print(response.result.value)
//                                if let dict = response.result.value as? Parameters{
//                                if let detail = dict["detail"] as? String{
//                                vc.view.makeToast(detail,position:.top)
//                                }
//                                }
//                            }})
//                        refreshMyUser(nil)
//                        vc.dismiss(animated: true, completion: nil)
//                    }
//                })
//            }
//            
//        }
        if segue.identifier == "toEmail"{
//            vc.mode = "toEmail"
//            vc.textFieldString = MyStaticUser.email
//            func post(){
//                let parameters: [String:String] = [
//                    "email":vc.textField.text!
//                ]
//                postParameters(Parameters: parameters, completion: { (cool, response) in
//                    if cool! {
//                        vc.dismiss(animated: true, completion: nil)
//                    }
//                })
//            }
//            vc.placeholderText = ""
//            vc.inputType = .default
//            vc.isSecondTextFieldisHidden = true
//
//            if MyStaticUser.email.isEmpty{
//                vc.headerText = "Введите email"
//                vc.secondTextField?.isHidden = true
//                vc.rightButtonAction = {success in
//                    if isValidEmail(testStr: vc.textField.text!){
//                        post()
//
//                    }else{
//                        vc.view.makeToast("Введите действительный e-mail",position:.top)
//
//                    }
//                }
//
//            }
//            vc.headerText = "Введите новый email"
//            vc.secondTextField?.isHidden = true
//            vc.rightButtonAction = {success in
//                if isValidEmail(testStr: vc.textField.text!){
//                    if vc.textField.text!.elementsEqual(MyStaticUser.email){
//                        vc.dismiss(animated: true, completion: nil)
//                        vc.view.makeToast("Email не был изменён",position:.top)
//                    }
//                    post()
//                }else{
//                    vc.view.makeToast("Введите действительный e-mail",position:.top)
//                }
//            }
        }
        
        func isValidEmail(testStr:String) -> Bool {
            let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
            
            let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
            return emailTest.evaluate(with: testStr)
        }
    }
}
extension SettingsPageViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}




