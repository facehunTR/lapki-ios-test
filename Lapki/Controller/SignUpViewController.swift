//
//  SignUpViewController.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 27/10/2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import Toast_Swift
import MaterialComponents.MaterialDialogs
import InputMask
import FlagKit

class SignUpViewController: UIViewController,CountryPickerDelegate {
    
    @IBOutlet weak var countryButton: UIButton!
    @IBOutlet weak var loginCard: MDCCard!
    @IBOutlet var inputMaskDelegate: MaskedTextFieldDelegate!
    @IBOutlet weak var scrollView: UIScrollView!
    var parentView: UIViewController!
    @IBOutlet weak var PasswordField: UITextField!
    @IBOutlet weak var SignUpButton: UIButton!
    @IBOutlet weak var phoneNumberField: UITextField!
    @IBOutlet weak var bottomHeight: NSLayoutConstraint!
    @IBOutlet weak var passwordRepeat: UITextField!
    var dialogTransitionController = MDCDialogTransitionController()
    var mainViewController: MyMainViewController!
    
    func set(country: Country) {
        self.inputMaskDelegate.primaryMaskFormat = country.phoneMask
        self.countryButton.setImage(country.flag, for: .normal)
       // self.selectedCountry = country.countryCode
        //        let index = country.phoneMask.index(before: country.phoneMask.firstIndex(of: "["))
        let placeholderString = country.phoneMask.prefix(while: {return $0 != "["})
        self.phoneNumberField.placeholder = String(placeholderString)
        inputMaskDelegate.put(text: "", into: phoneNumberField)
        //        print("countryCode \(countryCode.rawValue)")
    }
    //var selectedCountry: CountryCode = .RU
    @IBAction func countryClicked(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let countryPickerViewController = storyBoard.instantiateViewController(withIdentifier: "CountryPickerViewController") as! CountryPickerViewController
        countryPickerViewController.countryDelegate = self
       // countryPickerViewController.selected = self.selectedCountry
        //        pickerViewController.data = subTypes
        //        pickerViewController.selected = subType
        //        pickerViewController.parentView = self
        //        pickerViewController.outPutName = "subType"
        let bottomSheet = MDCBottomSheetController(contentViewController: countryPickerViewController)
        bottomSheet.preferredContentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height/3)
        self.present(bottomSheet, animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        countryButton.layer.masksToBounds = true
        countryButton.layer.borderColor = UIColor.black.withAlphaComponent(0.26).cgColor
        countryButton.layer.borderWidth = 0.5
        countryButton.contentEdgeInsets = .zero
        countryButton.imageEdgeInsets = .zero
        countryButton.setImage(Flag(countryCode: "RU")?.image(style: .roundedRect), for: .normal)
        loginCard.setShadowElevation(.none, for: .normal)
        self.addLineToView(view: phoneNumberField, position: .LINE_POSITION_BOTTOM,color: UIColor.black,width: 0.5)
        self.addLineToView(view: PasswordField, position: .LINE_POSITION_BOTTOM,color: UIColor.black,width: 0.5)
        self.addLineToView(view: passwordRepeat, position: .LINE_POSITION_BOTTOM,color: UIColor.black,width: 0.5)
        SignUpButton.layer.cornerRadius=5
        // Do any additional setup after loading the view.
    }
    @objc func backButtonPressed(_ sender: Any){
        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
    }
    @objc func keyboardWillShow(notification: NSNotification){
        guard let userInfo = notification.userInfo else {return}
        guard let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {return}
        bottomHeight.constant = keyboardSize.cgRectValue.height
    }
    @objc func keyboardWillHide(notification: NSNotification){
        bottomHeight.constant = 0
    }
    @IBAction func LoginButtonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func isPasswordValid(_ password : String) -> Bool{
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{4,}$")
        return passwordTest.evaluate(with: password)
    }
    
    @IBAction func SignUpButtonClicked(_ sender: UIButton) {
        if !isPasswordValid(PasswordField.text!){
            self.view.makeToast("Пароль должен содержать не менее: 4 символов, 1 латинской буквы, 1 цифры",position: .top)
            return
        }
        if !passwordRepeat.text!.elementsEqual(PasswordField.text!){
            self.view.makeToast("Пароль подтвержден неверно",position: .top)
            return
        }
        if phoneNumberField.text == nil {
            self.view.makeToast("Неверно указан номер телефона",position: .top)
            return
        }
        var text = phoneNumberField.text!
        for a in text {
            if a == "+" || a == " " || a == "(" || a == ")"{
                text.remove(at: text.firstIndex(of: a)!)
            }
        }
        if text.count < 11 {
            self.view.makeToast("Неверно указан номер телефона",position: .top)
            return
        }
        Network.registerUser(phonenumber:text,password: PasswordField.text!,responseHandler: weakCompletion(of: self, onResult: SignUpViewController.registerSuccess, onError: SignUpViewController.registerOrLoginError)
        )
    }
    func registerSuccess(_ result: DetailedResponse){
        if result.statusCode == 201 {
            var text = phoneNumberField.text!
            for a in text {
                if a == "+" || a == " " || a == "(" || a == ")"{
                    text.remove(at: text.firstIndex(of: a)!)
                }
            }
            Network.loginUser(username:text, password: self.PasswordField.text!, responseHandler: weakCompletion(of: self, onResult: SignUpViewController.loginSuccess, onError: SignUpViewController.registerOrLoginError))
        }else {
            makeToast(result.detail, position: .top)
        }
    }
    func registerOrLoginError(_ error: Error){
        makeToast("\(error)", position: .top)
        self.handleResponseError(error)
    }
    func loginSuccess(_ success: Bool){
        if success{
            //let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let alertController = createAlertController(title: "Обратите внимание", message: "Сейчас вам поступит звонок.\nИспользуйте последние четыре цифры номера телефона в качестве кода подтверждения.\nЕсли звонок не поступил нажмите \"Отправить код повторно\" для отправки SMS с кодом")
            let action = MDCAlertAction(title:"ПОНЯТНО") {[weak self] (action) in
                guard let self = self else {return}
                Network.genVerificationCode(call: true,responseHandler: weakCompletion(of: self, onResult: SignUpViewController.generationSuccess, onError: SignUpViewController.generationError))
            }
            alertController.addAction(action)
            self.present(alertController, animated:true, completion:nil)
        }
        else{
            makeToast("Произошла ошибка при получении данных пользователя",position: .top)
        }
    }
    func generationSuccess(_ result: DetailedResponse){
        if result.statusCode == 200{
            self.performSegue(withIdentifier: "toConfirmReg", sender:self)
        }else{
            makeToast(result.detail, position: .top)
        }
    }
    func generationError(_ error: Error){
        if let error = error as? ResponseError {
            switch error {
            case .detailReturned(let detail):
                makeToast(detail,position: .top)
            case .withoutDetails,.notJSON,.emptyResponse,.notExpectedValue:
                makeToast("Произошла ошибка код \(error)", position: .top)
            }
        }else{
            makeToast("Произошла ошибка код \(error)", position: .top)
        }
        self.handleResponseError(error)
    }
    
override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //var vc = UIViewController()
        if segue.identifier == "toConfirmReg"{
            
            
            
                        
//                        let vc = segue.destination as! MyDialogViewController
//            vc.mode = "toConfirm"
//            vc.isTimerLabelHidden = false
//            vc.isSecondTextFieldisHidden = true
//            vc.placeholderText = ""
//            vc.inputType = UIKeyboardType.numberPad
//            vc.headerText = "Введите код подтверждения"
//                        vc.codeRepeatAction = {success in
//                            genVerificationCode(responseHandler: {response in
//    
//                                vc.timerRestart()
//                            })
//                            
//                        }
//                        vc.rightButtonAction = {success in
//                            numberVerification(code: vc.textField.text!, responseHandler: {response in
//                                if response.response?.statusCode == 400 {
//                                    vc.view.makeToast("Неверный код подтверждения",position:.top)
//                                }else{
//                                    refreshMyUser({success in
//                                        vc.dismiss(animated: true, completion: nil)
//                                        if !(self.view.window!.rootViewController is MyMainViewController){
//                                            self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
//                                        }
//                                    })
//                                }
//                            })
//                        }
                }
    
            
            
    
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}





