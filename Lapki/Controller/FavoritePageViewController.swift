//
//  FavoritePageViewController.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 24/10/2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//

import UIKit

class FavoritePageViewController: UIViewController, PageObservation {

    @IBOutlet weak var segmentedController: UISegmentedControl!
//    @IBOutlet weak var tabBar: UITabBar!
    var myPageViewController : MyPageViewController!
    func getParentPageViewController(parentRef: MyPageViewController) {
        myPageViewController = parentRef
    }
    var myFavoritesPageViewController: MyFavoritesPageViewController!
    override func viewDidLoad() {
        super.viewDidLoad()
//        tabBar.delegate=self
//        tabBar.selectedItem = tabBar.items![0]
        segmentedController.setTitleTextAttributes([NSAttributedString.Key .foregroundColor:UIColor.lapkiColors.lightGray], for: .normal)
        segmentedController.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.lapkiColors.orange], for: .selected)
    }
    @IBAction func segmentValueChanged(_ sender: UISegmentedControl) {
        myFavoritesPageViewController.changePageTo(index: sender.selectedSegmentIndex)
    }
  
   
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "FavoritePageViewSegue") {
            myFavoritesPageViewController = segue.destination as? MyFavoritesPageViewController
            //myFavoritesPageViewController.tabBar = self.tabBar
            myFavoritesPageViewController.segmentedControlP = self.segmentedController
            let myFavoritesPageWithParent = myFavoritesPageViewController as PageObservation
            myFavoritesPageWithParent.getParentPageViewController(parentRef: myPageViewController)
        }
    }
    
    
    
    
    
 

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
