//
//  MyWebViewController.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 15/12/2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import WebKit
import MaterialComponents.MaterialButtons

class MyWebViewController: UIViewController, WKNavigationDelegate {
    let progressView = UIProgressView(progressViewStyle: .default)
    private var estimatedProgressObserver: NSKeyValueObservation?
    var urlString: String!
    var titleString: String?
    var needOpenButton = false
    var webView: WKWebView!
    var parentView: UIViewController?
    
    @IBOutlet weak var navBar: MDCNavigationBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        initToolbar()
        initProgressView()
        webView = WKWebView()
        self.view.addSubview(webView)
        initEstimatedProgressObserver()
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.topAnchor.constraint(equalTo: navBar.bottomAnchor).isActive = true
        webView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        webView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        webView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        webView.allowsLinkPreview = false
        webView.allowsBackForwardNavigationGestures = true
        webView.load(URLRequest(url: URL(string: urlString)!))
        webView.navigationDelegate = self
        
        // Do any additional setup after loading the view.
    }
    func webView(_: WKWebView, didStartProvisionalNavigation _: WKNavigation!) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        UIView.transition(with: progressView,
                          duration: 0.33,
                          options: [.transitionCrossDissolve],
                          animations: {
                            self.progressView.isHidden = false
        },
                          completion: nil)
    }
    
    func webView(_: WKWebView, didFinish _: WKNavigation!) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
        UIView.transition(with: progressView,
                          duration: 0.33,
                          options: [.transitionCrossDissolve],
                          animations: {
                            self.progressView.isHidden = true
        },
                          completion: nil)
    }
    private func initProgressView() {
        
        progressView.translatesAutoresizingMaskIntoConstraints = false
        
        navBar.addSubview(progressView)
        progressView.progressTintColor = UIColor.lapkiColors.orange
        progressView.isHidden = true
        
        NSLayoutConstraint.activate([
            progressView.leadingAnchor.constraint(equalTo: navBar.leadingAnchor),
            progressView.trailingAnchor.constraint(equalTo: navBar.trailingAnchor),
            
            progressView.bottomAnchor.constraint(equalTo: navBar.bottomAnchor),
            progressView.heightAnchor.constraint(equalToConstant: 2.0)
            ])
    }
    private func initEstimatedProgressObserver() {
        estimatedProgressObserver = webView.observe(\.estimatedProgress, options: [.new]) { [weak self] webView, _ in
            self?.progressView.progress = Float(webView.estimatedProgress)
        }
    }
    func initToolbar(){
        if navBar.leftBarButtonItem == nil{
            navBar.leftBarButtonItem = MyBarButtonItem(frame: CGRect(x: 0, y: 0, width: 44, height: 44), imageName: "back_orange",action: #selector(backButtonClick(_:)))
            navBar.title = titleString ?? ""
        }
    }
    @objc func backButtonClick(_ sender: Any){
        if self.parentView != nil{
            if let parent = self.parentView as? UpliftViewController {
                parent.resultUrl = webView.url
                self.dismiss(animated: true, completion: nil)
            }
        }else{
            if webView.canGoBack {
                webView.goBack()
            }else{
                self.dismiss(animated: true, completion: nil)
            }
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
