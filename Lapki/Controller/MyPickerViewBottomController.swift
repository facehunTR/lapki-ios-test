//
//  MyPickerViewBottomController.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 25/01/2019.
//  Copyright © 2019 MEGAKOM, OOO. All rights reserved.
//

import UIKit

class MyPickerViewBottomController: UIViewController{
    var delegate: MyPickerViewBottomControllerDelegate!
    var outPutName: String!
    var data: [String?] = [String]()
    var selected: String?
    var buttonTitle: String?
    @IBOutlet weak var acceptButton: MyButton!
    @IBOutlet weak var pickerView: UIPickerView!
    
    @IBAction func acceptClicked(_ sender: Any) {
        delegate.result(outputName: outPutName, result: data[pickerView.selectedRow(inComponent: 0)])
        self.dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        self.acceptButton.setTitle(buttonTitle ?? "Готово", for: .normal)
        if self.data.firstIndex(of: selected) != nil {
            self.pickerView.selectRow(self.data.firstIndex(of: selected!)!, inComponent: 0, animated: false)
        }
    }
}
extension MyPickerViewBottomController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return data.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return nil
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        var pickerLabel = view as? UILabel;
        if (pickerLabel == nil)
        {
            pickerLabel = UILabel()
            
            pickerLabel!.font = UIFont.LapkiFonts.LatoRegular
            pickerLabel!.textAlignment = NSTextAlignment.center
        }
        pickerLabel!.text = data[row]
        return pickerLabel!;
    }
    
}
protocol MyPickerViewBottomControllerDelegate {
    func result(outputName: String, result: String?)
}
