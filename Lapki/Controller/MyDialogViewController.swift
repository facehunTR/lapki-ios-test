////
////  MyDialogViewController.swift
////  Lapki
////
////  Created by Yuriy Yashchenko on 15/12/2018.
////  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
////
//
//import UIKit
//import MaterialComponents.MaterialTextFields
//import InputMask
//import FlagKit
//
//
//
//class MyDialogViewController: UIViewController, CountryPickerDelegate {
//    
//    @IBOutlet weak var dialogueView: UIView!
//    @IBOutlet weak var rightButton: MDCButton!
//    @IBOutlet weak var leftButton: MDCButton!
//    @IBOutlet weak var textField: UITextField!
//    @IBOutlet weak var labelHeader: UILabel!
//    @IBOutlet var inputMaskDelegate: MaskedTextFieldDelegate?
//    @IBOutlet weak var secondTextField: UITextField?
//     var secondTextFieldController: MDCTextInputControllerOutlined!
//    var secondPlaceholderText: String?
//    var secondTextFieldString: String?
//    var codeRepeatAction: CompletionHandler?
//    @IBOutlet weak var codeRepeat: UIView!
//    var placeholderText: String?
//    var helperText: String?
//    var inputType: UIKeyboardType?
//    var textFieldController: MDCTextInputControllerOutlined!
//    var leftButtonTitle: String = "Отмена"
//    var rightButtonTitle: String = "Принять"
//    var leftButtonAction: CompletionHandler?
//    var rightButtonAction: CompletionHandler?
//    var textFieldString: String?
//    var headerText: String?
//    var timer = Timer()
//    var timeRemaining: Int = 60
//    var isTimerRunning = false
//    var isSecondTextFieldisHidden = false
//    var isTimerLabelHidden = true
//    var selectedCountry: CountryCode = .RU
//    var mode = ""
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(true)
//        textField.becomeFirstResponder()
//    }
//    
//    
//    @IBOutlet weak var countryButton: UIButton!
//    
//    func set(country: Country) {
//        self.inputMaskDelegate?.primaryMaskFormat = country.phoneMask
//        self.countryButton.setImage(country.flag, for: .normal)
//        self.selectedCountry = country.countryCode
//        let placeholderString = country.phoneMask.prefix(while: {return $0 != "["})
//        self.textField.placeholder = String(placeholderString)
//        inputMaskDelegate?.put(text: "", into: textField)
//    }
//    
//    @IBAction func countryClicked(_ sender: Any) {
//        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//        let countryPickerViewController = storyBoard.instantiateViewController(withIdentifier: "CountryPickerViewController") as! CountryPickerViewController
//        countryPickerViewController.countryDelegate = self
////        countryPickerViewController.selected = self.inputMaskDelegate?.primaryMaskFormat
//        countryPickerViewController.selected = self.selectedCountry
//        let bottomSheet = MDCBottomSheetController(contentViewController: countryPickerViewController)
//        bottomSheet.preferredContentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height/3)
//        self.present(bottomSheet, animated: true, completion: nil)
//    }
//    
//   
//    @IBOutlet weak var textFieldToFlag: NSLayoutConstraint!
//    @IBOutlet weak var textFieldToBorder: NSLayoutConstraint!
//    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//                let touch: UITouch? = touches.first
//        
//                if touch?.view != dialogueView && touch?.view != codeRepeat {
//                    self.dismiss(animated: true) {
//        
//                    }
//                }
//        
//    }
//    func setTheShadow(){
//        dialogueView.layer.shadowOffset = CGSize(width: 5, height: 5)
//        dialogueView.layer.shadowRadius = 50
//        dialogueView.layer.shadowOpacity = 0.5
//        dialogueView.layer.masksToBounds = false
//        dialogueView.layer.shadowColor = UIColor.black.cgColor
//        rightButton.layer.masksToBounds = true
//        leftButton.layer.masksToBounds = true
//    }
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(true)
//        secondTextField?.isHidden = isSecondTextFieldisHidden
//        codeRepeat.isHidden = isTimerLabelHidden
//        initTextField()
//        initButtons()
//        if codeRepeat != nil {
//            timerRestart()
//            let gesture = UITapGestureRecognizer(target: self, action: #selector(codeRepeatClick(_:)))
//            codeRepeat.addGestureRecognizer(gesture)
//        }
//        if secondTextField != nil {
//            secondTextField!.translatesAutoresizingMaskIntoConstraints = false
//            secondTextField!.autocapitalizationType = .none
//            secondTextField!.keyboardType = inputType ?? .default
//            secondTextField!.font = UIFont.LapkiFonts.LatoRegular
//            if self.inputMaskDelegate == nil {
//                secondTextField!.delegate = self
//            }else{
//                secondTextField!.delegate = self.inputMaskDelegate
//            }
//            secondTextField!.text = self.secondTextFieldString ?? ""
//            secondTextField!.tintColor = UIColor.lapkiColors.orange
//            secondTextField!.placeholder = self.secondPlaceholderText ?? ""
//            secondTextField!.font = UIFont.LapkiFonts.LatoRegular
//            secondTextField!.sizeToFit()
//        }
//        if mode == "toNameSurname" || mode == "toEmail" || mode == "toConfirm"{
//            textFieldToBorder.isActive = true
//            textFieldToFlag.isActive = false
//            countryButton.isHidden = true
//            textField.delegate = nil
//            secondTextField?.delegate = nil
//            
//            switch mode{
//                
//            case "toNameSurname":
//                textField.text = textFieldString
//                secondTextField!.text = secondTextFieldString
//                break
//            case "toEmail":
//                textField.text = textFieldString
//                break
//            
//            default:
//                break
//            }
//        }
//        else{
//            textFieldToBorder.isActive = false
//            textFieldToFlag.isActive = true
//            countryButton.isHidden = false
//            if textFieldString?.starts(with: "77") ?? false {
//                countryButton.setImage(Flag(countryCode: "KZ")!.originalImage, for: .normal)
//                inputMaskDelegate?.primaryMaskFormat = "+7 (7[00]) [000] [00] [00]"
//            }else if textFieldString?.starts(with: "7") ?? true {
//                countryButton.setImage(Flag(countryCode: "RU")!.originalImage, for: .normal)
//                inputMaskDelegate?.primaryMaskFormat = "+7 ([000]) [000] [00] [00]"
//            }else {
//                let threeDigits = String(textFieldString!.prefix(3))
//                inputMaskDelegate?.primaryMaskFormat = "+\(threeDigits) [00] [000] [00] [00]"
//                if threeDigits.contains("995"){
//                    countryButton.setImage(Flag(countryCode: "GE")!.originalImage, for: .normal)
//                }else if threeDigits.contains("380") {
//                    countryButton.setImage(Flag(countryCode: "UA")!.originalImage, for: .normal)
//                }else if threeDigits.contains("375"){
//                    countryButton.setImage(Flag(countryCode: "BY")!.originalImage, for: .normal)
//                }else{
//                    inputMaskDelegate?.primaryMaskFormat = "+7 ([000]) [000] [00] [00]"
//                    countryButton.setImage(Flag(countryCode: "RU")!.originalImage, for: .normal)
//                }
//            }
//            inputMaskDelegate?.put(text: textFieldString!, into: textField)
//        }
//        self.textField.layoutIfNeeded()
//    }
//    func timerRestart(){
//        let label = codeRepeat.subviews[0] as! UILabel
//        
//        timer.invalidate()
//        timeRemaining = 60
//        label.text = "Отправить код повторно (\(timeRemaining))"
//        codeRepeat.isUserInteractionEnabled = false
//        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerUpdate(_:)), userInfo: nil, repeats: true)
//    }
//    @objc func timerUpdate(_ sender: Any){
//        if timeRemaining > 0 {
//            timeRemaining -= 1
//            let label = codeRepeat.subviews[0] as! UILabel
//            label.text = "Отправить код повторно (\(timeRemaining))"
//        }else{
//            codeRepeat.isUserInteractionEnabled = true
//            let label = codeRepeat.subviews[0] as! UILabel
//            label.text = "Отправить код повторно"
//        }
//    }
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        setTheShadow()
//        countryButton.layer.masksToBounds = true
//        countryButton.layer.borderColor = UIColor.black.withAlphaComponent(0.26).cgColor
//        countryButton.layer.borderWidth = 0.5
//        countryButton.contentEdgeInsets = .zero
//        countryButton.imageEdgeInsets = .zero
//        countryButton.setImage(Flag(countryCode: "RU")?.image(style: .roundedRect), for: .normal)
//    }
//    
//    @objc func codeRepeatClick(_ sender: Any){
//        self.labelHeader.text = "Введите код подтверждения"
//        self.codeRepeatAction?(true)
//    }
//    func initButtons(){
//        leftButton.setTitle(leftButtonTitle, for: .normal)
//        leftButton.setTitleColor(UIColor.lapkiColors.orange, for: .normal)
//        leftButton.imageEdgeInsets = UIEdgeInsets.zero
//        leftButton.contentEdgeInsets = UIEdgeInsets.zero
//        leftButton.backgroundColor = UIColor.white
//        leftButton.addTarget(self, action: #selector(leftButtonClick(_:)), for: .touchUpInside)
//        leftButton.inkColor = UIColor.inkColors.orange
//        leftButton.tintColor = UIColor.lapkiColors.orange
//        rightButton.setTitle(rightButtonTitle, for: .normal)
//        rightButton.setTitleColor(UIColor.lapkiColors.orange, for: .normal)
//        rightButton.imageEdgeInsets = UIEdgeInsets.zero
//        rightButton.contentEdgeInsets = UIEdgeInsets.zero
//        rightButton.backgroundColor = UIColor.white
//        rightButton.addTarget(self, action: #selector(rightButtonClick(_:)), for: .touchUpInside)
//        rightButton.inkColor = UIColor.inkColors.orange
//        rightButton.tintColor = UIColor.lapkiColors.orange
//    }
//    @objc func leftButtonClick(_ sender: Any){
//        if self.leftButtonAction == nil {
//            self.dismiss(animated: true, completion: nil)
//        }else{
//            self.leftButtonAction!(true)
//        }
//    }
//    @objc func rightButtonClick(_ sender: Any){
//        if self.rightButtonAction == nil {
//            self.dismiss(animated: true, completion: nil)
//        }else{
//            self.rightButtonAction?(true)
//        }
//    }
//    func initTextField(){
//        
//        self.labelHeader.text = self.headerText ?? ""
//        textField.translatesAutoresizingMaskIntoConstraints = false
//        textField.autocapitalizationType = .none
//        textField.keyboardType = inputType ?? .default
//        textField.font = UIFont.LapkiFonts.LatoRegular
//        if self.inputMaskDelegate == nil {
//            textField.delegate = self
//        }else{
//            textField.delegate = self.inputMaskDelegate
//        }
//        textField.text = self.textFieldString ?? ""
//        textField.tintColor = UIColor.lapkiColors.orange
//        textField.placeholder = self.placeholderText ?? ""
//        textField.font = UIFont.LapkiFonts.LatoRegular
//        textField.sizeToFit()
//    }
//    
//    /*
//     // MARK: - Navigation
//     
//     // In a storyboard-based application, you will often want to do a little preparation before navigation
//     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//     // Get the new view controller using segue.destination.
//     // Pass the selected object to the new view controller.
//     }
//     */
//    
//    
//    
//}
//extension MyDialogViewController: UITextFieldDelegate {
//    
//}
