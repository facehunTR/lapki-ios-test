//
//  NoticeDetailMapViewController.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 13/12/2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//
//

import UIKit
import MapKit
import MaterialComponents.MDCMultilineTextField
import MaterialComponents.MaterialButtons
import SearchTextField

class NoticeDetailMapViewController: UIViewController {
    var notice: Notice!
    @IBOutlet weak var mapView: MKMapView!
    var tileRenderer: MKTileOverlayRenderer!
    var mapRegion: MKCoordinateRegion!
    var userLocation: MKUserLocation?
    @IBOutlet weak var navBar: MyNavigationBar!
    
    var region: MKCoordinateRegion?
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        mapView.showsUserLocation = true
        initToolbar()
        setupTileOverlay()
        let annotation = MKPointAnnotation()
        annotation.coordinate = notice.location!.coordinate
        annotation.accessibilityHint = "\(notice.id)"
        self.mapView.addAnnotation(annotation)
        let region = MKCoordinateRegion(center: notice.location!.coordinate, latitudinalMeters: 500, longitudinalMeters: 500)
        mapView.setRegion(region,animated: true)
    }
    @IBAction func plusClicked(_ sender: Any){
        if region == nil {
            region = mapView.region
        }
        region!.center = mapView.region.center
        region!.span.latitudeDelta /= 2.0
        region!.span.longitudeDelta /= 2.0
        mapView.setRegion(region!, animated: true)
    }
    @IBAction func minusClicked(_ sender: Any){
        if region == nil {
            region = mapView.region
        }
        region!.center = mapView.region.center
        region!.span.latitudeDelta = min(region!.span.latitudeDelta * 2.0, 180.0)
        region!.span.longitudeDelta = min(region!.span.longitudeDelta * 2.0, 180.0)
        mapView.setRegion(region!, animated: true)
    }

    
    func getZoom(_ mapView: MKMapView) -> Double {
        // function returns current zoom of the map
        var angleCamera = mapView.camera.heading
        if angleCamera > 270 {
            angleCamera = 360 - angleCamera
        } else if angleCamera > 90 {
            angleCamera = fabs(angleCamera - 180)
        }
        let angleRad = .pi * angleCamera / 180
        let width = Double(mapView.bounds.width)
        let height = Double(mapView.bounds.height)
        let spanStraight = width * mapView.region.span.longitudeDelta / (width * cos(angleRad) + (height) * sin(angleRad))
        return log2(360 * ((width / 256) / spanStraight)) + 1;
    }

    func initToolbar(){
        if navBar.leftBarButtonItem == nil{
            navBar.leftBarButtonItem = MyBarButtonItem(frame: CGRect(x: 0, y: 0, width: 44, height: 44), imageName: "back_orange",action: #selector(backButtonClick(_:)))
        }
    }
    @objc func backButtonClick(_ sender: Any){
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func userLocationButtonClicked(_ sender: Any){
        if userLocation != nil {
            let mapRegion = MKCoordinateRegion(center: userLocation!.coordinate, latitudinalMeters: 500, longitudinalMeters: 500)
            
            self.mapView.setRegion(mapRegion, animated: true)
        }else {
            self.mapView.setCenter(MyStaticUser.defaultLocation, animated: true)
        }
    }
    func setupTileOverlay(){
        let overlay = MyMKTileOverlay()
        overlay.canReplaceMapContent = true
        for overlay in mapView.overlays {
            mapView.removeOverlay(overlay)
        }
        mapView.addOverlay(overlay)
        tileRenderer = MKTileOverlayRenderer(tileOverlay: overlay)
    }
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
        
     }
    
}

extension NoticeDetailMapViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
extension NoticeDetailMapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        guard let tileOverlay = overlay as? MKTileOverlay else {
            return MKOverlayRenderer()
        }
        return MKTileOverlayRenderer(tileOverlay: tileOverlay)
    }

    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        self.userLocation = userLocation
    }
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation  {
            return nil
        }
        let annotationIdentifier = "Identifier"
        var annotationView: MKAnnotationView?
        if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
            annotationView = dequeuedAnnotationView
            annotationView?.annotation = annotation
        }
        else {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        if let pointAnnotation = annotation as? MKPointAnnotation{
                
            if "\(notice.id)".elementsEqual(pointAnnotation.accessibilityHint ?? ""){
                        annotationView?.canShowCallout = true
                        switch notice.category {
                        case "Потеряшка":
                            if notice.price > 0 {
                                annotationView?.image = UIImage(named: "lapki_notice_money")
                                annotationView!.centerOffset = CGPoint(x: 0, y: -annotationView!.image!.size.height / 2)
                                annotationView?.accessibilityHint = pointAnnotation.accessibilityHint
                            }else{
                                annotationView?.image = UIImage(named: "lapki_notice_default")
                                annotationView!.centerOffset = CGPoint(x: 0, y: -annotationView!.image!.size.height / 2)
                                annotationView?.accessibilityHint = pointAnnotation.accessibilityHint
                            }
                            break
                        case "Находка":
                            annotationView!.image = UIImage(named: "lapki_notice_found")
                            annotationView!.centerOffset = CGPoint(x: 0, y: -annotationView!.image!.size.height / 2)
                            annotationView?.accessibilityHint = pointAnnotation.accessibilityHint
                            break
                        default:
                            annotationView!.image = UIImage(named: "geopoint_orange")
                            annotationView!.centerOffset = CGPoint(x: 0, y: -annotationView!.image!.size.height / 2)
                            annotationView?.accessibilityHint = pointAnnotation.accessibilityHint
                            break
                        }
                    
                }
            
        }
        return annotationView
}
}



