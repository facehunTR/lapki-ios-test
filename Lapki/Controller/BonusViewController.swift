//
//  BonusViewController.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 10/01/2019.
//  Copyright © 2019 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import MaterialComponents.MDCButton
import MaterialComponents.MaterialDialogs

class BonusViewController: UIViewController {
    
    @IBOutlet weak var navBar: MyNavigationBar!
    var dialogTransitionController: MDCDialogTransitionController!
    @IBOutlet weak var myTableView: UITableView!
    var parentView: UIViewController?
    var bonuses = [Bonus]()
    var lapki: Int? {
        didSet{
            let L = (self.lapki ?? 0)/100
            let IntL = Int(L)
            self.navBar.title = "На вашем счету: \(IntL) бонусов"
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        dialogTransitionController = MDCDialogTransitionController()
        initToolbar()
        myTableView.delegate = self
        myTableView.dataSource = self
        myTableView.sectionHeaderHeight = UITableView.automaticDimension
        myTableView.estimatedSectionHeaderHeight = 3
        Network.getLapki(responseHandler: weakCompletion(of: self, onResult: BonusViewController.lapkiUpdate, onError: BonusViewController.handleResponseError))
        loadData()
    }
    func orderCreated(result: CreatedOrder){
        Network.getLapki(responseHandler: weakCompletion(of: self, onResult: BonusViewController.lapkiUpdate, onError: BonusViewController.handleResponseError))
        makeToast("\(result)",position: .top)
        self.loadData()
    }
    func bonusesUpdate(usedBonuses: [UsedBonus]){
        let reasons = usedBonuses.map({return $0.reason})
        bonuses = [Bonus]()
        let date = Date()
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(abbreviation: "MST")!
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        let reasonDay = "day \(dateFormatter.string(from: date))"
        self.bonuses.append(Bonus(isAvailable: !reasons.contains(reasonDay),number: 1, text: "Заходите в Лапки!\nПользуйтесь ежедневно и получайте каждый день бонусные 20 лапок", action: {
            if !reasons.contains(reasonDay){
                Network.createOrder(order_reason:reasonDay,ad: 0,byBonuses: nil,responseHandler: weakCompletion(of: self, onResult: BonusViewController.orderCreated, onError: BonusViewController.handleBonusesError))
            }else{
                //TODO: Тоаст для уже получивших ежедневный бонус
            }
        }))
        self.bonuses.append(Bonus(isAvailable: !reasons.contains("info"),number: 2, text: "Добавьте и подтвердите свой номер телефона", action: {
            if !reasons.contains("info"){
                if MyStaticUser.verificationPhone {
                    Network.createOrder(order_reason:"info",ad: 0,byBonuses: nil,responseHandler: weakCompletion(of: self, onResult: BonusViewController.orderCreated, onError: BonusViewController.handleBonusesError))
                }else {
                    self.dismiss(animated: false, completion: {
                        NotificationCenter.default.post(name:Notification.Name(rawValue: "toTelConfirm"),object:nil)
                    })
                }
            }
        }
        ))
        self.bonuses.append(Bonus(isAvailable: !reasons.contains("subscribe"),number: 3, text: "Добавьте и подтвердите свой номер телефона", action: {
            let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            let customView = SocialShareBottomActionView()
            actionSheet.view.addSubview(customView)
            self.present(actionSheet, animated: true, completion: {
            })
        }))
        self.bonuses.append(Bonus(isAvailable: !reasons.contains("share"),number: 4, text: "Добавьте и подтвердите свой номер телефона", action: {
        }))
        
        self.myTableView.beginUpdates()
        self.myTableView.insertSections(IndexSet(integersIn: 0...self.bonuses.count-1), with: .automatic)
        self.myTableView.endUpdates()
        self.myTableView.reloadData()
    }
    func handleBonusesError(_ error: Error){
        Network.getLapki(responseHandler: weakCompletion(of: self, onResult: BonusViewController.lapkiUpdate, onError: BonusViewController.handleResponseError))
        print(error)
        if let responseError = error as? ResponseError{
            switch responseError {
            case .detailReturned(let detail):
                makeToast(detail, position: .top)
            case .withoutDetails:
                makeToast("Произошла ошибка: Нет деталей ошибки", position: .top)
            case .notJSON,.emptyResponse,.notExpectedValue:
                break
            }
        }
    }
    func loadData(){
        myTableView.reloadData()
        Network.getUsedBonuses(responseHandler: weakCompletion(of: self, onResult: BonusViewController.bonusesUpdate, onError: BonusViewController.handleBonusesError))
        // Do any additional setup after loading the view.
    }
    func initToolbar(){
        if navBar.leftBarButtonItem == nil{
            navBar.leftBarButtonItem = MyBarButtonItem(frame: CGRect(x: 0, y: 0, width: 44, height: 44), imageName: "back_orange",action: #selector(backButtonClick(_:)))
        }
    }
    @objc func backButtonClick(_ sender: AnyObject){
        Network.getLapki(responseHandler: weakCompletion(of: self, onResult: BonusViewController.lapkiUpdate, onError: BonusViewController.handleResponseError))
    }
    
    func lapkiUpdate(lapki: LapkiResponse){
        self.lapki = lapki.count
        if let pvc = self.parentView as? SettingsPageViewController{
            pvc.lapki = lapki.count
        }
        self.dismiss(animated: true, completion: nil)
    }
}

extension BonusViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return bonuses.count
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let label = HeaderLabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 44))
            label.textInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
            label.numberOfLines = 0
            label.textAlignment = .center
            label.text = "Получать лапки очень просто! Нужно на них нажимать.\n Убедитесь сами:"
            label.sizeToFit()
            label.backgroundColor = UIColor.clear
            return label
        }else{
            let v = UIView()
            v.backgroundColor = UIColor.clear
            return v
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = self.myTableView.dequeueReusableCell(withIdentifier: "bonusCell", for: indexPath) as? BonusTableViewCell {
            cell.bonus = bonuses[indexPath.section]
            return cell
        }
        return UITableViewCell()
    }
//    func postParameters(Parameters:[String:String],completion:@escaping(Bool?,DataResponse<Any>?) -> Void )  {
//        
//        patchUserData(parameters: Parameters, responseHandler: {response in
//            if response.result.isSuccess {
//                completion(true,response)
//            }else{
//                completion(false,nil)
//            }
//        })
//    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        let vc = segue.destination as! MyDialogViewController
        
//        if segue.identifier == "toConfirm"{
//            vc.mode = "toConfirm"
//            vc.isTimerLabelHidden = false
//            vc.isSecondTextFieldisHidden = true
//            vc.placeholderText = ""
//            vc.inputType = UIKeyboardType.numberPad
//            vc.headerText = "Введите код подтверждения"
//            
//            
//            vc.codeRepeatAction = {success in
//                genVerificationCode(responseHandler: {response in
//                    if let dict = response.result.value as? [String:Any?]{
//                        if let detail = dict["detail"] as? String{
//                            vc.view.makeToast(detail,position:.top)
//                        }
//                    }
//                    // print(response)
//                    vc.timerRestart()
//                })
//                
//            }
//            vc.rightButtonAction = {success in
//                
//                numberVerification(code: vc.textField.text!, responseHandler: {response in
//                    if response.response?.statusCode == 400 {
//                        vc.view.makeToast("Неверный код подтверждения",position:.top)
//                    }else{
//                        refreshMyUser(nil)
//                        vc.dismiss(animated: true, completion: nil)
//                    }
//                })
//            }
//            
//        }
    }
}
class HeaderLabel: UILabel {
    var textInsets = UIEdgeInsets.zero {
        didSet { invalidateIntrinsicContentSize() }
    }
    override func textRect(forBounds bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
        let insetRect = bounds.inset(by: textInsets)
        let textRect = super.textRect(forBounds: insetRect, limitedToNumberOfLines: numberOfLines)
        let invertedInsets = UIEdgeInsets(top: -textInsets.top,
                                          left: -textInsets.left,
                                          bottom: -textInsets.bottom,
                                          right: -textInsets.right)
        return textRect.inset(by: invertedInsets)
    }
    
    override func drawText(in rect: CGRect) {
        super.drawText(in: rect.inset(by: textInsets))
    }
}
