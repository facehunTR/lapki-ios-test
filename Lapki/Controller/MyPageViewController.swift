//
//  MyPageViewController.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 24/10/2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import MaterialComponents.MDCBottomNavigationBar
class MyPageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    var pages = [UIViewController?]()
    var mainViewController: MyMainViewController!
    var currentIndex = 0
    var botNavBar: MDCBottomNavigationBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.dataSource = nil
        // Do any additional setup after loading the view.
        
        let homePage: UIViewController! = storyboard?.instantiateViewController(withIdentifier: "HomePageViewController")
        let homePageWithParent = homePage as! PageObservation
        homePageWithParent.getParentPageViewController(parentRef: self)
        let favoritePage: UIViewController! = storyboard?.instantiateViewController(withIdentifier: "FavoritePageViewController")
        let favoritePageWithParent = favoritePage as! PageObservation
        favoritePageWithParent.getParentPageViewController(parentRef: self)
        let lapkiSearchPage: UIViewController! = storyboard?.instantiateViewController(withIdentifier: "LapkiSearchPageViewController")
        let lapkiSearchPageWithParent = lapkiSearchPage as! PageObservation
        lapkiSearchPageWithParent.getParentPageViewController(parentRef: self)
        let settingsPage: UIViewController! = storyboard?.instantiateViewController(withIdentifier: "SettingsPageViewController")
        let settingsPageWithParent = settingsPage as! PageObservation
        settingsPageWithParent.getParentPageViewController(parentRef: self)
        
        pages.append(homePage)
        pages.append(favoritePage)
        pages.append(lapkiSearchPage)
        pages.append(settingsPage)
        
        setViewControllers([homePage], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        currentIndex = pages.firstIndex(of: viewController)!
        let previousIndex = abs((currentIndex - 1) % pages.count)
        return pages[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        currentIndex = pages.firstIndex(of: viewController)!
        let nextIndex = abs((currentIndex + 1) % pages.count)
        return pages[nextIndex]
    }
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return pages.count
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
    func changePageTo(index: NSInteger){
        let count = pages.count
        let indexes = [0,1,3,4]
        if index < count {
            if index > currentIndex {
                if let vc = pages[index] {
                    self.setViewControllers([vc], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: { (complete) -> Void in
                        self.currentIndex = index
                        self.botNavBar.selectedItem = self.botNavBar.items[indexes[self.currentIndex]]
                    })
                }
            } else if index < currentIndex {
                if let vc = pages[index] {
                    self.setViewControllers([vc], direction: UIPageViewController.NavigationDirection.reverse, animated: true, completion: { (complete) -> Void in
                        self.currentIndex = index
                        self.botNavBar.selectedItem = self.botNavBar.items[indexes[self.currentIndex]]
                        //                        completion?()
                    })
                }
            }
        }
    }
    func refreshOthers(){
        var newPages = [UIViewController?]()
        if(currentIndex != 0){
            let homePage: UIViewController! = storyboard?.instantiateViewController(withIdentifier: "HomePageViewController")
            let homePageWithParent = homePage as! PageObservation
            homePageWithParent.getParentPageViewController(parentRef: self)
            newPages.append(homePage)
        }else{
            newPages.append(pages[currentIndex])
        }
        if(currentIndex != 1){
            let favoritePage: UIViewController! = storyboard?.instantiateViewController(withIdentifier: "FavoritePageViewController")
            let favoritePageWithParent = favoritePage as! PageObservation
            favoritePageWithParent.getParentPageViewController(parentRef: self)
            newPages.append(favoritePage)
        }else{
            newPages.append(pages[currentIndex])
        }
        if(currentIndex != 2){
            let lapkiSearchPage: UIViewController! = storyboard?.instantiateViewController(withIdentifier: "LapkiSearchPageViewController")
            let lapkiSearchPageWithParent = lapkiSearchPage as! PageObservation
            lapkiSearchPageWithParent.getParentPageViewController(parentRef: self)
            newPages.append(lapkiSearchPage)
        }else{
            newPages.append(pages[currentIndex])
        }
        if(currentIndex != 3){
            let settingsPage: UIViewController! = storyboard?.instantiateViewController(withIdentifier: "SettingsPageViewController")
            let settingsPageWithParent = settingsPage as! PageObservation
            settingsPageWithParent.getParentPageViewController(parentRef: self)
            newPages.append(settingsPage)
        }else{
            newPages.append(pages[currentIndex])
        }
        pages = newPages
    }
    func refreshAll(){
        pages = [UIViewController?]()
        let homePage: UIViewController! = storyboard?.instantiateViewController(withIdentifier: "HomePageViewController")
        let homePageWithParent = homePage as! PageObservation
        homePageWithParent.getParentPageViewController(parentRef: self)
        let favoritePage: UIViewController! = storyboard?.instantiateViewController(withIdentifier: "FavoritePageViewController")
        let favoritePageWithParent = favoritePage as! PageObservation
        favoritePageWithParent.getParentPageViewController(parentRef: self)
        let lapkiSearchPage: UIViewController! = storyboard?.instantiateViewController(withIdentifier: "LapkiSearchPageViewController")
        let lapkiSearchPageWithParent = lapkiSearchPage as! PageObservation
        lapkiSearchPageWithParent.getParentPageViewController(parentRef: self)
        let settingsPage: UIViewController! = storyboard?.instantiateViewController(withIdentifier: "SettingsPageViewController")
        let settingsPageWithParent = settingsPage as! PageObservation
        settingsPageWithParent.getParentPageViewController(parentRef: self)
        
        pages.append(homePage)
        pages.append(favoritePage)
        pages.append(lapkiSearchPage)
        pages.append(settingsPage)
        self.setViewControllers([pages[currentIndex]!], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
    }
    
    func refreshOneNotice(notice: Notice){
        for page in self.pages {
            if let home = page as? HomePageViewController {
                home.refreshOneNotice(notice: notice)
            }else if let fav = page as? FavoritePageViewController{
                fav.myFavoritesPageViewController?.refreshOneNotice(notice: notice)
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

