//
//  FilterViewController
//  Lapki
//
//  Created by Yuriy Yashchenko on 29/11/2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import MaterialComponents.MDCButton
import MaterialComponents.MaterialTextFields
import RangeSeekSlider
import SearchTextField
import CoreLocation


class FilterViewController: UIViewController {
    
    var isInternal: Bool = false
    var pojo: Pojo!
    var parentView: UIViewController!
    var endOfList: CompletionHandler?
    var checkedType: String?
    var checkedCategory: String?
    var subType: String? {
        didSet{
            subTypeTF.text = subType
            let row = subTypes.firstIndex(of: subType) ?? 0
            if row == 0 {
                return
            }
            if let subsubtypes = pojo.getSubsubtypesNames(category: checkedCategory!, type: checkedType!, row: row){
                subSubTF.filterStrings(subsubtypes as! [String])
                subSubTypeCard.isHidden = false
            }
            }
    }
    var subSubType: String?
    var age__gt: String?
    var age__lt: String?
    var price__gt: String?
    var price__lt: String?
    var lat: String?
    var lng: String?
    var dist: String?
    var subTypes: [String?] = [String]()
    let age__gtText:[String] = [" ","От 1 недели","От 2 недель","От месяца","От 2 месяцев","От 3 месяцев","От полугода","От 9 месяцев","От года","От 2 лет","От 4 лет"," "]
    let age__ltText:[String] = [" ","До 1 недели","До 2 недель","До месяца","До 2 месяцев","До 3 месяцев","До полугода","До 9 месяцев","До года","До 2 лет","До 4 лет"," "]
    let ageValue:[String?] = [nil,"7","14","30","60","90","180","270","365","730","1460",nil]
    
    let distText:[String] = ["До 1 километра","До 5 километров","До 10 километров","До 25 километров","До 50 километров","До 100 километров","До 200 километров","Без ограничений"]
    let distValue:[String?] = ["1000","5000","10000","25000","50000","100000","200000",nil]
    
    @IBOutlet weak var addressCard: MDCCard!
    
    @IBAction func addressClicked(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let addNoticeMapViewController = storyBoard.instantiateViewController(withIdentifier: "AddNoticeMapViewController") as! AddNoticeMapViewController
        addNoticeMapViewController.parentView = self
        self.present(addNoticeMapViewController, animated: true, completion: nil)
    }
    
    @IBOutlet weak var addressText: UILabel!
    @IBOutlet weak var categoriesHeader: UILabel!
    @IBOutlet weak var categoriesBody: UILabel!
    @IBOutlet weak var categoriesIcon: UIImageView!
    @IBOutlet weak var categoriesCard: MDCCard!
    @IBOutlet weak var priceCard: MDCCard!
    @IBOutlet weak var priceIcon: UIImageView!
    @IBOutlet weak var priceMinTF: MDCTextField!
    @IBOutlet weak var priceMaxTF: MDCTextField!
    @IBOutlet weak var ageCard: MDCCard!
    @IBOutlet weak var ageSlider: RangeSeekSlider!
    @IBOutlet weak var distCard: MDCCard!
    @IBOutlet weak var distSlider: RangeSeekSlider!
    @IBOutlet weak var subTypeTF: UILabel!
    @IBOutlet weak var subTypeCard: MDCCard!
    
    @IBAction func subTypeCardClicked(_ sender: Any) {
//        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//        let pickerViewController = storyBoard.instantiateViewController(withIdentifier: "PickerViewController") as! MyPickerViewBottomController
//        pickerViewController.data = subTypes
//        pickerViewController.selected = subType
//        pickerViewController.parentView = self
//        pickerViewController.outPutName = "subType"
//        let bottomSheet = MDCBottomSheetController(contentViewController: pickerViewController)
//        self.present(bottomSheet, animated: true, completion: nil)
    }
    
    @IBOutlet weak var navBar: MyNavigationBar!
    
    @IBOutlet var subSubTypeCard: MDCCard!
    
    let distTag = 202
    let ageTag = 101
    var allTextFieldsControllers = [MDCTextInputControllerOutlined]()
    
    
    @IBAction func categoriesCardClicked(_ sender: MDCCard) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let categoriesNavigationController = storyBoard.instantiateViewController(withIdentifier: "CategoriesNavigationController") as! CategoriesNavigationController
        categoriesNavigationController.parentView = self
        categoriesNavigationController.pojo = pojo
        categoriesNavigationController.checkedType = checkedType
        categoriesNavigationController.checkedCategory = checkedCategory
        self.present(categoriesNavigationController, animated: true, completion: nil)
    }
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var stackView: UIStackView!
    @IBOutlet var subSubTF: SearchTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initScrollStackView()
        initToolbar()
        initCategoriesView()
        initPriceView()
        initAgeSlider()
        initDistSlider()
        initSubTypeView()
    }
    func initSubTypeView(){
//        subSubTypeCard.isHidden = true
//        subTypeCard.isHidden = true
//        subSubTF.font = UIFont.LapkiFonts.LatoRegular
//        subSubTF.backgroundColor = UIColor.white
//        subSubTF.theme.bgColor = UIColor.white
//        subSubTF.theme.font = UIFont.LapkiFonts.LatoRegular
//        if (checkedType != nil && checkedCategory != nil &&
//            !checkedCategory!.contains("Находка") && !checkedCategory!.contains(oneOf: ["Услуги","Для бизнеса","Мероприятия"]) ){
//            if (pojo.getSubs()![pojo.getNames().firstIndex(of: checkedCategory)!]?.getSubs()![(pojo.getSubs()![pojo.getNames().firstIndex(of: checkedCategory)!]?.getNames().firstIndex(of: checkedType))!]?.getSubs()) != nil{
//                subTypes = pojo.getSubtypesNames(category: checkedCategory!, type: checkedType!) ?? [String]()
//                subTypeCard.isHidden = false
//                if let id = subTypes.firstIndex(of: subType){
//                    subTypeTF.text = subTypes[id]
//                    if let subSubTypes = pojo.getSubs()![pojo.getNames().firstIndex(of: checkedCategory)!]?.getSubs()![(pojo.getSubs()![pojo.getNames().firstIndex(of: checkedCategory)!]?.getNames().firstIndex(of: checkedType))!]?.getSubs()![id]!.getNames(){
//                        if let id2 = subSubTypes.firstIndex(of: subSubType){
//                            subSubTypeCard.isHidden = false
//                            subSubTF.text = subSubTypes[id2]
//                        }
//                    }
//                }else {
//                    subTypeTF.text = subTypes[0]
//                }
//
//            }
//            else{
//                subTypes = (pojo.getSubs()![pojo.getNames().firstIndex(of: checkedCategory)!]?.getSubs()![(pojo.getSubs()![pojo.getNames().firstIndex(of: checkedCategory)!]?.getNames().firstIndex(of: checkedType))!]?.getNames())!
//                subTypeCard.isHidden = true
//                subSubTypeCard.isHidden = false
//                subSubTF.filterStrings(subTypes as! [String])
//                subSubTF.placeholder = "Укажите породу"
//            }
//        }else{
//            subTypeCard.isHidden = true
//            subSubTypeCard.isHidden = true
//        }
    }
    func initScrollStackView(){
        self.scrollView.addSubview(self.stackView)
        self.stackView.translatesAutoresizingMaskIntoConstraints = false
        self.stackView.leadingAnchor.constraint(equalTo: self.scrollView.leadingAnchor).isActive = true
        self.stackView.trailingAnchor.constraint(equalTo: self.scrollView.trailingAnchor).isActive = true
        self.stackView.topAnchor.constraint(equalTo: self.scrollView.topAnchor).isActive = true
        self.stackView.bottomAnchor.constraint(equalTo: self.scrollView.bottomAnchor).isActive = true
        self.stackView.widthAnchor.constraint(equalTo: self.scrollView.widthAnchor).isActive = true
    }
    func initDistSlider(){
        distSlider.tag = distTag
        distSlider.delegate = self
        distSlider.selectedMaxValue = CGFloat(distValue.lastIndex(of: dist) ?? 7)
    }
    func initAgeSlider(){
        ageSlider.tag = ageTag
        ageSlider.delegate = self
        ageSlider.selectedMinValue = CGFloat(ageValue.firstIndex(of: age__gt) ?? 0)
        ageSlider.selectedMaxValue = CGFloat(ageValue.lastIndex(of: age__lt) ?? 11)
    }
    @objc func priceFilterChanged(_ sender: MDCTextField){
        price__gt = priceMinTF.text!
        price__lt = priceMaxTF.text!
        if ((sender == priceMaxTF || sender == priceMinTF) &&
            (price__gt != nil && price__lt != nil)) {
            if(price__gt!.compare(price__lt!, options: .numeric) == .orderedDescending){
                allTextFieldsControllers[1].setErrorText( "Мин. не может быть больше макс.", errorAccessibilityValue: nil)
            }else {
                allTextFieldsControllers[1].setErrorText(nil, errorAccessibilityValue: nil)
            }
        }else{
            allTextFieldsControllers[1].setErrorText(nil, errorAccessibilityValue: nil)
        }
    }
    func initPriceView() {
        let priceMaxTFController = MDCTextInputControllerOutlined(textInput: priceMaxTF)
        allTextFieldsControllers.append(priceMaxTFController)
        priceMaxTF.delegate = self
        if price__lt != nil && !price__lt!.isEmpty{
            priceMaxTF.text = "\(Int(price__lt!)!-1)"
        }
        priceMaxTF.addTarget(self, action: #selector(priceFilterChanged(_:)), for: UIControl.Event.editingChanged)
        priceMaxTFController.floatingPlaceholderNormalColor = UIColor.gray
        priceMaxTFController.floatingPlaceholderActiveColor = UIColor.lapkiColors.orange
        priceMaxTFController.normalColor = UIColor.gray
        priceMaxTFController.activeColor = UIColor.lapkiColors.orange
        priceMaxTFController.placeholderText = "Макс."
        priceMaxTFController.helperText = ""
        priceMaxTF.sizeToFit()
        let priceMinTFController = MDCTextInputControllerOutlined(textInput: priceMinTF)
        allTextFieldsControllers.append(priceMinTFController)
        priceMinTF.delegate = self
        if price__gt != nil && !price__gt!.isEmpty{
            priceMinTF.text = "\(Int(price__gt!)!+1)"
        }
        
        priceMinTF.addTarget(self, action: #selector(priceFilterChanged(_:)), for: UIControl.Event.editingChanged)
        priceMinTFController.floatingPlaceholderNormalColor = UIColor.gray
        priceMinTFController.floatingPlaceholderActiveColor = UIColor.lapkiColors.orange
        priceMinTFController.normalColor = UIColor.gray
        priceMinTFController.activeColor = UIColor.lapkiColors.orange
        priceMinTFController.placeholderText = "Мин."
        priceMinTFController.helperText = ""
        priceMinTF.sizeToFit()
    }
    
    func initCategoriesView(){
        categoriesIcon.image = UIImage(named: "categories_gray_icon")
        
//        if let type = checkedType, let category = checkedCategory{
////            categoriesHeader.text = pojo.getNames()[0]?.replacingOccurrences(of: "Укажите", with: "Изменить")
////            categoriesBody.text = checkedCategory! + "/" + checkedType!
////            if pojo.subs![pojo.names.firstIndex(of: category)!]!.names.contains("Кошки") {
////                if category.contains(oneOf: ["Потеря","Находка","Случка"]){
////                    categoriesIcon.image = UIImage.lostAnimalsImages[pojo.subs![pojo.names.firstIndex(of: category)!]!.names.firstIndex(of: type)!]
////                }else{
////                    categoriesIcon.image = UIImage.animalsImages[pojo.subs![pojo.names.firstIndex(of: category)!]!.names.firstIndex(of: type)!]
////                }
////            }else{
////                categoriesIcon.image = UIImage.categoriesImages[pojo.names.firstIndex(of: category)!]
////            }
//        }else if checkedCategory != nil {
////            categoriesHeader.text = pojo.getNames()[0]?.replacingOccurrences(of: "Укажите", with: "Изменить")
////            categoriesBody.text = checkedCategory!
////            categoriesIcon.image = UIImage.categoriesImages[pojo.getNames().firstIndex(of: checkedCategory)!]
//        }else {
////            categoriesHeader.text = pojo.getNames()[0]
////            categoriesBody.text = "Выбраны все категории"
//        }
    }
    func initToolbar(){
        if navBar.leftBarButtonItem == nil{
            navBar.leftBarButtonItem = MyBarButtonItem(frame: CGRect(x: 0, y: 0, width: 44, height: 44), imageName: "clear_filter_button",action: #selector(backButtonClick(_:)))
            navBar.rightBarButtonItem = MyBarButtonItem(frame: CGRect(x: 0, y: 0, width: 44, height: 44), title: "Принять",action: #selector(acceptButtonClick(_:)))
            navBar.title = "Настройки фильтра"
        }
    }
    @objc func backButtonClick(_ sender: Any){
        guard let pvc = self.parentView as? HomePageViewController else {return}
        if self.checkedType != nil || self.checkedCategory != nil || self.subType != nil || (subSubTF.text != nil && !subSubTF.text!.isEmpty && !subSubTF.text!.contains("Укажите")) || self.age__lt != nil || self.age__gt != nil || self.dist != nil || self.price__gt != nil || self.price__lt != nil || (self.addressText.text != nil && !self.addressText.text!.contains("Укажите")) {
            let alert = createAlertController(title: "Вы уверены что хотите сбросить параметры фильтрации?", message: "Все указанные вами параметры будут заменены на стандартные.")
            alert.addAction(MDCAlertAction(title: "Да", handler: {action in
                pvc.parametersClear()
                self.dismiss(animated: true,completion: nil)
            }))
            alert.addAction(MDCAlertAction(title: "Отмена", handler: {action in
                alert.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }else{
            pvc.parametersClear()
            self.dismiss(animated: true, completion: nil)
        }
    }
    @objc func acceptButtonClick(_ sender: Any){
        for controller in allTextFieldsControllers {
            if (controller.errorText != nil){
                return
            }
        }
        if let pvc = self.parentView as? HomePageViewController{
            pvc.parameters["type"] = self.checkedType
            pvc.parameters["category"] = self.checkedCategory
            if !(subType?.contains("Укажите") ?? true){
            pvc.parameters["subtype"] = subType
            }else{
                pvc.parameters["subtype"] = nil
            }
            let subSubTypeString = subSubTF.text ?? ""
            if !subSubTypeString.contains("Укажите"){
                subSubType=subSubTF.text
            }
            pvc.parameters["subsubtype"] = subSubType
            pvc.parameters["age__lt"] = age__lt
            pvc.parameters["age__gt"] = age__gt
            pvc.parameters["dist"] = dist
            if priceMinTF.text == nil || priceMaxTF.text!.isEmpty  {
                pvc.parameters.removeValue(forKey: "price__lt")
            }else{
                pvc.parameters["price__lt"] = "\(Int(priceMaxTF.text!)!+1)"
            }
            if priceMinTF.text == nil || priceMinTF.text!.isEmpty  {
              pvc.parameters["price__gt"] = "-1"
            }else{
                pvc.parameters["price__gt"] = "\(Int(priceMinTF.text!)!-1)"
            }
            if Double(self.lat ?? "") != nil && Double(self.lng ?? "") != nil {
                pvc.settedUserLocation = CLLocation(latitude: Double(self.lat!)!, longitude: Double(self.lng!)!)
                pvc.settedAddress = self.addressText.text
            }else{
                pvc.settedUserLocation = nil
                pvc.settedAddress = nil
            }
            pvc.refresh(self)
        }
        self.dismiss(animated: true,completion: nil)
    }
}

extension FilterViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        
        // TextField
        price__gt = priceMinTF.text!
        price__lt = priceMaxTF.text!
        if ((textField == priceMaxTF || textField == priceMinTF) &&
            (price__gt != nil && price__lt != nil) && (!price__gt!.isEmpty && (price__lt?.isEmpty)!)) {
            if(price__gt!.compare(price__lt!, options: .numeric) == .orderedDescending){
                allTextFieldsControllers[1].setErrorText( "Мин. не может быть больше макс.", errorAccessibilityValue: nil)
            }else {
                allTextFieldsControllers[1].setErrorText(nil, errorAccessibilityValue: nil)
            }
        }else{
            allTextFieldsControllers[1].setErrorText(nil, errorAccessibilityValue: nil)
        }
        
        return false
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        if textField == priceMaxTF || textField == priceMinTF{
            let maxLength = 8
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
    
    
}
extension FilterViewController: RangeSeekSliderDelegate {
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat){
        switch slider.tag {
        case ageTag:
            age__gt = ageValue[Int(minValue)]
            age__lt = ageValue[Int(maxValue)]
            break
        case distTag:
            dist = distValue[Int(maxValue)]
            break
        default:
            break
        }
    }
    func rangeSeekSlider(_ slider: RangeSeekSlider, stringForMinValue minValue: CGFloat) -> String?{
        switch slider.tag {
        case ageTag:
            return age__gtText[Int(minValue)]
        case distTag:
            return nil
        default:
            return nil
        }
    }
    func rangeSeekSlider(_ slider: RangeSeekSlider, stringForMaxValue maxValue: CGFloat) -> String?{
        switch slider.tag {
        case ageTag:
            return age__ltText[Int(maxValue)]
        case distTag:
            return distText[Int(maxValue)]
        default:
            return nil
        }
    }
}
extension FilterViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return subTypes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return subTypes[row]
    }
}


