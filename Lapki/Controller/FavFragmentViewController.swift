//
//  FavFragmentViewController.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 27/10/2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import CoreLocation


class FavFragmentViewController: UIViewController, PageObservationFavorites{
    
    var refreshControl = UIRefreshControl()
    var itemCount = 0
    var parameters: Parameters = ["ordering":"-date_uplift"]
    var nextPage, previousPage: String?
    var isWaiting: Bool = false
    var lastLocation: CLLocation?{
        didSet{
            locationRequestBuilder()
        }
    }
    @IBOutlet weak var placeholderView: UIView!
    
    var myFavoritesPageViewController : MyFavoritesPageViewController!
    func getParentPageViewController(parentRef: MyFavoritesPageViewController) {
        myFavoritesPageViewController = parentRef
    }
    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    var noticeArray = Notices()
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        myFavoritesPageViewController.segmentedControlP.selectedSegmentIndex = 0
        myFavoritesPageViewController.segmentedControlP.sendActions(for: UIControl.Event.valueChanged)
    }
    func locationRequestBuilder() {
        if lastLocation != nil {
            self.parameters["lat"] = String(lastLocation!.coordinate.latitude)
            self.parameters["lng"] =  String(lastLocation!.coordinate.longitude)
        }else if MyStaticUser.defaultLocation.longitude != 0 && MyStaticUser.defaultLocation.latitude != 0 {
            self.parameters["lat"] = String(MyStaticUser.defaultLocation.latitude)
            self.parameters["lng"] =  String(MyStaticUser.defaultLocation.longitude)
        }else{
            self.parameters.removeValue(forKey: "lat")
            self.parameters.removeValue(forKey: "lng")
        }
    }
    @objc func somethingChanged(notification: Notification){
            self.lastLocation = lastUserLocation
            refresh(self)
    }
    @IBOutlet weak var noticeCollectionView: UICollectionView!
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let leftEdgePan = UIScreenEdgePanGestureRecognizer(target:self, action:#selector(leftEdgeSwiped))
        leftEdgePan.edges = .left
        view.addGestureRecognizer(leftEdgePan)
        let rightEdgePan = UIScreenEdgePanGestureRecognizer(target:self, action:#selector(rightEdgeSwiped))
        rightEdgePan.edges = .right
        view.addGestureRecognizer(rightEdgePan)
        noticeCollectionView.dataSource = self
        noticeCollectionView.delegate = self
        refreshControl.tintColor = UIColor.lapkiColors.lostOrange
        refreshControl.attributedTitle = nil
        refreshControl.addTarget(self, action: #selector(self.refresh(_ :)), for: UIControl.Event.valueChanged)
        noticeCollectionView.refreshControl = refreshControl
        self.lastLocation = lastUserLocation
        refresh(self)
        
        NotificationCenter.default.addObserver(self, selector: #selector(somethingChanged(notification:)), name: Notification.Name("locationUpdated"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(somethingChanged(notification:)), name: Notification.Name("userUpdated"), object: nil)
        // Do any additional setup after loading the view.
    }
    func doPaging(){
        guard let nextPage = self.nextPage else {
            self.isWaiting = false
            return}
        Network.getNextPage(url: nextPage, responseHandler: weakCompletion(of: self, onResult: FavFragmentViewController.nextPageSuccess, onError: FavFragmentViewController.nextPageError))
    }
    func nextPageError(_ error: Error){
        self.handleResponseError(error)
        self.isWaiting = false
    }
    func nextPageSuccess(_ page: PageResult){
        self.nextPage = page.next
        self.noticeArray.append(contentsOf: page.results)
        self.noticeCollectionView.reloadData()
        self.isWaiting = false
    }
    
    @objc func refresh(_ sender:AnyObject?) {
        guard MyStaticUser.id != 0 else {
            self.placeholderView.isHidden = false
            self.isWaiting = false
            self.noticeCollectionView.refreshControl?.endRefreshing()
            return
        }
        self.noticeCollectionView.refreshControl?.beginRefreshing()
        self.placeholderView.isHidden = true
        Network.getFavorites(parameters: parameters, responseHandler: weakCompletion(of: self, onResult: FavFragmentViewController.refreshSuccess, onError: FavFragmentViewController.refreshError))
    }
    func refreshSuccess(_ page: PageResult){
        self.nextPage = page.next
        self.noticeArray = page.results
        self.noticeCollectionView.performBatchUpdates({
            self.noticeCollectionView.reloadSections(IndexSet(integer: 0))
        }, completion: { (success) in
            self.isWaiting = false
            self.noticeCollectionView.refreshControl?.endRefreshing()
        })
    }
    func refreshError(_ error: Error){
        self.noticeCollectionView.refreshControl?.endRefreshing()
        self.isWaiting = false
        self.handleResponseError(error)
    }
    @objc func leftEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            if myFavoritesPageViewController.currentIndex>0{
                myFavoritesPageViewController.changePageTo(index: myFavoritesPageViewController.currentIndex-1)
            }
            else if myFavoritesPageViewController.currentIndex==0{
                myFavoritesPageViewController.myPageViewController.changePageTo(index: 0)
                
            }
        }
    }
    @objc func rightEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            if myFavoritesPageViewController.currentIndex<myFavoritesPageViewController.pages.count{
                myFavoritesPageViewController.changePageTo(index: myFavoritesPageViewController.currentIndex+1)
            }
        }
    }
}
extension FavFragmentViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if self.noticeArray.count > 40 {
            if !(indexPath.row < self.noticeArray.count-20) && !isWaiting{
                isWaiting = true
                self.doPaging()
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let notice = noticeArray[indexPath.row]
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let noticeDetailViewController = storyBoard.instantiateViewController(withIdentifier: "NoticeDetailViewController") as! NoticeDetailViewController
        noticeDetailViewController.notice = notice
        noticeDetailViewController.myMainViewController = self.myFavoritesPageViewController.myPageViewController.mainViewController
        self.present(noticeDetailViewController, animated: true, completion: nil)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return noticeArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let notice = noticeArray[indexPath.row]
        if let noticeCell =  collectionView.dequeueReusableCell(withReuseIdentifier: "simpleNoticeCell", for: indexPath) as? SimpleNoticeCell{
            noticeCell.triangleIconImageView.isHidden = false
            noticeCell.triangleView.isHidden = false
            noticeCell.triangleView.tintColor = UIColor.lapkiColors.green
            noticeCell.triangleIconImageView.image = UIImage.init(named:"triangleLapka" )
            if notice.category == "Потеряшка"{
                if notice.price >= 1 {
                    
                    noticeCell.triangleView.tintColor = UIColor.lapkiColors.red
                    noticeCell.triangleIconImageView.image = UIImage.init(named:"ruble" )
                    noticeCell.triangleIconImageView.tintColor = UIColor.white
                }else{
                    
                    noticeCell.triangleView.tintColor = UIColor.lapkiColors.lostOrange
                    noticeCell.triangleIconImageView.image = UIImage.init(named:"triangleLapka" )
                    noticeCell.triangleIconImageView.tintColor = UIColor.white
                }
                
            }
            else if notice.category != "Находка"{
                noticeCell.triangleIconImageView.isHidden = true
                noticeCell.triangleView.isHidden = true
            }
            
            if notice.highlight == true{
                noticeCell.setGradientBorder(width: 10, colors: [UIColor.lapkiColors.gradientStart,UIColor.lapkiColors.gradientEnd],startPoint: CGPoint(x: 0.5, y: 0),endPoint: CGPoint(x: 0.5, y: 1))
            }
            else{
                noticeCell.removeGradientBorder()
            }
            //print(notice)
            noticeCell.btnTapAction = {
                () in
                self.myFavoritesPageViewController.myPageViewController.refreshOneNotice(notice: notice)
            }
            if lastUserLocation != nil {
                noticeCell.location = lastUserLocation
            }
            noticeCell.notice = notice
            
            noticeCell.layer.shouldRasterize = true
            noticeCell.layer.rasterizationScale = UIScreen.main.scale
            noticeCell.parentView = self
            return noticeCell
        }
        
        return UICollectionViewCell()
    }
    func refreshOneNotice(notice:Notice){
        if !self.noticeArray.contains(where: { (not) -> Bool in
            return not.id == notice.id
        }) {
            self.noticeArray.append(notice)
            self.noticeCollectionView.reloadData()
        }else{
            self.noticeArray.removeAll { (not) -> Bool in
                return not.id == notice.id
            }
            self.noticeCollectionView.reloadData()
        }
    }
}
extension FavFragmentViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 6.0, left: 4, bottom: 0, right: 4)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let bounds = collectionView.bounds
        return CGSize(width: bounds.width/2-7.5, height: bounds.width/2+48.5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 7.5
    }
}

