//
//  HomePageViewController.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 24/10/2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialCards
import MaterialComponents.MDCAppBarViewController
import CoreLocation

class HomePageViewController: UIViewController, PageObservation {
    
    @IBOutlet weak var placeholderView: UIView!
    
    @IBAction func placeholderCardClicked(_ sender: Any) {
        
    }
    func refreshOneNotice(notice:Notice){
        var n = notice
        if let r = self.noticeArray.firstIndex(where: { (not) -> Bool in
            return not.id == n.id
        }){
            n.distance = self.noticeArray[r].distance
            self.noticeArray[r]=n
            self.noticeCollectionView.reloadItems(at: [IndexPath(row:
                r, section: 0)])
        }
    }
    func parametersClear(){
        self.parameters.removeValue(forKey: "type")
        self.parameters.removeValue(forKey: "category")
        self.parameters.removeValue(forKey: "subtype")
        self.parameters.removeValue(forKey: "subsubtype")
        self.parameters.removeValue(forKey: "age__lt")
        self.parameters.removeValue(forKey: "age__gt")
        self.parameters.removeValue(forKey: "dist")
        self.parameters.removeValue(forKey: "price__gt")
        self.parameters.removeValue(forKey: "price__lt")
        self.settedUserLocation = nil
        self.settedAddress = nil
        self.refresh(self)
    }
    var indicator: UIActivityIndicatorView?
    @IBOutlet weak var navBar: MyNavigationBar!
    var nextPage: String? {
        didSet{
            if self.nextPage != nil {
                indicator?.startAnimating()
            }else{
                self.indicator?.stopAnimating()
                for footer in self.noticeCollectionView.visibleSupplementaryViews(ofKind:  UICollectionView.elementKindSectionFooter){
                    footer.removeFromSuperview()
                }
            }
        }
    }
    var previousPage: String?
    var isWaiting: Bool = false
    @IBOutlet weak var bottomHeight: NSLayoutConstraint!
    var refreshControl = UIRefreshControl()
    var parameters = [String:Any?]()
    var lastLocation: CLLocation? {
        didSet{
            self.locationRequestBuilder()
        }
    }
    var settedUserLocation: CLLocation? {
        didSet {
            self.locationRequestBuilder()
        }
    }
    var settedAddress: String?
    var itemCount = 0
    
    func locationRequestBuilder() {
        if settedUserLocation != nil {
            self.parameters["lat"] = String(settedUserLocation!.coordinate.latitude)
            self.parameters["lng"] =  String(settedUserLocation!.coordinate.longitude)
        }else if lastLocation != nil {
            self.parameters["lat"] = String(lastLocation!.coordinate.latitude)
            self.parameters["lng"] =  String(lastLocation!.coordinate.longitude)
        }else if MyStaticUser.defaultLocation.longitude != 37.6169 && MyStaticUser.defaultLocation.latitude != 55.7520 {
            self.parameters["lat"] = String(MyStaticUser.defaultLocation.latitude)
            self.parameters["lng"] =  String(MyStaticUser.defaultLocation.longitude)
        }else{
            self.parameters.removeValue(forKey: "lat")
            self.parameters.removeValue(forKey: "lng")
        }
    }
    private var didTapDeleteKey = false
    var myPageViewController : MyPageViewController!
    func getParentPageViewController(parentRef: MyPageViewController) {
        myPageViewController = parentRef
    }
    var categoriesViewController: CategoriesViewController!
    
    @objc func categoriesbuttonclick(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let categoriesNavigationController = storyBoard.instantiateViewController(withIdentifier: "CategoriesNavigationController") as! CategoriesNavigationController
        categoriesNavigationController.parentView = self
        categoriesNavigationController.pojo = myPageViewController.mainViewController.Categories
        categoriesNavigationController.checkedType = parameters["type"] as? String
        categoriesNavigationController.checkedCategory = parameters["category"] as? String
        self.present(categoriesNavigationController, animated: true, completion: nil)
    }
    
    @objc func filterbuttonclick(_ sender: Any) {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let filterViewController = storyBoard.instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
        filterViewController.parentView = self as UIViewController
        filterViewController.pojo = myPageViewController.mainViewController.Categories
        filterViewController.checkedType = parameters["type"] as? String
        filterViewController.checkedCategory = parameters["category"] as? String
        filterViewController.subType = parameters["subtype"] as? String
        filterViewController.subSubType = parameters["subsubtype"] as? String
        filterViewController.age__gt = parameters["age__gt"] as? String
        filterViewController.age__lt = parameters["age__lt"] as? String
        filterViewController.price__gt = parameters["price__gt"] as? String
        filterViewController.price__lt = parameters["price__lt"] as? String
        filterViewController.dist = parameters["dist"] as? String
        if settedUserLocation != nil && settedAddress != nil && !(settedAddress?.isEmpty ?? false){
            filterViewController.lat = "\(settedUserLocation!.coordinate.latitude)"
            filterViewController.lng = "\(settedUserLocation!.coordinate.longitude)"
            filterViewController.addressText.text = self.settedAddress
        }else{
            filterViewController.lat = nil
            filterViewController.lng = nil
            filterViewController.addressText.text = "Укажите адрес"
        }
        self.present(filterViewController, animated: true, completion: nil)
    }
    
    var noticeArray = Notices()
    @IBOutlet weak var noticeCollectionView: UICollectionView!
    let navItem = UINavigationItem()
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl.tintColor = UIColor.lapkiColors.lostOrange
//        refreshControl.attributedTitle = nil
        noticeCollectionView.refreshControl = refreshControl
        noticeCollectionView.refreshControl?.addTarget(self, action: #selector(self.refresh(_ :)), for: UIControl.Event.valueChanged)
        self.initToolbar()
        itemCount=0
        
        self.noticeCollectionView.alwaysBounceVertical = true
        let leftEdgePan = UIScreenEdgePanGestureRecognizer(target:self, action:#selector(leftEdgeSwiped))
        leftEdgePan.edges = .left
        view.addGestureRecognizer(leftEdgePan)
        let rightEdgePan = UIScreenEdgePanGestureRecognizer(target:self, action:#selector(rightEdgeSwiped))
        rightEdgePan.edges = .right
        view.addGestureRecognizer(rightEdgePan)
        noticeCollectionView.dataSource = self
        noticeCollectionView.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(refresh(_:)), name: Notification.Name("userUpdated"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.locationChanged(notification:)), name: Notification.Name("locationUpdated"), object: nil)
        self.lastLocation = lastUserLocation
        refresh(self)
    }
    @objc func gestureStarted(_ sender: Any){
        self.view.endEditing(true)
    }
    @objc func locationChanged(notification: Notification){
        if self.lastLocation == nil {
            self.lastLocation = lastUserLocation
            refresh(self)
        }else{
            self.lastLocation = lastUserLocation
        }
    }
    @objc func refresh(_ sender:AnyObject?) {
        Network.getNotices(parameters: parameters, responseHandler: weakCompletion(of: self, onResult: HomePageViewController.refreshSuccess, onError: HomePageViewController.refreshError))
    }
    func refreshSuccess(_ page: PageResult){
        self.placeholderView.isHidden = (page.count != 0)
        self.nextPage = page.next
        self.noticeArray = page.results
        self.noticeCollectionView.performBatchUpdates({
            self.noticeCollectionView.reloadSections(IndexSet(integer: 0))
        }, completion: { (success) in
            self.isWaiting = false
            self.noticeCollectionView.refreshControl?.endRefreshing()
        })
    }
    func refreshError(_ error: Error){
        self.noticeCollectionView.refreshControl?.endRefreshing()
        self.isWaiting = false
        self.handleResponseError(error)
    }
    @objc func leftEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .ended {
            if myPageViewController.currentIndex>0{
                myPageViewController.changePageTo(index: myPageViewController.currentIndex-1)
            }
        }
    }
    @objc func rightEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .ended {
            if myPageViewController.currentIndex<myPageViewController.pages.count{
                myPageViewController.changePageTo(index: myPageViewController.currentIndex+1)
            }
        }
    }
    func doPaging(){
        guard let nextPage = self.nextPage else {
            self.isWaiting = false
            return}
        Network.getNextPage(url: nextPage, responseHandler: weakCompletion(of: self, onResult: HomePageViewController.nextPageSuccess, onError: HomePageViewController.nextPageError))
    }
    func nextPageError(_ error: Error){
        self.handleResponseError(error)
        self.isWaiting = false
    }
    func nextPageSuccess(_ page: PageResult){
        self.nextPage = page.next
        self.noticeArray.append(contentsOf: page.results)
        self.noticeCollectionView.reloadData()
        self.isWaiting = false
    }
    
    func initToolbar(){
        if navBar.leftBarButtonItem == nil{
            navBar.leftBarButtonItem = MyBarButtonItem(frame: CGRect(x: 0, y: 0, width: 44, height: 44), imageName: "categories",action: #selector(categoriesbuttonclick(_:)))
            navBar.rightBarButtonItem = MyBarButtonItem(frame: CGRect(x: 0, y: 0, width: 44, height: 44), imageName: "filter",action: #selector(filterbuttonclick(_:)))
            let searchBar = MySearchBar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width - 88, height: 44))
            navBar.titleView?.removeFromSuperview()
            navBar.translatesAutoresizingMaskIntoConstraints = false
            searchBar.translatesAutoresizingMaskIntoConstraints = false
            searchBar.layoutMargins = .zero
            searchBar.delegate = self
            searchBar.placeholder = "Поиск"
            searchBar.showsCancelButton = false
            searchBar.tintColor = UIColor.lapkiColors.orange
            searchBar.backgroundColor = UIColor.white
            searchBar.tintColor = UIColor.white
            searchBar.barTintColor = UIColor.white
            UITextField.appearance(whenContainedInInstancesOf: [MySearchBar.self]).tintColor = UIColor.lapkiColors.lostOrange
            navBar.titleViewLayoutBehavior = .fill
            navBar.titleView = searchBar
            let rect = navBar.titleView?.frame ?? CGRect()
            //            UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, rect.size.height-2,rect.size.width, 2)];
            let lineView = UIView(frame: CGRect(x: 0, y: rect.size.height-2, width: rect.size.width, height: 2))
            lineView.backgroundColor = UIColor.white
            navBar.titleView?.addSubview(lineView)
            let lineView2 = UIView(frame: CGRect(x: 0, y: 0, width: rect.size.width, height: 2))
            lineView2.backgroundColor = UIColor.white
            navBar.titleView?.addSubview(lineView2)
            definesPresentationContext = true
            if #available(iOS 11.0, *) {
                searchBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
            }
        }
    }
}
extension HomePageViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if self.noticeArray.count > 40 {
            if !(indexPath.row < self.noticeArray.count-20) && !isWaiting{
                isWaiting = true
                self.doPaging()
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let notice = self.noticeArray[indexPath.row]
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let noticeDetailViewController = storyBoard.instantiateViewController(withIdentifier: "NoticeDetailViewController") as! NoticeDetailViewController
        noticeDetailViewController.notice = notice
        noticeDetailViewController.parentView = self
        noticeDetailViewController.myMainViewController = self.myPageViewController.mainViewController
        self.present(noticeDetailViewController, animated: true, completion: nil)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return noticeArray.count
    }
    func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath) {
        if self.nextPage == nil {
            view.removeFromSuperview()
        }else{
            indicator?.startAnimating()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let notice = noticeArray[indexPath.row]
        if let noticeCell =  collectionView.dequeueReusableCell(withReuseIdentifier: "simpleNoticeCell", for: indexPath) as? SimpleNoticeCell{
            noticeCell.triangleIconImageView.isHidden = false
            noticeCell.triangleView.isHidden = false
            noticeCell.triangleView.roundCorners(corners: [.layerMinXMinYCorner], cornerRadius: 5.8)
            noticeCell.triangleView.tintColor = UIColor.lapkiColors.green
            noticeCell.triangleIconImageView.image = UIImage.init(named:"triangleLapka" )
            if notice.category == "Потеряшка"{
                if notice.price >= 1 {
                    
                    noticeCell.triangleView.tintColor = UIColor.lapkiColors.red
                    noticeCell.triangleIconImageView.image = UIImage.init(named:"ruble" )
                    noticeCell.triangleIconImageView.tintColor = UIColor.white
                }else{
                    noticeCell.triangleView.tintColor = UIColor.lapkiColors.lostOrange
                    noticeCell.triangleIconImageView.image = UIImage.init(named:"triangleLapka" )
                    noticeCell.triangleIconImageView.tintColor = UIColor.white
                    
                }
                
            }
            else if notice.category != "Находка"{
                noticeCell.triangleIconImageView.isHidden = true
                noticeCell.triangleView.isHidden = true
            }
            
            if notice.highlight == true{
                noticeCell.setGradientBorder(width: 10, colors: [UIColor.lapkiColors.gradientStart,UIColor.lapkiColors.gradientEnd],startPoint: CGPoint(x: 0.5, y: 0),endPoint: CGPoint(x: 0.5, y: 1))
            }
            else{
                noticeCell.removeGradientBorder()
            }
            //print(notice)
            noticeCell.btnTapAction = {
                () in
                //                self.myPageViewController.refreshOthers()
                self.myPageViewController.refreshOneNotice(notice: notice)
            }
            if lastLocation != nil {
                noticeCell.location = lastLocation
            }
            noticeCell.notice = notice
            
            noticeCell.layer.shouldRasterize = true
            noticeCell.layer.rasterizationScale = UIScreen.main.scale
            noticeCell.parentView = self
            return noticeCell
            
        }
        
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var v = UICollectionReusableView()
        let footer = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "footerView", for: indexPath)
        indicator = UIActivityIndicatorView()
        indicator!.color = .black
        indicator!.translatesAutoresizingMaskIntoConstraints = false
        indicator!.hidesWhenStopped = true
        footer.addSubview(indicator!)
        indicator!.centerXAnchor.constraint(equalTo: footer.centerXAnchor).isActive = true
        indicator!.centerYAnchor.constraint(equalTo: footer.centerYAnchor).isActive = true
        v = footer
        return v
    }
}

extension HomePageViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 6.0, left: 4, bottom: 0, right: 4)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let bounds = collectionView.bounds
        return CGSize(width: bounds.width/2-7.5, height: bounds.width/2+48.5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 7.5
    }
}
extension HomePageViewController: UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if !didTapDeleteKey && searchText.isEmpty {
            searchBar.endEditing(true)
        }
        
        didTapDeleteKey = false
        parameters["search"] = searchText
        self.refresh(self)
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }
    
}
extension HomePageViewController: UIGestureRecognizerDelegate{
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

extension UIRefreshControl {
    func programaticallyBeginRefreshing(in collectionView: UICollectionView) {
        beginRefreshing()
        self.tintColor = UIColor.lapkiColors.lostOrange
        let offsetPoint = CGPoint.init(x: 0, y: -frame.size.height)
        collectionView.setContentOffset(offsetPoint, animated: true)
    }
}






