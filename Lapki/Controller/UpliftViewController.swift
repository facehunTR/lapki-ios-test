//
//  UpliftViewController.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 24/10/2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialCards
import MaterialComponents.MDCAppBarViewController
import CoreLocation

class UpliftViewController: UIViewController{
    
    @IBOutlet weak var Button: MDCButton!
    @IBOutlet weak var bonusAmoutLabel: UILabel!
    @IBOutlet weak var bonusSwitch: UISwitch!
    @IBOutlet weak var bonusView: UIView!
    @IBOutlet weak var myCollectionView: UICollectionView!
    @IBOutlet weak var navBar: MyNavigationBar!
    
    var notice: Notice!
    var dialogTransitionController: MDCDialogTransitionController!
    var parentView: UIViewController?
    var orders = [Order]()
    var collectionData = [Order]()
    
    @IBAction func switchChanged(_ sender: Any) {
        if self.bonusSwitch.isOn {
            if lapki == nil {
                self.bonusSwitch.setOn(false, animated: true)
            }else{
                if lapki == 0 {
                    self.bonusSwitch.setOn(false, animated: true)
                }
            }
        }
        let order = self.selectedOrder
        self.selectedOrder = order
    }
    
    func orderCreated(result: CreatedOrder){
        guard let url = result.formUrl, url.starts(with: "http") else{
            self.dismiss(animated: true, completion: {
                makeToast("\(result)",position:.top)})
            return
        }
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        guard let myWebViewController = storyBoard.instantiateViewController(withIdentifier: "MyWebViewController") as? MyWebViewController else {
            print("\(self.debugDescription) ERROR: MyWebViewController not instantinated")
            return
        }
        myWebViewController.urlString = url
        myWebViewController.titleString = "Процедура оплаты"
        myWebViewController.parentView = self
        self.present(myWebViewController, animated: true, completion: nil)
    }
    @IBAction func buttonAction(_ sender: Any) {
        guard let selectedOrder = self.selectedOrder else{ return }
            Network.createOrder(order_reason: selectedOrder.reason, ad: self.notice.id, byBonuses: self.bonusSwitch.isOn, responseHandler: weakCompletion(of: self, onResult: UpliftViewController.orderCreated, onError: UpliftViewController.handleResponseError))
    }
    @objc func backButtonClick(_ sender: AnyObject){
            self.dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var linkTextView: UITextView!
    var resultUrl: URL? {
        didSet{
            if resultUrl?.absoluteString.contains("/order/success/") ?? false{
                NotificationCenter.default.post(name: NSNotification.Name("userUpdated"), object: nil)
                self.dismiss(animated: true, completion: nil)
            }else{
                makeToast("Произошла ошибка при оплате попробуйте ещё раз.",position: .top)
            }
        }
    }
    
    var itemCount = 0
    
    func initToolbar(){
        if navBar.leftBarButtonItem == nil{
            navBar.leftBarButtonItem = MyBarButtonItem(frame: CGRect(x: 0, y: 0, width: 44, height: 44), imageName: "back_orange",action: #selector(backButtonClick(_:)))
            navBar.title = "Как продать быстрее?"
        }
    }
    
    var selectedOrder: Order?{
        didSet{
            if self.selectedOrder != nil {
                Button.setEnabled(true, animated: true)
                var s: String!
                switch self.selectedOrder!.reason {
                case "uplift":
                    s = "Поднять"
                    break
                case "highlight":
                    s = "Выделить"
                    break
                case "uplifthighlight":
                    s = "Поднять и выделить"
                    break
                default:
                    s = ""
                    break
                }
                if bonusSwitch.isOn {
                    if lapki == nil{
                        bonusSwitch.setOn(false, animated: true)
                        makeToast("Нет связи с сервером, попробуйте через некоторое время",position: .top)
                    }else{
                        if lapki! >= self.selectedOrder!.amount! {
                            let string = "\(s!) за \((self.selectedOrder!.amount!)/100) бонусов"
                            Button.setTitle( string, for: .normal)
                        }else{
                            Button.setTitle("\(s!) за \(lapki!/100) бонусов и \((self.selectedOrder!.amount!-lapki!)/100) рублей", for: .normal)
                        }
                    }
                }else{
                    Button.setTitle("\(s!) за \(self.selectedOrder!.amount/100) рублей", for: .normal)
                }
            }else{
                 Button.setEnabled(false, animated: true)
            }
        }
    }
    
    private var lapki: Int?{
        didSet{
            self.bonusAmoutLabel.text = "На вашем счету: \(Int((self.lapki ?? 0)/100)) бонусов"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initToolbar()
          itemCount=0
        bonusView.layer.borderColor = UIColor.gray.cgColor
        bonusView.layer.borderWidth = 1
        Button.isUppercaseTitle = false
        Button.setBackgroundColor(UIColor.lapkiColors.orange,for:.normal)
        Button.setBackgroundColor(UIColor.gray,for:.disabled)
        Button.titleLabel?.adjustsFontSizeToFitWidth = true
        myCollectionView.dataSource = self
        myCollectionView.delegate = self
        if let flowLayout = self.myCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.estimatedItemSize = CGSize(width: UIScreen.main.bounds.size.width-40, height: 80)
        }
        
        if notice.category.contains("Потеря") || notice.category.contains("Находка"){
            navBar.title = ""
        }
        Network.getLapki(responseHandler: weakCompletion(of: self, onResult: UpliftViewController.lapkiUpdate, onError: UpliftViewController.handleResponseError))
        let attributedString = NSMutableAttributedString(string: "Подключая услугу, вы соглашаетесь с Офертой Лапки и Условиями оказания услуги")
        attributedString.addAttribute(.link, value: "https://www.lapki.com/offer-pay/", range: NSRange(location: 36, length: 13))
        linkTextView.attributedText = attributedString
        linkTextView.delegate = self
        linkTextView.sizeToFit()
    }
    
    func lapkiUpdate(lapki: LapkiResponse){
        self.lapki = lapki.count
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.collectionData.isEmpty {
            loadData()
        }
    }
    func loadData(){
        
        orders = [Order]()
        orders.append(Order(reason:"highlight",title:"Выделение",description:"Объявление в течение 7 дней будет выделено в ленте цветом",amount:9500))
        orders.append(Order(reason:"uplift",title:"Поднятие",description:"Объявление поднимется один раз на верхние строчки ленты",amount:14500))
        orders.append(Order(reason:"uplifthighlight",title:"Поднятие + выделение",description:"Объявление выделяется на 7 дней в ленте цветом, и поднимается один раз в ленте",amount:22500))
        
        if self.orders.count > 0 {
            var i = self.collectionData.count - 1
            self.orders.forEach { notice in
                self.collectionData.append(notice)
                i=i+1
                self.myCollectionView.performBatchUpdates({
                    self.myCollectionView.performBatchUpdates({ () -> Void in
                        self.myCollectionView.insertItems(at: [NSIndexPath(item: i, section: 0) as IndexPath])
                        self.itemCount = self.itemCount+1
                    }, completion: nil)
                })
            }
        }
    }
}
extension UpliftViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let myWebViewController = storyBoard.instantiateViewController(withIdentifier: "MyWebViewController") as? MyWebViewController
        myWebViewController!.urlString = URL.absoluteString
        myWebViewController!.titleString = "Оферта"
        self.present(myWebViewController!, animated: true, completion: nil)
        return false
    }
}
extension UpliftViewController: UICollectionViewDataSource, UICollectionViewDelegate {

func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    if let selectedItems = collectionView.indexPathsForSelectedItems{
        for indexP in selectedItems {
            collectionView.deselectItem(at: indexP, animated:true)
        }
    }
    self.selectedOrder = self.collectionData[indexPath.row]
    collectionView.selectItem(at: indexPath, animated: true, scrollPosition: [])
}

func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return self.collectionData.count
}

func collectionView(_ collectionView: UICollectionView,
                    cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    if let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "orderCell", for: indexPath) as? UpliftTableViewCell{
        cell.order = self.collectionData[indexPath.row]
        return cell
    }
    return UICollectionViewCell()
}
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row == 2{
            return CGSize (width:collectionView.frame.width - 20 , height: 130)
        }
        else{
        return CGSize (width:collectionView.frame.width - 20,  height: 110)
        }
    }
}

extension UpliftViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 6.0, left: 4, bottom: 0, right: 4)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 7.5
    }
}

extension UpliftViewController: UIGestureRecognizerDelegate{
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}


