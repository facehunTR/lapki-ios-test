//
//  MyNoticesFragmentViewController.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 27/10/2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialCards
import CoreLocation


class MyNoticesFragmentViewController: UIViewController, PageObservationFavorites {
    
    @IBOutlet weak var placeholderView: UIView!
    var nextPage, previousPage: String?
    var isWaiting: Bool = false
    var itemCount = 0
    var myFavoritesPageViewController : MyFavoritesPageViewController!
    func getParentPageViewController(parentRef: MyFavoritesPageViewController) {
        myFavoritesPageViewController = parentRef
    }
    var lastUserLocation: CLLocation!
    let manager = CLLocationManager()
    var refreshControl = UIRefreshControl()
    
    var noticeArray = Notices()
    @IBOutlet weak var noticeCollectionView: UICollectionView!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        myFavoritesPageViewController.segmentedControlP.selectedSegmentIndex = 1
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        manager.delegate = self
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            // Request when-in-use authorization initially
            manager.requestWhenInUseAuthorization()
            break
            
        case .restricted, .denied:
            // Disable location features
            //            disableMyLocationBasedFeatures()
            break
            
        case .authorizedWhenInUse:
            // Enable basic location features
            //            enableMyWhenInUseFeatures()
            manager.requestLocation()
            break
            
        case .authorizedAlways:
            // Enable any of your app's location features
            //            enableMyAlwaysFeatures()
            break
        @unknown default:
            print("unkowne case")
            break
        }
        
        let leftEdgePan: UIScreenEdgePanGestureRecognizer = UIScreenEdgePanGestureRecognizer(target:self, action:#selector(leftEdgeSwiped))
        leftEdgePan.edges = .left
        view.addGestureRecognizer(leftEdgePan)
        let rightEdgePan: UIScreenEdgePanGestureRecognizer = UIScreenEdgePanGestureRecognizer(target:self, action:#selector(rightEdgeSwiped))
        rightEdgePan.edges = .right
        view.addGestureRecognizer(rightEdgePan)
        
        noticeCollectionView.dataSource = self
        noticeCollectionView.delegate = self
        refreshControl.tintColor = UIColor.lapkiColors.lostOrange
        refreshControl.attributedTitle = nil
        refreshControl.addTarget(self, action: #selector(self.refresh(_ :)), for: UIControl.Event.valueChanged)
        noticeCollectionView.refreshControl = refreshControl
        NotificationCenter.default.addObserver(self, selector: #selector(refresh(_:)), name: NSNotification.Name(rawValue: "userUpdated"), object: nil)
        refresh(nil)
        // Do any additional setup after loading the view.
    }
    @objc func refresh(_ sender:AnyObject?) {
        guard MyStaticUser.id != 0 else {
            self.noticeArray = Notices()
            self.noticeCollectionView.reloadData()
            self.placeholderView.isHidden = false
            return
        }
        self.noticeCollectionView.refreshControl?.beginRefreshing()
        self.placeholderView.isHidden = true
        Network.getMyNotices(responseHandler: weakCompletion(of: self, onResult: MyNoticesFragmentViewController.refreshSuccess, onError: MyNoticesFragmentViewController.refreshError))
    }
    func refreshSuccess(_ page: PageResult){
        self.nextPage = page.next
        self.noticeArray = page.results
        self.noticeCollectionView.performBatchUpdates({
            self.noticeCollectionView.reloadSections(IndexSet(integer: 0))
        }, completion: { (success) in
            self.isWaiting = false
            self.noticeCollectionView.refreshControl?.endRefreshing()
        })
    }
    func refreshError(_ error: Error){
        self.noticeCollectionView.refreshControl?.endRefreshing()
        self.isWaiting = false
        self.handleResponseError(error)
    }
    @objc func leftEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            if myFavoritesPageViewController.currentIndex>0{
                myFavoritesPageViewController.changePageTo(index: myFavoritesPageViewController.currentIndex-1)
            }
            else if myFavoritesPageViewController.currentIndex==0{
                myFavoritesPageViewController.myPageViewController.changePageTo(index: 0)
            }
        }
    }
    @objc func rightEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            if myFavoritesPageViewController.currentIndex<myFavoritesPageViewController.pages.count{
            myFavoritesPageViewController.myPageViewController.changePageTo(index: myFavoritesPageViewController.myPageViewController.currentIndex+1)
            }
        }
    }
    func doPaging(){
        guard let nextPage = self.nextPage else {
            self.isWaiting = false
            return}
        Network.getNextPage(url: nextPage, responseHandler: weakCompletion(of: self, onResult: MyNoticesFragmentViewController.nextPageSuccess, onError: MyNoticesFragmentViewController.nextPageError))
    }
    func nextPageError(_ error: Error){
        self.handleResponseError(error)
        self.isWaiting = false
    }
    func nextPageSuccess(_ page: PageResult){
        self.nextPage = page.next
        self.noticeArray.append(contentsOf: page.results)
        self.noticeCollectionView.reloadData()
        self.isWaiting = false
    }
}
extension MyNoticesFragmentViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if self.noticeArray.count > 40 {
            if !(indexPath.row < self.noticeArray.count-20) && !isWaiting{
                isWaiting = true
                self.doPaging()
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let notice = noticeArray[indexPath.row]
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let noticeDetailViewController = storyBoard.instantiateViewController(withIdentifier: "NoticeDetailViewController") as! NoticeDetailViewController
        noticeDetailViewController.notice = notice
        noticeDetailViewController.myMainViewController = self.myFavoritesPageViewController.myPageViewController.mainViewController
        self.present(noticeDetailViewController, animated: true, completion: nil)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return noticeArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let notice = noticeArray[indexPath.row]
        if let noticeCell =  collectionView.dequeueReusableCell(withReuseIdentifier: "simpleNoticeCell", for: indexPath) as? SimpleNoticeCell{
            noticeCell.triangleIconImageView.isHidden = false
            noticeCell.triangleView.isHidden = false
            noticeCell.triangleView.tintColor = UIColor.lapkiColors.green
            noticeCell.triangleIconImageView.image = UIImage.init(named:"triangleLapka" )
            if notice.category == "Потеряшка"{
                if notice.price >= 1 {
                    
                    noticeCell.triangleView.tintColor = UIColor.lapkiColors.red
                    noticeCell.triangleIconImageView.image = UIImage.init(named:"ruble" )
                    noticeCell.triangleIconImageView.tintColor = UIColor.white
                }else{
                    
                    noticeCell.triangleView.tintColor = UIColor.lapkiColors.lostOrange
                    noticeCell.triangleIconImageView.image = UIImage.init(named:"triangleLapka" )
                    noticeCell.triangleIconImageView.tintColor = UIColor.white
                
                }
                
            }
            else if notice.category != "Находка"{
                noticeCell.triangleIconImageView.isHidden = true
                noticeCell.triangleView.isHidden = true
            }
            
            if notice.highlight == true{
                noticeCell.setGradientBorder(width: 10, colors: [UIColor.lapkiColors.gradientStart,UIColor.lapkiColors.gradientEnd],startPoint: CGPoint(x: 0.5, y: 0),endPoint: CGPoint(x: 0.5, y: 1))
            }
            else{
                noticeCell.removeGradientBorder()
            }
            noticeCell.notice = notice
            
            noticeCell.layer.shouldRasterize = true
            noticeCell.layer.rasterizationScale = UIScreen.main.scale
            noticeCell.parentView = self
            return noticeCell
        }
        return UICollectionViewCell()
    }
}
extension MyNoticesFragmentViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 6.0, left: 4, bottom: 0, right: 4)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let bounds = collectionView.bounds
        return CGSize(width: bounds.width/2-7.5, height: bounds.width/2+48.5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 7.5
    }
}
extension MyNoticesFragmentViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            lastUserLocation = location
            refresh(nil)
            print("Found user's location: \(location)")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Failed to find user's location: \(error.localizedDescription)")
    }
}



