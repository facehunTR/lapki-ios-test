//
//  AgePickerViewController.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 25/01/2019.
//  Copyright © 2019 MEGAKOM, OOO. All rights reserved.
//

import UIKit

class AgePickerViewController: UIViewController{
    
    var parentView: UIViewController!
    var outPutName: String?
    var buttonTitle: String?
    let yearCount: Int = 100
    let monthCount: Int = 12
    let weekCount: Int = 4
    var year: Int! = 0
    var month: Int! = 0
    var week: Int! = 0
    @IBOutlet weak var yearPicker: UIPickerView!
    @IBOutlet weak var monthPicker: UIPickerView!
    @IBOutlet weak var weekPicker: UIPickerView!
    
    @IBOutlet weak var acceptButton: MyButton!
    @IBAction func acceptClicked(_ sender: Any) {
        if let pvc = parentView as? AddNoticeViewController {
            pvc.ageYear = self.yearPicker.selectedRow(inComponent: 0)%self.yearCount
            pvc.ageMonth = self.monthPicker.selectedRow(inComponent: 0)%self.monthCount
            pvc.ageWeek = self.weekPicker.selectedRow(inComponent: 0)%self.weekCount
            pvc.ageFormat()
            self.dismiss(animated: true, completion: nil)
        }
        if let pvc = parentView as? EditNoticeViewController {
            pvc.ageYear = self.yearPicker.selectedRow(inComponent: 0)%self.yearCount
            pvc.ageMonth = self.monthPicker.selectedRow(inComponent: 0)%self.monthCount
            pvc.ageWeek = self.weekPicker.selectedRow(inComponent: 0)%self.weekCount
            pvc.ageFormat()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.yearPicker.delegate = self
        self.yearPicker.dataSource = self
        self.monthPicker.delegate = self
        self.monthPicker.dataSource = self
        self.weekPicker.delegate = self
        self.weekPicker.dataSource = self
        self.yearPicker.selectRow(yearCount*(500/yearCount)+year, inComponent: 0, animated: false)
        self.monthPicker.selectRow(monthCount*(500/monthCount)+month, inComponent: 0, animated: false)
        self.weekPicker.selectRow(weekCount*(500/weekCount)+week, inComponent: 0, animated: false)
        self.acceptButton.setTitle(buttonTitle ?? "Готово", for: .normal)
    }
}
extension AgePickerViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
       return 1000
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var pickerLabel = view as? UILabel;
        if (pickerLabel == nil)
        {
            pickerLabel = UILabel()
            
            pickerLabel!.font = UIFont.LapkiFonts.LatoRegular
            pickerLabel!.textAlignment = NSTextAlignment.center
        }
        if pickerView == self.yearPicker {
            pickerLabel!.text = "\(row%self.yearCount)"
        }
        if pickerView == self.monthPicker {
            pickerLabel!.text = "\(row%self.monthCount)"
        }
        if pickerView == self.weekPicker {
            pickerLabel!.text = "\(row%self.weekCount)"
        }
        
        return pickerLabel!;
    }
    
}

