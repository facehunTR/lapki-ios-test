//
//  ConfirmNoticeViewController.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 22/03/2019.
//  Copyright © 2019 MEGAKOM, OOO. All rights reserved.
//

import UIKit

class ConfirmNoticeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func confirmPressed(_sender:Any){
        self.dismiss(animated: true, completion: nil)
        
        
    }
    @IBAction func rulesPressed(_sender:Any){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let myWebViewController = storyBoard.instantiateViewController(withIdentifier: "MyWebViewController") as? MyWebViewController
        myWebViewController!.urlString = "https://www.lapki.com/rules-ad/"
        myWebViewController!.titleString = "Лицензионное соглашение"
        self.present(myWebViewController!, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
