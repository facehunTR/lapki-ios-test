//
//  MyFavoritesPageViewController.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 27/10/2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//

import UIKit

class MyFavoritesPageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate,PageObservation {
    
    
    
//    var myToolBar: UIToolbar!
    //var tabBar: UITabBar!
    var segmentedControlP: UISegmentedControl!
    var myPageViewController: MyPageViewController!
    var pages = [UIViewController?]()
    var currentIndex = 0
    func getParentPageViewController(parentRef: MyPageViewController) {
        self.myPageViewController = parentRef
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.dataSource = nil
        // Do any additional setup after loading the view.
        //segmentedControlP = UISegmentedControl.init()
        //segmentedControlP.isMomentary = false
        
        let favoriteFragment: UIViewController! = storyboard?.instantiateViewController(withIdentifier: "FavFragmentViewController")
        let favoriteFragmentWithParent = favoriteFragment as! PageObservationFavorites
        favoriteFragmentWithParent.getParentPageViewController(parentRef: self)
        let myNoticesFragment: UIViewController! = storyboard?.instantiateViewController(withIdentifier: "MyNoticesFragmentViewController")
        let myNoticesFragmentWithParent = myNoticesFragment as! PageObservationFavorites
        myNoticesFragmentWithParent.getParentPageViewController(parentRef: self)
        
        pages.append(favoriteFragment)
        pages.append(myNoticesFragment)
        
        setViewControllers([favoriteFragment], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        currentIndex = pages.firstIndex(of: viewController)!
        let previousIndex = abs((currentIndex - 1) % pages.count)
        return pages[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        currentIndex = pages.firstIndex(of: viewController)!
        let nextIndex = abs((currentIndex + 1) % pages.count)
        return pages[nextIndex]
    }
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return pages.count
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
    func changePageTo(index: NSInteger){
        let count = pages.count
        if index < count {
            if index > currentIndex {
                if let vc = pages[index] {
                    self.setViewControllers([vc], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: { (complete) -> Void in
                        self.currentIndex = index
                        self.segmentedControlP.selectedSegmentIndex =  index
                        self.segmentedControlP.sendActions(for: UIControl.Event.valueChanged)
                       
                        
                        
                        
                        //                        completion?()
                    })
                }
            } else if index < currentIndex {
                if let vc = pages[index] {
                    self.setViewControllers([vc], direction: UIPageViewController.NavigationDirection.reverse, animated: true, completion: { (complete) -> Void in
                        self.currentIndex = index
                        
                        self.segmentedControlP.selectedSegmentIndex =  index 
                        self.segmentedControlP.sendActions(for: UIControl.Event.valueChanged)
                        //valuechanged
                        
                        //                        completion?()
                    })
                }
            }
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    func refreshOthers(){
        var newPages = [UIViewController?]()
        if currentIndex != 0 {
            let favoriteFragment: UIViewController! = storyboard?.instantiateViewController(withIdentifier: "FavFragmentViewController")
            let favoriteFragmentWithParent = favoriteFragment as! PageObservationFavorites
            favoriteFragmentWithParent.getParentPageViewController(parentRef: self)
            newPages.append(favoriteFragment)
        }else{
            newPages.append(pages[currentIndex])
        }
        if currentIndex != 1 {
            let myNoticesFragment: UIViewController! = storyboard?.instantiateViewController(withIdentifier: "MyNoticesFragmentViewController")
            let myNoticesFragmentWithParent = myNoticesFragment as! PageObservationFavorites
            myNoticesFragmentWithParent.getParentPageViewController(parentRef: self)
            newPages.append(myNoticesFragment)
        }else{
            newPages.append(pages[currentIndex])
        }
       pages = newPages
    myPageViewController.refreshOthers()
    }
    func refreshOneNotice(notice: Notice){
        for page in self.pages {
            if let fav = page as? FavFragmentViewController {
                fav.refreshOneNotice(notice: notice)
                return
            }
        }
    }
}
