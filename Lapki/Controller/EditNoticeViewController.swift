//
//  EditNoticeViewController
//  Lapki
//
//  Created by Yuriy Yashchenko on 04/12/2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import MaterialComponents.MDCButton
import MaterialComponents.MaterialTextFields
import MaterialComponents.MDCMultilineTextField
import MaterialComponents.MaterialBottomSheet
import RangeSeekSlider
import Toast_Swift
import SearchTextField
import Kingfisher

import DKImagePickerController


class EditNoticeViewController: UIViewController, UICollectionViewDelegate{
    
    @IBOutlet weak var deleteButton: MyButton!
    @IBOutlet weak var photoCollectioView: PhotoCollectionView!
    
    let dataSource = PhotoCollectionDataSource()
    var defaultPhotos: [UIImage?] = [nil,nil,nil,nil,nil]
    var defaultNotice: EditNotice!
    var notice: EditNotice!
    func restoreNoticeSuccess(_ result: DetailedResponse){
        if (result.statusCode ?? 404) == 202 {
            NotificationCenter.default.post(name: NSNotification.Name("userUpdated"), object: nil)
            makeToast("Объявление успешно восстановленно и ожидает модерации", position: .top, completion: {success in
                self.dismiss(animated: true, completion: nil)
            })
        }else{
            makeToast("Произошла ошибка при восстановлении объявления. \(result.detail)", position: .top, completion: {success in
                self.deleteButton.isEnabled = true
            })
        }
       
    }
    func restoreNoticeFailure(_ error: Error){
        makeToast("Произошла ошибка при восстановлении объявления.", position: .top, completion: {success in
            self.deleteButton.isEnabled = true
        })
    }
    @IBAction func deleteButtonClicked(_ sender: Any) {
        self.deleteButton.isEnabled = false
        if self.notice.status!.contains("deleted") {
            Network.restoreNotice(id: self.notice.id!,status:self.notice.status!, responseHandler: weakCompletion(of: self, onResult: EditNoticeViewController.restoreNoticeSuccess, onError: EditNoticeViewController.restoreNoticeFailure))
        }else{
            Network.deleteNotice(id: self.notice.id!,status:self.notice.status!, responseHandler: weakCompletion(of: self, onResult: EditNoticeViewController.deleteNoticeSuccess, onError: EditNoticeViewController.deleteNoticeFailure))
        }
    }
    
    func deleteNoticeSuccess(_ result: DetailedResponse){
        if (result.statusCode ?? 404) == 202 {
            NotificationCenter.default.post(name: NSNotification.Name("userUpdated"), object: nil)
            makeToast("Объявление успешно отправленно в архив", position: .top, completion: {success in
                self.dismiss(animated: true, completion: nil)
            })
        }else{
            makeToast("Произошла ошибка при восстановлении объявления. \(result.detail)", position: .top, completion: {success in
                self.deleteButton.isEnabled = true
            })
        }
    }
    func deleteNoticeFailure(_ error: Error){
        makeToast("Произошла ошибка при архивировании объявления.", position: .top, completion: {success in
            self.deleteButton.isEnabled = true
        })
    }
    
    var pojo: Pojo!
    var parentView: UIViewController!
    var endOfList: CompletionHandler?
    var category: String?
    var type: String?
    var subType: String? {
        didSet {
            subTypeLabel.text = subType
        }
    }
    
    @IBAction func subTypeClicked(_ sender: Any) {
        //        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        //        let pickerViewController = storyBoard.instantiateViewController(withIdentifier: "PickerViewController") as! MyPickerViewBottomController
        //        pickerViewController.data = subTypes
        //        pickerViewController.selected = subType
        //        pickerViewController.parentView = self
        //        pickerViewController.outPutName = "subType"
        //        let bottomSheet = MDCBottomSheetController(contentViewController: pickerViewController)
        //        self.present(bottomSheet, animated: true, completion: nil)
    }
    
    @IBOutlet weak var subTypeLabel: UILabel!
    var subTypes: [String?] = [String]()
    
    @IBOutlet weak var bottomHeight: NSLayoutConstraint!
    
    @IBOutlet weak var navBar: MyNavigationBar!
    
    @IBOutlet weak var categoriesHeader: UILabel!
    @IBOutlet weak var categoriesBody: UILabel!
    @IBOutlet weak var categoriesIcon: UIImageView!
    @IBOutlet weak var categoriesCard: MDCCard!
    @IBOutlet weak var subTypeCard: MDCCard!
    @IBOutlet weak var subSubTF: SearchTextField!
    @IBOutlet weak var subSubTypeCard: MDCCard!
    @IBOutlet weak var priceTF: MDCTextField!
    @IBOutlet weak var priceIcon: UIImageView!
    @IBOutlet weak var priceCard: MDCCard!
    @IBOutlet var noticeStackView: UIStackView!
    @IBOutlet weak var noticeScrollView: UIScrollView!
    
    @IBOutlet weak var photoScrollView: UIScrollView!
    @IBOutlet var photoStackView: UIStackView!
    var allTextFieldsControllers = [MDCTextInputControllerOutlined]()
    var allTextFields = [MDCTextField]()
    
    var allTextAreaControllers = [MDCTextInputControllerOutlinedTextArea]()
    var allTextAreas = [MDCMultilineTextField]()
    @IBOutlet weak var photoCard: UIView!
    @IBOutlet weak var photoCard1: MDCCard!
    @IBOutlet weak var photoCard2: MDCCard!
    @IBOutlet weak var photoCard3: MDCCard!
    @IBOutlet weak var photoCard4: MDCCard!
    @IBOutlet weak var photoCard5: MDCCard!
    
    var photoCards = [MDCCard]()
    var photoButtons = [MDCButton]()
    @IBOutlet weak var photoImage1: UIImageView!
    @IBOutlet weak var photoImage2: UIImageView!
    @IBOutlet weak var photoImage3: UIImageView!
    @IBOutlet weak var photoImage4: UIImageView!
    @IBOutlet weak var photoImage5: UIImageView!
    
    
    var photoImages = [UIImageView]()
    var titleTF: MDCMultilineTextField?
    var descriptionTF: MDCMultilineTextField?
    @IBOutlet weak var titleCard: MDCCard!
    @IBOutlet weak var titleIcon: UIImageView!
    @IBOutlet weak var descriptionCard: MDCCard!
    @IBOutlet weak var descriptionIcon: UIImageView!
    
    @IBOutlet weak var genderLabel: UILabel!
    
    var genders = ["Укажите пол","Самец","Самка","Не определено"]
    @IBOutlet weak var genderCard: MDCCard!
    
    @IBAction func ageCardClicked(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let agePickerViewController = storyBoard.instantiateViewController(withIdentifier: "AgePickerViewController") as! AgePickerViewController
        agePickerViewController.parentView = self
        agePickerViewController.year = ageYear
        agePickerViewController.month = ageMonth
        agePickerViewController.week = ageWeek
        let bottomSheet = MDCBottomSheetController(contentViewController: agePickerViewController)
        self.present(bottomSheet, animated: true, completion: nil)
    }
    @IBOutlet weak var ageCard: MDCCard!
    var gender :String?{
        didSet {
            genderLabel.text = gender
        }
    }
    
    
    @IBAction func genderCardClicked(_ sender: Any) {
        //        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        //        let pickerViewController = storyBoard.instantiateViewController(withIdentifier: "PickerViewController") as! MyPickerViewBottomController
        //        pickerViewController.data = genders
        //        pickerViewController.selected = gender
        //        pickerViewController.parentView = self
        //        pickerViewController.outPutName = "gender"
        //        let bottomSheet = MDCBottomSheetController(contentViewController: pickerViewController)
        //        self.present(bottomSheet, animated: true, completion: nil)
    }
    
    let imagePickerController = DKImagePickerController()
    
    var ageYear: Int = 0
    var ageMonth: Int = 0
    var ageWeek: Int = 0
    
    @IBAction func categoriesCardClicked(_ sender: MDCCard) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let categoriesNavigationController = storyBoard.instantiateViewController(withIdentifier: "CategoriesNavigationController") as! CategoriesNavigationController
        categoriesNavigationController.parentView = self
        categoriesNavigationController.pojo = pojo
        categoriesNavigationController.checkedType = type
        categoriesNavigationController.checkedCategory = category
        self.present(categoriesNavigationController, animated: true, completion: nil)
    }
    
    
    @IBOutlet weak var ageLabel: UILabel!
    
    var photos: [UIImage?] = []
    
    
    @IBOutlet weak var addressCard: MDCCard!
    
    @IBOutlet weak var addressText: UILabel!
    var lat: String?
    var lng: String?
    var location_address: String?
    var first = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        photoCollectioView.dataSource = dataSource
        //dataSource.photoImages = defaultPhotos as! [UIImage]
        photoCollectioView.collectionViewSet()
        
        defaultNotice = notice
        if MyStaticUser.id != notice.owner {
            self.dismiss(animated: true, completion: nil)
        }
        initScrollStackView()
        //initPhotoScrollStackView()
        initToolbar()
        initCategoriesView()
        initSubTypeView()
        initPriceView()
        initTitleView()
        initDescriptionView()
        initAddressButton()
        if first {
            noticeInsert()
            first = false
        }
        if self.notice.status!.contains("deleted"){
            self.deleteButton.setTitle("Восстановить объявление из архива", for: .normal)
        }else{
            self.deleteButton.setTitle("Убрать объявление в архив", for: .normal)
        }
        viewReorder()
        
    }
    func noticeInsert() {
        //var iSuccess = [false,false,false,false,false]
        for i in 0...notice.images.count-1 {
            
            //self.photoImages[i].contentMode = .scaleAspectFit
            print(dataSource.photoImageViews, "\n images: \n",notice.images)
            if let url = URL(string: notice.images[i] ?? ""){
                
                KingfisherManager.shared.retrieveImage(with: url, options: [], progressBlock: nil) { result in
                    switch (result){
                    case .success(let value):
                        self.photos.append(value.image)
                        self.defaultPhotos = self.photos
                        self.dataSource.photoImages = self.defaultPhotos as! [UIImage]
                        self.photoCollectioView.reloadData()
                        break
                    case .failure(let error):
                        print(error.localizedDescription)
                        break
                    }
                    
                    KingfisherManager.shared.retrieveImage(with: url, options: [], progressBlock: nil) { result in
                        switch result {
                        case .success(let value):
                            self.photos.append(value.image)
                        case .failure(let error):
                            print("Error: \(error)")
                        }
                    }
                    
                }
                // self.photos.
            }
            
            
            
            titleTF!.text = notice.title
            descriptionTF!.text = notice.description
            if notice.price != nil && notice.price != -1 {
                priceTF!.text = "\(notice.price!)"
            }else{
                
                priceCard.isHidden = true
            }
            //        notice.category
            //        notice.type
            self.type = notice.type
            self.category = notice.category
            initCategoriesView()
            self.subType = notice.subtype
            self.gender = notice.gender ?? "Укажите пол"
            subSubTF!.text = notice.subsubtype
            //        notice.date
            //        notice.expirationDate
            
            //        notice.location
            addressText.text = notice.locationAddress
            location_address = notice.locationAddress
            lat = "\(notice.lat!)"
            lng = "\(notice.lng!)"
            self.ageYear = notice.oldYear ?? 0
            self.ageMonth = notice.oldMonth ?? 0
            self.ageWeek = notice.oldWeek ?? 0
            self.ageFormat()
        }
    }
    @objc func keyboardWillShow(notification: NSNotification){
        guard let userInfo = notification.userInfo else {return}
        let window = UIApplication.shared.keyWindow
        guard let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {return}
        if #available(iOS 11.0, *) {
            bottomHeight.constant = keyboardSize.cgRectValue.height - 44 - (window?.safeAreaInsets.bottom ?? 0)
        } else {
            bottomHeight.constant = keyboardSize.cgRectValue.height - 44
        }
    }
    @objc func keyboardWillHide(notification: NSNotification){
        bottomHeight.constant = 0
    }
    func initAddressButton() {
        let gestureAdd = UITapGestureRecognizer(target: self, action: #selector(addressCardClicked(_:)))
        gestureAdd.delegate = self
        if !(addressCard.gestureRecognizers?.contains(gestureAdd) ?? false){
            addressCard.addGestureRecognizer(gestureAdd)
        }
        
        addressText.text = location_address ?? "Укажите адрес"
    }
    @objc func addressCardClicked(_ sender: Any){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let addNoticeMapViewController = storyBoard.instantiateViewController(withIdentifier: "AddNoticeMapViewController") as! AddNoticeMapViewController
        addNoticeMapViewController.parentView = self
        self.present(addNoticeMapViewController, animated: true, completion: nil)
    }
    func viewReorder() {
        guard let category = category, let type = type else {
            return
        }
        allTextFieldsControllers[0].placeholderText = (category.contains("Потеря")) ?  "Вознаграждение, руб" : "Стоимость, руб"
        
        priceCard.isHidden = category.contains(oneOf: ["Вязка","Находка","Мероприятия"])
        
        genderCard.isHidden = category.contains(oneOf: ["Зоотовары","Мероприятия","Услуги","Для бизнеса"])
        
        ageCard.isHidden = category.contains(oneOf: ["Находка","Зоотовары","Мероприятия","Услуги","Для бизнеса"])
        
        subTypeCard.isHidden = (category.contains(oneOf: ["Зоотовары","Находка","Услуги","Для бизнеса","Мероприятия"]) || type.contains("Собаки"))
        
        subSubTypeCard.isHidden = !type.contains("Собаки")
    }
    
    func initPhotoScrollStackView(){
        self.photoScrollView.addSubview(self.photoStackView)
        self.photoStackView.translatesAutoresizingMaskIntoConstraints = false
        self.photoStackView.leadingAnchor.constraint(equalTo: self.photoScrollView.leadingAnchor).isActive = true
        self.photoStackView.trailingAnchor.constraint(equalTo: self.photoScrollView.trailingAnchor).isActive = true
        self.photoStackView.topAnchor.constraint(equalTo: self.photoScrollView.topAnchor).isActive = true
        self.photoStackView.bottomAnchor.constraint(equalTo: self.photoScrollView.bottomAnchor).isActive = true
        self.photoStackView.heightAnchor.constraint(equalTo: self.photoScrollView.heightAnchor).isActive = true
        photoCards = [photoCard1,photoCard2,photoCard3,photoCard4,photoCard5]
        photoImages = [photoImage1,photoImage2,photoImage3,photoImage4,photoImage5]
        
        for card in photoCards {
            let button = MDCButton(frame: CGRect(x: 70, y: 0, width: 30, height: 30))
            //            button.translatesAutoresizingMaskIntoConstraints = false
            button.contentEdgeInsets = .zero
            button.imageEdgeInsets = .zero
            button.setBackgroundColor(UIColor.white.withAlphaComponent(0))
            button.setImage(UIImage(named: "clear_filter_button"), for: .normal)
            button.contentMode = .scaleAspectFit
            button.inkColor = UIColor.inkColors.orange
            button.addTarget(self, action: #selector(imageClearClicked(sender:)), for: .touchUpInside)
            photoButtons.append(button)
            card.addSubview(button)
            button.isHidden = true
            card.heightAnchor.constraint(equalToConstant: 100).isActive = true
            card.widthAnchor.constraint(equalToConstant: 100).isActive = true
            button.trailingAnchor.constraint(equalTo: card.trailingAnchor).isActive = true
            button.topAnchor.constraint(equalTo: card.topAnchor).isActive = true
            
            let gesture = UITapGestureRecognizer(target: self, action: #selector(imageClicked(sender:)))
            card.addGestureRecognizer(gesture)
        }
    }
    @objc func imageClearClicked(sender: MDCButton){
        for i in 0...photoButtons.count-1{
            if sender == photoButtons[i]{
                photos[i] = nil
                UIView.transition(with: self.photoImages[i],
                                  duration: 0.75,
                                  options: .transitionCrossDissolve,
                                  animations: { self.photoImages[i].image = UIImage(named: "camera_orange_big")},
                                  completion: nil)
                photoImages[i].contentMode = .center
                photoButtons[i].isHidden = true
            }
        }
    }
    @objc func imageClicked(sender: UIGestureRecognizer){
        for index in 0...photoCards.count-1{
            if photoCards[index].gestureRecognizers?.contains(sender) ?? false   {
                imagePickerController.didSelectAssets = { (assets: [DKAsset]) in
                    if !assets.isEmpty{
                        for i in index...assets.count+index-1 {
                            assets[i-index].fetchOriginalImage(options: nil , completeBlock: { (image, info) in
                                self.photoImages[i%self.photoImages.count].contentMode = .scaleToFill
                                
                                UIView.transition(with: self.photoImages[i%self.photoImages.count],
                                                  duration: 0.75,
                                                  options: .transitionCrossDissolve,
                                                  animations: { self.photoImages[i%self.photoImages.count].image = image },
                                                  completion: nil)
                                self.photos[i%self.photoImages.count] = image
                                self.photoButtons[i%self.photoImages.count].isHidden = false
                            })
                        }
                    }
                }
                
                imagePickerController.sourceType = .both
                imagePickerController.maxSelectableCount = 5
                imagePickerController.showsEmptyAlbums = false
                imagePickerController.assetType = .allPhotos
                present(imagePickerController, animated: true, completion: nil)
                //                presentBottomSheet(imageView:photoImages[index])
            }
        }
    }
    func presentBottomSheet(imageView: UIImageView){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let bottomSheetController = storyBoard.instantiateViewController(withIdentifier: "BottomSheetController")
        let bottomSheet = MDCBottomSheetController(contentViewController: bottomSheetController)
        bottomSheet.preferredContentSize = CGSize(width: self.view.frame.size.width, height: 136)
        present(bottomSheet, animated: true, completion: nil)
    }
    func initTitleView() {
        if titleTF == nil {
            titleTF = MDCMultilineTextField()
            titleCard.addSubview(titleTF!)
            titleTF!.translatesAutoresizingMaskIntoConstraints = false
            titleTF!.textView!.autocapitalizationType = .none
            titleTF!.textView!.keyboardType = .default
            titleTF!.font = UIFont.LapkiFonts.LatoRegular
            
            let titleTFController = MDCTextInputControllerOutlinedTextArea(textInput: titleTF)
            allTextAreaControllers.append(titleTFController)
            allTextAreas.append(titleTF!)
            titleTF!.textView!.delegate = self
            titleTFController.floatingPlaceholderNormalColor = UIColor.gray
            titleTFController.floatingPlaceholderActiveColor = UIColor.lapkiColors.orange
            titleTFController.normalColor = UIColor.gray
            titleTFController.activeColor = UIColor.lapkiColors.orange
            titleTFController.placeholderText = "Заголовок объявления"
            titleTFController.helperText = ""
            titleTFController.characterCountMax = 50
            titleTFController.minimumLines = 1
            titleTFController.textInputFont = UIFont.LapkiFonts.LatoRegular
            titleTFController.inlinePlaceholderFont = UIFont.LapkiFonts.LatoRegular
            titleTFController.leadingUnderlineLabelFont = UIFont.LapkiFonts.LatoRegular
            titleTFController.trailingUnderlineLabelFont = UIFont.LapkiFonts.LatoRegular
            titleTF!.minimumLines = 1
            titleIcon.image = UIImage(named: "label_gray_icon")
            titleCard.inkView.inkColor = UIColor.black.withAlphaComponent(0.0)
            self.titleTF!.leadingAnchor.constraint(equalTo: self.titleIcon.trailingAnchor,constant: 0).isActive = true
            self.titleTF!.trailingAnchor.constraint(equalTo: self.titleCard.trailingAnchor,constant: -11).isActive = true
            self.titleTF!.topAnchor.constraint(equalTo: self.titleCard.topAnchor,constant: 8).isActive = true
            self.titleTF!.bottomAnchor.constraint(equalTo: self.titleCard.bottomAnchor).isActive = true
        }
    }
    func initDescriptionView() {
        if descriptionTF == nil {
            
            descriptionTF = MDCMultilineTextField()
            descriptionCard.addSubview(descriptionTF!)
            descriptionTF!.translatesAutoresizingMaskIntoConstraints = false
            descriptionTF!.textView!.autocapitalizationType = .none
            descriptionTF!.textView!.keyboardType = .default
            descriptionTF!.font = UIFont.LapkiFonts.LatoRegular
            let descriptionTFController = MDCTextInputControllerOutlinedTextArea(textInput: descriptionTF)
            allTextAreaControllers.append(descriptionTFController)
            allTextAreas.append(descriptionTF!)
            descriptionTF!.textView!.delegate = self
            descriptionTFController.floatingPlaceholderNormalColor = UIColor.gray
            descriptionTFController.floatingPlaceholderActiveColor = UIColor.lapkiColors.orange
            descriptionTFController.normalColor = UIColor.gray
            descriptionTFController.activeColor = UIColor.lapkiColors.orange
            descriptionTFController.placeholderText = "Описание объявления"
            descriptionTFController.helperText = ""
            descriptionTFController.characterCountMax = 3000
            descriptionTFController.minimumLines = 1
            descriptionTFController.textInputFont = UIFont.LapkiFonts.LatoRegular
            descriptionTFController.inlinePlaceholderFont = UIFont.LapkiFonts.LatoRegular
            descriptionTFController.leadingUnderlineLabelFont = UIFont.LapkiFonts.LatoRegular
            descriptionTFController.trailingUnderlineLabelFont = UIFont.LapkiFonts.LatoRegular
            descriptionTF!.minimumLines = 1
            descriptionIcon.image = UIImage(named: "description_gray_icon")
            descriptionCard.inkView.inkColor = UIColor.black.withAlphaComponent(0.0)
            self.descriptionTF!.leadingAnchor.constraint(equalTo: self.descriptionIcon.trailingAnchor,constant: 0).isActive = true
            self.descriptionTF!.trailingAnchor.constraint(equalTo: self.descriptionCard.trailingAnchor,constant: -11).isActive = true
            self.descriptionTF!.topAnchor.constraint(equalTo: self.descriptionCard.topAnchor,constant: 8).isActive = true
            self.descriptionTF!.bottomAnchor.constraint(equalTo: self.descriptionCard.bottomAnchor).isActive = true
        }
        
    }
    @objc func subTypeCardClicked(_ sender: AnyObject){
        
    }
    func initSubTypeView(){
        subType = nil
        subSubTF.backgroundColor = UIColor.white
        subSubTF.theme.bgColor = UIColor.white
        subSubTF.theme.font = UIFont.LapkiFonts.LatoRegular
        subSubTF.font = UIFont.LapkiFonts.LatoRegular
        if (type != nil && category != nil &&
            !category!.contains("Находка") && !category!.contains("Услуги")){
            if (pojo.subs![pojo.names.firstIndex(of: category)!]?.subs![(pojo.subs![pojo.names.firstIndex(of: category)!]?.names.firstIndex(of: type))!]?.subs) != nil{
                subTypes = (pojo.subs![pojo.names.firstIndex(of: category)!]?.subs![(pojo.subs![pojo.names.firstIndex(of: category)!]?.names.firstIndex(of: type))!]?.names)!
                subTypeCard.isHidden = false
                subType = subTypes[0]
                //                subTypeLabel.text = subTypes[0]
                subSubTF.text = ""
                subSubTypeCard.isHidden = true
            }
            else{
                subTypes = (pojo.subs![pojo.names.firstIndex(of: category)!]?.subs![(pojo.subs![pojo.names.firstIndex(of: category)!]?.names.firstIndex(of: type))!]?.names)!
                subTypeCard.isHidden = true
                subSubTypeCard.isHidden = false
                subSubTF.filterStrings(subTypes as! [String])
                subSubTF.placeholder = "Укажите породу"
            }
        }else{
            subTypeCard.isHidden = true
            subSubTypeCard.isHidden = true
        }
    }
    
    func initScrollStackView(){
        if !self.noticeScrollView.subviews.contains(self.noticeStackView){
            self.noticeScrollView.addSubview(self.noticeStackView)
            self.noticeStackView.translatesAutoresizingMaskIntoConstraints = false
            self.noticeStackView.leadingAnchor.constraint(equalTo: self.noticeScrollView.leadingAnchor).isActive = true
            self.noticeStackView.trailingAnchor.constraint(equalTo: self.noticeScrollView.trailingAnchor).isActive = true
            self.noticeStackView.topAnchor.constraint(equalTo: self.noticeScrollView.topAnchor).isActive = true
            self.noticeStackView.bottomAnchor.constraint(equalTo: self.noticeScrollView.bottomAnchor).isActive = true
            self.noticeStackView.widthAnchor.constraint(equalTo: self.noticeScrollView.widthAnchor).isActive = true
        }
    }
    func initPriceView() {
        if priceCard.inkView.alpha != 0.0 {
            priceTF.translatesAutoresizingMaskIntoConstraints = false
            priceTF.autocapitalizationType = .none
            priceTF.keyboardType = .numberPad
            priceTF.font = UIFont.LapkiFonts.LatoRegular
            let priceTFController = MDCTextInputControllerOutlined(textInput: priceTF)
            allTextFieldsControllers.append(priceTFController)
            priceTF.delegate = self
            priceTFController.floatingPlaceholderNormalColor = UIColor.gray
            priceTFController.floatingPlaceholderActiveColor = UIColor.lapkiColors.orange
            priceTFController.normalColor = UIColor.gray
            priceTFController.activeColor = UIColor.lapkiColors.orange
            priceTFController.placeholderText = "Стоимость, руб"
            priceTFController.helperText = ""
            priceTFController.textInputFont = UIFont.LapkiFonts.LatoRegular
            priceTFController.inlinePlaceholderFont = UIFont.LapkiFonts.LatoRegular
            priceTFController.leadingUnderlineLabelFont = UIFont.LapkiFonts.LatoRegular
            priceTFController.trailingUnderlineLabelFont = UIFont.LapkiFonts.LatoRegular
            priceTF.sizeToFit()
            
            priceIcon.image = UIImage(named: "price_gray_icon")
            priceCard.inkView.alpha = 0.0
            self.priceTF.trailingAnchor.constraint(equalTo: self.priceCard.trailingAnchor,constant: -11).isActive = true
            self.priceTF.topAnchor.constraint(equalTo: self.priceCard.topAnchor,constant: 8).isActive = true
            self.priceTF.bottomAnchor.constraint(equalTo: self.priceCard.bottomAnchor).isActive = true
        }else {
            for controller in allTextFieldsControllers {
                controller.textInput?.text = nil
            }
        }
        
        
    }
    
    func initCategoriesView(){
        categoriesCard.inkView.inkColor = UIColor.inkColors.orange
        //        categoriesCard.setBorderColor(UIColor.gray, for: .normal)
        //        categoriesCard.setBorderWidth(1.0, for: .normal)
        
        
        if type != nil && category != nil {
            categoriesHeader.text = pojo.names[0]?.replacingOccurrences(of: "Укажите", with: "Изменить")
            categoriesBody.text = category! + "/" + type!
            categoriesIcon.image = getPojoImage(name: type ?? category!)
        }else if category != nil {
            categoriesHeader.text = pojo.names[0]?.replacingOccurrences(of: "Укажите", with: "Изменить")
            categoriesBody.text = category!
            categoriesIcon.image = getPojoImage(name: type ?? category!)
        }else {
            categoriesHeader.text = pojo.names[0]
            categoriesIcon.image = UIImage(named: "categories_gray_icon")
            categoriesBody.text = "Выбраны все категории"
        }
    }
    func initToolbar(){
        if navBar.leftBarButtonItem == nil{
            navBar.leftBarButtonItem = MyBarButtonItem(frame: CGRect(x: 0, y: 0, width: 44, height: 44), imageName: "back_orange",action: #selector(backButtonClick(_:)))
            if notice.owner == MyStaticUser.id {
                navBar.rightBarButtonItem = MyBarButtonItem(frame: CGRect(x: 0, y: 0, width: 44, height: 44), title: "Сохранить",action: #selector(acceptButtonClick(_:)))
            }
        }
    }
    @objc func backButtonClick(_ sender: Any){
        self.dismiss(animated: true,completion: nil)
    }
    @objc func acceptButtonClick(_ sender: Any){
        
        navBar.rightBarButtonItem?.isEnabled = false
        var blank = false
        for controller in allTextAreaControllers {
            if (controller.textInput?.text?.isEmpty ?? true) && !(controller.textInput?.isHidden ?? true){
                controller.setErrorText("Обязательное поле", errorAccessibilityValue: nil)
                blank = true
            }
            if (controller.errorText != nil){
                let animation = CABasicAnimation(keyPath: "position")
                animation.duration = 0.07
                animation.repeatCount = 4
                animation.autoreverses = true
                animation.fromValue = NSValue(cgPoint: CGPoint(x: (controller.textInput?.center.x)! - 10, y: (controller.textInput?.center.y)!))
                animation.toValue = NSValue(cgPoint: CGPoint(x: (controller.textInput?.center.x)! + 10, y: (controller.textInput?.center.y)!))
                controller.textInput?.layer.add(animation, forKey: "position")
            }
        }
        if blank {
            navBar.rightBarButtonItem?.isEnabled = true
            return
        }
        var photoBlank = true
        
        if dataSource.photoImages.count != 0{
            photoBlank = false
        }
        
        
        if photoBlank{
            makeToast("Добавьте не менее одной фотографии", position: .top)
            navBar.rightBarButtonItem?.isEnabled = true
            return
        }
        if type?.isEmpty ?? true {
            makeToast("Указана неверная категория объявления", position: .top)
            
            navBar.rightBarButtonItem?.isEnabled = true
            return
        }
        notice.title = titleTF?.text ?? nil
        notice.description = descriptionTF?.text ?? nil
        
        if !(gender?.contains("Укажите") ?? true){
            notice.gender = gender
        }
        if !priceTF.isHidden {
            print(priceTF!.text ?? "Пусто")
            notice.price = Int(priceTF!.text ?? "")
        }
        if notice.price == nil{
            notice.price  = -1
        }
        notice.locationAddress = location_address
        if notice.locationAddress == nil {
            makeToast("Укажите адрес", position: .top)
            navBar.rightBarButtonItem?.isEnabled = true
            return
        }
        notice.lat = Double(lat!)
        notice.lng = Double(lng!)
        notice.oldWeek = self.ageWeek
        notice.oldMonth = self.ageMonth
        notice.oldYear = self.ageYear
        notice.category = category
        notice.type = type
        notice.subtype = subType
        notice.subsubtype = subSubTF?.text ?? nil
        var photosDifference = [String:UIImage?]()
        for i in 0...dataSource.photoImages.count-1 {
            if !(defaultPhotos.contains(dataSource.photoImages[i])){
                
                let key = "image\(i+1)"
                photosDifference[key] = dataSource.photoImages[i]
                
            }
        }
        if notice.category!.contains("Услуги") || notice.category!.contains("товары"){
            notice.gender = nil
        }
//        let parameters = notice.compare(defaultNotice)
//        Network.patchNotice(id:notice.id!,images: photosDifference, parameters: parameters, responseHandler: weakCompletion(of: self, onResult: EditNoticeViewController.patchNoticeSuccess, onError: EditNoticeViewController.patchNoticeFailure))
        
        navBar.rightBarButtonItem?.isEnabled = true
    }
    func patchNoticeSuccess(_ notice: Notice){
        // ToDo - ParentView notice edit delegate
        NotificationCenter.default.post(name: NSNotification.Name("userUpdated"), object: nil)
        makeToast("Объявление ожидает проверки", position: .top, completion: {success in
            self.dismiss(animated: true, completion: nil)
        })
    }
    func patchNoticeFailure(_ error: Error){
        self.navBar.rightBarButtonItem?.isEnabled = true
        makeToast("Произошла ошибка при изменении объявления", position: .top)
    }
    func ageFormat(){
        if (ageYear + ageMonth + ageWeek) == 0 {
            self.ageLabel.text = "Укажите возраст"
            return
        }
        var yearString, monthString, weekString: String?
        if ageYear > 0 {
            switch (ageYear % 10){
            case 1:
                yearString = " \(ageYear) год"
                break
            case 2,3,4:
                yearString = " \(ageYear) года"
                break
            default:
                yearString = " \(ageYear) лет"
                break
            }
        }
        if ageMonth > 0 {
            switch (ageMonth){
            case 1:
                monthString = " \(ageMonth) месяц"
                break
            case 2,3,4:
                monthString = " \(ageMonth) месяца"
                break
            default:
                monthString = " \(ageMonth) месяцев"
                break
            }
        }
        if ageWeek > 0 {
            if ageWeek == 1 {
                weekString = " \(ageWeek) неделя"
            }else{
                weekString = " \(ageWeek) недели"
            }
            if (ageYear > 0 || ageMonth > 0) {
                weekString = " и" + weekString!
            }
        }
        self.ageLabel.text = "Возраст: \(yearString ?? "")\(monthString ?? "")\(weekString ?? "")"
    }
}

extension EditNoticeViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        
        return false
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        if textField == priceTF{
            let maxLength = 8
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    
}
extension EditNoticeViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        print("print1")
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView == titleTF!.textView {
            allTextAreaControllers[allTextAreas.firstIndex(of: titleTF!)!].setErrorText(nil,errorAccessibilityValue: nil)
            let maxLength = 50
            let currentString: NSString = titleTF!.textView!.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: text) as NSString
            return newString.length <= maxLength
        }
        if textView == descriptionTF!.textView {
            allTextAreaControllers[allTextAreas.firstIndex(of: descriptionTF!)!].setErrorText(nil,errorAccessibilityValue: nil)
            let maxLength = 3000
            let currentString: NSString = descriptionTF!.textView!.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: text) as NSString
            return newString.length <= maxLength
        }
        return true
    }
}
extension EditNoticeViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}








