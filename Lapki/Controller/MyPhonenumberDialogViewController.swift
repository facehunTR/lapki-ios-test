//
//  MyPhonenumberDialogViewController.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 15/12/2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextFields

class MyPhonenumberDialogViewController: UIViewController {
    
    @IBOutlet weak var rightButton: MDCButton!
    @IBOutlet weak var leftButton: MDCButton!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var labelHeader: UILabel!
    
    
    
    var placeholderText: String?
    var helperText: String?
    var inputType: UIKeyboardType?
    var textFieldController: MDCTextInputControllerOutlined!
    var leftButtonTitle: String = "Отмена"
    var rightButtonTitle: String = "Принять"
    var leftButtonAction: CompletionHandler?
    var rightButtonAction: CompletionHandler?
    var textFieldString: String?
    var headerText: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        initTextField()
        initButtons()
        // Do any additional setup after loading the view.
    }
    func initButtons(){
        leftButton.setTitle(leftButtonTitle, for: .normal)
        leftButton.setTitleColor(UIColor.lapkiColors.orange, for: .normal)
        leftButton.imageEdgeInsets = UIEdgeInsets.zero
        leftButton.contentEdgeInsets = UIEdgeInsets.zero
        leftButton.backgroundColor = UIColor.white
        leftButton.addTarget(self, action: #selector(leftButtonClick(_:)), for: .touchUpInside)
        leftButton.inkColor = UIColor.inkColors.orange
        leftButton.tintColor = UIColor.lapkiColors.orange
        rightButton.setTitle(rightButtonTitle, for: .normal)
        rightButton.setTitleColor(UIColor.lapkiColors.orange, for: .normal)
        rightButton.imageEdgeInsets = UIEdgeInsets.zero
        rightButton.contentEdgeInsets = UIEdgeInsets.zero
        rightButton.backgroundColor = UIColor.white
        rightButton.addTarget(self, action: #selector(rightButtonClick(_:)), for: .touchUpInside)
        rightButton.inkColor = UIColor.inkColors.orange
        rightButton.tintColor = UIColor.lapkiColors.orange
    }
    @objc func leftButtonClick(_ sender: Any){
        if self.leftButtonAction == nil {
            self.dismiss(animated: true, completion: nil)
        }else{
            self.leftButtonAction!(true)
        }
    }
    @objc func rightButtonClick(_ sender: Any){
        if self.rightButtonAction == nil {
            self.dismiss(animated: true, completion: nil)
        }else{
            self.rightButtonAction?(true)
        }
    }
    func initTextField(){
        labelHeader.text = headerText ?? ""
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.autocapitalizationType = .none
        textField.keyboardType = inputType ?? .default
        textField.font = UIFont.LapkiFonts.LatoRegular
        textField.delegate = self
        textField.text = self.textFieldString ?? ""
        textField.tintColor = UIColor.lapkiColors.orange
        textField.placeholder = self.placeholderText ?? ""
        textField.font = UIFont.LapkiFonts.LatoRegular
        textField.sizeToFit()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension MyPhonenumberDialogViewController: UITextFieldDelegate {
    
}
