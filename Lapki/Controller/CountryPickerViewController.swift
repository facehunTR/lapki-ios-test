//
//  CountryPickerViewController.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 25/01/2019.
//  Copyright © 2019 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import FlagKit

enum modeEnum{
    case filter
    case phone
}
struct Country {
    let countryCode: String!
    let fullName: String!
    let flag: UIImage!
    let phoneMask: String!
    let phoneCode: String!
    let countryOnRu: String?
    
    init(phoneMask:String? = nil,
         phoneCode:String? = nil,
         countryOnRu:String? = nil) {
        
        if let val = Countries.init(rawValue:countryOnRu ?? ""){
            self.countryCode = "\(val)"
            self.fullName = val.origin
            self.flag = Flag(countryCode: self.countryCode)!.originalImage
        }
        else{
            self.countryCode = nil
            self.fullName = nil
            self.flag = nil
        }
        self.phoneMask = phoneMask
        self.phoneCode = phoneCode
        self.countryOnRu = countryOnRu
    }
}
protocol CountryPickerDelegate {
    func set(country: Country)
}
class CountryPickerViewController: UIViewController{
    var countryDelegate: CountryPickerDelegate!
    var data: [Country] = [Country]()
    var selected: String?
    var buttonTitle: String?
    var mode = modeEnum.phone
    
    @IBOutlet weak var acceptButton: MyButton!
    @IBOutlet weak var pickerView: UIPickerView!
    
    @IBAction func acceptClicked(_ sender: Any) {
                countryDelegate.set(country: data[pickerView.selectedRow(inComponent: 0)%data.count])
                self.dismiss(animated: true, completion: nil)
                
    }
   
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        data.append(Country(phoneMask: "+7 ([000]) [000] [00] [00]", phoneCode: "+7",countryOnRu: Countries.RU.origin))
        data.append(Country(phoneMask: "+7 ([000]) [000] [00] [00]", phoneCode: "+7",countryOnRu: Countries.KZ.rawValue))
        data.append(Country(phoneMask: "+373 [00] [000] [00] [00]", phoneCode: "+373",countryOnRu: Countries.MD.rawValue))
        data.append(Country(phoneMask: "+374 [00] [000] [00] [00]", phoneCode: "+374",countryOnRu: Countries.AM.rawValue))
        data.append(Country(phoneMask: "+375 [00] [000] [00] [00]", phoneCode: "+375",countryOnRu: Countries.BY.rawValue))
        data.append(Country(phoneMask: "+380 [00] [000] [00] [00]", phoneCode: "+380",countryOnRu: Countries.UA.rawValue))
        data.append(Country(phoneMask: "+992 [00] [000] [00] [00]", phoneCode: "+992", countryOnRu: Countries.TJ.rawValue ))
        data.append(Country(phoneMask: "+993 [00] [000] [00] [00]", phoneCode: "+993",countryOnRu: Countries.TM.rawValue))
        data.append(Country(phoneMask: "+994 [00] [000] [00] [00]", phoneCode: "+994",countryOnRu: Countries.AZ.rawValue))
        data.append(Country(phoneMask: "+995 [00] [000] [00] [00]", phoneCode: "+995",countryOnRu: Countries.GE.rawValue))
        data.append(Country(phoneMask: "+996 [00] [000] [00] [00]", phoneCode: "+996",countryOnRu: Countries.KG.rawValue))
        data.append(Country(phoneMask: "+998 [00] [000] [00] [00]", phoneCode: "+998",countryOnRu: Countries.UZ.rawValue))
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        self.acceptButton.setTitle(buttonTitle ?? "Готово", for: .normal)
        let index = data.firstIndex(where: {return $0.countryCode == selected})
        self.pickerView.selectRow((index ?? 0) + 50*data.count, inComponent: 0, animated: false)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch: UITouch? = touches.first
        
        if touch?.view == view {
            self.dismiss(animated: true) {
            }
        }
    }
}
extension CountryPickerViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 1200
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return nil
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
         let view = CountryView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 40))
        view.country = data[row%data.count]
        if mode == .filter{
        view.phoneNumberCode.isHidden = true
        }
        return view
    }
    
}
