//
//  SocialShareBottomActionView.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 18/03/2019.
//  Copyright © 2019 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import ok_ios_sdk

class SocialShareBottomActionView: UIView {

    @IBOutlet var contentView: SocialShareBottomActionView!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    enum social:Int{
        case vk = 0
        case ok = 1
        case insta = 2
        case fb = 3
        case twitter = 4
    }
    
    @IBAction func vkButtonTouchedUp(_ sender: UIButton) {
        
        let selectedSocial:social = SocialShareBottomActionView.social(rawValue: sender.tag)!
        switch selectedSocial {
        case .vk:
            openLink(appURL:"vk://vk.com/lapki_com" , safariURL: "https://vk.com/widget_community.php?act=a_subscribe_box&oid=-77150616&state=1&widget=4")
            break
        case .ok:
            openLink(appURL:"odnoklassniki://ok.ru/group/59335598735422", safariURL: "https://vk.com/away.php?utf=1&to=https://connect.ok.ru/dk?cmd=WidgetSubscriptionConfirm&st.cmd=WidgetSubscriptionConfirm&st.fid=__okGroup2&st.settings=%7B%22width%22%3A360%2C%22height%22%3A285%7D&st.targetid=59335598735422&st._ns=ok_join&st.hosterId=47126&st._aid=ExternalGroupWidget_joinConfirm&st.isgroup=on&st._wt=0&st.hoster=https%3A%2F%2Fapiok.ru%2Fext%2Fgroup")
            break
        case .insta:
            openLink(appURL: "instagram://instagram.com", safariURL: "https://www.instagram.com/lapki_com/")
            break
        case .fb:
            openLink(appURL: "fb://page/?id=285630462292248", safariURL: "https://www.facebook.com/lapkicom")
            break
        case .twitter:
            openLink(appURL: "twitter://user?id=1072106661110824960", safariURL: "https://twitter.com/Lapki_com")
            break
        }
    
}
    func openLink(appURL:String,safariURL:String){
        
        let app = URL(string:appURL)
        let safari = URL(string:safariURL)
        
        if UIApplication.shared.canOpenURL(app!){
           // UIApplication.shared.openURL(app!)
            UIApplication.shared.open(app!, completionHandler: nil)
        } else {
            UIApplication.shared.open(safari!, completionHandler: nil)
        }
        let getBonus = NSNotification.Name(rawValue: "getBonus")
       NotificationCenter.default.post(name: getBonus, object: nil)
        NotificationCenter.default.removeObserver(self, name: getBonus, object: nil)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    private func commonInit(){
        Bundle.main.loadNibNamed("SocialShareBottomActionView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        contentView.layer.cornerRadius = 8
    }
}
