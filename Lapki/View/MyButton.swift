//
//  MyButton.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 22/01/2019.
//  Copyright © 2019 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import MaterialComponents.MDCButton

@IBDesignable class MyButton: MDCButton {
    
    enum MyButtonStyle: Int {
        case Orange,White,WhiteBorderless,Custom
    }
    var style: MyButtonStyle!{
        didSet{
            switch self.style! {
            case .Orange:
                
                self.imageEdgeInsets = .zero
                self.contentEdgeInsets = .zero
                self.setBackgroundColor(UIColor.lapkiColors.orange)
                self.setTitleFont(UIFont.LapkiFonts.LatoSemibold, for: .normal)
                self.setTitleColor(UIColor.white, for: .normal)
                self.setImageTintColor(UIColor.white, for: .normal)
                self.setBorderColor(UIColor.lapkiColors.orange, for: .normal)
                self.setBorderWidth(0, for: .normal)
                self.isUppercaseTitle = false
                self.inkColor = UIColor.inkColors.white
                self.layer.cornerRadius = 8.0
                break
            case .White:
                
                self.imageEdgeInsets = .zero
                self.contentEdgeInsets = .zero
                self.setTitleFont(UIFont(name: "Lato-regular", size: 17), for: .normal)
                self.setBorderWidth(1.0, for: .normal)
                self.setBorderColor(UIColor.lapkiColors.orange, for: .normal)
                self.setBackgroundColor(UIColor.white)
                self.setTitleColor(UIColor.lapkiColors.orange, for: .normal)
                self.setImageTintColor(UIColor.lapkiColors.orange, for: .normal)
                self.inkColor = UIColor.inkColors.orange
                self.isUppercaseTitle = false
                self.layer.cornerRadius = 8.0
                break
            case .WhiteBorderless:

                self.imageEdgeInsets = .zero
                self.contentEdgeInsets = .zero
                self.setTitleFont(UIFont(name: "Lato-regular", size: 17), for: .normal)
                self.setBackgroundColor(UIColor.white)
                self.setTitleColor(UIColor.lapkiColors.orange, for: .normal)
                self.setImageTintColor(UIColor.lapkiColors.orange, for: .normal)
                self.inkColor = UIColor.inkColors.orange
                self.isUppercaseTitle = false
                self.layer.cornerRadius = 8.0
                break
            case .Custom:
                self.imageView?.contentMode = .scaleAspectFit
                
                self.isUppercaseTitle = false
                break
            }
        }
    }
//    override func awakeFromNib() {
//        super.awakeFromNib()
//    }
    @IBInspectable var styleAdapter:Int {
        get {
            return self.style.rawValue
        }
        set(styleIndex) {
            self.style = MyButtonStyle(rawValue: styleIndex) ?? .Custom
        }
    }
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
}

