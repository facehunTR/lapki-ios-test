//
//  MyNavigationBar.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 15/11/2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import MaterialComponents.MDCNavigationBar
import MaterialComponents.MDCShadowLayer

@IBDesignable
class MyNavigationBar: MDCNavigationBar {

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.allowAnyTitleFontSize = true
        self.titleFont = UIFont.LapkiFonts.LatoRegular
//        self.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Lato-Regular", size: 10.0)]
        self.titleTextColor = UIColor.lapkiColors.grayText
        self.clipsToBounds = false
        self.shadowLayer.shadowColor = UIColor.black.cgColor
        self.shadowLayer.masksToBounds = false
        self.layer.masksToBounds = false
        self.layoutIfNeeded()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.allowAnyTitleFontSize = true
        self.titleFont = UIFont.LapkiFonts.LatoRegular
        //        self.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Lato-Regular", size: 10.0)]
        self.titleTextColor = UIColor.lapkiColors.grayText
        self.clipsToBounds = false
        self.shadowLayer.shadowColor = UIColor.black.cgColor
        self.shadowLayer.masksToBounds = false
        self.layer.masksToBounds = false
        self.layoutIfNeeded()
    }
    
        override class var layerClass: AnyClass {
            return MDCShadowLayer.self
        }
    
    var shadowLayer: MDCShadowLayer {
        return self.layer as! MDCShadowLayer
    }
    
    func setDefaultElevation() {
        self.shadowLayer.elevation = .appBar
    }
    
}

