//
//  MySearchBar.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 21/11/2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//

import UIKit

class MySearchBar: UISearchBar {
    
    override func setShowsCancelButton(_ showsCancelButton: Bool, animated: Bool) {
        //Ничего не делает, специально
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
class MySearchController: UISearchController {
    let mySearchBar = MySearchBar()
    override var searchBar: UISearchBar { return mySearchBar}
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override init(searchResultsController: UIViewController?) {
        super.init(searchResultsController: searchResultsController)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
