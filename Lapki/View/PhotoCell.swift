//
//  PhotoCell.swift
//  Lapki
//
//  Created by Denis Dolgopolov on 06/03/2019.
//  Copyright © 2019 MEGAKOM, OOO. All rights reserved.
//
import UIKit
import MaterialComponents.MDCCardCollectionCell

class PhotoCell: MDCCardCollectionCell{
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var photoButton: UIButton!
    
    @IBAction func clearImage(_ sender: UIButton) {
        UIView.transition(with:self.photo,
                          duration:0.75 ,
                          options: .transitionCrossDissolve,
                          animations:{self.photo.image = UIImage(named:"camera_orange_big")},
                          completion:{_ in
                            self.photo.clipsToBounds = false
        })
        self.photo.contentMode = .center
        sender.isHidden = true
    }
}
