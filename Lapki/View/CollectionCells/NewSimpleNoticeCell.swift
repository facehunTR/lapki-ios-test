//
//  NewSimpleNoticeCell.swift
//  Lapki
//
//  Created by Denis Dolgopolov on 01/03/2019.
//  Copyright © 2019 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import MaterialComponents.MDCCardCollectionCell
import CoreLocation
import Kingfisher

class NewSimpleNoticeCell: MDCCardCollectionCell {
    @IBOutlet weak var favButtonContainer: UIView!
    @IBOutlet weak var favButtonOutline: UIImageView!
    var processor: ImageProcessor!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        layer.masksToBounds = false
        isSelectable = false
        cornerRadius = 8
        setShadowElevation(ShadowElevation(rawValue: 6.0), for: .normal)
        setShadowElevation(ShadowElevation(rawValue: 0), for: .highlighted)
        setShadowElevation(ShadowElevation(rawValue: 0), for: .selected)
        inkView.inkColor = UIColor.inkColors.orange
        contentView.layoutMargins = .zero
        
        commonInit()
    }
    @IBOutlet var cellView: UICollectionViewCell!
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        commonInit()
    }
    private func commonInit(){
        Bundle.main.loadNibNamed("NewSimpleNoticeCell", owner: self, options: nil)
        content.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        content.translatesAutoresizingMaskIntoConstraints = true
        content.frame = self.contentView.bounds
        self.contentView.addSubview(content)
        setContentParameters()
    }
    func setContentParameters(){
        //FavButton
        favButtonContainer.layer.zPosition = CGFloat(Float.greatestFiniteMagnitude)
        favButtonOutline.alpha = 0.2
        //Distance
        distanceContainer.layer.cornerRadius = 4
        distanceLabel.textColor = UIColor.black
        distanceContainer.layer.zPosition = CGFloat(Float.greatestFiniteMagnitude)
        //ImageView
        imageView.layer.cornerRadius = 6
        imageView.clipsToBounds = true
        imageView.contentMode = ContentMode.scaleAspectFit
        processor = ResizingImageProcessor(referenceSize: CGSize(width: imageView.bounds.width*UIScreen.main.scale,height: imageView.bounds.height*UIScreen.main.scale), mode: .aspectFill).append(another: CroppingImageProcessor(size: CGSize(width: imageView.bounds.height*UIScreen.main.scale,height: imageView.bounds.height*UIScreen.main.scale), anchor: CGPoint(x: 0.5,y: 0.5)))
        imageView.kf.indicatorType = .activity
        let indicator = imageView.kf.indicator?.view as? UIActivityIndicatorView
        indicator?.color = UIColor.lapkiColors.lostOrange
        //Corner
        triangleView.layer.zPosition = CGFloat(Float.greatestFiniteMagnitude)
        triangleIconImageView.layer.zPosition = CGFloat(Float.greatestFiniteMagnitude)
    }
    
    @IBOutlet var content: UIView!
    @IBOutlet weak var triangleIconImageView: UIImageView!
    @IBOutlet weak var triangleView: UIImageView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var favButton: UIButton!
    @IBOutlet weak var distanceContainer: UIView!
    @IBOutlet weak var distanceLabel: UILabel!
    var location: CLLocation?
    var parentView: UIViewController?
    let collectionId: NSInteger? = 0
    @IBAction func favButtonClicked(_ sender: MDCButton) {
        favButton.isUserInteractionEnabled = false
        guard let id = self.notice?.id, self.notice?.id != 0 else{
            self.favButton.isUserInteractionEnabled = true
            return
        }
//        if let vc = parentView as? HomeViewController{
//            if vc.noticeCollectionView.selectedCellIndex != vc.noticeCollectionView.indexPath(for: self){
//                vc.noticeCollectionView.selectedCellIndex = vc.noticeCollectionView.indexPath(for: self) ?? IndexPath(row: -1, section: 0)
//            }else{
//                vc.noticeCollectionView.selectedCellIndex = IndexPath(row: -1, section: 0)
//            }
//            print("azazazaaa \(vc.noticeCollectionView.selectedCellIndex)")
//        }
        UIView.transition(with:self.favButton,
                          duration:0.75 ,
                          options: .transitionCrossDissolve,
                          animations:{
                            self.favButton.tintColor = ((self.favButton.tintColor == UIColor.lapkiColors.orange) ? UIColor.white : UIColor.lapkiColors.orange)},
                            completion:nil)
        Network.patchFavorites(id:id,responseHandler: weakCompletion(of: self, onResult: NewSimpleNoticeCell.favoritesResponse, onError: NewSimpleNoticeCell.handleError))
    }
    func favoritesResponse(_ detailedResponse: DetailedResponse){
        guard let id = self.notice?.id, self.notice?.id != 0 else{
            return
        }
        if MyStaticUser.favorites.contains(id) {
            MyStaticUser.favorites.removeAll(where:{return $0 == id})
        }else{
            MyStaticUser.favorites.append(id)
        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "favClicked"), object: nil, userInfo: ["notice":self.notice as Any])
        self.favButton.isUserInteractionEnabled = true
    }
    
    func handleError(_ error: Error){
        parentView?.handleResponseError(error)
    }
    var notice: Notice?{
        didSet{
            switch notice!.price {
            case nil:
                priceLabel.text = "Бесплатно"
                titleLabel.text = notice!.title
                break
            case -1: priceLabel.text = notice!.title
            titleLabel.text = notice!.description
                break
            case 0:
                if notice!.category.contains("Потеря") || notice!.category.contains("Находка"){
                    priceLabel.text = notice!.title
                    titleLabel.text = notice!.description
                }else{
                    priceLabel.text = "Бесплатно"
                    titleLabel.text = notice!.title
                }
                break
            default:
                priceLabel.text = "\(notice!.price) Р"
                titleLabel.text = notice!.title
            }
            
            setImageUrl(urlString: notice!.thumbnail ?? notice!.image1)
            
            favButtonContainer.isHidden = (MyStaticUser.id == 0 || MyStaticUser.id == notice!.owner)
            favButton.tintColor = ((MyStaticUser.favorites.contains(notice!.id)) ? UIColor.lapkiColors.orange : UIColor.white)
            self.bringSubviewToFront(favButtonContainer)
            
            //Дефолтная альфа
            imageView.alpha = ((notice?.status == "active") ? 1.0 : 0.5)
            distanceContainer.alpha = 1.0
            distanceLabel.alpha = 1.0
            
            switch notice!.status {
            case "deleted":
                distanceContainer.backgroundColor = UIColor.white
                distanceLabel.text = "В архиве"
                break
            case "active":
                distanceContainer.backgroundColor = UIColor.white
                setDistanceString()
                break
            case "rejected":
                distanceContainer.backgroundColor = UIColor.red
                distanceLabel.text = "Не одобрено"
                break
            case "moderation":
                distanceContainer.backgroundColor = UIColor.yellow
                distanceLabel.text = "Ждёт одобрения"
                break
            default:
                break
            }
            self.bringSubviewToFront(distanceContainer)
        }
    }
    func setDistanceString(){
        var distance: Int
        if notice?.distance != 0 {
            distance = notice!.distance ?? 0
        }else{
            if notice!.location == nil{
                distance = 0
            }else{
                distance = Int(location?.distance(from: notice!.location!) ?? 0)
            }
        }
        if distance == 0{
            distanceContainer.alpha = 0.0
            distanceLabel.alpha = 0
        }else if distance < 100 && distance != 0 {
            distanceLabel.text = "100 м"
        } else if distance < 250 {
            distanceLabel.text = "250 м"
        } else if distance < 500 {
            distanceLabel.text = "500 м"
        }else if distance < 1000 {
            distanceLabel.text = "1 км"
        }else {
            distanceLabel.text = "\(Int(distance/1000)) км"
        }
    }
    func setImageUrl(urlString: String?){
        guard var url = URL(string: urlString?.replacingOccurrences(of: "http:", with: "https:", options: .literal, range: nil) ?? "") else{
            return
        }
        if !url.absoluteString.starts(with: "htt"){
            url = URL(string: ("https://api.lapki.com" + (url.absoluteString)))!
        }
        self.imageView.kf.setImage(with: url, placeholder: nil, options: [.processor(processor)],completionHandler:{result in
            self.bringSubviewToFront(self.favButtonOutline)
            self.bringSubviewToFront(self.favButton)
            self.bringSubviewToFront(self.distanceContainer)
            self.bringSubviewToFront(self.triangleIconImageView)
            self.bringSubviewToFront(self.triangleView)
        })
    }
}

