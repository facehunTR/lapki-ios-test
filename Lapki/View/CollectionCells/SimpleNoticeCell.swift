//
//  SimpleNoticeCell
//  Lapki
//
//  Created by Yuriy Yashchenko on 24/10/2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import Kingfisher
import MaterialComponents.MaterialCards
import MaterialComponents.MDCButton
import CoreLocation

class SimpleNoticeCell: MDCCardCollectionCell {
    
    @IBOutlet weak var triangleIconImageView: UIImageView!
    @IBOutlet weak var triangleView: UIImageView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var favButton: MDCButton!
    @IBOutlet weak var distanceContainer: UIView!
    @IBOutlet weak var distanceLabel: UILabel!
    var location: CLLocation?
    var parentView: UIViewController?
    var btnTapAction : (()->())?
    
    let collectionId: NSInteger? = 0
    @IBAction func favButtonClicked(_ sender: MDCButton) {
        favButton.isUserInteractionEnabled = false
        let id = self.notice?.id ?? 0
        if id == 0 {return}
        Network.patchFavorites(id:id,responseHandler: weakCompletion(of: self, onResult: SimpleNoticeCell.favoritesResponse, onError: SimpleNoticeCell.handleError))
    }
    
    func favoritesResponse(_ detailedResponse: DetailedResponse){
        guard let id = self.notice?.id, self.notice?.id != 0 else{
            return
        }
        if MyStaticUser.favorites.contains(id) {
            MyStaticUser.favorites.removeAll(where:{return $0 == id})
        }else{
            MyStaticUser.favorites.append(id)
        }
        self.btnTapAction?()
        self.favButton.isUserInteractionEnabled = true
    }
    
    func handleError(_ error: Error){
        parentView?.handleResponseError(error)
    }
    
    var notice: Notice!{
        didSet{
            contentView.layoutMargins = .zero
            switch notice.price {
            case -1:
                priceLabel.text = notice.title
                titleLabel.text = notice.description
                break
            case 0:
                if notice.category.contains(oneOf: ["Потеря","Находка"]){
                    priceLabel.text = notice.title
                    titleLabel.text = notice.description
                }else{
                    priceLabel.text = "Бесплатно"
                    titleLabel.text = notice.title
                }
                break
            default:
                priceLabel.text = "\(notice.price) Р"
                titleLabel.text = notice.title
            }
            if let image = notice.thumbnail ?? notice.image1{
                setImageUrl(urlString: image)
            }
            favButton.setImage(UIImage(named: "heart")?.withRenderingMode(.alwaysTemplate), for: UIControl.State.normal)
            favButton.isHidden = (MyStaticUser.id == 0 || MyStaticUser.id == notice.owner)
            favButton.tintColor = (MyStaticUser.favorites.contains(notice.id)) ? UIColor.lapkiColors.orange : UIColor.white
            self.layer.masksToBounds = false
            self.isSelectable = false
            self.cornerRadius = 8
            self.setShadowElevation(ShadowElevation(rawValue: 6.0), for: .normal)
            self.setShadowElevation(ShadowElevation(rawValue: 0), for: .highlighted)
            self.setShadowElevation(ShadowElevation(rawValue: 0), for: .selected)
            self.setShadowColor(UIColor.black, for: .normal)
            self.setShadowColor(UIColor.black, for: .highlighted)
            self.setShadowColor(UIColor.black, for: .selected)
            self.inkView.inkColor = UIColor.inkColors.orange
            self.bringSubviewToFront(favButton)
            favButton.contentEdgeInsets = .zero
            favButton.imageEdgeInsets = .zero
            favButton.layer.zPosition = CGFloat(Float.greatestFiniteMagnitude)
            favButton.inkColor = UIColor.inkColors.orange
            favButton.inkStyle = .unbounded
            favButton.inkMaxRippleRadius = favButton.bounds.size.width / 2
            distanceContainer.layer.cornerRadius = 4
            imageView.alpha = (notice.status.contains("active")) ? 1.0 : 0.5
            distanceContainer.alpha = 1.0
            distanceLabel.alpha = 1.0
            switch notice.status {
            case "deleted":
                distanceContainer.backgroundColor = UIColor.white
                distanceLabel.text = "В архиве"
                break
            case "active":
                let distance = (notice.location == nil) ? 0 : (location?.distance(from: notice.location!) ?? 0)
                setDistance(Int(distance))
                break
            case "rejected":
                distanceContainer.backgroundColor = UIColor.red
                distanceLabel.text = "Не одобрено"
                break
            case "moderation":
                distanceContainer.backgroundColor = UIColor.yellow
                distanceLabel.text = "Ждёт одобрения"
                break
            default:
                break
            }
            distanceLabel.textColor = UIColor.black
        }
    }
    func setDistance(_ distance: Int?){
        guard var distance = distance else {
            distanceContainer.alpha = 0.0
            distanceLabel.alpha = 0
            return}
        if notice?.distance != 0 {
            distance = notice!.distance ?? 0
        }
        switch distance {
        case 0:
            distanceContainer.alpha = 0.0
            distanceLabel.alpha = 0
        case 1...100:
            distanceLabel.text = "100 м"
        case 101...250:
            distanceLabel.text = "250 м"
        case 251...500:
            distanceLabel.text = "500 м"
        case 501...1000:
            distanceLabel.text = "1 км"
        default:
            distanceLabel.text = "\(Int(distance/1000)) км"
        }
    }
    func setImageUrl(urlString: String){
        guard var url = URL(string: urlString.replacingOccurrences(of: "http:", with: "https:", options: .literal, range: nil)) else {return}
        if !url.absoluteString.starts(with: "htt"){
            url = URL(string: ("https://api.lapki.com" + (url.absoluteString)))!
        }
        imageView.layer.cornerRadius = 6
        imageView.clipsToBounds = true
        imageView.contentMode = ContentMode.scaleAspectFit
        let processor = ResizingImageProcessor(referenceSize: CGSize(width: imageView.bounds.width*UIScreen.main.scale,height: imageView.bounds.height*UIScreen.main.scale), mode: .aspectFill).append(another: CroppingImageProcessor(size: CGSize(width: imageView.bounds.height*UIScreen.main.scale,height: imageView.bounds.height*UIScreen.main.scale), anchor: CGPoint(x: 0.5,y: 0.5)))
        self.imageView.kf.indicatorType = .activity
        let indicator = self.imageView.kf.indicator?.view as? UIActivityIndicatorView
        indicator?.color = UIColor.lapkiColors.lostOrange
        self.imageView.kf.setImage(with: url, placeholder: nil, options: [.processor(processor)],completionHandler:{
            _ in
            self.bringSubviewToFront(self.favButton)
        })
    }
}

