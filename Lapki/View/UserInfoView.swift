//
//  UserInfoView.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 23/11/2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import Kingfisher
import MaterialComponents.MDCButton

@IBDesignable
class UserInfoView: UIView {
    
    @IBOutlet weak var UserImage: UIImageView!
    @IBOutlet weak var UserFirstLastName: UILabel!
    
    @IBOutlet weak var SignOutButton: MDCButton!
    @IBOutlet weak var UserFirstLastNameEditButton: MDCButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        UserImage.layer.borderColor = UIColor.lapkiColors.lightGray.cgColor
        UserImage.layer.borderWidth = 1.0
        
        SignOutButton.backgroundColor = UIColor.white
        SignOutButton.setBorderWidth(1.0, for: .normal)
        SignOutButton.setBorderColor(UIColor.lapkiColors.orange, for: .normal)
        SignOutButton.inkColor = UIColor.inkColors.orange
        SignOutButton.tintColor = UIColor.lapkiColors.orange
        
        
        UserFirstLastNameEditButton.setImage(UIImage(named: "home")?.withRenderingMode(.alwaysTemplate), for: UIControl.State.normal)
        UserFirstLastNameEditButton.setImageTintColor(UIColor.lapkiColors.orange, for: UIControl.State.normal)
        UserFirstLastNameEditButton.contentEdgeInsets = .zero
        UserFirstLastNameEditButton.imageEdgeInsets = .zero
        UserFirstLastNameEditButton.tintColor = UIColor.lapkiColors.orange
        UserFirstLastNameEditButton.inkColor = UIColor.inkColors.orange
        
    }
    
    
        
    
    
    var user: MyStaticUser? {
        didSet{
            let processor = ResizingImageProcessor(referenceSize: CGSize(width: 100,height: 100), mode: .aspectFill).append(another: CroppingImageProcessor(size: CGSize(width: 100,height: 100), anchor: CGPoint(x: 0.5,y: 0.5))).append(another: RoundCornerImageProcessor(cornerRadius: 50))
            var urlAvatar: Resource
            if !MyStaticUser.avatar.isEmpty {
                UserImage.contentMode = ContentMode.scaleAspectFit
                urlAvatar = URL(string: MyStaticUser.avatar) ?? URL(string: "")!
                UserImage.kf.setImage(with: urlAvatar, placeholder: nil, options: [.processor(processor)])
            }else if !MyStaticUser.urlAvatar.isEmpty {
                UserImage.contentMode = ContentMode.scaleAspectFit
                urlAvatar = URL(string: MyStaticUser.urlAvatar) ?? URL(string: "")!
                UserImage.kf.setImage(with: urlAvatar, placeholder: nil, options: [.processor(processor)])
            }else {
                UserImage.contentMode = ContentMode.center
                UserImage.image = UIImage(named: "camera_orange_big")
            }
            UserFirstLastName.text = MyStaticUser.firstName + " " + MyStaticUser.lastName
            if MyStaticUser.id == 0{
                SignOutButton.setTitle("ВОЙТИ", for: .normal)
                SignOutButton.titleLabel?.sizeToFit()
            }else{
                
                SignOutButton.setTitle("ВЫЙТИ", for: .normal)
                SignOutButton.titleLabel?.sizeToFit()
            }
        }
        
    }
}
