//
//  CountryView.swift
//  Lapki
//
//  Created by Denis Dolgopolov on 20/03/2019.
//  Copyright © 2019 MEGAKOM, OOO. All rights reserved.
//

import UIKit

class CountryView: UIView {

    @IBOutlet var content: UIView!
    @IBOutlet weak var flagImage: UIImageView!
    @IBOutlet weak var phoneNumberCode: UILabel!
    @IBOutlet weak var fullNameLabel: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    var country: Country!{
        didSet{
            self.flagImage.image = country.flag
            self.phoneNumberCode.text = country.phoneCode
            self.fullNameLabel.text = country.fullName
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        layer.masksToBounds = false
//        isSelectable = false
//        cornerRadius = 8
//        setShadowElevation(ShadowElevation(rawValue: 6.0), for: .normal)
//        setShadowElevation(ShadowElevation(rawValue: 0), for: .highlighted)
//        setShadowElevation(ShadowElevation(rawValue: 0), for: .selected)
//        inkView.inkColor = UIColor.inkColors.orange
//        contentView.layoutMargins = .zero
//
        commonInit()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        commonInit()
    }
    private func commonInit(){
        Bundle.main.loadNibNamed("CountryView", owner: self, options: nil)
        content.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        content.translatesAutoresizingMaskIntoConstraints = true
        content.frame = self.bounds
        self.addSubview(content)
        setContentParameters()
    }
    func setContentParameters(){
        
        //ImageView
        flagImage.layer.cornerRadius = 4
        flagImage.clipsToBounds = true
        flagImage.contentMode = ContentMode.scaleAspectFit
        flagImage.layer.borderWidth = 0.5
        flagImage.layer.borderColor = UIColor.black.withAlphaComponent(0.26).cgColor
    
    }

}
