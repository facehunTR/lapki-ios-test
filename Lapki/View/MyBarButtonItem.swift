//
//  MyBarButtonItem.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 18/01/2019.
//  Copyright © 2019 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import MaterialComponents.MDCButton

class MyBarButtonItem: UIBarButtonItem {
    init(frame: CGRect,imageName: String?,action: Selector) {
        let btn = MDCButton(frame: frame)
        btn.minimumSize = frame.size
        btn.backgroundColor = UIColor.clear
        btn.setImage(UIImage(named: imageName ?? "")?.withRenderingMode(.alwaysTemplate), for: .normal)
        btn.imageView?.contentMode = .center
        btn.inkColor = UIColor.inkColors.orange
        btn.inkMaxRippleRadius = min(frame.height, frame.width)/2
        btn.contentEdgeInsets = .zero
        btn.imageEdgeInsets = .zero
        btn.tintColor = UIColor.lapkiColors.orange
        btn.addTarget(nil, action: action, for: .touchUpInside)
        btn.sizeToFit()
        super.init()
        self.imageInsets = .zero
        if #available(iOS 11.0, *) {
            self.largeContentSizeImageInsets = .zero
        }
        self.customView = btn
    }
    init(frame: CGRect,title: String?,action: Selector) {
        let btn = MDCButton(frame: frame)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.minimumSize = frame.size
        btn.backgroundColor = UIColor.clear
        btn.isUppercaseTitle = false
        btn.setTitle(title, for: .normal)
        btn.setTitleFont(UIFont.LapkiFonts.LatoRegular, for: .normal)
        btn.setTitleColor(UIColor.lapkiColors.orange, for: .normal)
        btn.inkColor = UIColor.inkColors.orange
        btn.inkMaxRippleRadius = min(frame.height, frame.width)/2
        btn.contentEdgeInsets = .init(top: 10, left: 10, bottom: 10, right: 10)
        btn.titleEdgeInsets = .zero
        btn.tintColor = UIColor.lapkiColors.orange
        btn.addTarget(nil, action: action, for: .touchUpInside)

        btn.titleLabel?.sizeToFit()
        if title != nil {
            let width = (title! as NSString).size(withAttributes: [NSAttributedString.Key.font:UIFont.LapkiFonts.LatoRegular]).width
                btn.widthAnchor.constraint(equalToConstant: width + 20).isActive = true
        }
        
         btn.sizeToFit()
//        btn.titleEdgeInsets = .zero
        super.init()
        self.customView = btn
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
