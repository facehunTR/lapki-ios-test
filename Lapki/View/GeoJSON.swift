//
//  GeoJSON.swift
//  Lapki
//
//  Created by Denis Dolgopolov on 18/04/2019.
//  Copyright © 2019 MEGAKOM, OOO. All rights reserved.
//

import Foundation
import UIKit

class GeoJSON{
    struct Collection: Codable {
        var type: String
         var features: [Feature]
        
        func toBeziers() -> [UIBezierPath]{
            var result = [UIBezierPath]()
            for each in features{
                result.append(each.toBezier())
            }
            return result
        }
    }
    
     struct Feature: Codable {
        var type: FeatureType
        var properties: Properties
        var geometry: Geometry
        func toBezier() -> UIBezierPath{
            let path = UIBezierPath()
            var first: CGPoint?
            for coords in geometry.coordinates.first! {
                if first == nil {
                    first = CGPoint(x: coords.first!, y: coords.last!)
                    path.move(to: first!)
                }else{
                    path.addLine(to: CGPoint(x: coords.first!, y: coords.last!))
                }
            }
            path.addLine(to: first!)
            path.fill()
            return path
        }
    }
    
    struct Geometry: Codable {
        var type: GeometryType
        var coordinates: [[[Double]]]
        mutating func setCoordinate(coordinates:[[[Double]]]){
            self.coordinates = coordinates
        }
    }
    
    enum GeometryType: String, Codable {
        case polygon = "Polygon"
    }
    struct Properties: Codable {
        var type: String?
    }
    
    enum FeatureType: String, Codable {
        case feature = "Feature"
    }
}
