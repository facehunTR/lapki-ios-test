import UIKit
import AudioToolbox
import MaterialComponents

class PhotoCollectionView: UICollectionView {
var selectedCell = MDCCardCollectionCell()
fileprivate var longPressGesture: UILongPressGestureRecognizer!
    @objc func handleLongGesture(gesture: UILongPressGestureRecognizer) {
        
        switch(gesture.state) {
            
        case .began:
            guard let selectedIndexPath = self.indexPathForItem(at: gesture.location(in: self)) else {
                break
            }
            self.beginInteractiveMovementForItem(at: selectedIndexPath)
            selectedCell =  self.cellForItem(at: selectedIndexPath)! as! MDCCardCollectionCell
            AudioServicesPlayAlertSound(SystemSoundID(1519))
            changeCardHighlight(interacted: true)
            
        case .changed:
            self.updateInteractiveMovementTargetPosition(gesture.location(in: gesture.view!))
            
        case .ended:
            self.endInteractiveMovement()
            
            changeCardHighlight(interacted: false)
            
        default:
            self.cancelInteractiveMovement()
        }
    }
    func collectionViewSet(){
        if let layout = self.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal  // . 
            layout.itemSize = CGSize(width: 100, height: 100)
            
        }
        longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongGesture(gesture:)))
        self.addGestureRecognizer(longPressGesture)
    }
    func changeCardHighlight(interacted:Bool){
        var shadowValue:CGFloat = 1.0
        var shadowColor = UIColor.black
        if interacted{
            shadowValue = 10.0
            shadowColor = UIColor.lapkiColors.orange
        }
        selectedCell.setShadowElevation(ShadowElevation(rawValue: shadowValue), for: .normal)
        selectedCell.setShadowColor(shadowColor, for: .normal)
    
    }
        
}
