//
//  BonusTableViewCell.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 10/01/2019.
//  Copyright © 2019 MEGAKOM, OOO. All rights reserved.
//
import UIKit


class BonusTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lableConstraint: NSLayoutConstraint!
    @IBOutlet weak var statusLabel: UILabel?
    @IBOutlet weak var lapkaButton: UIImageView!
    @IBOutlet weak var bonusLabel: UILabel!
    @IBOutlet weak var numberView: UIImageView!
    var bonus: Bonus! {
        didSet{
            if bonus.isAvailable{
                self.numberView.image = UIImage(named: "number\(bonus!.number!)_gray")
                self.statusLabel?.removeFromSuperview()
                self.lapkaButton.image = UIImage(named: "lapkaButton_gray")
                
            }else{
                self.numberView.image = UIImage(named: "number\(bonus!.number!)")
                self.statusLabel?.text = "Бонусы начислены"
                self.contentView.addSubview(statusLabel ?? UIView())
                self.statusLabel?.tintColor = UIColor.lapkiColors.orange
                self.lapkaButton.image = UIImage(named: "lapkaButton")
            }
            self.bonusLabel.text = bonus.text
            //            let gesture = UITapGestureRecognizer(target: self, action: #selector(bonusClicked(_:)))
            //            self.lapkaButton.addGestureRecognizer(gesture)
            self.sizeToFit()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }
    override func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        // Configure the view for the selected state
        if selected {
            bonus!.action?()
        }
    }
    @objc func bonusClicked(_ sender: AnyObject){
        bonus!.action?()
    }
}
struct Bonus {
    var isAvailable:Bool!
    let number: Int!
    let text: String!
    let action: (() -> ())?
}
