//
//  CategoriesTableViewCell.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 29/11/2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//
import UIKit

class CategoriesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var categoriesLabel: UILabel!
    @IBOutlet weak var rightArrow: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        rightArrow.image = UIImage(named: "right_gray_arrow")
        // Initialization code
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(false, animated: false)
        if animated {
        UIView.transition(with: self,
                          duration: 3,
                          options: .transitionCrossDissolve,
                          animations: { self.backgroundColor = (selected) ? UIColor.inkColors.orange : UIColor.white},
                          completion: nil)
        }else{
            self.backgroundColor = (selected) ? UIColor.inkColors.orange : UIColor.white
        }
        // Configure the view for the selected state
    }
    
}
