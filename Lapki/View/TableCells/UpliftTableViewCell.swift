//
//  UpliftTableViewCell.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 15/01/2019.
//  Copyright © 2019 MEGAKOM, OOO. All rights reserved.
//
import UIKit
import MaterialComponents.MDCCard

class UpliftTableViewCell: MDCCardCollectionCell {
    
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    var order: Order! {
        didSet{
            contentView.layoutMargins = .zero
            self.layer.masksToBounds = false
            self.setImageTintColor(UIColor.lapkiColors.orange, for: .selected)
            self.isSelectable = true
            self.inkView.inkColor = UIColor.inkColors.orange
            self.setBorderColor(UIColor.gray, for: .normal)
            self.setBorderWidth(2, for: .normal)
            self.setBorderColor(UIColor.lapkiColors.lostOrange, for: .selected)
            self.setBorderWidth(2, for: .selected)
            self.cornerRadius = 10
            self.titleLabel.text = self.order.title
            
            self.priceLabel.text = "Стоимость: \(self.order.amount/100) Р"
            self.descriptionLabel.text = self.order.description
            self.sizeToFit()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let screenWidth = UIScreen.main.bounds.size.width
        self.widthConstraint.constant = screenWidth - 20
        self.widthConstraint.isActive = true
        // Initialization code
    }
}
struct Order {
    var reason: String!
    var title: String!
    var description: String!
    var amount: Int!
}
