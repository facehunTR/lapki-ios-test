//
//  myToolBar.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 20/11/2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialShadowElevations
import MaterialComponents.MDCShadowLayer
import MaterialComponents.MDCInkTouchController

class MyToolBar: UIToolbar {

    var inkTouchControllers = [MDCInkTouchController?]()
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override class var layerClass: AnyClass {
        return MDCShadowLayer.self
    }
    
    var shadowLayer: MDCShadowLayer {
        return self.layer as! MDCShadowLayer
    }
    
    func setDefaultElevation() {
        self.shadowLayer.elevation = .bottomNavigationBar
    }
    var myMainViewController: MyMainViewController!
    
    
    override func awakeFromNib() {
    }
    

    
}
