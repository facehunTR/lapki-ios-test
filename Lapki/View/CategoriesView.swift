//
//  CategoriesView.swift
//  Lapki
//
//  Created by Denis Dolgopolov on 16/04/2019.
//  Copyright © 2019 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import MaterialComponents.MDCCard

class CategoriesView: MDCCard {

    @IBOutlet var content: MDCCard!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var bottomLabel: UILabel!
    @IBOutlet weak var topLabel: UILabel!
    
    @IBAction func contentClicked(_ sender: Any) {
        self.sendActions(for: .touchUpInside)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        commonInit()
    }
    // TODO: Краш из аккаунта ВК
    func setCategoryImage(pojo: Pojo,category: String, type: String?){
        guard pojo.names.firstIndex(of: category) != nil else{
            imageView.image = UIImage(named: "categories_gray_icon")
            return
        }
        var image = getPojoImage(name: type ?? category)
        if image.size.height == 0 || image.size.width == 0{
            image = getPojoImage(name: category)
        }
        imageView.image = image
    }
    
    func set(pojo: Pojo,category: String?, type: String?){
        imageView.image = UIImage(named: "categories_gray_icon")
        if let type = type, let category = category {
            topLabel.text = pojo.names[0]?.replacingOccurrences(of: "Укажите", with: "Изменить")
            bottomLabel.text = category + "/" + type
            setCategoryImage(pojo: pojo,category: category, type: type)
        }else if let category = category{
            topLabel.text = pojo.names[0]?.replacingOccurrences(of: "Укажите", with: "Изменить")
            bottomLabel.text = category
            imageView.image = UIImage.categoriesImages[pojo.names.firstIndex(of: category)!]
        }else {
            topLabel.text = pojo.names[0]
            bottomLabel.text = "Выбраны все категории"
        }
    }
    private func commonInit(){
        Bundle.main.loadNibNamed("CategoriesView", owner: self, options: nil)
        content.inkView.inkColor = UIColor.inkColors.orange
        content.setShadowElevation(.init(0), for: .normal)
        content.autoresizingMask = [.flexibleWidth]
        content.translatesAutoresizingMaskIntoConstraints = true
        content.frame = self.bounds
        self.addSubview(content)
        setContentParameters()
    }
    func setContentParameters(){
        
        //ImageView
//        flagImage.layer.cornerRadius = 4
//        flagImage.clipsToBounds = true
//        flagImage.contentMode = ContentMode.scaleAspectFit
//        flagImage.layer.borderWidth = 0.5
//        flagImage.layer.borderColor = UIColor.black.withAlphaComponent(0.26).cgColor
        
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
}
