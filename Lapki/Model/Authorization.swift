//
//  File.swift
//  Lapki
//
//  Created by Denis Dolgopolov on 22/02/2019.
//  Copyright © 2019 MEGAKOM, OOO. All rights reserved.
//

import Foundation
import Firebase
typealias VkUsers = [VkUser]
struct LoginResponse: Codable,ResponseWithStatus {
    let token: String
    let id: Int
    var statusCode: Int?
}
struct VkUser: Codable {
    let mobilePhone: String?
    let id: Int?
    let lastName, photoID, screenName, homePhone: String?
    let firstNam: String?
    
    enum CodingKeys: String, CodingKey {
        case mobilePhone = "mobile_phone"
        case id
        case lastName = "last_name"
        case photoID = "photo_id"
        case screenName = "screen_name"
        case homePhone = "home_phone"
        case firstNam = "first_nam"
    }
}


struct AuthToken: Codable {
    let accessToken: String
    var expiresIn: Int
    let refreshToken, scope, tokenType: String
    
    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case expiresIn = "expires_in"
        case refreshToken = "refresh_token"
        case scope
        case tokenType = "token_type"
    }
}
//func refreshToken(completionHandler: CompletionHandler?){
//    let query: [String: Any] = [kSecClass as String: kSecClassInternetPassword,
//                                kSecAttrServer as String: Bundle.main.infoDictionary!["Lapki_API_URL"] as! String,
//                                kSecReturnAttributes as String: true,
//                                kSecReturnData as String: true]
//    var item: CFTypeRef?
//    let status = SecItemCopyMatching(query as CFDictionary, &item)
//    guard status != errSecItemNotFound else {  tokenHandler?(nil)
//        return
//    }
//    guard status == errSecSuccess else {  tokenHandler?(nil)
//        return
//    }
//    guard let existingItem = item as? [String : Any],
//        let tokenData = existingItem[kSecValueData as String] as? Data
//        else{
//            return
//    }
//    var token:String?
//    var t = try? JSONDecoder().decode(AuthToken.self, from: tokenData)
//    refreshToken(authToken: t!, responseHandler: {response in
//        if let newt = try? JSONDecoder().decode(AuthToken.self, from: response.data!){
//            t = newt
//        }
//        tokenHandler?("\(t!.tokenType) \(t!.accessToken)")
//        return
//    })
//}
func refreshAuthToken(){
    let query: [String: Any] = [kSecClass as String: kSecClassInternetPassword,
                                kSecAttrServer as String: Bundle.main.infoDictionary!["Lapki_API_URL"] as! String,
                                kSecReturnAttributes as String: true,
                                kSecReturnData as String: true]
    var item: CFTypeRef?
    let status = SecItemCopyMatching(query as CFDictionary, &item)
    guard status != errSecItemNotFound,
        status == errSecSuccess,
        let existingItem = item as? [String : Any],
        let tokenData = existingItem[kSecValueData as String] as? Data
        else {return}
    let t = try? JSONDecoder().decode(AuthToken.self, from: tokenData)
    Network.refreshToken(authToken: t!, responseHandler: {(t,error) in
        refreshTokenCompleted(t)
        refreshTokenFailure(error)
    })
}
func refreshTokenCompleted(_ token: AuthToken?){
    guard let token = token else {return}
    currentToken = token.tokenType + "" + token.accessToken
    Network.getCurrentUserData({ (_,_) in})
    try? saveTokenAndId(authToken: token)
}
func refreshTokenFailure(_ error: Error?){
    guard let error = error else {return}
    Analytics.logEvent("refreshTokenFailure", parameters: ["error": error])
    clearMyUser()
    try? clearTokenAndId()
}
func getAuthToken(tokenHandler: TokenHandler?){
    if currentToken == nil {
        let query: [String: Any] = [kSecClass as String: kSecClassInternetPassword,
                                    kSecAttrServer as String: Bundle.main.infoDictionary!["Lapki_API_URL"] as! String,
                                    kSecReturnAttributes as String: true,
                                    kSecReturnData as String: true]
        var item: CFTypeRef?
        let status = SecItemCopyMatching(query as CFDictionary, &item)
        guard status != errSecItemNotFound else {  tokenHandler?(nil)
            return
        }
        guard status == errSecSuccess else {  tokenHandler?(nil)
            return
        }
        guard let existingItem = item as? [String : Any],
            let tokenData = existingItem[kSecValueData as String] as? Data
            else{
                return
        }
        var token:String?
        let t = try? JSONDecoder().decode(AuthToken.self, from: tokenData)
        if t == nil {
            token = String(data: tokenData, encoding: String.Encoding.utf8)!
            tokenHandler?(token!)
            return
        }else{
            if t!.expiresIn < Int(Date().timeIntervalSince1970) {
                Network.refreshToken(authToken: t!, responseHandler: {(token,error) in
                    refreshTokenCompleted(token)
                    refreshTokenFailure(error)
                    guard let token = token else {return}
                    tokenHandler?("\(token.tokenType) \(token.accessToken)")
                    return
                })
                return
            }else{
                tokenHandler?("\(t!.tokenType) \(t!.accessToken)")
                return
            }
        }
    }else{
        tokenHandler?(currentToken)
    }
}
func clearTokenAndId() throws{
    currentToken = nil
    let query: [String: Any] = [kSecClass as String: kSecClassInternetPassword,
                                kSecAttrServer as String: Bundle.main.infoDictionary!["Lapki_API_URL"] as! String]
    let status = SecItemDelete(query as CFDictionary)
    guard status == errSecSuccess || status == errSecItemNotFound else { throw KeychainError.unhandledError(status: status) }
}
func saveTokenAndId(authToken: Any) throws{
    try? clearTokenAndId()
    let tokenData: Data
    if authToken is AuthToken {
        var t = authToken as! AuthToken
        t.expiresIn = t.expiresIn + Int(Date().timeIntervalSince1970)
        tokenData = try! JSONEncoder().encode(t)
    }else{
        tokenData = (authToken as! String).data(using: .utf8)!
    }
    let query: [String: Any] = [kSecClass as String: kSecClassInternetPassword,
                                kSecAttrServer as String: Bundle.main.infoDictionary!["Lapki_API_URL"] as! String,
                                kSecValueData as String: tokenData]
    let status = SecItemAdd(query as CFDictionary, nil)
    guard status == errSecSuccess else { throw KeychainError.unhandledError(status: status) }
}

enum KeychainError: Error {
    case noPassword
    case unexpectedPasswordData
    case unhandledError(status: OSStatus)
}
