//
//  MyMapKitView.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 10/12/2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//

import Foundation
import MapKit

class MyMKTileOverlay: MKTileOverlay {
    let cache = NSCache<AnyObject, AnyObject>()
    let operationQueue = OperationQueue()
    
    override func url(forTilePath path: MKTileOverlayPath) -> URL {
        var z = path.z
        if z > 18 {
            z = 18
        }
        return URL(string: String(format: "https://map.lapki.com/osm_tiles/%d/%d/%d.png", z, path.x, path.y))!
    }
    override func loadTile(at path: MKTileOverlayPath, result: @escaping (Data?, Error?) -> Void) {
        let url = self.url(forTilePath: path)
        if let cachedData = cache.object(forKey: url as AnyObject) as? Data {
            result(cachedData, nil)
        } else {
            let request = URLRequest(url: url)
            let task = URLSession.shared.dataTask(with: request){
                (data, response, error) in
                if error != nil {
                    print(error!)
                } else {
                    if data != nil {
                        self.cache.setObject(data as AnyObject, forKey: url as AnyObject)
                        result(data,error)
                    }
                }
            }
            task.resume()
        }
    }
}
