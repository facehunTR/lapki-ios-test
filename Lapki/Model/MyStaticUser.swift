import UIKit
import MapKit
import Foundation
import ReactiveCocoa
import ReactiveSwift
import Kingfisher
var currentToken:String?

struct MyStaticUser {
    //Reactive Mutables
    static var fullName: MutableProperty<String> = MutableProperty("")
    static var userImage: MutableProperty<UIImage> = MutableProperty(UIImage(named: "camera_orange_big")!)
    static var buttonText: MutableProperty<String> = MutableProperty("ВОЙТИ")
    static var isEmpty: MutableProperty<Bool> = MutableProperty(true)
    static var emailProperty: MutableProperty<String> = MutableProperty("Не указан")
    static var phoneProperty: MutableProperty<String> = MutableProperty("Не указан")
    static var addressProperty: MutableProperty<String> = MutableProperty("Не указан")
    static var verificationColor: MutableProperty<UIColor> = MutableProperty(UIColor.red)
    
    
    static var id: Int = 0 {
        didSet {
            isEmpty.swap(id == 0)
            buttonText.swap((id == 0) ? "ВОЙТИ" : "ВЫЙТИ")
        }
    }
    private static var lastUrl: URL?
    static var userImageUrl: URL? = nil{
        willSet{
            lastUrl = userImageUrl
        }
        didSet{
            guard let url = userImageUrl, lastUrl != userImageUrl else {
                return
            }
            ImageDownloader.default.downloadImage(with: url){result in
                switch (result){
                case .success(let value):
                    userImage.swap(value.image)
                    break
                case .failure(let error):
                    userImage.swap(UIImage(named:"camera_orange_big")!)
                    print(error.errorDescription ?? "nil")
                    break
                }
            }
        }
    }
  
    static var firstName: String = "" {
        didSet{
            fullName.swap( firstName + " " + lastName)
        }
    }
    static var lastName: String = ""{
        didSet{
            fullName.swap( firstName + " " + lastName)
        }
    }
    static var email: String = "" {
        didSet{
            emailProperty.swap((email.isEmpty) ? "Не указан":email)
        }
    }
    static var phonenumber: String = "" {
        didSet{
            phoneProperty.swap((phonenumber.isEmpty) ? "Не указан" : "+\(phonenumber)")
        }
    }
    static var verificationPhone: Bool = false {
        didSet{
            verificationColor.swap((verificationPhone) ? UIColor.green : UIColor.red)
        }
    }
    static var avatar: String = "" {
        didSet{
            userImageUrl = URL(string: (avatar.isEmpty) ? urlAvatar : avatar)
        }
    }
    static var urlAvatar: String = ""{
        didSet{
            userImageUrl = URL(string: (avatar.isEmpty) ? urlAvatar : avatar)
        }
    }
    static var ad: [Int] = []
    static var favorites: [Int] = []
    static var defLat: Double = 55.7520
    static var defLng: Double = 37.6169
    static var defAddress: String = "" {
        didSet{
            addressProperty.swap((defAddress.isEmpty || defLat == 55.7520 || defLng == 37.6169) ? "Не указан":defAddress)
        }
    }
    static var defaultLocation = CLLocationCoordinate2D(latitude: MyStaticUser.defLat, longitude: MyStaticUser.defLng)
    func toMyUser() -> MyUser {
        var user = MyUser()
        user.id = MyStaticUser.id
        user.firstName = MyStaticUser.firstName
        user.lastName = MyStaticUser.lastName
        user.email = MyStaticUser.email
        user.phonenumber = MyStaticUser.phonenumber
        user.verificationPhone = MyStaticUser.verificationPhone
        user.avatar = MyStaticUser.avatar
        user.urlAvatar = MyStaticUser.urlAvatar
        user.ad = MyStaticUser.ad
        user.favorites = MyStaticUser.favorites
        user.defLng = MyStaticUser.defLng
        user.defLat = MyStaticUser.defLat
        user.defAddress = MyStaticUser.defAddress
        
        return user
    }
}
func getUserFromDefaults(){
    guard let data = UserDefaults.standard.object(forKey: Bundle.main.infoDictionary!["Lapki_API_URL"] as! String) as? Data, let user = try? JSONDecoder().decode(MyUser.self, from: data) else{
        return
    }
    userToStatic(user)
}
func clearMyUser(){
    MyStaticUser.id = 0
    MyStaticUser.firstName = ""
    MyStaticUser.lastName = ""
    MyStaticUser.email = ""
    MyStaticUser.phonenumber = ""
    MyStaticUser.verificationPhone = false
    MyStaticUser.urlAvatar = ""
    MyStaticUser.avatar = ""
    MyStaticUser.ad = []
    MyStaticUser.favorites = []
    MyStaticUser.defLat = 55.7520
    MyStaticUser.defLng = 37.6169
    MyStaticUser.defAddress = ""
    UserDefaults.standard.removeObject(forKey: Bundle.main.infoDictionary!["Lapki_API_URL"] as! String)
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userUpdated"), object: nil)
}


func userToStatic(_ User: MyUser) {
    MyStaticUser.id = User.id
    MyStaticUser.firstName = User.firstName ?? ""
    MyStaticUser.lastName = User.lastName ?? ""
    MyStaticUser.email = User.email ?? ""
    MyStaticUser.phonenumber = User.phonenumber ?? ""
    MyStaticUser.verificationPhone = User.verificationPhone
    MyStaticUser.avatar = User.avatar ?? ""
    if MyStaticUser.avatar.first == "/" {
        MyStaticUser.avatar = apiUrl + MyStaticUser.avatar
    }else{
        MyStaticUser.avatar = MyStaticUser.avatar.replacingOccurrences(of: "http:", with: "https:", options: String.CompareOptions.literal)
    }
    MyStaticUser.urlAvatar = User.urlAvatar ?? ""
    if MyStaticUser.urlAvatar.first == "/" {
        MyStaticUser.urlAvatar = apiUrl + MyStaticUser.urlAvatar
    }else{
        MyStaticUser.urlAvatar = MyStaticUser.urlAvatar.replacingOccurrences(of: "http:", with: "https:", options: String.CompareOptions.literal)
    }
    
    MyStaticUser.ad = User.ad ?? []
    MyStaticUser.favorites = User.favorites ?? []
    MyStaticUser.defLat = User.defLat ?? 55.7520
    MyStaticUser.defLng = User.defLng ?? 37.6169
    MyStaticUser.defaultLocation = CLLocationCoordinate2D.init(latitude: MyStaticUser.defLat, longitude: MyStaticUser.defLng)
    MyStaticUser.defAddress = User.defAddress ?? ""
    let data = try? JSONEncoder().encode(MyStaticUser().toMyUser())
    UserDefaults.standard.set(data, forKey: Bundle.main.infoDictionary!["Lapki_API_URL"] as! String)
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userUpdated"), object: nil)
}







