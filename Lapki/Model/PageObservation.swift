//
//  File.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 25/10/2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//

import Foundation

protocol PageObservation: class {
    func getParentPageViewController(parentRef: MyPageViewController)
}
protocol PageObservationFavorites: class {
    func getParentPageViewController(parentRef: MyFavoritesPageViewController)
}
