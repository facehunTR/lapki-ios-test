//
//  Pojo.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 29/11/2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//

import Foundation

struct Pojo: Codable {
    var names: [String?] = [String?]()
    var subs: [Pojo?]?
    
    init (names: [String?]?,subs: [Pojo?]?){
        if names != nil{
            for name in names! {
            self.names.append(name)
            }}
        self.subs = subs
    }
    func getSubsubtypesNames(category:String, type: String, row: Int) -> [String?]? {
        return self.subs![self.names.firstIndex(of: category)!]?.subs![(self.subs![self.names.firstIndex(of: category)!]?.names.firstIndex(of: type))!]?.subs![row]!.names
    }
    func getSubtypesNames(category:String, type: String) -> [String?]?{
        return self.subs![self.names.firstIndex(of: category)!]?.subs![(self.subs![self.names.firstIndex(of: category)!]?.names.firstIndex(of: type))!]?.names
    }
}
struct Query {
    var list :[String:[String?]]?
    init(list: [String:[String?]]) {
        self.list = list
    }
    init() {
        list = [String:[String?]]()
    }
    mutating func addColumn(name:String,values:[String?]?){
        list![name] = values
    }
}
