//
//  Network.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 27/11/2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//

import Foundation
import UIKit.UIImage

let apiUrl = Bundle.main.infoDictionary!["Lapki_API_URL"] as! String
let mapUrl = "https://map.lapki.com"
var kladr = ""
var city = ""
var country:String = ""
    



func weakCompletion<Object : AnyObject, Result>(of object: Object,
                                                onResult: @escaping (Object) -> (Result) -> (),
                                                onError: @escaping (Object) -> (Error) -> ()) -> ((Result?, Error?) -> Void) {
    return { [weak object] result, error in
        guard let object = object else {
            return
        }
        if let result = result {
            onResult(object)(result)
        } else if let error = error {
            onError(object)(error)
        }
    }
}


var isUserInRussia = true

enum ResponseError: Error {
    case detailReturned(detail: String)
    case withoutDetails
    case notJSON
    case emptyResponse
    case notExpectedValue
}

enum URLSessionErrors: Error {
    case invalidUrl
    case urlRequestIsNil
}
typealias Parameters = [String:Any]
enum RequestEncoding: String {
    case JSON
    case Multipart
    case UrlEncoded
}
enum MultipartError: Error {
    case contentTypeNotGenerated
    case bodyNotGenerated
}
enum HTTPMethod: String {
    case get     = "GET"
    case post    = "POST"
    case put     = "PUT"
    case patch   = "PATCH"
}
public typealias HTTPHeaders = [String: String]
class Network{
    class func taskExecute<T: Codable>(_ method: HTTPMethod, urlString: String, parameters: Any? = nil, headers: HTTPHeaders? = nil, encoding: RequestEncoding = .JSON, responseHandler: @escaping (T?,Error?) -> Void) {
        do {
            let request = try Network.urlRequestBuild(method,urlString:urlString, parameters: parameters,headers: headers, encoding: encoding)
            if encoding == .Multipart {
                print(String(decoding: request!.httpBody!, as: UTF8.self))
            }
            let task = URLSession.shared.dataTask(with: request!){(data, response, err) in
                print(String(decoding: data ?? Data(), as: UTF8.self))
                let result: Result<T,Error> = Network.responseValidation(data, response: response, error: err)
                DispatchQueue.main.async {
                do {
                    let val = try result.get()
                    var valueWithStatus = val as? ResponseWithStatus
                    valueWithStatus?.statusCode = (response as? HTTPURLResponse)?.statusCode ?? 404
                    responseHandler((valueWithStatus as? T) ?? val, nil)
                }catch{
                    responseHandler(nil,error)
                }
                }
            }
            task.resume()
        }catch{
            responseHandler(nil,error)
        }
    }
    class func responseValidation<T: Decodable>(_ data: Data?, response: URLResponse?, error: Error?) -> Result<T,Error>{
        guard let data = data, error == nil else {
            return .failure(error ?? ResponseError.emptyResponse)
        }
        do {
            print(String(decoding: data, as: UTF8.self))
            let result = try JSONDecoder().decode(T.self, from: data)
            return .success(result)
        }catch{
            print(error)
            do {
                let result = try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
                let detail = (result?["detail"] as? String) ?? (result?["non_field_errors"] as? NSArray)?.firstObject as? String
                let err = (detail != nil) ? ResponseError.detailReturned(detail: detail!) : error
                return .failure(err)
            }catch{
                print(String(decoding: data, as: UTF8.self))
                return .failure(error)
            }
        }
    }
    class func queryBuilder(parameters: Any?) -> [URLQueryItem]?{
        var myQueryItems = [URLQueryItem]()
        guard let parameters = parameters as? Dictionary<String,Any> else{
            print("Parameters is not Dictionary<String,Any>")
            return nil
        }
        for (key,value) in parameters{
            switch value{
            case let stringValue as String: myQueryItems.append(URLQueryItem(name: key, value: stringValue))
            case let intValue as Int: myQueryItems.append(URLQueryItem(name: key, value: String(intValue)))
            case let doubleValue as Double: myQueryItems.append(URLQueryItem(name: key, value: String(doubleValue)))
            case let boolValue as Bool: myQueryItems.append(URLQueryItem(name: key, value: (boolValue) ?"True":"False"))
            default: myQueryItems.append(URLQueryItem(name: key, value: "\(value)"))
            }
        }
        return myQueryItems
    }
    class func urlRequestBuild(_ method: HTTPMethod,urlString: String, parameters: Any? = nil, headers: HTTPHeaders? = nil, encoding: RequestEncoding) throws -> URLRequest? {
        guard var urlComponents = URLComponents(string: urlString) else {throw URLSessionErrors.invalidUrl}
        // get method query
        if method == .get, parameters != nil{
            urlComponents.queryItems = queryBuilder(parameters: parameters)
        }
        var request = URLRequest(url: urlComponents.url!)
        request.httpMethod = method.rawValue
        // Headers
        let buildNumber: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
        let appVersionString: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        request.addValue("Lapki/\(appVersionString) (com.MEGAKOM.Lapki; build:\(buildNumber); iOS \(UIDevice.current.systemVersion)) URLSession/0.0.0", forHTTPHeaderField: "User-Agent")
        for (key,value) in headers ?? [String:String]() {
            request.addValue(value, forHTTPHeaderField: key)
        }
        
        if method != .get, parameters != nil {
            if encoding == .JSON{
                request.httpBody = try JSONSerialization.data(withJSONObject: parameters!, options: .prettyPrinted)
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            }else if encoding == .Multipart{
                try request.addMultipartEncodingBody(parameters: parameters!)
            }
        }
        request.httpMethod = method.rawValue
        return request
    }
    // MARK: GetNotices Functions
    
    class func getFavorites(parameters: Parameters?,responseHandler: @escaping (PageResult?,Error?)-> Void){
        getAuthToken(tokenHandler: {token in
            guard token != nil else {return}
            let headers: HTTPHeaders = [
                "Authorization": token!
            ]
            Network.taskExecute(.get, urlString:"\(apiUrl)/favorites/", parameters: parameters, headers: headers, responseHandler: responseHandler)
        })
    }
    
    class func getMyNotices(responseHandler: @escaping (PageResult?,Error?)-> Void){
        let parameters = [
            "owner": "\(MyStaticUser.id)",
            "ordering": "-date_uplift"
        ]
        Network.taskExecute(.get, urlString: "\(apiUrl)/ad/", parameters: parameters, responseHandler: responseHandler)
    }
    
    class func getSocialNotices(responseHandler: @escaping (Notices?,Error?)-> Void){
        Network.taskExecute(.get, urlString: "\(apiUrl)/social-ad/", responseHandler: responseHandler)
    }
    
    class func getNextPage(url: String, responseHandler: @escaping (PageResult?,Error?) -> Void){
        Network.taskExecute(.get, urlString: url, responseHandler: responseHandler)
    }
    class func getNotice(id: Int, responseHandler: @escaping (Notice?,Error?) -> Void){
        Network.taskExecute(.get, urlString: "\(apiUrl)/ad/\(id)/", responseHandler: responseHandler)
    }
    class func getNotices(status: String, responseHandler: @escaping (PageResult?,Error?) -> Void){
        let parameters:[String:String]=[
            "status" : status,
            "ordering" : "-date_uplift"]
        Network.taskExecute(.get, urlString:"\(apiUrl)/ad/", parameters: parameters, responseHandler: responseHandler)
    }
    class func getNotices(parameters: [String:Any?], responseHandler: @escaping (PageResult?,Error?)-> Void){
        var param = parameters
        if let lat = param["lat"], let lng = param["lng"], param["dist"] != nil {
            param["point"] = "\(lat!),\(lng!)"
        }
        if param["status"] == nil {
            param["status"] = "active"
        }
        if param["ordering"] == nil {
            param["ordering"] = "-date_uplift"
        }
        Network.taskExecute(.get, urlString:"\(apiUrl)/ad/", parameters: param, responseHandler: responseHandler)
    }
    // MARK: GetBonuses functions
    class func getLapki(responseHandler: @escaping (LapkiResponse?, Error?) -> Void){
        getAuthToken(tokenHandler: {token in
            guard token != nil else {return}
            let headers: HTTPHeaders = [
                "Authorization" : token!,
                "Content-Type":"application/json"
            ]
            Network.taskExecute(.get, urlString: "\(apiUrl)/order/lapki/",headers: headers, responseHandler: responseHandler)
        })
    }
    class func getUsedBonuses(responseHandler: @escaping ([UsedBonus]?, Error?) -> Void){
        getAuthToken(tokenHandler: {token in
            guard token != nil else {return}
            let headers: HTTPHeaders = [
                "Authorization": token!,
                "Content-Type":"application/json"
            ]
            Network.taskExecute(.get, urlString: "\(apiUrl)/order/getusebonus/", headers: headers, responseHandler: responseHandler)
        })
    }
    class func createBonusOrder(order_reason: String?,ad:Int?,byBonuses: Bool?, responseHandler: @escaping (DetailedResponse?, Error?) -> Void){
        getAuthToken(tokenHandler: {token in
            guard let token = token else {return}
            var parameters = [String: Any?]()
            parameters["order_reason"] = order_reason
            parameters["ad"] = ad
            parameters["bybonuses"] = byBonuses
            let headers: HTTPHeaders = [
                "Authorization": token
            ]
            Network.taskExecute(.post, urlString: "\(apiUrl)/order/order/",parameters: parameters, headers: headers,encoding: .Multipart, responseHandler: responseHandler)
        })
    }
    class func createOrder(order_reason: String?,ad:Int?,byBonuses: Bool?, responseHandler: @escaping (CreatedOrder?, Error?) -> Void){
        getAuthToken(tokenHandler: {token in
            guard let token = token else {return}
            var parameters = [String: Any?]()
            parameters["order_reason"] = order_reason
            parameters["ad"] = ad
            parameters["bybonuses"] = byBonuses
            let headers: HTTPHeaders = [
                "Authorization": token
            ]
            Network.taskExecute(.post, urlString: "\(apiUrl)/order/order/",parameters: parameters, headers: headers,encoding: .Multipart, responseHandler: responseHandler)
        })
    }
    
    // MARK: Notice actions
    class func patchNotice(id: Int,parameters: [String:Any],responseHandler: @escaping (Notice?,Error?) -> Void){
        getAuthToken(tokenHandler: {token in
            guard token != nil else {return}
            let headers: HTTPHeaders = [
                "Authorization": token!
            ]
            if parameters.isEmpty{
                responseHandler(nil,MultipartError.bodyNotGenerated)
                return
            }
            Network.taskExecute(.patch, urlString: "\(apiUrl)/ad/\(id)/",parameters: parameters, headers: headers, encoding: .Multipart, responseHandler: responseHandler)
        })
        
    }
    class func deleteNotice(id:Int,status: String, responseHandler: @escaping (DetailedResponse?,Error?) -> Void){
        getAuthToken(tokenHandler: {token in
            guard token != nil else {return}
            let headers: HTTPHeaders = [
                "Authorization": token!,
                "Content-Type": "multipart/form-data"
            ]
            var parameters = [String:String]()
            parameters["id"] = "\(id)"
            parameters["owner_id"] = "\(MyStaticUser.id)"
            parameters["status"] = status
            
            Network.taskExecute(.post, urlString: "\(apiUrl)/ad/delete/",parameters: parameters, headers: headers, encoding: .Multipart, responseHandler: responseHandler)
        })
        
    }
    class func restoreNotice(id:Int,status: String, responseHandler: @escaping (DetailedResponse?,Error?) -> Void){
        getAuthToken(tokenHandler: {token in
            guard token != nil else {return}
            let headers: HTTPHeaders = [
                "Authorization": token!,
                "Content-Type": "application/json"
            ]
            var parameters = [String:String]()
            parameters["id"] = "\(id)"
            parameters["owner_id"] = "\(MyStaticUser.id)"
            parameters["status"] = status
            Network.taskExecute(.post, urlString: "\(apiUrl)/ad/restore/",parameters: parameters, headers: headers, encoding: .Multipart, responseHandler: responseHandler)
        })
        
    }
    class func postNotice(parameters: [String:Any],responseHandler: @escaping (Notice?,Error?) -> Void){
        getAuthToken(tokenHandler: {token in
            guard token != nil else {return}
            let headers: HTTPHeaders = [
                "Authorization": token!
            ]
            Network.taskExecute(.post, urlString: "\(apiUrl)/ad/",parameters: parameters, headers: headers, encoding: .Multipart, responseHandler: responseHandler)
        })
    }
    
    class func noticeViewed(id: Int){
        let emptyHandler: (LapkiResponse?,Error?) -> Void = {(_,_) in}
        Network.taskExecute(.get, urlString: "\(apiUrl)/views-ad/\(id)",responseHandler:emptyHandler)
    }
    
    // MARK: User actions
    class func refreshToken(authToken: AuthToken, responseHandler: @escaping (AuthToken?,Error?) -> Void){
        var parameters = [String:String]()
        parameters["client_id"] = "26w1diqa5GUOzPcAbmvPCPsjsSff5UwOHPo9j845"
        parameters["grant_type"] = "refresh_token"
        parameters["refresh_token"] = authToken.refreshToken
        Network.taskExecute(.post, urlString: "\(apiUrl)/auth/token/",parameters: parameters, responseHandler: responseHandler)
    }
    
    class func registerSocial(parameters: Parameters,responseHandler: @escaping (AuthToken?,Error?) -> Void){
        var params = parameters
        params["client_id"] = "26w1diqa5GUOzPcAbmvPCPsjsSff5UwOHPo9j845"
        params["grant_type"] = "convert_token"
        Network.taskExecute(.post, urlString: "\(apiUrl)/auth/convert-token/",parameters: params, responseHandler: responseHandler)
    }
    
    class func patchUserAvatar(image: UIImage, responseHandler: @escaping (MyUser?,Error?) -> Void){
        getAuthToken(tokenHandler: {token in
            guard token != nil else {return}
            let headers: HTTPHeaders = [
                "Authorization": token!
            ]
            Network.taskExecute(.patch, urlString: "\(apiUrl)/users/\(MyStaticUser.id)/",parameters: ["avatar":image] as Parameters,headers: headers, encoding: .Multipart, responseHandler: responseHandler)
        })
    }
    
    class func genVerificationCode(call:Bool,responseHandler: @escaping (DetailedResponse?,Error?) -> Void){
        getAuthToken(tokenHandler: {token in
            guard token != nil else {return}
            let headers: HTTPHeaders = [
                "Authorization": token!
            ]
            var callString: String! = "False"
            if call{
                callString = "True"
            }else{
                callString = "False"
            }
            if !MyStaticUser.phonenumber.starts(with: "7"){
                callString = "True"
            }
            let parameters: Parameters = [
                "user_id": "\(MyStaticUser.id)",
                "phonenumber": MyStaticUser.phonenumber,
                "call":callString!
            ]
            Network.taskExecute(.post, urlString: "\(apiUrl)/register/gencode", parameters: parameters, headers:headers, responseHandler: responseHandler)
        })
    }
    
    class func numberVerification(code:String, responseHandler: @escaping (DetailedResponse?,Error?) -> Void){
        getAuthToken(tokenHandler: {token in
            guard token != nil else {return}
            let headers: HTTPHeaders = [
                "Authorization": token!
            ]
            let parameters: [String:Any] = [
                "email": MyStaticUser.email,
                "phonenumber": MyStaticUser.phonenumber,
                "verification_code": code as Any
            ]
            Network.taskExecute(.post, urlString: "\(apiUrl)/register/verification/",parameters: parameters,headers: headers, responseHandler: responseHandler)
        })
    }
    class func getCurrentUserData(_ completionHandler: @escaping (Bool?,Error?) -> Void){
        getAuthToken(tokenHandler: {token in
            guard token != nil else {clearMyUser()
                return}
            getUserFromDefaults()
            let headers: HTTPHeaders = [
                "Authorization": token!,
                "Content-Type":"application/json"
            ]
            let responseHandler: (MyUser?,Error?) ->Void =  { (user,error) in
                guard let user = user, error == nil else {
                    completionHandler(nil,error)
                    return
                }
                userToStatic(user)
                completionHandler(true,nil)
            }
            Network.taskExecute(.get, urlString: "\(apiUrl)/users/current/",headers: headers, responseHandler: responseHandler)
        })
    }
    
    class func loginUser(username: String, password: String, responseHandler: @escaping (Bool?,Error?) -> Void){
        let parameters:[String:String]=[
            "username" : username,
            "password" : password]
        let respHandler: (LoginResponse?,Error?) -> Void = { (result,error) in
            guard let result = result, error == nil else{
                responseHandler(nil,error)
                return
            }
            if (result.statusCode ?? 404) < 400 {
                do {
                    try saveTokenAndId(authToken: "Token \(result.token)")
                    getCurrentUserData({(success,error) in
                        responseHandler(success,error)
                    })
                }catch{
                    responseHandler(nil,error)
                }
            }else{
                responseHandler(nil,ResponseError.notExpectedValue)
            }
        }
        Network.taskExecute(.post, urlString: "\(apiUrl)/login/",parameters: parameters, responseHandler: respHandler)
    }
    class func registerUser(phonenumber: String, password: String, responseHandler: @escaping (DetailedResponse?,Error?) -> Void){
        let parameters:[String:String]=[
            "phonenumber" : phonenumber,
            "password" :password,
            "email":""]
        Network.taskExecute(.post, urlString: "\(apiUrl)/register/",parameters: parameters, responseHandler: responseHandler)
    }
    class func patchFavorites(id: Int,responseHandler: @escaping (DetailedResponse?,Error?)->Void){
        getAuthToken(tokenHandler: {token in
            guard token != nil else {return}
            let wasFavorite = MyStaticUser.favorites.contains(id)
            var parameters = Parameters()
            if wasFavorite{
                parameters["action"] = "remove"
            }else{
                parameters["action"] = "add"
            }
            parameters["ad"] = id
            let headers: HTTPHeaders = [
                "Authorization": token!
            ]
            Network.taskExecute(.post, urlString: "\(apiUrl)/favor/",parameters: parameters, headers: headers, responseHandler: responseHandler)
        })
    }
    class func patchUserData(parameters: [String:String],responseHandler: @escaping (MyUser?,Error?) -> Void){
        getAuthToken(tokenHandler: {token in
            guard token != nil else {return}
            let headers: HTTPHeaders = [
                "Authorization": token!
            ]
            Network.taskExecute(.patch, urlString: "\(apiUrl)/users/\(MyStaticUser.id)/",parameters: parameters, headers: headers, responseHandler: responseHandler)
        })
    }
    class func getUserData(id: String,responseHandler: @escaping (MyUser?,Error?) -> Void){
        Network.taskExecute(.get, urlString: "\(apiUrl)/users/\(id)/", responseHandler: responseHandler)
    }
    // MARK: MapsAndGeocoding
    // TODO: Перепроверить работу запросов к карте
    class func reverseGeo(lat: Double,
                          lng:Double,
                          zoom: Double,
                          responseHandler: @escaping (ReverseGeoData?, Error?) -> Void){
        let z = Int(round(zoom))
        let parameters: [String:String]=[
            "format":"jsonv2",
            "lat":"\(lat)",
            "lon":"\(lng)",
            "zoom":"\(z)",
            "addressdetails":"1",
            "accept-language":country
        ]
        Network.taskExecute(.get, urlString: "\(mapUrl)/nominatim/reverse",parameters: parameters, responseHandler: responseHandler)
    }
    class func geoSearch(q: String,
                         responseHandler: @escaping (GeoDatas?,Error?) -> Void){
        let parameters: [String:String]=[
            "q":q,
            "format":"jsonv2",
            "addressdetails":"1",
            "accept-language": country, //Locale.current.identifier
            "dedupe":"1"
        ]
        Network.taskExecute(.get, urlString: "\(mapUrl)/nominatim/search/", parameters: parameters, responseHandler: responseHandler)
    }
    //TODO: Проверить dadata
    class func geoDadata(lat:Double,
                         lon:Double,
                         responseHandler:@escaping (Dadata?,Error?)-> Void){
        let headers = ["Content-type":"application/json",
        "Accept":"application/json",
        "Authorization":"Token a129736a8588682826faee795eb063a37123c0ae"]
        let url = "https://suggestions.dadata.ru/suggestions/api/4_1/rs/geolocate/address/"
        
        let parameters:Parameters = [
        "lat":lat,
        "lon":lon,
        "radius_meters":100,
        "count":1
        ]
        
        Network.taskExecute(.post, urlString: url,parameters: parameters,headers: headers, responseHandler: responseHandler)
        
        
    }
    class func dadata(address:String,
                      autoComplete:Bool,
                      completion:@escaping (Dadata?, Error?)->Void){
        let locationBoost:Array = [
            [
                "kladr_id":kladr
            ]
        ]
        let count = 10
        let headers = ["Content-type":"application/json",
                       "Accept":"application/json",
                       "Authorization":"Token a129736a8588682826faee795eb063a37123c0ae"]
        let url = "https://suggestions.dadata.ru/suggestions/api/4_1/rs/"
        let path = "suggest/address/"
        var parameters:Parameters = ["query":address,
                                     //"locations":country,
            "count": count,
            "locations_boost":locationBoost]
        if autoComplete == false{
            parameters["count"] = 1
        }else{
            parameters["from_bound"] = ["value":"block"]
            parameters["to_bound"] = ["value":"city"]
        }
        for param in parameters{
            if let str = param.value as? String, str == ""{
                parameters.removeValue(forKey: param.key)
            }
        }
        let handler: (Dadata?,Error?) -> Void = {(dadata,error) in
            var value = dadata
            value?.autoComplete = autoComplete
            completion(value,error)
        }
        Network.taskExecute(.post, urlString: url + path,parameters: parameters,headers: headers, responseHandler: handler)
    }
    class func updateAds(){
        let responseHandler: (Ads?,Error?) -> Void = {(value,error) in
            guard let value = value, error == nil else{
                print(error ?? ResponseError.emptyResponse)
                return
            }
            if Storage.fileExists("adv.json", in: .caches){
                Storage.remove("adv.json", from: .caches)
            }
            Storage.store(value, to: .caches, as: "adv.json")
        }
        Network.taskExecute(.get, urlString: "\(apiUrl)/adv/", responseHandler: responseHandler)
    }
}
//func dadataGeoCode(){
////    https://suggestions.dadata.ru/suggestions/api/4_1/rs/geolocate/address/?lat=55.601983&lon=37.359486&token=9b063011d0b81204bda57dd72ff283ec69179cb1
//    var headers = HTTPHeaders()
//    headers["Authorization"] = "Token 9b063011d0b81204bda57dd72ff283ec69179cb1"
//    headers["Content-Type"] = "application/json"
//    headers["Accept"] = "application/json"
//    var parameters = Parameters()
//    parameters["lat"] = 55.601983
//    parameters["lon"] = 37.359486
//    Alamofire.request("https://suggestions.dadata.ru/suggestions/api/4_1/rs/geolocate/address", method: .get, bo,headers: headers).debugLog().responseJSON { response in
//        print("azazaza" + response.debugDescription)
//    }
//}

private let arrayParametersKey = "arrayParametersKey"
/// Extension that allows an array be sent as a request parameters
extension Array {
    /// Convert the receiver array to a `Parameters` object.
    func asParameters() -> Parameters {
        return [arrayParametersKey: self]
    }
}
extension URLRequest {
    mutating func addMultipartEncodingBody(parameters: Any) throws{
        guard let params = (parameters as? [String:Any?])?.toDataDict()  else {throw MultipartError.bodyNotGenerated}
        let makeRandom = { UInt32.random(in: (.min)...(.max)) }
        let boundary = String(format: "------------------------%08X%08X", makeRandom(), makeRandom())
        guard let contentType: String = try? {
            guard let charset = CFStringConvertEncodingToIANACharSetName(CFStringConvertNSStringEncodingToEncoding(String.Encoding.utf8.rawValue)) else {
                throw MultipartError.contentTypeNotGenerated
            }
            return "multipart/form-data; charset=\(charset); boundary=\(boundary)"
            }() else {throw MultipartError.contentTypeNotGenerated}
        self.setValue(contentType, forHTTPHeaderField: "Content-Type")
        guard let httpBody: Data? = try? {
            var body = Data()
            for (rawName, rawValue) in params {
                if !body.isEmpty {body.append("\r\n".data(using: .utf8)!)}
                body.append("--\(boundary)\r\n".data(using: .utf8)!)
                guard rawName.canBeConverted(to: String.Encoding.utf8),
                    var disposition = "Content-Disposition: form-data; name=\"\(rawName)\"".data(using: .utf8) else {
                        throw MultipartError.bodyNotGenerated
                }
                if rawName.contains(oneOf: ["image","avatar"]) && rawValue.count > 0 {
                    disposition.append("; filename=\"\(rawName).jpg\"\r\nContent-Type: image/jpeg\r\n".data(using: .utf8)!)
                }else{
                    disposition.append("\r\n".data(using: .utf8)!)
                }
                body.append(disposition)
                body.append("\r\n".data(using: .utf8)!)
                body.append(rawValue)
            }
            body.append("\r\n--\(boundary)--\r\n".data(using: .utf8)!)
            return body
            }() else {throw MultipartError.bodyNotGenerated}
        
        self.httpBody = httpBody
    }
}
