import Foundation

struct MyUser: Codable {
    var id: Int
    var firstName, lastName, email, phonenumber: String?
    var verificationPhone: Bool
    var avatar, urlAvatar: String?
    var ad, favorites: [Int]?
    var defLat, defLng: Double?
    var defAddress: String?
    
    init(){
        self.id = 0
        self.verificationPhone = false
        self.email = nil
        self.phonenumber = nil
        self.firstName = nil
        self.lastName = nil
        self.avatar = nil
        self.ad = nil
        self.favorites = nil
        self.defAddress = nil
        self.urlAvatar = nil
        self.defLat = nil
        self.defLng = nil
    }
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case firstName = "first_name"
        case lastName = "last_name"
        case email = "email"
        case phonenumber = "phonenumber"
        case verificationPhone = "verification_phone"
        case avatar = "avatar"
        case urlAvatar = "url_avatar"
        case ad, favorites
        case defLat = "def_lat"
        case defLng = "def_lng"
        case defAddress = "def_address"
    }
}

