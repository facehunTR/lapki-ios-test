//
//  Ads.swift
//  Lapki
//
//  Created by Denis Dolgopolov on 12/03/2019.
//  Copyright © 2019 MEGAKOM, OOO. All rights reserved.
//

import Foundation

private var globalAds: [Ad]?

func getAds(category: String?) -> Ads?{
    if globalAds == nil && Storage.fileExists("adv.json", in: .caches){
        globalAds = Storage.retrieve("adv.json", from: .caches, as: Ads.self)
    }
    guard  let ads = globalAds?.filter({return $0.category == category}), !ads.isEmpty else{
        return nil
    }
    return ads
}

func setAds(_ ads: Ads){
    globalAds = ads
    print("ads: \(globalAds?.description ?? "nil")")
}

typealias Ads = [Ad]

struct Ad: Codable {
    let id, version: Int
    let title: String?
    let category: String?
    let number: Int
    let url: String
    let image: String?
    let metr_image: String?
    let coefficient: Int
    var dictionary: [String: Any] {
        return (try? JSONSerialization.jsonObject(with: JSONEncoder().encode(self))) as? [String: Any] ?? [:]
    }
}
