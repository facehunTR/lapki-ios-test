//
//  NetworkObjects.swift
//  Lapki
//
//  Created by Denis Dolgopolov on 12/04/2019.
//  Copyright © 2019 MEGAKOM, OOO. All rights reserved.
//

import Foundation
/**
 Printing description of result:
 ▿ Optional<Dictionary<String, Any>>
 ▿ some : 11 elements
 ▿ 0 : 2 elements
 - key : "form_url"
 - value : https://securepayments.sberbank.ru/payment/merchants/sbersafe/payment_ru.html?mdOrder=59e08eba-fdf0-7f72-a0d9-cfc100166e6d
 ▿ 1 : 2 elements
 - key : "ad"
 - value : 594
 ▿ 2 : 2 elements
 - key : "date"
 - value : 2019-04-17T14:48:21.573502
 ▿ 3 : 2 elements
 - key : "order_reason"
 - value : uplifthighlight
 ▿ 4 : 2 elements
 - key : "bybonuses"
 - value : 0
 ▿ 5 : 2 elements
 - key : "order_id"
 - value : 921
 ▿ 6 : 2 elements
 - key : "order_number"
 - value : 59e08eba-fdf0-7f72-a0d9-cfc100166e6d
 ▿ 7 : 2 elements
 - key : "success"
 - value : <null>
 ▿ 8 : 2 elements
 - key : "user"
 - value : 267
 ▿ 9 : 2 elements
 - key : "error_message"
 - value : <null>
 ▿ 10 : 2 elements
 - key : "amount"
 - value : 22500
 (lldb)
 **/
struct CreatedOrder: Codable, ResponseWithStatus{
    
    var statusCode: Int?
    let formUrl: String?
    let detail: String?
    let success: Bool?
    let orderReason: String?
    let amount: Int?
    let orderId: Int?
    enum CodingKeys: String, CodingKey {
        case statusCode
        case formUrl = "form_url"
        case detail
        case success
        case orderId = "order_id"
        case amount
        case orderReason = "order_reason"
    }
}

protocol ResponseWithStatus {
    var statusCode: Int? {get set}
}
struct LapkiResponse: Codable {
    let count: Int
}
struct DetailedResponse: Codable, ResponseWithStatus {
    let detail: String
    var statusCode: Int?
}
struct RegisterResponse: Codable,ResponseWithStatus {
    let detail: String
    var statusCode: Int?
}
struct UsedBonus: Codable {
    let ad: Int
    let reason: String
    let amount: Int
    enum CodingKeys: String, CodingKey {
        case ad
        case reason = "order_reason"
        case amount
    }
}
