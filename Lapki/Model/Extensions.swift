//
//  Extensions.swift
//  Lapki
//
//  Created by Denis Dolgopolov on 01/02/2019.
//  Copyright © 2019 MEGAKOM, OOO. All rights reserved.
//
import UIKit
import Foundation
import MaterialComponents.MDCAlertController
import Toast_Swift
import FlagKit

typealias CompletionHandler = (_ success:Bool) -> Void
typealias TokenHandler = (_ token:String?) -> Void

extension UIColor {
    struct lapkiColors {
        static let orange =  UIColor(red: 0xF6/0xFF, green: 0x4F/0xFF, blue: 0x2B/0xFF, alpha: 1.0)
        static let gradientStart = UIColor(red: 0xF2/0xFF, green: 0x0C/0xFF, blue: 0x0C/0xFF, alpha: 1.0)
        static let gradientEnd =  UIColor(red: 0xF8/0xFF, green: 0xCD/0xFF, blue: 0x65/0xFF, alpha: 1.0)
        static let red = UIColor(red: 0xE9/0xFF, green: 0x1A/0xFF, blue: 0x1A/0xFF, alpha: 1.0)
        static let green = UIColor(red: 0x1B/0xFF, green: 0xBA/0xFF, blue: 0x31/0xFF, alpha: 1.0)
        static let lostOrange = UIColor(red: 0xFF/0xFF, green: 0x59/0xFF, blue: 0x00/0xFF, alpha: 1.0)
        static let grayText = UIColor(red: 0x66/0xFF, green: 0x66/0xFF, blue: 0x66/0xFF, alpha: 1.0)
        static let lightGray = UIColor(red: 0xCC/0xFF, green:  0xCC/0xFF, blue:  0xCC/0xFF, alpha: 1.0)
    }
    struct inkColors {
        static let orange = UIColor(red: 0xF6/0xFF, green: 0x4F/0xFF, blue: 0x2B/0xFF, alpha: 0.26)
        static let white = UIColor.white.withAlphaComponent(0.26)
        
    }
}
var russiaPaths = russiaPolygons()

func isRussia(lat: Double,lng: Double) -> Bool {
    guard let russiaPaths = russiaPaths else {
        return false
    }
    for path in russiaPaths{
        if path.contains(CGPoint(x:lng , y: lat)) {return true}
    }
    return false
}

func russiaPolygons() -> [UIBezierPath]?{
    var paths = [UIBezierPath]()
    guard let russia = parseGeoJSON("russia") else{
        return nil
    }
    paths.append(contentsOf: russia)
    return paths
}

func parseGeoJSON(_ name: String) -> [UIBezierPath]?{
    let fileURL = Bundle.main.url(forResource: name, withExtension: "geojson")
    if let data = try? Data(contentsOf: fileURL!){
        do{
            let collection = try JSONDecoder().decode(GeoJSON.Collection.self, from: data)
            return collection.toBeziers()
        }catch{
            print(error)
            do{
                let feature = try JSONDecoder().decode(GeoJSON.Feature.self, from: data)
                return [feature.toBezier()]
            }catch{
                print(error)
                return nil
            }
        }    
    }else{
        return nil
    }
}
extension Sequence where Iterator.Element: SignedNumeric & Comparable {
    /// Finds the nearest (offset, element) to the specified element.
    func nearestOffsetAndElement(to toElement: Iterator.Element) -> (offset: Int, element: Iterator.Element) {
        
        guard let nearest = enumerated().min( by: {
            let left = $0.1 - toElement
            let right = $1.1 - toElement
            return abs(left) <= abs(right)
        } ) else {
            return (offset: 0, element: toElement)
        }
        
        return nearest
    }
    
    func nearestElement(to element: Iterator.Element) -> Iterator.Element {
        return nearestOffsetAndElement(to: element).element
    }
    
    func indexOfNearestElement(to element: Iterator.Element) -> Int {
        return nearestOffsetAndElement(to: element).offset
    }
}
func getPojoImage(name: String) -> UIImage{
    switch name{
    case "Кошки": return UIImage.Category.cat.image
    case "Собаки": return UIImage.Category.dog.image
    case "Птицы": return UIImage.Category.bird.image
    case "Грызуны": return UIImage.Category.hamster.image
    case "Рептилии": return UIImage.Category.reptile.image
    case "Амфибии": return UIImage.Category.frog.image
    case "Насекомые": return UIImage.Category.bug.image
    case "Экзотика": return UIImage.Category.exotic.image
    case "Лошади и пони": return UIImage.Category.horse.image
    case "Морские рыбки": return UIImage.Category.seafish.image
    case "Аквариумные рыбки": return UIImage.Category.fish.image
    case "Беспозвоночные и кораллы": return UIImage.Category.shrimp.image
    case "Продажа": return UIImage.Category.trade.image
    case "Потеряшка": return UIImage.Category.lost.image
    case "Находка": return UIImage.Category.found.image
    case "Вязка (Случка)": return UIImage.Category.love.image
    case "Зоотовары": return UIImage.Category.accessories.image
    case "Услуги": return UIImage.Category.services.image
    case "Для бизнеса": return UIImage.Category.business.image
    case "Мероприятия": return UIImage.Category.medal.image
    default: return UIImage()
    }
}

enum Countries:String{
    case RU = "Россия"
    case KZ = "Казахстан"
    case MD = "Молдавия"
    case AM = "Армения"
    case BY = "Беларусь"
    case UA = "Украина"
    case TJ = "Таджикистан"
    case TM = "Туркменистан"
    case AZ = "Азербайджан"
    case GE = "Грузия"
    case KG = "Киргизия"
    case UZ = "Узбекистан"
    
    var origin:String{
        switch self {
        case .RU:
            return "Россия"
        case .KZ:
            return "Қазақстан"
        case .MD:
            return "Moldova"
        case .AM:
            return "Հայաստան"
        case .BY:
            return "Беларусь"
        case .UA:
            return "Україна"
        case .TJ:
            return "Тоҷикистон"
        case .TM:
            return "Türkmenistan"
        case .AZ:
            return "Azərbaycan"
        case .GE:
            return "საქართველო"
        case .KG:
            return "Кыргызстан"
        case .UZ:
            return "Oʻzbekiston"
        }
    }
    var flag:UIImage{
        return Flag(countryCode:"\(self)")!.originalImage
    }
        
    }



extension UIImage {
    
    enum Category:String{
        case dog = "dog_gray_icon"
        case cat = "cat_gray_icon"
        case bird = "bird_gray_icon"
        case hamster = "hamster_gray_icon"
        case reptile = "reptile_gray_icon"
        case frog = "frog_grey_icon"
        case bug = "bug_gray_icon"
        case exotic = "exotic_gray_icon"
        case horse = "horse_gray_icon"
        case seafish = "seafish_icon_gray"
        case fish = "fish_gray_icon"
        case shrimp = "shrimp_icon_gray"
        case trade = "trade_icon_gray"
        case lost = "lost_icon_gray"
        case found = "found_icon_gray"
        case love = "love_icon_gray"
        case accessories = "accessories_icon_gray"
        case services = "services_icon_gray"
        case business = "business"
        case medal = "medal"
        case folder = "categories_gray_icon"
        var image:UIImage{
            return UIImage(named:self.rawValue) ?? UIImage()
        }
    }
    
    static let animalsImages = [
        UIImage(),
        UIImage.Category.dog.image,
        UIImage.Category.cat.image,
        UIImage.Category.bird.image,
        UIImage.Category.hamster.image,
        UIImage.Category.reptile.image,
        UIImage.Category.frog.image,
        UIImage.Category.bug.image,
        UIImage.Category.exotic.image,
        UIImage.Category.horse.image,
        UIImage.Category.seafish.image,
        UIImage.Category.fish.image,
        UIImage.Category.shrimp.image
    ]
    static let lostAnimalsImages = [
        UIImage(),
        UIImage.Category.dog.image,
        UIImage.Category.cat.image,
        UIImage.Category.bird.image,
        UIImage.Category.hamster.image,
        UIImage.Category.reptile.image,
        UIImage.Category.exotic.image,
        UIImage.Category.horse.image
    ]
    static let categoriesImages = [
        UIImage(),
        UIImage.Category.trade.image,
        UIImage.Category.lost.image,
        UIImage.Category.found.image,
        UIImage.Category.love.image,
        UIImage.Category.accessories.image,
        //        UIImage.category.found.image,
        UIImage.Category.services.image,
        UIImage.Category.business.image,
        UIImage.Category.medal.image
    ]
}

public extension UIView {
    
    func roundCorners(corners: CACornerMask, cornerRadius: CGFloat){
        
        if #available(iOS 11.0, *) {
            self.layer.maskedCorners = corners
            self.layer.cornerRadius = cornerRadius
            self.layer.masksToBounds = true
        } else {
            var cornerMask = UIRectCorner()
            if(corners.contains(.layerMinXMinYCorner)){
                cornerMask.insert(.topLeft)
            }
            if(corners.contains(.layerMaxXMinYCorner)){
                cornerMask.insert(.topRight)
            }
            if(corners.contains(.layerMinXMaxYCorner)){
                cornerMask.insert(.bottomLeft)
            }
            if(corners.contains(.layerMaxXMaxYCorner)){
                cornerMask.insert(.bottomRight)
            }
            let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: cornerMask, cornerRadii: CGSize(width: cornerRadius, height: cornerRadius))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            self.layer.mask = mask
        }
    }
    
    private static let kLayerNameGradientBorder = "GradientBorderLayer"
    
    func setGradientBorder(
        width: CGFloat,
        colors: [UIColor],
        startPoint: CGPoint = CGPoint(x: 0.5, y: 0),
        endPoint: CGPoint = CGPoint(x: 0.5, y: 1)
        ) {
        let existedBorder = gradientBorderLayer()
        let border = existedBorder ?? CAGradientLayer()
        border.frame = self.bounds
        border.cornerRadius = 8
        border.colors = colors.map { return $0.cgColor }
        border.startPoint = startPoint
        border.endPoint = endPoint
        border.name = UIView.kLayerNameGradientBorder
        
        let mask = CAShapeLayer()
        mask.path = UIBezierPath(roundedRect: self.bounds, cornerRadius: 8).cgPath
        mask.fillColor = UIColor.clear.cgColor
        mask.strokeColor = UIColor.white.cgColor
        mask.lineWidth = width
        border.mask = mask
        
        let exists = existedBorder != nil
        if !exists {
            layer.addSublayer(border)
        }
    }
    
    func removeGradientBorder() {
        self.gradientBorderLayer()?.removeFromSuperlayer()
    }
    
    private func gradientBorderLayer() -> CAGradientLayer? {
        let borderLayers = layer.sublayers?.filter { return $0.name == UIView.kLayerNameGradientBorder }
        if borderLayers?.count ?? 0 > 1 {
            fatalError()
        }
        return borderLayers?.first as? CAGradientLayer
    }
}
extension Dictionary where Key == String, Value == Any? {
    func contains(oneOf: [String]) -> Bool{
        for key in oneOf{
            if self[key] != nil {return true}
        }
        return false
    }
    func toDataDict() -> [String:Data]{
        return (self as Dictionary<String,Any?>).compactMapValues({
            var res: Data?
            switch $0 {
            case nil: res = nil
            case let valueString as String: res = valueString.data(using: .utf8)
            case let valueInt as Int: res = "\(valueInt)".data(using: .utf8)
            case let valueDouble as Double: res = "\(valueDouble)".data(using: .utf8)
            case let valueFloat as Float: res = "\(valueFloat)".data(using: .utf8)
            case let valueImage as UIImage:
                var q: CGFloat = 1.0
                var data = (valueImage.jpegData(compressionQuality: q) ?? Data()) as NSData
                while (Double(data.length) / 1024.0) > 1500 && q > 0.1 {
                    q -= 0.1
                    data = valueImage.jpegData(compressionQuality: q)! as NSData
                }
                res = data as Data
            case let valueBool as Bool: res = ((valueBool) ? "True" : "False").data(using: .utf8)
            default: res = withUnsafeBytes(of: $0) {Data($0)}
            }
            return res ?? Data()
        })
    }
    func toParametersDict() -> [String:String]{
        var dc = [String:String]()
        let dict = self as Dictionary<String,Any?>
        dc = dict.mapValues({
            var res: String
            switch $0 {
            case let valueString as String:res = valueString
            case let valueInt as Int: res = "\(valueInt)"
            case let valueDouble as Double: res = "\(valueDouble)"
            case let valueCGFloat as CGFloat: res = "\(valueCGFloat)"
            case let valueBool as Bool: res = (valueBool) ? "True" : "False"
            case let valueFloat as Float: res = "\(valueFloat)"
            default: res = ""
            }
            return res
        })
        dc = dc.filter({return $0.value != ""})
        return dc
    }
    func compare(oldDict: [String:Any?]) -> [String:Any?]{
        var difference = [String:Any?]()
        for (key,value) in self{
            var equal = false
            equal = isEqual(type: Int.self, a: value, b: oldDict[key] as Any?)
                || isEqual(type: String.self, a: value, b: oldDict[key] as Any?)
                || isEqual(type: CGFloat.self, a: value, b: oldDict[key] as Any?)
                || isEqual(type: Bool.self, a: value, b: oldDict[key] as Any?)
            if !equal {
                difference[key] = value
            }
        }
        return difference
    }
}
private func isEqual<T: Equatable>(type: T.Type, a: Any?, b: Any?) -> Bool {
    guard let a = a as? T, let b = b as? T else { return false }
    return a == b
}
extension UITextField{
    var isTextEmpty: Bool{
        guard let text = self.text else {
            return true
        }
        return text.isEmpty
    }
    func isTextEmptyOrContains(_ oneOf: [String]) -> Bool{
        guard let text = self.text, !text.isEmpty else {
            return true
        }
        return text.contains(oneOf: oneOf)
    }
    func isTextEmptyOrContains(_ string: String) -> Bool{
        guard let text = self.text, !text.isEmpty else {
            return true
        }
        return text.contains(string)
    }
}
extension UILabel{
    var isTextEmpty: Bool{
        guard let text = self.text else {
            return true
        }
        return text.isEmpty
    }
    func isTextEmptyOrContains(_ oneOf: [String]) -> Bool{
        guard let text = self.text, !text.isEmpty else {
            return true
        }
        return text.contains(oneOf: oneOf)
    }
    func isTextEmptyOrContains(_ string: String) -> Bool{
        guard let text = self.text, !text.isEmpty else {
            return true
        }
        return text.contains(string)
    }
}
func isAnyEqual(a: Any?,b: Any?) -> Bool{
    var equal = false
    equal = isEqual(type: Int.self, a: a, b: b)
        || isEqual(type: String.self, a: a, b: b)
        || isEqual(type: CGFloat.self, a: a, b: b)
        || isEqual(type: Bool.self, a: a, b: b)
    return equal
}
extension StringProtocol where Index == String.Index {
    func index(of string: Self, options: String.CompareOptions = []) -> Index? {
        return range(of: string, options: options)?.lowerBound
    }
    func endIndex(of string: Self, options: String.CompareOptions = []) -> Index? {
        return range(of: string, options: options)?.upperBound
    }
    func contains(oneOf: [String]) -> Bool{
        var result = false
        for s in oneOf{
            if self.contains(s) {
                result = true
            }
        }
        return result
    }
    func indexes(of string: Self, options: String.CompareOptions = []) -> [Index] {
        var result: [Index] = []
        var start = startIndex
        while start < endIndex,
            let range = self[start..<endIndex].range(of: string, options: options) {
                result.append(range.lowerBound)
                start = range.lowerBound < range.upperBound ? range.upperBound :
                    index(range.lowerBound, offsetBy: 1, limitedBy: endIndex) ?? endIndex
        }
        return result
    }
    func ranges(of string: Self, options: String.CompareOptions = []) -> [Range<Index>] {
        var result: [Range<Index>] = []
        var start = startIndex
        while start < endIndex,
            let range = self[start..<endIndex].range(of: string, options: options) {
                result.append(range)
                start = range.lowerBound < range.upperBound ? range.upperBound :
                    index(range.lowerBound, offsetBy: 1, limitedBy: endIndex) ?? endIndex
        }
        return result
    }
}
extension UIFont {
    enum LapkiFonts {
        static let LatoRegular = UIFont(name: "Lato-Regular", size: 16)!
        static let LatoSemibold = UIFont(name: "Lato-Semibold", size: 16)!
        static let LatoBold = UIFont(name: "Lato-Bold", size: 16)!
        static let LatoLight = UIFont(name: "Lato-Light", size: 16)!
    }
}
public func createAlertController(title: String?,message: String?) -> MDCAlertController {
    let alert = MDCAlertController(title: title, message: message)
    alert.scrimColor = UIColor.black.withAlphaComponent(0.6)
    alert.backgroundColor = UIColor.white
    alert.buttonFont = UIFont.LapkiFonts.LatoLight
    alert.buttonInkColor = UIColor.inkColors.orange
    alert.buttonTitleColor = UIColor.lapkiColors.lostOrange
    alert.cornerRadius = 8
    alert.messageColor = UIColor.lapkiColors.grayText
    alert.messageFont = UIFont.LapkiFonts.LatoRegular.withSize(15)
    alert.titleColor = UIColor.lapkiColors.grayText
    alert.titleFont = UIFont.LapkiFonts.LatoSemibold.withSize(18)
    alert.restorationIdentifier = "AlertController"
    return alert
}

extension Int{
    func toStringKm() -> String{
        if self > 999{
            let km = self / 1000
            return ("До " + String(km) + " км")
        }
        else{
            return ("До " + String(self) + " м")
        }
    }
    func daysToMonthsAndYears() -> String{
        let age__ltText:[String] = [" ","1 недели","2 недель","месяца","2 месяцев","3 месяцев","полугода","9 месяцев","года","2 лет","4 лет"," "]
        let ageValue:[Int?] = [nil,7,14,30,60,90,180,270,365,730,1460,nil]
        return age__ltText[ageValue.firstIndex(of: self)!]
    }
}

extension UIViewController {
    func handleResponseError(_ error: Error){
        print("\(self.debugDescription): \(error)")
    }
    func setTabBarHidden(_ hidden: Bool, animated: Bool = true, duration: TimeInterval = 0.5) {
        if self.tabBarController?.tabBar.isHidden != hidden{
            if animated {
                //Show the tabbar before the animation in case it has to appear
                if (self.tabBarController?.tabBar.isHidden)!{
                    self.tabBarController?.tabBar.isHidden = hidden
                }
                if let frame = self.tabBarController?.tabBar.frame {
                    let factor: CGFloat = hidden ? 1 : -1
                    let y = frame.origin.y + (frame.size.height * factor)
                    UIView.animate(withDuration: duration, animations: {
                        self.tabBarController?.tabBar.frame = CGRect(x: frame.origin.x, y: y, width: frame.width, height: frame.height)
                    }) { (bool) in
                        //hide the tabbar after the animation in case ti has to be hidden
                        if (!(self.tabBarController?.tabBar.isHidden)!){
                            self.tabBarController?.tabBar.isHidden = hidden
                        }
                    }
                }
            }
        }
    }

            

    
    func addLineToView(view : UIView, position : LINE_POSITION, color: UIColor, width: Double) {
        let lineView = UIView()
        lineView.backgroundColor = color
        lineView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lineView)
        
        let metrics = ["width" : NSNumber(value: width)]
        let views = ["lineView" : lineView]
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lineView]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
        
        switch position {
        case .LINE_POSITION_TOP:
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lineView(width)]", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        case .LINE_POSITION_BOTTOM:
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[lineView(width)]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        }
    }
    
    enum LINE_POSITION {
        case LINE_POSITION_TOP
        case LINE_POSITION_BOTTOM
    }
}
func makeToast(_ message: String, position: ToastPosition){
    UIApplication.shared.keyWindow?.rootViewController?.view.makeToast(message,position: position)
}
func makeToast(_ message: String, position: ToastPosition, completion: CompletionHandler?){
    UIApplication.shared.keyWindow?.rootViewController?.view.makeToast(message,position: position,completion: completion)
}
extension UIApplication {
    
    static func topViewController(base: UIViewController? = UIApplication.shared.delegate?.window??.rootViewController) -> UIViewController? {
        
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        
        if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
            return topViewController(base: selected)
        }
        
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        
        return base
    }
}
// Errors pretty printed)
extension String.StringInterpolation {
    mutating func appendInterpolation(_ error: Error) {
        if let err = error as? ResponseError {
            switch err {
            case ResponseError.notExpectedValue:
                appendLiteral("RESPONSE ERROR: Not expected values in response")
            case ResponseError.detailReturned(let detail):
                appendLiteral("RESPONSE DETAIL: \(detail)")
            case ResponseError.notJSON:
                appendLiteral("RESPONSE ERROR: response is not JSON")
            case ResponseError.withoutDetails:
                appendLiteral("RESPONSE ERROR: response without details")
            case ResponseError.emptyResponse:
                appendLiteral("RESPONSE ERROR: empty response")
            }
        }else{
            appendLiteral("ERROR: \(error.localizedDescription)")
        }
    }
}
extension UINavigationController {
    //Same function as "popViewController", but allow us to know when this function ends
    func popViewControllerWithHandler(completion: @escaping ()->()) {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        self.popViewController(animated: true)
        CATransaction.commit()
    }
    func pushViewController(viewController: UIViewController, completion: @escaping ()->()) {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        self.pushViewController(viewController, animated: true)
        CATransaction.commit()
    }
}
func getYearString(_ ageYear: Int) -> String?{
    switch (ageYear % 10){
    case let x where x<1:
        return nil
    case 1:
        return " \(ageYear) год"
    case 2,3,4:
        return " \(ageYear) года"
    default:
        return " \(ageYear) лет"
    }
}
func getMonthString(_ ageMonth: Int) -> String?{
    switch (ageMonth){
    case let x where (x<1 || x>12):
        return nil
    case 1:
        return " \(ageMonth) месяц"
    case 2,3,4:
        return " \(ageMonth) месяца"
    default:
        return " \(ageMonth) месяцев"
    }
}
func getWeekString(_ ageWeek: Int) -> String?{
    guard ageWeek > 0, ageWeek < 4 else {return nil}
    var weekString: String!
    if ageWeek == 1 {
        weekString = " \(ageWeek) неделя"
    }else{
        weekString = " \(ageWeek) недели"
    }
    return weekString
}
func ageFormat(year: Int = 0, month: Int = 0, week: Int = 0) -> String?{
    guard year > 0 || month > 0 || week > 0 else {
        return nil
    }
    let yearString = getYearString(year)
    let monthString = getMonthString(month)
    let weekString = getWeekString(week)
    if (year > 0 || month > 0), var weekString = weekString {
        weekString.insert(contentsOf: " и", at: weekString.startIndex)
    }
    return "Возраст: \(yearString ?? "")\(monthString ?? "")\(weekString ?? "")"
}
