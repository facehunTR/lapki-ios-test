//
//  geocoding.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 10/12/2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//

import Foundation

struct ReverseGeoData: Codable {
    let placeID, licence, osmType, osmID: String?
    let lat, lon, placeRank, category: String?
    let type, importance, addresstype: String?
    let name: String?
    let displayName: String?
    let address: Address?
    let boundingbox: [String?]?
    
    enum CodingKeys: String, CodingKey {
        case placeID = "place_id"
        case licence
        case osmType = "osm_type"
        case osmID = "osm_id"
        case lat, lon
        case placeRank = "place_rank"
        case category, type, importance, addresstype, name
        case displayName = "display_name"
        case address, boundingbox
    }
}

struct Address: Codable {
    let houseNumber, road, town, city: String?
    let county, stateDistrict, state, postcode: String?
    let country, countryCode: String?
    let region, village: String?
    let suburb: String?
    let pedestrian: String?
    var editedValue: String?
    enum CodingKeys: String, CodingKey {
        case houseNumber = "house_number"
        case road, town, city, county
        case stateDistrict = "state_district"
        case state, postcode, country
        case countryCode = "country_code"
        case region, village
        case suburb, pedestrian,editedValue
    }
}
typealias GeoDatas = [GeoData]

struct GeoData: Codable {
    var address: Address?
    let boundingbox: [String?]?
    let geoDataClass, displayName: String?
    let icon: String?
    let importance: Double?
    let lat, licence, lon, osmID: String?
    let osmType, placeID, type: String?
    let region: String?
    
    
    
    enum CodingKeys: String, CodingKey {
        case address = "address"
        case boundingbox = "boundingbox"
        case geoDataClass = "class"
        case displayName = "display_name"
        case icon = "icon"
        case importance = "importance"
        case lat = "lat"
        case licence = "licence"
        case lon = "lon"
        case osmID = "osm_id"
        case osmType = "osm_type"
        case placeID = "place_id"
        case type = "type"
        case region = "region"
    }
}
struct Dadata: Codable {
    
    var suggestions : [Suggestion]?
    var autoComplete : Bool?
}
struct Suggestion: Codable {
    let value, unrestrictedValue: String
    let data: Datam?
    var editedValue: String?
    
    enum CodingKeys: String, CodingKey {
        case value
        case unrestrictedValue = "unrestricted_value"
        case data
        case editedValue
    }
}
struct Datam : Codable {
    

    let city : String?
    let cityArea : String?
    let cityDistrict : String?
    let country : String?
    let geoLat : String?
    let geoLon : String?
    let kladrId : String?
    let postalCode : String?
    let region : String?
    let street : String?
    let house: String?
    let houseType: String?
    let block: String?
    let blockType: String?
    let settlement: String?
    
    
    enum CodingKeys: String, CodingKey {
        case city = "city_with_type"
        case cityArea = "city_area"
        case cityDistrict = "city_district"
        case country = "country"
        case geoLat = "geo_lat"
        case geoLon = "geo_lon"
        case kladrId = "kladr_id"
        case postalCode = "postal_code"
        case region = "region_with_type"
        case street = "street_with_type"
        case block
        case blockType = "block_type"
        case settlement = "settlement_with_type"
        case house
        case houseType = "house_type"
        
}
}



