//
//  NoticeCollection.swift
//  Lapki
//
//  Created by Denis Dolgopolov on 04/03/2019.
//  Copyright © 2019 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import CoreLocation

protocol NoticeCollectionDelegate {
    var noticeArray: Notices { get set }
    func didSelectItem(indexPath: IndexPath)
    var nextPage: String? {get set}
    var indicator: UIActivityIndicatorView? {get set}
    var lastLocation: CLLocation? {get set}
    func refresh(_ sender: AnyObject?)
}

class NoticeCollection: UICollectionView {
    var selectedCellIndex: IndexPath = IndexPath(row: -1, section: 0)
    var isWaiting: Bool = false
    var parentVC: NoticeCollectionDelegate!
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        initialSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialSetup()
    }
    func initialSetup(){
        self.delegate = self
        self.dataSource = self
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.tintColor = UIColor.lapkiColors.lostOrange
        self.refreshControl?.addTarget(self, action: #selector(self.refresh(_ :)), for: UIControl.Event.valueChanged)
    }
    func doPaging(){
        guard let nextPage = self.parentVC.nextPage else {
            self.isWaiting = false
            return}
        Network.getNextPage(url: nextPage, responseHandler: weakCompletion(of: self, onResult: NoticeCollection.nextPageSuccess, onError: NoticeCollection.nextPageError))
    }
    func nextPageError(_ error: Error){
        (self.parentVC as? UIViewController)?.handleResponseError(error)
        self.isWaiting = false
    }
    func nextPageSuccess(_ page: PageResult){
        self.parentVC.nextPage = page.next
        self.parentVC.noticeArray.append(contentsOf: page.results)
        self.performBatchUpdates({
            self.reloadSections(IndexSet(integer: 0))
        }, completion: { (success) in
            self.isWaiting = false
        })
    }
    @objc func refresh(_ sender: AnyObject?){
        parentVC.refresh(sender)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        parentVC.didSelectItem(indexPath: indexPath)
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if parentVC.noticeArray.count > 40 {
            if !(indexPath.row < parentVC.noticeArray.count-20) && !isWaiting{
                isWaiting = true
                self.doPaging()
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath) {
        if parentVC.nextPage == nil {
            view.removeFromSuperview()
        }else{
            parentVC.indicator?.startAnimating()
        }
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var v = UICollectionReusableView()
        let footer = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "footerView", for: indexPath)
        parentVC.indicator = UIActivityIndicatorView()
        parentVC.indicator!.color = .black
        parentVC.indicator!.translatesAutoresizingMaskIntoConstraints = false
        parentVC.indicator!.hidesWhenStopped = true
        footer.addSubview(parentVC.indicator!)
        parentVC.indicator!.centerXAnchor.constraint(equalTo: footer.centerXAnchor).isActive = true
        parentVC.indicator!.centerYAnchor.constraint(equalTo: footer.centerYAnchor).isActive = true
        v = footer
        return v
    }
    
}
extension NoticeCollection: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 6.0, left: 4, bottom: 0, right: 4)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let bounds = collectionView.bounds
        if indexPath == self.selectedCellIndex {
            return CGSize(width: bounds.width-15, height: bounds.width+48.5)
        }
        return CGSize(width: bounds.width/2-7.5, height: bounds.width/2+48.5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 7.5
    }
}
extension NoticeCollection: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let notice = parentVC.noticeArray[indexPath.row]
        if let noticeCell =  collectionView.dequeueReusableCell(withReuseIdentifier: "simpleNoticeCell", for: indexPath) as? NewSimpleNoticeCell{
            noticeCell.triangleIconImageView.isHidden = false
            noticeCell.triangleView.isHidden = false
            noticeCell.triangleView.roundCorners(corners: [.layerMinXMinYCorner], cornerRadius: 5.8)
            noticeCell.triangleView.tintColor = UIColor.lapkiColors.green
            noticeCell.triangleIconImageView.image = UIImage.init(named:"triangleLapka" )
            if notice.category == "Потеряшка"{
                if notice.price >= 1 {
                    noticeCell.triangleView.tintColor = UIColor.lapkiColors.red
                    noticeCell.triangleIconImageView.image = UIImage.init(named:"ruble" )
                    noticeCell.triangleIconImageView.tintColor = UIColor.white
                }else{
                    noticeCell.triangleView.tintColor = UIColor.lapkiColors.lostOrange
                    noticeCell.triangleIconImageView.image = UIImage.init(named:"triangleLapka" )
                    noticeCell.triangleIconImageView.tintColor = UIColor.white
                    
                }
            }
            else if notice.category != "Находка"{
                noticeCell.triangleIconImageView.isHidden = true
                noticeCell.triangleView.isHidden = true
            }
            
            if notice.highlight == true{
                noticeCell.setGradientBorder(width: 10, colors: [UIColor.lapkiColors.gradientStart,UIColor.lapkiColors.gradientEnd],startPoint: CGPoint(x: 0.5, y: 0),endPoint: CGPoint(x: 0.5, y: 1))
            }
            else{
                noticeCell.removeGradientBorder()
            }
            if parentVC.lastLocation != nil {
                noticeCell.location = parentVC.lastLocation
            }
            noticeCell.notice = notice
            
            noticeCell.layer.shouldRasterize = true
            noticeCell.layer.rasterizationScale = UIScreen.main.scale
            noticeCell.parentView = parentVC as? UIViewController
            return noticeCell
            
        }
        
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return parentVC.noticeArray.count
    }
}
