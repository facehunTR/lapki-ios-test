// To parse the JSON, add this file to your project and do:
//
//   v

import Foundation
import CoreLocation

typealias Notices = [Notice]

struct PageResult: Codable {
    let count: Int
    let next, previous: String?
    let results: Notices
}

extension DateFormatter {
    func date(fromPythonDate dateString:String) -> Date? {
        self.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS"
        self.timeZone = TimeZone(abbreviation: "UTC")
        self.locale = Locale(identifier: "en_US_POSIX")
        return self.date(from: dateString)
    }
}

struct Notice: Codable {
    let id, owner: Int
    let title: String
    let gender: String?
    let description: String
    let price: Int
    let locationAddress: String
    var date: String 
    let oldWeek, oldMonth, oldYear: Int
    let lat, lng: Double
    let category, type: String
    let subtype, subsubtype: String?
    let thumbnail: String?
    let image1: String?
    let image2, image3: String?
    let image4, image5: String?
    let age: Int
    let status: String
    let reasonRejection: String?
    let point: Point
    let highlight: Bool?
    var distance: Int?
    let views: Int?
    let inFavorites: Int?
    var location: CLLocation? {
        if self.lat == 0 || self.lng == 0 {
            return nil
        }
        let loc = CLLocation(latitude: self.lat, longitude: self.lng)
        return loc
    }
    
    var dictionary: [String: Any?] {
        return (try? JSONSerialization.jsonObject(with: JSONEncoder().encode(self))) as? [String: Any?] ?? [:]
    }
    
    lazy var images: [String?] = [self.image1,self.image2,self.image3,self.image4,self.image5]
    
    
    func compare(_ defaultNotice: Notice) -> [String:String?]{
        var parameters = [String:String?]()
        for (key,value) in self.dictionary {
            if value == nil{
                if defaultNotice.dictionary[key] != nil{
                    parameters[key] = nil
                }
            }else {
                if value! is String {
                    if !(value! as! String).elementsEqual((defaultNotice.dictionary[key] as? String) ?? "") {
                        parameters[key] = value! as? String
                    }
                }else if value! is Int{
                    if (value as! Int) != (defaultNotice.dictionary[key] as? Int) {
                        parameters[key] = "\(value!)"
                    }
                }else if value! is Double {
                    if (value! as! Double) != (defaultNotice.dictionary[key] as? Double) {
                        parameters[key] = "\(value!)"
                    }
                }
            }
            
        }
        return parameters
    }
    func toEditNotice() -> EditNotice{
        let data = try! JSONEncoder().encode(self)
        return try! JSONDecoder().decode(EditNotice.self, from: data)
    }
    
    enum CodingKeys: String, CodingKey {
        case id, owner, title, gender, description, price
        case locationAddress = "location_address"
        case date
        case oldWeek = "old_week"
        case oldMonth = "old_month"
        case oldYear = "old_year"
        case lat, lng, category, type, subtype, subsubtype, thumbnail, image1, image2, image3, image4, image5, age, status
        case highlight
        case reasonRejection = "reason_rejection"
        case point
        case distance
        case views
        case inFavorites = "in_favorites"
    }
}

struct EditNotice: Codable {
    var id:Int? = nil
    var owner: Int? = nil
    var title: String? = nil
    var gender: String? = nil
    var description: String? = nil
    var price: Int? = nil
    var locationAddress:String? = nil
    var date: String? = nil
    var oldWeek: Int? = nil
    var oldMonth: Int? = nil
    var oldYear: Int? = nil
    var lat: Double? = nil
    var lng: Double? = nil
    var category:String?  = nil
    var type: String? = nil
    var subtype: String? = nil
    var subsubtype: String? = nil
    var image1: String? = nil
    var image2: String? = nil
    var image3: String? = nil
    var image4: String? = nil
    var image5: String? = nil
    var age: Int? = nil
    var status: String? = nil
    var reasonRejection: String? = nil
    var point: Point? = nil
    lazy var location: CLLocation? = {
        if self.lat == 0 || self.lng == 0 {
            return nil
        }
        let loc = CLLocation(latitude: self.lat!, longitude: self.lng!)
        return loc
    }()
    lazy var images: [String?] = [self.image1,self.image2,self.image3,self.image4,self.image5]
    var dictionary: [String: Any?] {
        return (try? JSONSerialization.jsonObject(with: JSONEncoder().encode(self))) as? [String: Any?] ?? [:]
    }
    
    enum CodingKeys: String, CodingKey {
        case id, owner, title, gender, description, price
        case locationAddress = "location_address"
        case date
        case oldWeek = "old_week"
        case oldMonth = "old_month"
        case oldYear = "old_year"
        case lat, lng, category, type, subtype, subsubtype, image1, image2, image3, image4, image5, age, status
        case reasonRejection = "reason_rejection"
        case point
    }
    
    func compare(_ defaultNotice: EditNotice) -> [String:String?]{
        var parameters = [String:String?]()
        for (key,value) in self.dictionary {
            switch value{
            case nil:
                if defaultNotice.dictionary[key] != nil{
                parameters[key] = nil
                }
            case let stringValue as String:
                if !stringValue.elementsEqual((defaultNotice.dictionary[key] as? String) ?? "") {
                    parameters[key] = stringValue
                }
            case let intValue as Int:
                if intValue != (defaultNotice.dictionary[key] as? Int) {
                    parameters[key] = "\(intValue)"
                }
            case let doubleValue as Double:
                if doubleValue != (defaultNotice.dictionary[key] as? Double) {
                    parameters[key] = "\(doubleValue)"
                }
            default:
                break
            }
        }
        return parameters
    }
}

struct Point: Codable {
    let type: String
    let coordinates: [Double]
}

struct Type: Codable{
    let name : String
    let apiName: String
    let items: Array<Type>
}


