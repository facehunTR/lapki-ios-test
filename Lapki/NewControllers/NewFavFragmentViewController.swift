
//  NewFavFragmentViewController.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 27/10/2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import CoreLocation


class NewFavFragmentViewController: UIViewController{
    
    var selectedNotice: Notice?
    var refreshControl = UIRefreshControl()
    var noticeArray = Notices()
    var indicator: UIActivityIndicatorView?
    var parameters: Parameters = ["ordering":"-date_uplift"]
    
    var nextPage, previousPage: String?
    var isWaiting: Bool = false
    var lastLocation: CLLocation?{
        didSet{
            locationRequestBuilder()
        }
    }
    @IBOutlet weak var placeholderView: UIView!
    var myFavoritesPageViewController : NewFavoritesPageViewController!
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        (self.navigationController?.parent as? NewFavoritesPageViewController)?.segmentedControlP.selectedSegmentIndex = 0
        (self.navigationController?.parent as? NewFavoritesPageViewController)?.segmentedControlP.sendActions(for: UIControl.Event.valueChanged)
    }
    func locationRequestBuilder() {
        if lastLocation != nil {
            self.parameters["lat"] = String(lastLocation!.coordinate.latitude)
            self.parameters["lng"] =  String(lastLocation!.coordinate.longitude)
        }else if MyStaticUser.defaultLocation.longitude != 0 && MyStaticUser.defaultLocation.latitude != 0 {
            self.parameters["lat"] = String(MyStaticUser.defaultLocation.latitude)
            self.parameters["lng"] =  String(MyStaticUser.defaultLocation.longitude)
        }else{
            self.parameters.removeValue(forKey: "lat")
            self.parameters.removeValue(forKey: "lng")
        }
    }
    @objc func refreshWrapper(_ sender: AnyObject){
    self.refresh(sender)
    }
    @objc func somethingChanged(notification: Notification){
        self.lastLocation = lastUserLocation
        refresh(self)
    }
    @IBOutlet weak var noticeCollectionView: NoticeCollection!
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        noticeCollectionView.parentVC = self
        refreshControl.tintColor = UIColor.lapkiColors.lostOrange
        refreshControl.attributedTitle = nil
        refreshControl.addTarget(self, action: #selector(self.refreshWrapper(_:)), for: UIControl.Event.valueChanged)
        noticeCollectionView.refreshControl = refreshControl
        self.lastLocation = lastUserLocation
        refresh(self)
        
        NotificationCenter.default.addObserver(self, selector: #selector(somethingChanged(notification:)), name: Notification.Name("locationUpdated"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(somethingChanged(notification:)), name: Notification.Name("userUpdated"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshOneNotice(_:)), name: Notification.Name("favClicked"), object: nil)
        // Do any additional setup after loading the view.
    }
    
    func doPaging(){
        guard let nextPage = self.nextPage else {
            self.isWaiting = false
            return}
        Network.getNextPage(url: nextPage, responseHandler: weakCompletion(of: self, onResult: NewFavFragmentViewController.nextPageSuccess, onError: NewFavFragmentViewController.nextPageError))
    }
    func nextPageError(_ error: Error){
        self.handleResponseError(error)
        self.isWaiting = false
    }
    func nextPageSuccess(_ page: PageResult){
        self.nextPage = page.next
        self.noticeArray.append(contentsOf: page.results)
        self.noticeCollectionView.reloadData()
        self.isWaiting = false
    }
    @objc func refreshOneNotice(_ notification: NSNotification){
        guard let notice = notification.userInfo?["notice"] as? Notice else{
            return
        }
        if !self.noticeArray.contains(where: { (not) -> Bool in
            return not.id == notice.id
        }) {
            self.noticeArray.insert(notice, at: 0)
            self.noticeCollectionView.reloadData()
        }else{
            self.noticeArray.removeAll { (not) -> Bool in
                return not.id == notice.id
            }
            self.noticeCollectionView.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "NoticeDetailSegue"{
            let noticeDetail = segue.destination as! NewNoticeDetailViewController
            noticeDetail.notice = self.selectedNotice
            self.selectedNotice = nil
        }
    }
}
extension NewFavFragmentViewController: NoticeCollectionDelegate {
    func refresh(_ sender:AnyObject?) {
        guard MyStaticUser.id != 0 else {
            self.placeholderView.isHidden = false
            self.noticeArray = Notices()
            self.noticeCollectionView.reloadData()
            self.isWaiting = false
            self.noticeCollectionView.refreshControl?.endRefreshing()
            return
        }
        self.noticeCollectionView.refreshControl?.beginRefreshing()
        self.placeholderView.isHidden = true
        Network.getFavorites(parameters: parameters, responseHandler: weakCompletion(of: self, onResult: NewFavFragmentViewController.refreshSuccess, onError: NewFavFragmentViewController.refreshError))
    }
    func refreshSuccess(_ page: PageResult){
        self.nextPage = page.next
        self.noticeArray = page.results
        self.noticeCollectionView.performBatchUpdates({
            self.noticeCollectionView.reloadSections(IndexSet(integer: 0))
        }, completion: { (success) in
            self.noticeCollectionView.isWaiting = false
            self.noticeCollectionView.refreshControl?.endRefreshing()
        })
    }
    func refreshError(_ error: Error){
        self.noticeCollectionView.refreshControl?.endRefreshing()
        self.noticeCollectionView.isWaiting = false
        self.handleResponseError(error)
    }
    func didSelectItem(indexPath: IndexPath) {
        self.selectedNotice = self.noticeArray[indexPath.row]
        self.performSegue(withIdentifier: "NoticeDetailSegue", sender: self)
    }
}
