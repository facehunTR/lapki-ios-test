//
//  NewFilterViewController
//  Lapki
//
//  Created by Yuriy Yashchenko on 29/11/2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import MaterialComponents.MDCButton
import MaterialComponents.MaterialTextFields
import RangeSeekSlider
import SearchTextField
import CoreLocation


class NewFilterViewController: UIViewController, CategoriesDelegate, MyPickerViewBottomControllerDelegate,CountryPickerDelegate {
    
    @IBOutlet weak var orderingDistSwitch: UISwitch!
    
    @IBAction func orderingSwitch(_ sender: UISwitch) {
        if (parameters["lat"] == nil || parameters["lng"] == nil) && settedLocation == nil{
            sender.isOn = false
            makeToast("Укажите адрес для сортировки", position: .top)
        }
    }
    
    func result(outputName: String, result: String?) {
        self.parameters[outputName] = result
    }
    
    func set(category: String?, type: String?) {
        self.parameters["category"] = category
        self.parameters["type"] = type
        self.initCategoriesView()
        self.initSubTypeView()
        self.viewReorder()
    }
    
    var currentChosenCountry:Country = Country(){
        didSet{
            if currentChosenCountry.countryOnRu != nil{
                parameters["country"] = currentChosenCountry.countryOnRu
                countryText.text = currentChosenCountry.fullName
                countryImageView.image = currentChosenCountry.flag
            }else{
                countryText.text = "Укажите страну"
            }
        }
    }
    var settedLocation: CLLocation?
    var filterDelegate: FilterDelegate!
    var oldParameters = [String:Any?]()
    
    var parameters: [String:Any?]!{
        didSet{
            guard let subtype = parameters["subtype"] as? String else{
                subSubTypeCard.isHidden = true
                return}
            subTypeTF.text = subtype
            guard let row = subTypes.firstIndex(of: subtype), let category = parameters["category"] as? String, let type = parameters["type"] as? String, row > 0 else {
                parameters["subtype"] = nil
                return}
            if let subsubtypes = (self.tabBarController as? MainTabBarController)?.Categories.getSubsubtypesNames(category:category , type: type, row: row) as? [String]{
                subSubTF.filterStrings(subsubtypes)
                subSubTypeCard.isHidden = false
            }else{
                subSubTypeCard.isHidden = true
            }
        }
    }
    var subTypes: [String?] = [String]()
    let age__gtText:[String] = [" ","От 1 недели","От 2 недель","От месяца","От 2 месяцев","От 3 месяцев","От полугода","От 9 месяцев","От года","От 2 лет","От 4 лет"," "]
    let age__ltText:[String] = [" ","До 1 недели","До 2 недель","До месяца","До 2 месяцев","До 3 месяцев","До полугода","До 9 месяцев","До года","До 2 лет","До 4 лет"," "]
    let ageValue:[Int?] = [nil,7,14,30,60,90,180,270,365,730,1460,nil]
    let distText:[String] = ["До 1 километра","До 5 километров","До 10 километров","До 25 километров","До 50 километров","До 100 километров","До 200 километров","Без ограничений"]
    let distValue:[Int?] = [1000,5000,10000,25000,50000,100000,200000,nil]
    
    @IBOutlet weak var addressCard: MDCCard!
    
    @IBAction func addressClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "toAddressMap", sender: self)
    }
    
    @IBOutlet weak var countryImageView: UIImageView!
    @IBOutlet weak var countryText: UILabel!
    @IBOutlet weak var addressText: UILabel!
    @IBOutlet weak var priceCard: MDCCard!
    @IBOutlet weak var priceIcon: UIImageView!
    @IBOutlet weak var priceMinTF: UITextField!
    @IBOutlet weak var priceMaxTF: UITextField!
    @IBOutlet weak var ageCard: MDCCard!
    @IBOutlet weak var ageSlider: RangeSeekSlider!
    @IBOutlet weak var distCard: MDCCard!
    @IBOutlet weak var distSlider: RangeSeekSlider!
    @IBOutlet weak var subTypeTF: UILabel!
    @IBOutlet weak var subTypeCard: MDCCard!
    
    @IBAction func subTypeCardClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "PickerViewSegue", sender: sender)
    }
    
    @IBOutlet var subSubTypeCard: MDCCard!
    
    let distTag = 202
    let ageTag = 101
    var allTextFieldsControllers = [MDCTextInputControllerOutlined]()
    
    @IBOutlet weak var categoriesView: CategoriesView!
    
    @IBAction func categoriesCardClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "CategoriesSegue", sender: self)
    }
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var stackView: UIStackView!
    @IBOutlet var subSubTF: SearchTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true
        self.orderingDistSwitch.isOn = (self.parameters["ordering"] as? String)?.contains("dist") ?? false
        initScrollStackView()
        initCategoriesView()
        initPriceView()
        initAgeSlider()
        initDistSlider()
        initSubTypeView()
        initCountry()
    }
    
    func initCountry(){
        if let val = parameters["country"]{
            currentChosenCountry = Country(countryOnRu:val as? String)
        }
    }
    func initSubTypeView(){
        subSubTypeCard.isHidden = true
        subTypeCard.isHidden = true
        guard let pojo = (tabBarController as? MainTabBarController)?.Categories else{
            return
        }
        subSubTF.font = UIFont.LapkiFonts.LatoRegular
        subSubTF.backgroundColor = UIColor.white
        subSubTF.theme.bgColor = UIColor.white
        subSubTF.theme.font = UIFont.LapkiFonts.LatoRegular
        guard let type = parameters["type"] as? String, let category = parameters["category"] as? String,
            (!category.contains(oneOf: ["Находка","Услуги","Для бизнеса","Мероприятия"])) else {subTypeCard.isHidden = true
                subSubTypeCard.isHidden = true
                return
        }
        if let categoryId = pojo.names.firstIndex(of: category),
            let typeId = pojo.subs![categoryId]?.names.firstIndex(of: type),
            let subs = pojo.subs![categoryId]?.subs![typeId]?.subs,
            let subNames = pojo.subs![categoryId]?.subs![typeId]?.names{
            subTypes = subNames
            subTypeCard.isHidden = false
            guard let id = subTypes.firstIndex(of: self.parameters["subtype"] as? String) else {
                subTypeTF.text = subTypes[0]
                return
            }
            subTypeTF.text = subTypes[id]
            let subSubTypes = subs[id]!.names
            guard let id2 = subSubTypes.firstIndex(of: self.parameters["subsubtype"] as? String) else{return}
            subSubTypeCard.isHidden = false
            subSubTF.text = subSubTypes[id2]
        }
        else{
            subTypes = pojo.getSubtypesNames(category: category, type: type) ?? [String]()
            subTypeCard.isHidden = true
            subSubTypeCard.isHidden = false
            subSubTF.filterStrings(subTypes as! [String])
            subSubTF.placeholder = "Укажите породу"
        }
    }
    func initScrollStackView(){
        scrollView.addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor).isActive = true
        stackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor).isActive = true
        stackView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
        stackView.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
    }
    func initDistSlider(){
        distSlider.delegate = self
        distSlider.selectedMaxValue = CGFloat(distValue.lastIndex(of:parameters["dist"] as? Int) ?? 7)
    }
    
    func viewReorder() {
        if let category = parameters["category"] as? String{
            ageCard.isHidden = category.contains(oneOf: ["Находка","Зоотовары","Мероприятия","Услуги","Для бизнеса"])
            priceCard.isHidden = category.contains(oneOf: ["Вязка","Находка","Мероприятия"])
            subTypeCard.isHidden = (category.contains(oneOf: ["Зоотовары","Находка","Услуги","Для бизнеса","Мероприятия"]) || (parameters["type"] as? String)?.contains("Собаки") ?? false)
        }else{
            ageCard.isHidden = true
            priceCard.isHidden = false
        }
        subSubTypeCard.isHidden = !((self.parameters["type"] as? String)?.contains("Собаки") ?? false)
    }
    func initAgeSlider(){
        ageSlider.delegate = self
        ageSlider.selectedMinValue = CGFloat(ageValue.firstIndex(of: parameters["age__gt"] as? Int) ?? 0)
        ageSlider.selectedMaxValue = CGFloat(ageValue.lastIndex(of: parameters["age__lt"] as? Int) ?? 11)
    }
    func initPriceView() {
        if let price__gt = parameters["price__gt"] as? Int{
            priceMinTF.text = "\(price__gt - 1)"
        }
        if let price__lt = parameters["price__lt"] as? Int, price__lt > 0{
            priceMaxTF.text = "\(price__lt - 1)"
        }
    }
    func initCategoriesView(){
        guard let pojo = (tabBarController as? MainTabBarController)?.Categories else{return}
        categoriesView.set(pojo: pojo, category: parameters["category"] as? String, type: parameters["type"] as? String)
    }
    
    @IBAction func backButtonClick(_ sender: Any){
        if !priceMinTF.isTextEmpty || !priceMaxTF.isTextEmpty || parameters.contains(oneOf: ["type","subtype","age__lt","age__gt","dist","price__gt","price__lt","ordering"]) || !subSubTF.isTextEmptyOrContains("Укажите") || !addressText.text!.contains("Укажите"){
            let alert = createAlertController(title: "Вы уверены что хотите сбросить параметры фильтрации?", message: "Все указанные вами параметры будут заменены на стандартные.")
            alert.addAction(MDCAlertAction(title: "Да", handler: {action in
                self.filterDelegate.set(parameters: nil)
                self.navigationController?.popViewController(animated: true)
            }))
            alert.addAction(MDCAlertAction(title: "Отмена", handler: {action in
                alert.dismiss(animated: true, completion: nil)
            }))
            present(alert, animated: true, completion: nil)
        }else{
            filterDelegate.set(parameters: nil)
            navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func acceptButtonClick(_ sender: Any){
        if let price__lt = Int(priceMaxTF.text ?? ""), let price__gt = Int(priceMinTF.text ?? ""), price__gt>price__lt {
            makeToast("Минимальная цена не может быть большке максимальной", position: .top)
            return
        }
        if let pricemax = Int(priceMaxTF.text ?? "") {
            parameters["price__lt"] = pricemax + 1
        }else{
            parameters.removeValue(forKey: "price__lt")
        }
        if let pricemin = Int(priceMinTF.text ?? "") {
            parameters["price__gt"] = pricemin + 1
        }else{
            parameters.removeValue(forKey: "price__gt")
        }
        parameters["ordering"] = (orderingDistSwitch.isOn) ? "-dist" : "-date_uplift"
        parameters["subsubtype"] = (subSubTF.text?.contains("Укажите") ?? true) ? nil : subSubTF.text
        filterDelegate.set(parameters: parameters)
        filterDelegate.settedUserLocation =  settedLocation
        filterDelegate.settedAddress = (settedLocation != nil) ? self.addressText.text : nil
        self.navigationController?.popViewController(animated: true)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case let categoriesVC as NewCategoriesViewController:
            categoriesVC.pojo = (self.tabBarController as! MainTabBarController).Categories
            categoriesVC.checkedCategory = self.parameters["category"] as? String
            categoriesVC.checkedType = self.parameters["type"] as? String
            categoriesVC.mode = .Home
            categoriesVC.categoriesDelegate = self
        case let mapVC as NewAddNoticeMapViewController:
            if parameters["lat"] != nil && parameters["lng"] != nil &&
                addressText.text != "" && addressText.text != "Укажите адрес"{
                mapVC.addressStruct = PrevAddress(lat: parameters!["lat"] as? String, long: parameters["lng"] as? String, address: addressText.text)
            }
        case let pickerVC as MyPickerViewBottomController:
            guard let id = (sender as? UIView)?.restorationIdentifier else {return}
            pickerVC.data = subTypes
            pickerVC.selected = parameters["subtype"] as? String
            pickerVC.delegate = self
            pickerVC.outPutName = id
        case let countryVC as CountryPickerViewController:
            countryVC.countryDelegate = self
            countryVC.selected = currentChosenCountry.countryCode
            countryVC.mode = modeEnum.filter
        default: break
        }
    }
    
    @IBAction func countryTouchUpped(_ sender: Any) {
        self.performSegue(withIdentifier: "toCountryPicker", sender: self)
        
    }
    func set(country:Country){
        currentChosenCountry = country
    }
    
}

extension NewFilterViewController: UITextFieldDelegate{
    //    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    //        textField.resignFirstResponder();
    //
    //        // TextField
    //        let price__gt = priceMinTF.text
    //        let price__lt = priceMaxTF.text
    //        if ((textField == priceMaxTF || textField == priceMinTF) &&
    //            (price__gt != nil && price__lt != nil) && (!price__gt!.isEmpty && (price__lt?.isEmpty)!)) {
    //            if(price__gt!.compare(price__lt!, options: .numeric) == .orderedDescending){
    //                allTextFieldsControllers[1].setErrorText( "Мин. не может быть больше макс.", errorAccessibilityValue: nil)
    //            }else {
    //                allTextFieldsControllers[1].setErrorText(nil, errorAccessibilityValue: nil)
    //            }
    //        }else{
    //            allTextFieldsControllers[1].setErrorText(nil, errorAccessibilityValue: nil)
    //        }
    //
    //        return false
    //    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool{
        if textField == priceMaxTF || textField == priceMinTF{
            let maxLength = 8
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
}

extension NewFilterViewController: RangeSeekSliderDelegate {
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat){
        switch slider.tag {
        case ageTag:
            self.parameters["age__gt"] = ageValue[Int(minValue)]
            self.parameters["age__lt"] = ageValue[Int(maxValue)]
            break
        case distTag:
            self.parameters["dist"] = distValue[Int(maxValue)]
            break
        default:
            break
        }
    }
    func rangeSeekSlider(_ slider: RangeSeekSlider, stringForMinValue minValue: CGFloat) -> String?{
        switch slider.tag {
        case ageTag:
            return age__gtText[Int(minValue)]
        case distTag:
            return nil
        default:
            return nil
        }
    }
    func rangeSeekSlider(_ slider: RangeSeekSlider, stringForMaxValue maxValue: CGFloat) -> String?{
        switch slider.tag {
        case ageTag:
            return age__ltText[Int(maxValue)]
        case distTag:
            return distText[Int(maxValue)]
        default:
            return nil
        }
    }
}

extension NewFilterViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return subTypes.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return subTypes[row]
    }
}

protocol FilterDelegate{
    var settedUserLocation: CLLocation? {get set}
    var settedAddress: String? {get set}
    func set(parameters: [String:Any?]?)
}
