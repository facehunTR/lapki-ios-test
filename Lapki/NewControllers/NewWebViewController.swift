//
//  NewWebViewController.swift
//  Lapki
//
//  Created by Denis Dolgopolov on 27/02/2019.
//  Copyright © 2019 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import WebKit
import MaterialComponents.MaterialButtons

class NewWebViewController: UIViewController, WKNavigationDelegate {
    let progressView = UIProgressView(progressViewStyle: .default)
    private var estimatedProgressObserver: NSKeyValueObservation?
    var urlString: String!
    var titleString: String?
    var delegate: WebViewResultDelegate?
    var webView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.title = titleString
        initProgressView()
        webView = WKWebView()
        self.view.addSubview(webView)
        initEstimatedProgressObserver()
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        webView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        webView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        webView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        webView.allowsLinkPreview = false
        webView.allowsBackForwardNavigationGestures = false
        webView.load(URLRequest(url: URL(string: urlString)!))
        webView.navigationDelegate = self
        
        // Do any additional setup after loading the view.
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        UIView.transition(with: progressView,
                          duration: 0.33,
                          options: [.transitionCrossDissolve],
                          animations: {self.progressView.isHidden = false},
                          completion: nil)
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if navigationAction.navigationType == .linkActivated {
            decisionHandler(.cancel)
            if navigationAction.request.url?.host?.contains("lapki.com") ?? false{
                if let newwebview = storyboard?.instantiateViewController(withIdentifier: "WebViewController") as? NewWebViewController{
                    newwebview.urlString = navigationAction.request.url!.absoluteString
                    newwebview.titleString = self.titleString
                    self.navigationController?.pushViewController(newwebview, animated: true)
                }
            }
        }else{
            decisionHandler(.allow)
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        if self.navigationController != nil {
            let i = self.navigationController!.viewControllers.count - 1
            print(self.navigationController!.viewControllers[i].restorationIdentifier ?? "nil")
            self.delegate?.lastUrlResult(webView.url)
        }
        super.viewWillDisappear(animated)
    }
    func webView(_: WKWebView, didFinish _: WKNavigation!) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
        UIView.transition(with: progressView,
                          duration: 0.33,
                          options: [.transitionCrossDissolve],
                          animations: {
                            self.progressView.isHidden = true
        },
                          completion: nil)
    }
    private func initProgressView() {
        // TODO: New Progressview
//        progressView.translatesAutoresizingMaskIntoConstraints = false
//        self.navigationItem.titleView?.addSubview(progressView)
//        progressView.progressTintColor = UIColor.lapkiColors.orange
//        progressView.isHidden = true
//
//        NSLayoutConstraint.activate([
//            progressView.leadingAnchor.constraint(equalTo: self.navigationItem.titleView?.leadingAnchor),
//            progressView.trailingAnchor.constraint(equalTo: self.navigationItem.titleView?.trailingAnchor),
//            progressView.bottomAnchor.constraint(equalTo: self.navigationItem.titleView?.bottomAnchor),
//            progressView.heightAnchor.constraint(equalToConstant: 2.0)
//            ])
    }
    private func initEstimatedProgressObserver() {
        estimatedProgressObserver = webView.observe(\.estimatedProgress, options: [.new]) { [weak self] webView, _ in
            self?.progressView.progress = Float(webView.estimatedProgress)
        }
    }
}

protocol WebViewResultDelegate {
    func lastUrlResult(_ url: URL?)
}
