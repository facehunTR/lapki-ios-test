//
//  NewMyNoticeFragmentViewController.swift
//  Lapki
//
//  Created by Denis Dolgopolov on 04/03/2019.
//  Copyright © 2019 MEGAKOM, OOO. All rights reserved.
//
//
//  NewMyNoticeFragmentViewController.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 27/10/2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialCards
import CoreLocation


class NewMyNoticeFragmentViewController: UIViewController {
    
    @IBOutlet weak var placeholderView: UIView!
    var nextPage: String?
    var lastLocation: CLLocation?
    let manager = CLLocationManager()
    var indicator: UIActivityIndicatorView?
    var noticeArray = Notices()
    
    var selectedNotice: Notice?
    @IBOutlet weak var noticeCollectionView: NoticeCollection!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        (self.navigationController?.parent as? NewFavoritesPageViewController)?.segmentedControlP.selectedSegmentIndex = 1
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        noticeCollectionView.parentVC = self
        refresh(nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.locationChanged), name: Notification.Name("locationUpdated"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshWrapper(_:)), name: NSNotification.Name("userUpdated"), object: nil)
        // Do any additional setup after loading the view.
    }
    
    @objc func refreshWrapper(_ sender: AnyObject?){
        self.refresh(sender)
    }
    @objc func locationChanged(){
        if self.lastLocation == nil {
            self.lastLocation = lastUserLocation
            refresh(self)
        }else{
            self.lastLocation = lastUserLocation
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print(selectedNotice.debugDescription)
        print(segue.description)
        if segue.identifier == "NoticeDetailSegue"{
            let noticeDetail = segue.destination as! NewNoticeDetailViewController
            noticeDetail.notice = self.selectedNotice
            self.selectedNotice = nil
        }
    }
}
extension NewMyNoticeFragmentViewController: NoticeCollectionDelegate {
    func didSelectItem(indexPath: IndexPath) {
        self.selectedNotice = noticeArray[indexPath.row]
        self.performSegue(withIdentifier: "NoticeDetailSegue", sender: self)
    }
    
    func refresh(_ sender:AnyObject?) {
        guard MyStaticUser.id != 0 else {
            self.noticeArray = Notices()
            self.noticeCollectionView.reloadData()
            self.placeholderView.isHidden = false
            return
        }
        self.noticeCollectionView.refreshControl?.beginRefreshing()
        self.placeholderView.isHidden = true
        Network.getMyNotices(responseHandler: weakCompletion(of: self, onResult: NewMyNoticeFragmentViewController.refreshSuccess, onError: NewMyNoticeFragmentViewController.refreshError))
    }
    func refreshSuccess(_ page: PageResult){
        self.nextPage = page.next
        self.noticeArray = page.results
        self.noticeCollectionView.performBatchUpdates({
            self.noticeCollectionView.reloadSections(IndexSet(integer: 0))
        }, completion: { (success) in
            self.noticeCollectionView.isWaiting = false
            self.noticeCollectionView.refreshControl?.endRefreshing()
        })
    }
    func refreshError(_ error: Error){
        self.noticeCollectionView.refreshControl?.endRefreshing()
        self.noticeCollectionView.isWaiting = false
        self.handleResponseError(error)
    }
}


