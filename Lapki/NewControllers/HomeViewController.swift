//
//  HomeViewController.swift
//  Lapki
//
//  Created by Denis Dolgopolov on 25/02/2019.
//  Copyright © 2019 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialCards
import MaterialComponents.MDCAppBarViewController
import CoreLocation
import Kingfisher
class HomeViewController: UIViewController, CategoriesDelegate,FilterDelegate,UICollectionViewDelegate{
    @IBOutlet weak var chipsCollectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!{
        didSet{
            //noticeCollectionTopConstraint = collectionViewHeightConstraint
        }
    }
    @IBOutlet weak var noticeCollectionTopConstraint: NSLayoutConstraint!
    let dataSource = ChipsDataSource()
    
    
    func set(category: String?, type: String?) {
        self.parameters["category"] = category
        self.parameters["type"] = type
        
        self.refresh(nil)
    }
    func set(parameters: [String:Any?]?){
        guard let parameters = parameters else{
            self.parametersClear()
            self.refresh(nil)
            return
        }
        self.parameters = parameters
        self.refresh(nil)
    }
    @IBAction func deleteFilterTouchUped(_ sender: UIButton) {
        let key = dataSource.chipsArray[sender.tag].filterType
        var priority = ["category","type","subtype","subsubtype"]
        if priority.contains(key){
            let keyIndex = priority.index(of: key)!
            priority.removeFirst(keyIndex)
            var params = parameters
            for each in priority{
                
                params[each] = nil
            }
            parameters = params
        }
        else{
            parameters[key] = nil
        }
       // dataSource.chipsArray.remove(at: sender.tag)
        //chipsCollectionView.reloadData()
        refresh(self)
    }
    @IBAction func upGesture(_ sender: Any) {
        if navigationController?.isNavigationBarHidden == false{
        navigationController?.setNavigationBarHidden(true, animated: true)
        setChipsHidden(hidden: true,animated: true,checkForEmpty: true)
        }
    }
    @IBAction func downGesture(_ sender: Any) {
        if navigationController?.isNavigationBarHidden == true{
        navigationController?.setNavigationBarHidden(false, animated: true)
            
            setChipsHidden(hidden: false,animated: true ,checkForEmpty: true)
        }
    
    }
    
    // NoticeCollectionDelegate variables
    var noticeArray = Notices()
    var nextPage: String? {
        didSet{
            if self.nextPage != nil {
                indicator?.startAnimating()
            }else{
                self.indicator?.stopAnimating()
                for footer in self.noticeCollectionView.visibleSupplementaryViews(ofKind:  UICollectionView.elementKindSectionFooter){
                    footer.removeFromSuperview()
                }
            }
        }
    }
    var indicator: UIActivityIndicatorView?
    
    //End of NoticeCollectionDelegate variables
    
    
    var searchController : UISearchController? = nil
    var settedAddress: String?
    
    var lastLocation: CLLocation? {
        didSet{
            self.locationRequestBuilder()
        }
    }
    var selectedNotice: Notice?
    var settedUserLocation: CLLocation? {
        didSet {
            self.locationRequestBuilder()
        }
    }
    
    func setChipsCollectionView(){
        if let layout = chipsCollectionView.collectionViewLayout as? UICollectionViewFlowLayout{
            
            layout.scrollDirection = .horizontal
            layout.estimatedItemSize = CGSize(width: 90, height: 30)
            
        }
        
    }
    func locationRequestBuilder() {
        if settedUserLocation != nil {
            self.parameters["lat"] = String(settedUserLocation!.coordinate.latitude)
            self.parameters["lng"] =  String(settedUserLocation!.coordinate.longitude)
        }else if lastLocation != nil {
            self.parameters["lat"] = String(lastLocation!.coordinate.latitude)
            self.parameters["lng"] =  String(lastLocation!.coordinate.longitude)
        }else if MyStaticUser.defaultLocation.longitude != 0 && MyStaticUser.defaultLocation.latitude != 0 {
            self.parameters["lat"] = String(MyStaticUser.defaultLocation.latitude)
            self.parameters["lng"] =  String(MyStaticUser.defaultLocation.longitude)
        }else{
            self.parameters.removeValue(forKey: "lat")
            self.parameters.removeValue(forKey: "lng")
        }
    }
    var parameters = [String:Any?](){
        didSet{
            dataSource.cc(parameters: parameters)
            if dataSource.chipsArray.count != 0{
                self.chipsCollectionView.performBatchUpdates({
                    self.chipsCollectionView.reloadSections(IndexSet(integer:0))
                }) { (true) in
                    self.chipsCollectionView.collectionViewLayout.invalidateLayout()
                    self.chipsCollectionView.layoutIfNeeded()
                }
                
            }
            
            if dataSource.chipsArray.count == 0{
                setChipsHidden(hidden: true , animated: true)
            }
            else if dataSource.chipsArray.count != 0 {
                setChipsHidden(hidden: false,animated: true)
            }
        }
    }
    
    func setChipsHidden(hidden:Bool , animated:Bool = false,checkForEmpty:Bool = false){
        if checkForEmpty == true && dataSource.chipsArray.count == 0{
            return
        }
        let hide = hidden ? 0 : 40
        let isHidden = collectionViewHeightConstraint.constant.isZero
        if isHidden != hidden{
            switch animated {
            case true:
                collectionViewHeightConstraint.constant = CGFloat(hide)
                UIView.animate(withDuration:0.5){
                    self.view.layoutIfNeeded()
                }
                chipsCollectionView.isHidden = hidden
            case false:
                collectionViewHeightConstraint.constant = CGFloat(hide)
                chipsCollectionView.isHidden = hidden
            }
        }
    }
    
    @IBOutlet weak var noticeCollectionView: NoticeCollection!
    
    @IBAction func placeholderViewClicked(_ sender: Any) {
        if MyStaticUser.verificationPhone == true {
            if let category = self.parameters["category"] as? String, let type = self.parameters["type"] as? String {
                let storyBoard = UIStoryboard(name: "Add&EditStoryboard", bundle: nil)
                let addVC = storyBoard.instantiateViewController(withIdentifier: "AddNoticeViewController") as! NewAddNoticeViewController
                addVC.category = category
                addVC.type = type
                self.navigationController?.pushViewController(addVC, animated: true)
            }else{
                let storyBoard = UIStoryboard(name: "Categories", bundle: nil)
                let categories = storyBoard.instantiateViewController(withIdentifier: "CategoriesViewController") as! NewCategoriesViewController
                categories.mode = .Add
                categories.pojo = (self.tabBarController as! MainTabBarController).Categories
                self.navigationController?.pushViewController(categories, animated: true)
            }
        }else{
            let storyBoard = UIStoryboard(name: "Login", bundle: nil)
            let loginVC = storyBoard.instantiateViewController(withIdentifier: "LoginViewController")
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
    }
    
    func parametersClear(){
        self.parameters.removeValue(forKey: "type")
        self.parameters.removeValue(forKey: "category")
        self.parameters.removeValue(forKey: "subtype")
        self.parameters.removeValue(forKey: "subsubtype")
        self.parameters.removeValue(forKey: "age__lt")
        self.parameters.removeValue(forKey: "age__gt")
        self.parameters.removeValue(forKey: "dist")
        self.parameters.removeValue(forKey: "price__gt")
        self.parameters.removeValue(forKey: "price__lt")
        self.dataSource.chipsArray.removeAll()
        self.settedUserLocation = nil
        self.settedAddress = nil
        self.refresh(self)
    }
    @IBOutlet weak var placeholderView: UIView!
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        //navigationController?.hidesBarsOnSwipe = true
        //self.navigationContorller?.hid
        noticeCollectionView.prefetchDataSource = self
        chipsCollectionView.dataSource = dataSource
        setChipsCollectionView()
        self.noticeCollectionView.parentVC = self
        if #available(iOS 11.0, *){} else{
            self.searchController?.searchBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
            self.navigationItem.titleView?.sizeToFit()
            self.edgesForExtendedLayout = UIRectEdge.bottom
            self.navigationController?.navigationBar.backgroundColor = UIColor.white
            self.navigationController?.navigationBar.isTranslucent = false
        }
        self.searchController = MySearchController(searchResultsController: nil)
        self.searchController?.searchResultsUpdater = self
        self.searchController?.searchBar.placeholder = "Поиск"
        self.searchController?.searchBar.barStyle = .default
        self.searchController?.hidesNavigationBarDuringPresentation = false
        self.searchController?.dimsBackgroundDuringPresentation = false
        self.searchController?.obscuresBackgroundDuringPresentation = false
        if (self.searchController?.searchBar.responds(to: NSSelectorFromString("searchBarStyle")))! {
            self.searchController?.searchBar.searchBarStyle = .minimal
        }
      
            self.navigationItem.titleView = self.searchController?.searchBar
        
        self.searchController?.searchBar.delegate = self
        self.searchController?.definesPresentationContext = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshWrapper(_:)), name: Notification.Name("userUpdated"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.locationChanged), name: Notification.Name("locationUpdated"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshOneNotice(_:)), name: Notification.Name("favClicked"), object: nil)
        self.lastLocation = lastUserLocation
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
  
    }
    @objc func locationChanged(){
        if self.lastLocation == nil {
            self.lastLocation = lastUserLocation
            refresh(self)
        }else{
            self.lastLocation = lastUserLocation
        }
    }
    @objc func refreshWrapper(_ sender: AnyObject?){
    self.refresh(sender)
    }
    @IBAction func rightSwipeAction(_ sender: Any) {
        self.tabBarController?.selectedIndex = 1
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let noticeDetail = segue.destination as? NewNoticeDetailViewController {
            noticeDetail.notice = self.selectedNotice
            self.selectedNotice = nil
        }else if let categories = segue.destination as? NewCategoriesViewController{
            categories.pojo = (self.tabBarController as! MainTabBarController).Categories
            categories.checkedCategory = self.parameters["category"] as? String
            categories.checkedType = self.parameters["type"] as? String
            categories.mode = .Home
            categories.categoriesDelegate = self
        }else if let filter = segue.destination as? NewFilterViewController {
            filter.filterDelegate = self
            filter.parameters = self.parameters
        }
    }
    @objc func refreshOneNotice(_ notification:NSNotification){
        guard let notice = notification.userInfo?["notice"] as? Notice else{
            return
        }
        if let r = self.noticeArray.firstIndex(where: { (not) -> Bool in
            return not.id == notice.id
        }){
            self.noticeArray[r]=notice
            self.noticeCollectionView.reloadItems(at: [IndexPath(row:
                r, section: 0)])
        }
    }
}
extension HomeViewController: UICollectionViewDataSourcePrefetching {
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        let array = indexPaths.compactMap({return $0.row})
        var urls = [URL]()
        for row in array{
            let notice = noticeArray[row]
            guard let url = URL(string:notice.thumbnail ?? notice.image1 ?? "") else {return}
            urls.append(url)
        }
        ImagePrefetcher(urls: urls).start()
    }
}
extension HomeViewController: NoticeCollectionDelegate{
    
    func didSelectItem(indexPath: IndexPath) {
        self.selectedNotice = noticeArray[indexPath.row]
        self.performSegue(withIdentifier: "NoticeDetailSegue", sender: self)
    }

    func refresh(_ sender:AnyObject?) {
        Network.getNotices(parameters: parameters, responseHandler: weakCompletion(of: self, onResult: HomeViewController.refreshSuccess, onError: HomeViewController.refreshError))
    }
    func refreshSuccess(_ page: PageResult){
        self.placeholderView.isHidden = (page.count != 0)
        self.nextPage = page.next
        self.noticeArray = page.results
        self.noticeCollectionView.performBatchUpdates({
            self.noticeCollectionView.reloadSections(IndexSet(integer: 0))
        }, completion: { (success) in
            self.noticeCollectionView.isWaiting = false
            self.noticeCollectionView.refreshControl?.endRefreshing()
        })
    }
    func refreshError(_ error: Error){
        self.noticeCollectionView.refreshControl?.endRefreshing()
        self.noticeCollectionView.isWaiting = false
        self.handleResponseError(error)
    }
}


extension HomeViewController: UIGestureRecognizerDelegate{
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
extension HomeViewController:  UISearchControllerDelegate, UISearchResultsUpdating, UISearchBarDelegate{
    func updateSearchResults(for searchController: UISearchController) {
            self.parameters["search"] = searchController.searchBar.text
            self.refresh(searchController)
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.parameters["search"] = searchBar.text
        self.refresh(searchBar)
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.parameters.removeValue(forKey: "search")
        self.refresh(searchBar)
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.parameters["search"] = searchBar.text
        self.refresh(searchBar)
    }
}

class ChipsCell:UICollectionViewCell{
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var filterLabel: UILabel!
    @IBOutlet weak var labelLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var clearImageView: UIImageView!
    
    @IBOutlet weak var exitButton: UIButton!

    override func layoutSubviews() {
        super.layoutSubviews()
        //exitImageView.image = UIImage.set
        if iconImageView.image == nil{
            labelLeadingConstraint.constant = -15
            layoutIfNeeded()
            //layoutSubviews()
        }
        else{
            labelLeadingConstraint.constant = 2
            layoutIfNeeded()
        }
    }
    func setParams(filter:String,image:UIImage?,index:IndexPath){
        
        iconImageView.image = image
        filterLabel.text = filter
        exitButton.tag = index.row
        clearImageView.tintColor = UIColor.lapkiColors.lostOrange
        layoutSubviews()
    }
    
}


