//
//  NewBonusViewController.swift
//  Lapki
//
//  Created by Denis Dolgopolov on 28/02/2019.
//  Copyright © 2019 MEGAKOM, OOO. All rights reserved.
//
import UIKit
import MaterialComponents.MDCButton
import MaterialComponents.MaterialDialogs

class NewBonusViewController: UIViewController {
    
    var dialogTransitionController: MDCDialogTransitionController!
    @IBOutlet weak var myTableView: UITableView!
    var parentView: UIViewController?
    var bonuses = [Bonus]()
    
    var lapki: Int? {
        didSet{
            let L = (self.lapki ?? 0)/100
            let IntL = Int(L)
            self.title = "На вашем счету: \(IntL) бонусов"
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        super.viewWillDisappear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        dialogTransitionController = MDCDialogTransitionController()
        myTableView.sectionHeaderHeight = UITableView.automaticDimension
        myTableView.estimatedSectionHeaderHeight = 3
        Network.getLapki(responseHandler: weakCompletion(of: self, onResult: NewBonusViewController.lapkiUpdate, onError: NewBonusViewController.handleResponseError))
        loadData()
    }
    
    func lapkiUpdate(lapki: LapkiResponse){
        self.lapki = lapki.count
    }
    
    func codeGenerationCompleted(_ result: DetailedResponse){
        if result.statusCode == 200{
            self.restorationIdentifier = "codeverification"
            self.performSegue(withIdentifier: "DialogCall", sender: self)
        }else{
            makeToast(result.detail, position: .top)
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case let dialogVC as MyDialogViewController:
            guard let sender = sender as? UIViewController else {return}
            dialogVC.placeholderText = ""
            switch sender.restorationIdentifier{
            case "phonenumber":
                dialogVC.senderId = "PhonenumberCard"
            case "codeverification":
                dialogVC.inputType = UIKeyboardType.numberPad
                dialogVC.headerText = "Введите 4 последние номера входящего звонка"
                dialogVC.codeRepeatAction = {success in
                    Network.genVerificationCode(call: false,responseHandler: {(_,_) in
                        dialogVC.timerRestart()
                    })
                }
                dialogVC.rightButtonAction = {_ in
                    Network.numberVerification(code: dialogVC.textField.text!, responseHandler:
                        weakCompletion(of: self, onResult: NewBonusViewController.numberVerificationCompleted, onError: NewBonusViewController.numberVerificationError)
                    )
                }
            default: break
            }
            dialogVC.modalPresentationStyle = .custom
            dialogVC.transitioningDelegate = self.dialogTransitionController
        default:
            break
        }
        
    }
    func numberVerificationCompleted(_ result: DetailedResponse){
        if result.statusCode != 400 {
            Network.createBonusOrder(order_reason:"info",ad: 0,byBonuses: nil,responseHandler: weakCompletion(of: self, onResult: NewBonusViewController.orderCreated, onError: NewBonusViewController.handleOrderCreateError))
        }else{
            makeToast(result.detail, position: .top)
        }
    }
    
    func numberVerificationError(_ error: Error){
        makeToast("Неверный код подтверждения",position:.top)
        self.handleResponseError(error)
    }
    
    func orderCreated(result: DetailedResponse){
        guard (result.statusCode ?? 404) < 400 else {
            self.handleOrderCreateError(ResponseError.detailReturned(detail: result.detail))
            return}
        Network.getLapki(responseHandler: weakCompletion(of: self, onResult: NewBonusViewController.lapkiUpdate, onError: NewBonusViewController.handleResponseError))
        makeToast(result.detail,position: .top)
         self.loadData()
    }
    
    func bonusesUpdate(usedBonuses: [UsedBonus]){
        let reasons = usedBonuses.map({return $0.reason})
        bonuses = [Bonus]()
        myTableView.reloadData()
        let date = Date()
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(abbreviation: "MST")!
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        let reasonDay = "day \(dateFormatter.string(from: date))"
        self.bonuses.append(Bonus(isAvailable: !reasons.contains(reasonDay),number: 1, text: "Заходите в Лапки!\nПользуйтесь ежедневно и получайте каждый день бонусные 20 лапок", action: {
            if !reasons.contains(reasonDay){
                Network.createBonusOrder(order_reason:reasonDay,ad: 0,byBonuses: nil,responseHandler: weakCompletion(of: self, onResult: NewBonusViewController.orderCreated, onError: NewBonusViewController.handleBonusesError))
            }
        }))
        self.bonuses.append(Bonus(isAvailable: !reasons.contains("info"),number: 2, text: "Добавьте и подтвердите свой номер телефона и получите 30 лапок!", action: {
            guard !reasons.contains("info")else {return}
            if MyStaticUser.verificationPhone {
                Network.createBonusOrder(order_reason:"info",ad: 0,byBonuses: nil,responseHandler: weakCompletion(of: self, onResult: NewBonusViewController.orderCreated, onError: NewBonusViewController.handleBonusesError))
            }else {
                self.restorationIdentifier = "phonenumber"
                self.performSegue(withIdentifier: "DialogCall", sender: self)
            }
        }
        ))
        self.bonuses.append(Bonus(isAvailable: !reasons.contains("subscribe"),number: 3, text: "Присоединяйтесь к Лапкам в соцсетях и получите 40 лапок!", action: {
            let getBonus = NSNotification.Name(rawValue: "getBonus")
            let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            let cancelAction = UIAlertAction(title: "Отмена", style: .cancel) { (action) in
                print("didPress cancel")
                NotificationCenter.default.removeObserver(self)
            }
            actionSheet.addAction(cancelAction)
            
            let customView = SocialShareBottomActionView()
            actionSheet.view.addSubview(customView)
            customView.translatesAutoresizingMaskIntoConstraints = false
            customView.topAnchor.constraint(equalTo: actionSheet.view.topAnchor, constant: 0).isActive = true
            customView.rightAnchor.constraint(equalTo: actionSheet.view.rightAnchor, constant: 0).isActive = true
            customView.leftAnchor.constraint(equalTo: actionSheet.view.leftAnchor, constant: 0).isActive = true
            customView.heightAnchor.constraint(equalToConstant: 78).isActive = true
            actionSheet.view.translatesAutoresizingMaskIntoConstraints = false
            actionSheet.view.heightAnchor.constraint(equalToConstant: 130).isActive = true
            self.present(actionSheet, animated: true, completion: {
                NotificationCenter.default.addObserver(self, selector: #selector(self.callCreateOrder), name: getBonus, object: nil)
            })
        }))
        self.myTableView.beginUpdates()
        self.myTableView.insertSections(IndexSet(integersIn: 0...self.bonuses.count-1), with: .automatic)
        self.myTableView.endUpdates()
        self.myTableView.reloadData()
    }
    
    func handleBonusesError(_ error: Error){
        Network.getLapki(responseHandler: weakCompletion(of: self, onResult: NewBonusViewController.lapkiUpdate, onError: NewBonusViewController.handleResponseError))
        if let responseError = error as? ResponseError{
            switch responseError {
            case .detailReturned(let detail):
                makeToast(detail, position: .top)
            case .withoutDetails:
                makeToast("Произошла ошибка: Нет деталей ошибки", position: .top)
            case .notJSON,.emptyResponse,.notExpectedValue:
                break
            }
        }
    }
    
    func handleOrderCreateError(_ error: Error){
        Network.getLapki(responseHandler: weakCompletion(of: self, onResult: NewBonusViewController.lapkiUpdate, onError: NewBonusViewController.handleResponseError))
    }
    
    func loadData(){
        Network.getUsedBonuses(responseHandler: weakCompletion(of: self, onResult: NewBonusViewController.bonusesUpdate, onError: NewBonusViewController.handleBonusesError))
    }
    
    
    @objc func callCreateOrder(){
        Network.createBonusOrder(order_reason:"subscribe",ad: 0,byBonuses: nil,responseHandler: weakCompletion(of: self, onResult: NewBonusViewController.orderCreated, onError: NewBonusViewController.handleBonusesError))
    }
}
extension NewBonusViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return bonuses.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let label = HeaderLabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 44))
            label.textInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
            label.numberOfLines = 0
            label.textAlignment = .center
            label.text = "Получать лапки очень просто! Нужно на них нажимать.\n Убедитесь сами:"
            label.sizeToFit()
            label.backgroundColor = UIColor.clear
            return label
        }else{
            let v = UIView()
            v.backgroundColor = UIColor.clear
            return v
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = self.myTableView.dequeueReusableCell(withIdentifier: "bonusCell", for: indexPath) as? BonusTableViewCell {
            cell.bonus = bonuses[indexPath.section]
            return cell
        }
        return UITableViewCell()
    }
}
