//
//  NewSignupViewController.swift
//  Lapki
//
//  Created by Denis Dolgopolov on 27/02/2019.
//  Copyright © 2019 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import Toast_Swift
import MaterialComponents.MaterialDialogs
import InputMask
class NewSignupViewController: UIViewController, CountryPickerDelegate {
    
    @IBOutlet weak var countryButton: UIButton!
    
    @IBAction func countryClicked(_ sender: Any) {
        //TODO: Доделать
        let storyBoard = UIStoryboard(name: "OldMain", bundle: nil)
        let countryPickerViewController = storyBoard.instantiateViewController(withIdentifier: "CountryPickerViewController") as! CountryPickerViewController
        countryPickerViewController.countryDelegate = self
        //countryPickerViewController.selected = self.selectedCountry
        //        pickerViewController.data = subTypes
        //        pickerViewController.selected = subType
        //        pickerViewController.parentView = self
        //        pickerViewController.outPutName = "subType"
        let bottomSheet = MDCBottomSheetController(contentViewController: countryPickerViewController)
        bottomSheet.preferredContentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height/3)
        self.present(bottomSheet, animated: true, completion: nil)
    }
    func set(country: Country) {
        self.inputMaskDelegate.primaryMaskFormat = country.phoneMask
        self.countryButton.setImage(country.flag, for: .normal)
        //        let index = country.phoneMask.index(before: country.phoneMask.firstIndex(of: "["))
        //self.selectedCountry = country.countryCode
        let placeholderString = country.phoneMask.prefix(while: {return $0 != "["})
        self.phoneNumberField.placeholder = String(placeholderString)
        inputMaskDelegate.put(text: "", into: phoneNumberField)
        //        print("countryCode \(countryCode.rawValue)")
    }
    
    @IBOutlet var inputMaskDelegate: MaskedTextFieldDelegate!
    @IBOutlet weak var scrollView: UIScrollView!
    var parentView: UIViewController!
    @IBOutlet weak var PasswordField: UITextField!
    @IBOutlet weak var SignUpButton: UIButton!
    @IBOutlet weak var phoneNumberField: UITextField!
    @IBOutlet weak var bottomHeight: NSLayoutConstraint!
    @IBOutlet weak var passwordRepeat: UITextField!
    var dialogTransitionController = MDCDialogTransitionController()
    var mainViewController: MyMainViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addLineToView(view: phoneNumberField, position: .LINE_POSITION_BOTTOM,color: UIColor.black,width: 0.5)
        self.addLineToView(view: PasswordField, position: .LINE_POSITION_BOTTOM,color: UIColor.black,width: 0.5)
        self.addLineToView(view: passwordRepeat, position: .LINE_POSITION_BOTTOM,color: UIColor.black,width: 0.5)
        SignUpButton.layer.cornerRadius=5
        // Do any additional setup after loading the view.
    }
    func isPasswordValid(_ password : String) -> Bool{
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{4,}$")
        return passwordTest.evaluate(with: password)
    }
    
    @IBAction func SignUpButtonClicked(_ sender: UIButton) {
        if !isPasswordValid(PasswordField.text!){
            makeToast("Пароль должен содержать не менее: 4 символов, 1 латинской буквы, 1 цифры",position: .top)
            return
        }
        if !passwordRepeat.text!.elementsEqual(PasswordField.text!){
            makeToast("Пароль подтвержден неверно",position: .top)
            return
        }
        if phoneNumberField.text == nil {
            makeToast("Неверно указан номер телефона",position: .top)
            return
        }
        var text = phoneNumberField.text!
        for a in text {
            if a == "+" || a == " " || a == "(" || a == ")"{
                text.remove(at: text.firstIndex(of: a)!)
            }
        }
        if text.count < 11 {
            makeToast("Неверно указан номер телефона",position: .top)
            return
        }
        Network.registerUser(phonenumber:text,password: PasswordField.text!,responseHandler: weakCompletion(of: self, onResult: NewSignupViewController.registerSuccess, onError: NewSignupViewController.registerOrLoginError))
    }
    
    func registerSuccess(_ result: DetailedResponse){
        if result.statusCode == 201 {
            var text = phoneNumberField.text!
            for a in text {
                if a == "+" || a == " " || a == "(" || a == ")"{
                    text.remove(at: text.firstIndex(of: a)!)
                }
            }
            Network.loginUser(username:text, password: self.PasswordField.text!, responseHandler: weakCompletion(of: self, onResult: NewSignupViewController.loginSuccess, onError: NewSignupViewController.registerOrLoginError))
        }else {
            makeToast(result.detail, position: .top)
        }
    }
    func registerOrLoginError(_ error: Error){
        makeToast("\(error)", position: .top)
        self.handleResponseError(error)
    }
    var alertController: MDCAlertController?
    func codeGenerationSuccess(_ result: DetailedResponse){
        if result.statusCode == 200{
            self.performSegue(withIdentifier: "DialogCall", sender:self.alertController)
        }else{
            makeToast(result.detail, position: .top)
        }
    }
    func codeGenerationFailure(_ error: Error){
        if let error = error as? ResponseError{
            switch error{
            case .detailReturned(let detail):
                makeToast(detail, position: .top)
            default: break
            }
            self.handleResponseError(error)
    }
    }
    
    func loginSuccess(_ success: Bool) {
        if success{
            alertController = createAlertController(title: "Обратите внимание", message: "Сейчас вам поступит звонок.\nИспользуйте последние четыре цифры номера телефона в качестве кода подтверждения.\nЕсли звонок не поступил нажмите \"Отправить код повторно\" для отправки SMS с кодом")
            let action = MDCAlertAction(title:"ПОНЯТНО") { (action) in
                Network.genVerificationCode(call: true,responseHandler: weakCompletion(of: self, onResult: NewSignupViewController.codeGenerationSuccess, onError: NewSignupViewController.codeGenerationFailure))
            }
            alertController!.addAction(action)
            self.present(alertController!, animated:true, completion:nil)
        }
        else{
            makeToast("Произошла ошибка при получении данных пользователя",position: .top)
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    
   // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let senderView = sender as? UIView
        if segue.identifier == "DialogCall" && senderView != nil {
            let vc = segue.destination as! MyDialogViewController
            vc.senderId = senderView?.restorationIdentifier
            vc.isTimerLabelHidden = false
            vc.isSecondTextFieldisHidden = true
            vc.placeholderText = ""
            vc.inputType = UIKeyboardType.numberPad
            vc.headerText = "Введите код подтверждения"
            vc.codeRepeatAction = {success in
                Network.genVerificationCode(call:false,responseHandler: {[weak self] (_,_) in
                    guard self != nil else {return}
                    vc.timerRestart()
                })
            }
            vc.rightButtonAction = {success in
                Network.numberVerification(code: vc.textField.text!, responseHandler: weakCompletion(of: self, onResult: NewSignupViewController.verificationDetails, onError: NewSignupViewController.registerOrLoginError))
            }
        }
    }
    func verificationDetails(_ result: DetailedResponse){
        if result.statusCode != 400 {
            Network.getCurrentUserData(weakCompletion(of: self, onResult: NewSignupViewController.verificationCompleted, onError: NewSignupViewController.registerOrLoginError))
        }else{
            makeToast(result.detail, position: .top)
        }
    }
    func verificationCompleted(_ success: Bool){
        self.navigationController?.popToRootViewController(animated: true)
        makeToast("Добро пожаловать", position: .top)
    }
    
}





