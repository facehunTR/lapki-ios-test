//
//  NewFavTabViewController.swift
//  Lapki
//
//  Created by Denis Dolgopolov on 01/03/2019.
//  Copyright © 2019 MEGAKOM, OOO. All rights reserved.
//

import UIKit

class NewFavTabViewController: UIViewController {
    
    @IBOutlet weak var segmentedController: UISegmentedControl!
    var myFavoritesPageViewController: NewFavoritesPageViewController!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.segmentedController.translatesAutoresizingMaskIntoConstraints = false
        self.navigationItem.titleView = self.segmentedController
       self.navigationItem.titleView?.translatesAutoresizingMaskIntoConstraints = false
        self.navigationItem.titleView?.widthAnchor.constraint(equalToConstant: UIApplication.shared.keyWindow!.bounds.width).isActive = true
        self.navigationItem.titleView?.centerXAnchor.constraint(equalTo: self.navigationController!.navigationBar.centerXAnchor).isActive = true
        segmentedController.setTitleTextAttributes([NSAttributedString.Key .foregroundColor:UIColor.lapkiColors.lightGray], for: .normal)
        segmentedController.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.lapkiColors.orange], for: .selected)
    }
    @IBAction func segmentValueChanged(_ sender: UISegmentedControl) {
        myFavoritesPageViewController.changePageTo(index: sender.selectedSegmentIndex)
    }
    
    
    @IBAction func leftSwipe(_ sender: Any) {
        if segmentedController.selectedSegmentIndex == 0 {
            myFavoritesPageViewController.changePageTo(index: 1)
        }else{
            self.tabBarController?.selectedIndex = 3
        }
    }
    @IBAction func rightSwipe(_ sender: Any) {
        if segmentedController.selectedSegmentIndex == 0{
            self.tabBarController?.selectedIndex = 0
        }else{
            myFavoritesPageViewController.changePageTo(index: 0)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "FavoritePageViewSegue") {
            myFavoritesPageViewController = segue.destination as? NewFavoritesPageViewController
            //myFavoritesPageViewController.tabBar = self.tabBar
            myFavoritesPageViewController.segmentedControlP = self.segmentedController
        }
    }
    
    
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
