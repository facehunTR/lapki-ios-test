//
//  MyDialogViewController.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 15/12/2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextFields
import InputMask



class MyDialogViewController: UIViewController {
    
    @IBOutlet weak var dialogueView: UIView!
    @IBOutlet weak var rightButton: MDCButton!
    @IBOutlet weak var leftButton: MDCButton!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var labelHeader: UILabel!
    @IBOutlet var inputMaskDelegate: MaskedTextFieldDelegate?
    @IBOutlet weak var secondTextField: UITextField?
    var secondTextFieldController: MDCTextInputControllerOutlined!
    var secondPlaceholderText: String?
    var secondTextFieldString: String?
    var codeRepeatAction: CompletionHandler?
    @IBOutlet weak var codeRepeat: UIView!
    var placeholderText: String?
    var helperText: String?
    var inputType: UIKeyboardType?
    var textFieldController: MDCTextInputControllerOutlined!
    var leftButtonTitle: String = "Отмена"
    var rightButtonTitle: String = "Принять"
    var leftButtonAction: CompletionHandler?
    var rightButtonAction: CompletionHandler?
    var textFieldString: String?
    var headerText: String?
    var timer = Timer()
    var timeRemaining: Int = 60
    var isTimerRunning = false
    var isSecondTextFieldisHidden = false
    var isTimerLabelHidden = true
    var senderId: String?
    var oldPhoneNumber: String?
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        textField.becomeFirstResponder()
    }
    
    var header = ""
    var placeholder = ""
    //
    //    func postParameters(Parameters:[String:String],completion:@escaping(Bool?,DataResponse<Any>?) -> Void )  {
    //
    //        patchUserData(parameters: Parameters, responseHandler: {response in
    //            if response.result.isSuccess {
    //                completion(response)
    //            }else{
    //                completion(nil)
    //            }
    //        })
    //    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch: UITouch? = touches.first
        
        if touch?.view != dialogueView && touch?.view != codeRepeat {
            self.dismiss(animated: true) {
                
            }
        }
        
    }
    func setTheShadow(){
        dialogueView.layer.shadowOffset = CGSize(width: 5, height: 5)
        dialogueView.layer.shadowRadius = 50
        dialogueView.layer.shadowOpacity = 0.5
        dialogueView.layer.masksToBounds = false
        dialogueView.layer.shadowColor = UIColor.black.cgColor
        rightButton.layer.masksToBounds = true
        leftButton.layer.masksToBounds = true
    }
    func nameOrEmailNameChanged(_ user: MyUser){
        userToStatic(user)
        self.dismiss(animated: true, completion: nil)
    }
    func userPhonenumberChanged(_ user: MyUser){
        userToStatic(user)
        let alertController = createAlertController(title: "Обратите внимание", message: "Сейчас вам поступит звонок.\nИспользуйте последние четыре цифры номера телефона в качестве кода подтверждения.\nЕсли звонок не поступил нажмите \"Отправить код повторно\" для отправки SMS с кодом")
        let action = MDCAlertAction(title:"ПОНЯТНО") { (action) in
            Network.genVerificationCode(call: true,responseHandler: weakCompletion(of: self, onResult: MyDialogViewController.generationSuccess, onError: MyDialogViewController.generationError))
        }
        alertController.addAction(action)
        self.present(alertController, animated:true, completion:nil)
    }
    
    func generationError(_ error: Error){
        if let error = error as? ResponseError {
            switch error{
            case .detailReturned(let detail):
                makeToast(detail, position: .top)
                break
            default:
                break
            }
        }
        self.handleResponseError(error)
    }
    
    func phoneNumberChangeError(_ error: Error){
        
        var text = self.textField.text!
        for a in text {
            if a == "+" || a == " " || a == "(" || a == ")"{
                text.remove(at: text.firstIndex(of: a)!)
            }
        }
        guard text != oldPhoneNumber else {
            let alertController = createAlertController(title: "Обратите внимание", message: "Сейчас вам поступит звонок.\nИспользуйте последние четыре цифры номера телефона в качестве кода подтверждения.\nЕсли звонок не поступил нажмите \"Отправить код повторно\" для отправки SMS с кодом")
            let action = MDCAlertAction(title:"ПОНЯТНО") { (action) in
                Network.genVerificationCode(call: true,responseHandler: weakCompletion(of: self, onResult: MyDialogViewController.generationSuccess, onError: MyDialogViewController.generationError))
            }
            alertController.addAction(action)
            self.present(alertController, animated:true, completion:nil)
            return
        }
        if let error = error as? ResponseError {
            switch error{
            case .detailReturned(let detail):
                makeToast(detail, position: .top)
                break
            default:
                break
            }
        }
        self.handleResponseError(error)
    }
    func generationSuccess(_ result: DetailedResponse){
        if result.statusCode == 200{
            let dialog = self.storyboard!.instantiateInitialViewController() as! MyDialogViewController
            dialog.senderId = "AlertController"
            let vc = self.presentingViewController
            self.dismiss(animated: true, completion: {
                vc?.present(dialog, animated: true, completion:nil)})
        }else{
            makeToast(result.detail, position: .top)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        guard let senderId = senderId else {
            self.dismiss(animated: true, completion: nil)
            return
        }
        
        switch senderId{
            
        case "UserFirstLastNameEditButton":
            textField.text = textFieldString
            secondTextField!.text = secondTextFieldString
            placeholderText  = "Имя"
            secondPlaceholderText = "Фамилия"
            inputType = .default
            headerText = "Введите Имя и Фамилию"
            textFieldString = MyStaticUser.firstName
            secondTextFieldString = MyStaticUser.lastName
            rightButtonAction = {success in
                Network.patchUserData(parameters: ["first_name":self.textField?.text ?? "","last_name":self.secondTextField?.text ?? ""], responseHandler: weakCompletion(of: self, onResult: MyDialogViewController.nameOrEmailNameChanged, onError: MyDialogViewController.handleResponseError))
            }
        case "EmailCard":
            textField.text = MyStaticUser.email
            func post(){
                let parameters: [String:String] = [
                    "email":textField.text!
                ]
                Network.patchUserData(parameters: parameters, responseHandler: weakCompletion(of: self, onResult: MyDialogViewController.nameOrEmailNameChanged, onError: MyDialogViewController.handleResponseError))
            }
            placeholderText = ""
            inputType = .default
            isSecondTextFieldisHidden = true
            
            if MyStaticUser.email.isEmpty{
                headerText = "Введите email"
                secondTextField?.isHidden = true
                rightButtonAction = {success in
                    
                    self.textField.text!.removeAll(where: {return $0 == " "})
                    if isValidEmail(testStr: self.textField.text!){
                        post()
                        
                    }else{
                        //makeToast("Введите действительный e-mail")
                        makeToast("Введите действительный e-mail",position:.top)
                        
                    }
                }
                
            }else{
                headerText = "Введите новый email"
                secondTextField?.isHidden = true
                rightButtonAction = {success in
                    self.textField.text!.removeAll(where: {return $0 == " "})
                    if isValidEmail(testStr: self.textField.text!){
                        if self.textField.text!.elementsEqual(MyStaticUser.email){
                            self.dismiss(animated: true, completion: nil)
                            makeToast("Email не был изменён",position:.top)
                        }else{
                            post()
                        }
                    }else{
                        makeToast("Введите действительный e-mail",position:.top)
                    }
                }
            }
        case "PhonenumberCard":
            textFieldString = MyStaticUser.phonenumber
            placeholderText = ""
            inputType = UIKeyboardType.phonePad
            headerText = "Введите номер телефона"
            isSecondTextFieldisHidden = true
            isTimerLabelHidden = true
            rightButtonTitle = "Подтвердить"
            rightButtonAction = {success in
                
                if self.textField.text!.count < "+7 (000) 000 00 00".count {
                    makeToast("Номер телефона указан неверно",position:.top)
                }else{
                    var text = self.textField.text!
                    for a in text {
                        if a == "+" || a == " " || a == "(" || a == ")"{
                            text.remove(at: text.firstIndex(of: a)!)
                        }
                    }
                    self.oldPhoneNumber = MyStaticUser.phonenumber
                    Network.patchUserData(parameters: ["phonenumber":text], responseHandler: weakCompletion(of: self, onResult: MyDialogViewController.userPhonenumberChanged, onError: MyDialogViewController.phoneNumberChangeError))
                    
                }
            }
        case "AlertController":
            isTimerLabelHidden = false
            isSecondTextFieldisHidden = true
            placeholderText = ""
            inputType = UIKeyboardType.numberPad
            headerText = "Введите код подтверждения"
            codeRepeatAction = {success in
                Network.genVerificationCode(call:false,responseHandler:{[weak self] (_,error) in
                    guard let self = self else {return}
                    if let error = error as? ResponseError {
                        switch error {
                        case .detailReturned(let detail):
                            makeToast(detail,position: .top)
                        default:
                            makeToast("Произошла ошибка код \(error)", position: .top)
                        }
                    }else if error != nil{
                        makeToast("Произошла ошибка код \(error!)", position: .top)
                    }
                    // print(response)
                    self.timerRestart()
                })
                
            }
            rightButtonAction = {success in
                Network.numberVerification(code: self.textField.text!, responseHandler: weakCompletion(of: self, onResult: MyDialogViewController.verificationDetails, onError: MyDialogViewController.registerError))
            }
        default:
            break
        }
        
        
        
        initTextField()
        initButtons()
        if codeRepeat != nil {
            timerRestart()
            let gesture = UITapGestureRecognizer(target: self, action: #selector(codeRepeatClick(_:)))
            codeRepeat.addGestureRecognizer(gesture)
        }
        if secondTextField != nil {
            secondTextField!.translatesAutoresizingMaskIntoConstraints = false
            secondTextField!.autocapitalizationType = .none
            secondTextField!.keyboardType = inputType ?? .default
            secondTextField!.font = UIFont.LapkiFonts.LatoRegular
            if self.inputMaskDelegate == nil {
                secondTextField!.delegate = self
            }else{
                secondTextField!.delegate = self.inputMaskDelegate
            }
            secondTextField!.text = self.secondTextFieldString ?? ""
            secondTextField!.tintColor = UIColor.lapkiColors.orange
            secondTextField!.placeholder = self.secondPlaceholderText ?? ""
            secondTextField!.font = UIFont.LapkiFonts.LatoRegular
            secondTextField!.sizeToFit()
        }
        secondTextField?.isHidden = isSecondTextFieldisHidden
        codeRepeat.isHidden = isTimerLabelHidden
        if senderId == "UserFirstLastNameEditButton" || senderId == "EmailCard" || senderId == "AlertController"{
            textField.delegate = nil
            secondTextField?.delegate = nil
        }
        else{
            // textField.text = textFieldString
            // textField.delegate = self.inputMaskDelegate
            
            inputMaskDelegate?.put(text: textFieldString!, into: textField)
        }
    }
    func timerRestart(){
        let label = codeRepeat.subviews[0] as! UILabel
        
        timer.invalidate()
        timeRemaining = 60
        label.text = "Отправить код повторно (\(timeRemaining))"
        codeRepeat.isUserInteractionEnabled = false
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerUpdate(_:)), userInfo: nil, repeats: true)
    }
    @objc func timerUpdate(_ sender: Any){
        if timeRemaining > 0 {
            timeRemaining -= 1
            let label = codeRepeat.subviews[0] as! UILabel
            label.text = "Отправить код повторно (\(timeRemaining))"
        }else{
            codeRepeat.isUserInteractionEnabled = true
            let label = codeRepeat.subviews[0] as! UILabel
            label.text = "Отправить код повторно"
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setTheShadow()
    }
    
    @objc func codeRepeatClick(_ sender: Any){
        self.labelHeader.text = "Введите код подтверждения"
        self.codeRepeatAction?(true)
    }
    func initButtons(){
        leftButton.setTitle(leftButtonTitle, for: .normal)
        leftButton.setTitleColor(UIColor.lapkiColors.orange, for: .normal)
        leftButton.imageEdgeInsets = UIEdgeInsets.zero
        leftButton.contentEdgeInsets = UIEdgeInsets.zero
        leftButton.backgroundColor = UIColor.white
        leftButton.addTarget(self, action: #selector(leftButtonClick(_:)), for: .touchUpInside)
        leftButton.inkColor = UIColor.inkColors.orange
        leftButton.tintColor = UIColor.lapkiColors.orange
        rightButton.setTitle(rightButtonTitle, for: .normal)
        rightButton.setTitleColor(UIColor.lapkiColors.orange, for: .normal)
        rightButton.imageEdgeInsets = UIEdgeInsets.zero
        rightButton.contentEdgeInsets = UIEdgeInsets.zero
        rightButton.backgroundColor = UIColor.white
        rightButton.addTarget(self, action: #selector(rightButtonClick(_:)), for: .touchUpInside)
        rightButton.inkColor = UIColor.inkColors.orange
        rightButton.tintColor = UIColor.lapkiColors.orange
    }
    @objc func leftButtonClick(_ sender: Any){
        if self.leftButtonAction == nil {
            self.dismiss(animated: true, completion: nil)
        }else{
            self.leftButtonAction!(true)
        }
    }
    @objc func rightButtonClick(_ sender: Any){
        if self.rightButtonAction == nil {
            self.dismiss(animated: true, completion: nil)
        }else{
            self.rightButtonAction?(true)
        }
    }
    func initTextField(){
        self.labelHeader.text = self.headerText ?? ""
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.autocapitalizationType = .none
        textField.keyboardType = inputType ?? .default
        textField.font = UIFont.LapkiFonts.LatoRegular
        if self.inputMaskDelegate == nil {
            textField.delegate = self
        }else{
            textField.delegate = self.inputMaskDelegate
        }
        textField.text = self.textFieldString ?? ""
        textField.tintColor = UIColor.lapkiColors.orange
        textField.placeholder = self.placeholderText ?? ""
        textField.font = UIFont.LapkiFonts.LatoRegular
        textField.sizeToFit()
    }
    //TO-DO: Проверить результат подтверждения
    func verificationDetails(_ result: DetailedResponse){
        if result.statusCode != 400 {
            Network.getCurrentUserData({(_,_) in})
            self.dismiss(animated: true, completion: {
                guard let bonusVC = self.presentingViewController as? NewBonusViewController else {return}
                Network.createBonusOrder(order_reason:"info",ad: 0,byBonuses: nil,responseHandler: weakCompletion(of: bonusVC, onResult: NewBonusViewController.orderCreated, onError: NewBonusViewController.handleOrderCreateError))
            })
        }else{
            makeToast(result.detail, position: .top)
        }
    }

    func registerError(_ error: Error){
        if let er = error as? ResponseError{
            switch er{
            case .detailReturned(let detail):
                makeToast(detail, position: .top)
            default:
                break
            }
        }
        self.handleResponseError(error)
    }
    /*
     // MARK: - Navigation
     
     In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     Get the new view controller using segue.destination.
     Pass the selected object to the new view controller.
     }
     */
    
    
    
}


func isValidEmail(testStr:String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: testStr)
}
extension MyDialogViewController: UITextFieldDelegate {
    
}
