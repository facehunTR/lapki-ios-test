//
//  NewSettingsViewController.swift
//  Lapki
//
//  Created by Denis Dolgopolov on 27/02/2019.
//  Copyright © 2019 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialCards
import MaterialComponents.MaterialDialogs
import Kingfisher
import DKImagePickerController
import MessageUI
import ImageSlideshow
import ReactiveCocoa
import ReactiveSwift
import CropViewController

class NewSettingsViewController: UIViewController,PageObservation, MFMailComposeViewControllerDelegate {
    
    
    @IBOutlet weak var lapkiLabel: UILabel!
    @IBOutlet weak var bonusLapkiCard: MDCCard!
    @IBOutlet weak var SignOutButton: MDCButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet var stackView: UIStackView!
    @IBOutlet weak var userImageCard: MDCCard!
    
    var lapki: Int? {
        didSet{
            let L = (self.lapki ?? 0)/100
            let IntL = Int(L)
            self.lapkiText.swap("На вашем счету: \(IntL) бонусов")
        }
    }
    func lapkiUpdate(lapki: LapkiResponse){
        self.lapki = lapki.count
    }
    
    var lapkiText: MutableProperty<String> = MutableProperty("На вашем счету: 0 бонусов")
    
    let imagePickerController = DKImagePickerController()
    var loginViewController: LoginViewController!
    var myPageViewController : MyPageViewController!
    
    func getParentPageViewController(parentRef: MyPageViewController) {
        myPageViewController = parentRef
    }
    
    @IBOutlet weak var bonusIcon: UIImageView!
    
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var phoneNumberCard: MDCCard!
    
    
    
    @IBOutlet weak var UserFirstLastNameEditButton: MDCButton!
    
    
    @IBOutlet weak var userInfoCard: MDCCard!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userFirstLastName: UILabel!
    
    @IBOutlet weak var addressCard: MDCCard!
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBAction func addressChangeClicked(_ sender: Any) {
        performSegue(withIdentifier: "toAddressMap", sender: self)
    }
    
    @IBOutlet weak var emailCard: MDCCard!
    @IBOutlet weak var emailLabel: UILabel!
    
    @IBAction func dialogCall(_ sender: Any) {
        self.performSegue(withIdentifier: "DialogCall", sender: sender)
    }
    
    @IBAction func supportClicked(_ sender: Any) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["support@lapki.com"])
            present(mail, animated: true)
        } else {
            makeToast("Не удается открыть приложение \"Почта\", e-mail поддержки - support@lapki.com",position: .top)
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    @IBAction func callWebView(_ sender: Any) {
        self.performSegue(withIdentifier: "WebViewSegue", sender: sender)
    }
    
    @IBOutlet weak var versionLabel: UILabel!
    
    @IBAction func bonusCardClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "BonusSegue", sender: sender)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        Network.getLapki(responseHandler: weakCompletion(of: self, onResult: NewSettingsViewController.lapkiUpdate, onError: NewSettingsViewController.handleResponseError))
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bonusLapkiCard.reactive.isHidden <~ MyStaticUser.isEmpty
        userInfoCard.reactive.isHidden <~ MyStaticUser.isEmpty
        phoneNumberCard.reactive.isHidden <~ MyStaticUser.isEmpty
        emailCard.reactive.isHidden <~ MyStaticUser.isEmpty
        addressCard.reactive.isHidden <~ MyStaticUser.isEmpty
        SignOutButton.reactive.title <~ MyStaticUser.buttonText
        userImageView.contentMode = .scaleAspectFill
        userImageView.layer.cornerRadius = userImageView.bounds.width/2
        userImageView.reactive.image <~ MyStaticUser.userImage
        userFirstLastName.reactive.text <~ MyStaticUser.fullName
        emailLabel.reactive.text <~ MyStaticUser.emailProperty
        phoneNumberLabel.reactive.text <~ MyStaticUser.phoneProperty
        addressLabel.reactive.text <~ MyStaticUser.addressProperty
        phoneNumberLabel.reactive.textColor <~ MyStaticUser.verificationColor
        lapkiLabel.reactive.text <~ lapkiText
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        initScrollStackView()
        initUserInfoCard()
        let buildNumber: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
        let appVersionString: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        let versionAndBuildNumber: String = "\(appVersionString) (\(buildNumber))"
        versionLabel.text = "Версия: \(versionAndBuildNumber)"
        self.stackView.layoutIfNeeded()
        Network.getLapki(responseHandler: weakCompletion(of: self, onResult: NewSettingsViewController.lapkiUpdate, onError: NewSettingsViewController.handleResponseError))
    }
    
    @IBAction func rightSwipe(_ sender: Any) {
        self.tabBarController?.selectedIndex = 3
    }
    
    func initUserInfoCard(){
        userImageCard.setShadowColor(UIColor.clear, for: .normal)
        userImageCard.cornerRadius = userImageCard.bounds.width/2
        SignOutButton.setBorderWidth(1.0, for: .normal)
        SignOutButton.setBorderColor(UIColor.lapkiColors.lightGray, for: .normal)
    }
    
    func userImageChangeSuccess(_ user: MyUser){
        userToStatic(user)
        makeToast("Фотография успешно изменена", position: .top)
    }
    
    func userImageChangeFailure(_ error: Error){
        if let error = error as? ResponseError {
            switch error{
            case .detailReturned(let detail):
                makeToast(detail, position: .top)
                break
            default:
                break
            }
        }
        self.handleResponseError(error)
    }
    
    @IBAction func userImageChange(_ sender: Any){
        
        func cropPhoto(image:UIImage){
            let cropController = TOCropViewController(croppingStyle: .circular, image: image)
            cropController.onDidCropToRect = {img, _, _ in
                Network.patchUserAvatar(image: img,responseHandler: weakCompletion(of: self, onResult: NewSettingsViewController.userImageChangeSuccess, onError: NewSettingsViewController.userImageChangeFailure))
                cropController.dismiss(animated: true, completion: nil)
            }
            self.present(cropController,animated:true,completion:nil)
        }
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let galleryAction = UIAlertAction(title: "Выбрать с галереи", style: .default) { (action) in
            self.imagePickerController.didSelectAssets = { (assets: [DKAsset]) in
                if !assets.isEmpty{
                    assets[0].fetchOriginalImage(options: nil , completeBlock: { (image, info) in
                        cropPhoto(image: image!)
                    })
                }
            }
            self.imagePickerController.sourceType = .both
            self.imagePickerController.maxSelectableCount = 1
            self.imagePickerController.showsEmptyAlbums = false
            self.present(self.imagePickerController, animated: true, completion: nil)
        }
        
        let editPhotoAction = UIAlertAction(title: "Редактировать", style: .default) { (action) in
            guard let url = MyStaticUser.userImageUrl else {return}
            let downloader = ImageDownloader.default
            downloader.downloadImage(with: url) { result in
                switch (result){
                case .success(let value):
                    cropPhoto(image: value.image)
                    break
                case .failure(let error):
                    print("Error \(error)")
                    break
                }
            }
        }
        
        let showPhotoAction = UIAlertAction(title: "Открыть полностью", style: .default) { (action) in
            let imageSlideShow = ImageSlideshow()
            imageSlideShow.center = self.userImageView.center
            self.userImageView.addSubview(imageSlideShow)
            imageSlideShow.activityIndicator = DefaultActivityIndicator(style: .white, color: UIColor.lapkiColors.lostOrange)
            imageSlideShow.sd_setShowActivityIndicatorView(true)
            imageSlideShow.backgroundColor = UIColor.black
            imageSlideShow.draggingEnabled = true
            var source = [InputSource]()
            if let url = MyStaticUser.userImageUrl {
                source.append(KingfisherSource(url: url))
            }
            imageSlideShow.setImageInputs(source)
            imageSlideShow.presentFullScreenController(from: self)
        }
        
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel) { (action) in
        }
        
        if MyStaticUser.userImageUrl != nil{
            actionSheet.addAction(editPhotoAction)
            actionSheet.addAction(showPhotoAction)
        }
        
        actionSheet.addAction(galleryAction)
        actionSheet.addAction(cancelAction)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func initScrollStackView(){
        guard !self.scrollView.subviews.contains(self.stackView) else {return}
        self.scrollView.addSubview(self.stackView)
        self.stackView.translatesAutoresizingMaskIntoConstraints = false
        self.stackView.leadingAnchor.constraint(equalTo: self.scrollView.leadingAnchor).isActive = true
        self.stackView.trailingAnchor.constraint(equalTo: self.scrollView.trailingAnchor).isActive = true
        self.stackView.topAnchor.constraint(equalTo: self.scrollView.topAnchor).isActive = true
        self.stackView.bottomAnchor.constraint(equalTo: self.scrollView.bottomAnchor).isActive = true
        self.stackView.widthAnchor.constraint(equalTo: self.scrollView.widthAnchor).isActive = true
    }
    
    @IBAction func SignOutButtonPressed(_ sender: AnyObject){
        if MyStaticUser.id == 0 {
            self.performSegue(withIdentifier: "LoginSegue", sender: self)
        }else{
            do {
                clearMyUser()
                try clearTokenAndId()
                Network.getCurrentUserData({(_,_) in})
            }catch{
                debugPrint(error)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case let dialogVC as MyDialogViewController:
            guard let senderView = sender as? UIView else {return}
            dialogVC.senderId = senderView.restorationIdentifier
        case let webVC as NewWebViewController:
            guard let senderView = sender as? UIView else {return}
            switch senderView.restorationIdentifier {
            case "LicenseCard":
                webVC.urlString = "https://www.lapki.com/license/"
                webVC.titleString = "Лицензионное соглашение"
            case "PolicyCard":
                webVC.urlString = "https://www.lapki.com/privacy/"
                webVC.titleString = "Политика конфиденциальности"
            default:
                break
            }
        case let addressVC as NewAddNoticeMapViewController:
            guard !(MyStaticUser.defLat.isZero), MyStaticUser.defAddress != "" else {return}
            addressVC.addressStruct.coordinates = MyStaticUser.defaultLocation
            addressVC.addressStruct.address = MyStaticUser.defAddress
        default:
            break
        }
    }
}




