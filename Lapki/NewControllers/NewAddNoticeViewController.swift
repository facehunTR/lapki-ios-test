//
//  NewAddNoticeViewController.swift
//  Lapki
//
//  Created by Denis Dolgopolov on 06/03/2019.
//  Copyright © 2019 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import MaterialComponents.MDCButton
import MaterialComponents.MaterialTextFields
import MaterialComponents.MDCMultilineTextField
import MaterialComponents.MaterialBottomSheet
import RangeSeekSlider
import Toast_Swift
import SearchTextField
import AudioToolbox
import DKImagePickerController
import Kingfisher
class NewAddNoticeViewController: UIViewController,UICollectionViewDelegate,CategoriesDelegate,MyPickerViewBottomControllerDelegate, AgeDelegate {
    var delegate: AddEditNoticeDelegate?
    let dataSource = PhotoCollectionDataSource()
    var category: String?
    var type: String?
    var defaultNotice = EditNotice()
    var notice = EditNotice()
    var defaultPhotos: [UIImage?] = [UIImage(),UIImage(),UIImage(),UIImage(),UIImage()]
    var indicator: UIActivityIndicatorView?
    func result(year: Int, month: Int, week: Int) {
        self.ageYear = year
        self.ageMonth = month
        self.ageWeek = week
        self.ageLabel.text = ageFormat(year: year, month: month, week: week) ?? "Укажите возраст"
    }
    
    var subType: String? {
        didSet {
            subTypeLabel.text = self.subType
            guard let subType = self.subType else {
                subSubTypeCard.isHidden = true
                return
            }
            guard let row = subTypes.firstIndex(of: subType), let category = category, let type = type, row > 0 else {
                self.subType = nil
                return
            }
            if let subsubtypes = (self.tabBarController as! MainTabBarController).Categories.getSubsubtypesNames(category:category , type: type, row: row){
                subSubTF.filterStrings(subsubtypes as! [String])
                subSubTypeCard.isHidden = false
            }else{
                subSubTypeCard.isHidden = true
            }
        }
    }
    
    func result(outputName: String, result: String?) {
        switch (outputName){
        case "GenderCard":
            gender = result
            break
        case "SubtypeCard":
            subType = result
            break
        default:
            break
        }
    }
    func set(category: String?, type: String?) {
        self.category = category
        self.type = type
        self.initCategoriesView()
        self.initSubTypeView()
        self.viewReorder()
    }
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var titleTextView: UITextView!
    @IBOutlet weak var collectionView: PhotoCollectionView!
    @IBAction func subTypeClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "PickerViewSegue", sender: sender)
    }
    
    @IBOutlet weak var subTypeLabel: UILabel!
    var subTypes: [String?] = [String]()
    
    @IBOutlet weak var bottomHeight: NSLayoutConstraint!
    @IBOutlet weak var categoriesView: CategoriesView!
    @IBOutlet weak var subTypeCard: MDCCard!
    @IBOutlet weak var subSubTF: SearchTextField!
    @IBOutlet weak var subSubTypeCard: MDCCard!
    @IBOutlet weak var priceTF: UITextField!
    @IBOutlet weak var priceIcon: UIImageView!
    @IBOutlet weak var priceCard: MDCCard!
    @IBOutlet var noticeStackView: UIStackView!
    @IBOutlet weak var noticeScrollView: UIScrollView!
    @IBOutlet weak var photoCard: UIView!
    
    var photoImages = [UIImageView]()
    @IBOutlet weak var titleCard: MDCCard!
    @IBOutlet weak var titleIcon: UIImageView!
    @IBOutlet weak var descriptionCard: MDCCard!
    @IBOutlet weak var descriptionIcon: UIImageView!
    
    @IBOutlet weak var genderLabel: UILabel!
    
    var genders = ["Укажите пол","Самец","Самка","Не определено"]
    @IBOutlet weak var genderCard: MDCCard!
    
    @IBAction func ageCardClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "AgePickerViewSegue", sender: sender)
    }
    @IBOutlet weak var ageCard: MDCCard!
    var gender :String?{
        didSet {
            genderLabel.text = gender
        }
    }
    
    
    @IBAction func genderCardClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "PickerViewSegue", sender: sender)
    }
    
    let imagePickerController = DKImagePickerController()
    
    var ageYear: Int = 0
    var ageMonth: Int = 0
    var ageWeek: Int = 0
    
    @IBAction func categoriesViewClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "CategoriesSegue", sender: self)
    }
    @IBOutlet weak var ageLabel: UILabel!
    
    //    var photos: [UIImage?] = [nil,nil,nil,nil,nil]
    
    
    @IBOutlet weak var addressCard: MDCCard!
    
    @IBOutlet weak var addressText: UILabel!
    var lat: String?
    var lng: String?
    var location_address: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        collectionView.dataSource = dataSource
        gender = self.genders[0]
        initScrollStackView()
        initCategoriesView()
        initSubTypeView()
        initTitleView()
        initDescriptionView()
        addressText.text = location_address ?? "Укажите адрес"
        viewReorder()
        collectionView.collectionViewSet()
        noticeInsert()
    }
    func noticeInsert(){
        guard notice.id != nil else {return}
        defaultNotice = notice
        for index in notice.images.indices{
            if let urlString = notice.images[index], let url = URL(string: urlString){
                KingfisherManager.shared.retrieveImage(with: url, completionHandler: {[weak self]result in
                    guard let self = self else {return}
                    switch result{
                    case .success(let value):
                        self.defaultPhotos[index] = value.image
                        self.dataSource.photoImages = self.defaultPhotos.compactMap({return $0})
                        self.collectionView.reloadData()
                    case .failure(let error):
                        print(error.localizedDescription)
                    }
                })}
        }
        if let title = notice.title {
            titleTextView.text = title
            titleTextView.textColor = .black
        }
        if let description = notice.description {
            descriptionTextView.text = description
            descriptionTextView.textColor = .black
        }
        if let price = notice.price, let priceString = (price != -1) ? "\(price)": "" {
            priceTF!.text = priceString
        }
        type = notice.type
        category = notice.category
        initCategoriesView()
        subType = notice.subtype
        gender = notice.gender ?? "Укажите пол"
        subSubTF!.text = notice.subsubtype
        addressText.text = notice.locationAddress
        location_address = notice.locationAddress
        lat = "\(notice.lat!)"
        lng = "\(notice.lng!)"
        ageYear = notice.oldYear ?? 0
        ageMonth = notice.oldMonth ?? 0
        ageWeek = notice.oldWeek ?? 0
        ageLabel.text = ageFormat(year: ageYear, month: ageMonth, week: ageWeek) ?? "Укажите возраст"
        viewReorder()
    }
    
    @IBAction func addressCardClicked(_ sender: Any){
        self.performSegue(withIdentifier: "toNewAddMapNotice", sender: self)
    }
    
    func viewReorder() {
        guard let category = category, let type = type else {
            return
        }
        priceTF.placeholder = (category.contains("Потеря")) ?  "Вознаграждение, руб" : "Стоимость, руб"
        
        priceCard.isHidden = category.contains(oneOf: ["Вязка","Находка","Мероприятия"])
        
        genderCard.isHidden = category.contains(oneOf: ["Зоотовары","Мероприятия","Услуги","Для бизнеса"])
        
        ageCard.isHidden = category.contains(oneOf: ["Находка","Зоотовары","Мероприятия","Услуги","Для бизнеса"])
        
        subTypeCard.isHidden = (category.contains(oneOf: ["Зоотовары","Находка","Услуги","Для бизнеса","Мероприятия"]) || type.contains("Собаки"))
        
        subSubTypeCard.isHidden = !type.contains("Собаки")
    }
    
    @available(iOS 11.0, *)
    func collectionView(_ collectionView: UICollectionView, shouldSpringLoadItemAt indexPath: IndexPath, with context: UISpringLoadedInteractionContext) -> Bool {
        if dataSource.photoImages.count <= indexPath.row{
            return false
        }
        else{
            return true
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case let categoriesVC as NewCategoriesViewController:
            categoriesVC.pojo = (self.tabBarController as! MainTabBarController).Categories
            categoriesVC.checkedCategory = self.category
            categoriesVC.checkedType = self.type
            categoriesVC.mode = .Add
            categoriesVC.categoriesDelegate = self
        case let mapVC as NewAddNoticeMapViewController:
            if location_address != "" && lat != "" && lng != ""{
                mapVC.addressStruct = PrevAddress(lat:lat,long:lng,address:location_address)
            }
        case let pickerVC as MyPickerViewBottomController:
            guard let id = (sender as? UIView)?.restorationIdentifier else {return}
            switch id {
            case "GenderCard":
                pickerVC.data = genders
                pickerVC.selected = gender
            case "SubtypeCard":
                pickerVC.data = subTypes
                pickerVC.selected = subType
            default: break
            }
            pickerVC.delegate = self
            pickerVC.outPutName = id
        case let ageVC as NewAgePickerViewController: ageVC.ageDelegate = self
        ageVC.year = ageYear
        ageVC.month = ageMonth
        ageVC.week = ageWeek
        default: break
        }
    }
    
    func initTitleView() {
        titleTextView.text = descriptionTextView.accessibilityHint
        titleTextView.textColor = UIColor.gray.withAlphaComponent(0.3)
        titleTextView.selectedTextRange = titleTextView.textRange(from: descriptionTextView.beginningOfDocument, to: descriptionTextView.beginningOfDocument)
        titleTextView.layer.borderColor = UIColor.gray.withAlphaComponent(0.3).cgColor
    }
    func initDescriptionView() {
        descriptionTextView.text = descriptionTextView.accessibilityHint
        descriptionTextView.textColor =  UIColor.gray.withAlphaComponent(0.3)
        descriptionTextView.selectedTextRange = descriptionTextView.textRange(from: descriptionTextView.beginningOfDocument, to: descriptionTextView.beginningOfDocument)
        descriptionTextView.layer.borderColor = UIColor.gray.withAlphaComponent(0.3).cgColor
    }
    func initSubTypeView(){
        guard let pojo = (self.tabBarController as? MainTabBarController)?.Categories else{
            return
        }
        subType = nil
        subSubTF.backgroundColor = UIColor.white
        subSubTF.theme.bgColor = UIColor.white
        subSubTF.theme.font = UIFont.LapkiFonts.LatoRegular
        subSubTF.font = UIFont.LapkiFonts.LatoRegular
        if let type = type,
            let category = category,
            !category.contains(oneOf: ["Находка","Услуги","Для бизнеса","Мероприятия"]){
            if (pojo.subs![pojo.names.firstIndex(of: category)!]?.subs![(pojo.subs![pojo.names.firstIndex(of: category)!]?.names.firstIndex(of: type))!]?.subs) != nil{
                subTypes = (pojo.subs![pojo.names.firstIndex(of: category)!]?.subs![(pojo.subs![pojo.names.firstIndex(of: category)!]?.names.firstIndex(of: type))!]?.names)!
                subTypeCard.isHidden = false
                subType = subTypes[0]
                subSubTF.text = ""
                subSubTypeCard.isHidden = true
            }
            else{
                subTypes = pojo.getSubtypesNames(category: category, type: type) ?? [String]()
                subTypeCard.isHidden = true
                subSubTypeCard.isHidden = false
                subSubTF.filterStrings(subTypes as! [String])
                subSubTF.placeholder = "Укажите породу"
            }
        }else{
            subTypeCard.isHidden = true
            subSubTypeCard.isHidden = true
        }
    }
    
    func initScrollStackView(){
        if !self.noticeScrollView.subviews.contains(self.noticeStackView){
            self.noticeScrollView.addSubview(self.noticeStackView)
            self.noticeStackView.translatesAutoresizingMaskIntoConstraints = false
            self.noticeStackView.leadingAnchor.constraint(equalTo: self.noticeScrollView.leadingAnchor).isActive = true
            self.noticeStackView.trailingAnchor.constraint(equalTo: self.noticeScrollView.trailingAnchor).isActive = true
            self.noticeStackView.topAnchor.constraint(equalTo: self.noticeScrollView.topAnchor).isActive = true
            self.noticeStackView.bottomAnchor.constraint(equalTo: self.noticeScrollView.bottomAnchor).isActive = true
            self.noticeStackView.widthAnchor.constraint(equalTo: self.noticeScrollView.widthAnchor).isActive = true
        }
    }
    
    func initCategoriesView(){
        guard let pojo = (self.tabBarController as? MainTabBarController)?.Categories, let category = self.category else{
            return
        }
        categoriesView.set(pojo: pojo, category: category, type: self.type)
        
    }
    
    @IBAction func backButtonClick(_ sender: Any){
        let alertController = createAlertController(title: "Вы уверены что хотите выйти?", message: nil)
        let action = MDCAlertAction(title:"ДА"){ (action) in
            self.navigationController?.popViewController(animated: true)
        }
        let action2 = MDCAlertAction(title:"НЕТ"){ (action) in
            alertController.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(action)
        alertController.addAction(action2)
        self.present(alertController, animated:true, completion:nil)
    }
    func startIndicator(){
        indicator = UIActivityIndicatorView(style:.whiteLarge)
        indicator!.backgroundColor = UIColor.white
        indicator!.color = UIColor.lapkiColors.lostOrange
        indicator!.center = self.view.center
        indicator!.startAnimating()
        self.view.addSubview(indicator!)
    }
    
    func textViewError(_ textView: UITextView, message:String){
        makeToast(message, position: .top)
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 4
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: (textView.inputView?.center.x)! - 10, y: (textView.inputView?.center.y)!))
        animation.toValue = NSValue(cgPoint: CGPoint(x: (textView.inputView?.center.x)! + 10, y: (textView.inputView?.center.y)!))
        textView.inputView?.layer.add(animation, forKey: "position")
    }
    
    @IBAction func acceptButtonClick(_ sender: Any){
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        startIndicator()
        var blank = false
        if titleTextView.text.isEmpty{
            textViewError(titleTextView, message: "Отсутствует заголовок")
            blank = true
        }
        if descriptionTextView.text.isEmpty{
            textViewError(descriptionTextView, message: "Отсутствует описание")
            blank = true
        }
        var photoBlank = true
        if dataSource.photoImages.count != 0{
            photoBlank = false
        }
        if blank || photoBlank || (type?.isEmpty ?? true) || location_address == nil {
            if photoBlank {
                makeToast("Добавьте не менее одной фотографии", position: .top)
            }
            else if type?.isEmpty ?? true {
                makeToast("Указана неверная категория объявления", position: .top)
            }
            else if location_address == nil{
                makeToast("Укажите адрес", position: .top)
            }
            self.navigationItem.rightBarButtonItem?.isEnabled = true
            indicator?.removeFromSuperview()
            return
        }
        notice.title = titleTextView.text
        notice.description = descriptionTextView.text
        notice.gender = (gender?.contains("Укажите") ?? true) ? nil : gender
        let priceString = (priceTF.isHidden) ? nil : priceTF?.text
        notice.price = (priceString == nil || priceString!.isEmpty) ? -1 : (Int(priceString!) ?? -1)
        notice.locationAddress = location_address
        notice.lat = Double(lat!)!
        notice.lng = Double(lng!)!
        notice.oldWeek = ageWeek
        notice.oldMonth = ageMonth
        notice.oldYear = ageYear
        notice.category = category
        notice.type = type
        notice.subtype = (subType?.contains("Укажите") ?? true) ? nil : subType
        notice.subsubtype = subSubTF?.text ?? nil
        var parameters = [String:Any?]()
        parameters.merge(notice.compare(defaultNotice), uniquingKeysWith: {return $1})
        var photos = dataSource.photoImages
        while photos.count < 5 {
            photos.append(UIImage())
        }
        for i in photos.indices{
            if defaultPhotos[i] != photos[i]{
                parameters["image\(i+1)"] = photos[i]
            }
        }
        //        parameters["old_week"] = parameters["old_week"] ?? defaultNotice.oldWeek ?? 0
        //        parameters["old_month"] = parameters["old_month"] ?? defaultNotice.oldMonth ?? 0
        //        parameters["old_year"] = parameters["old_year"] ?? defaultNotice.oldYear ?? 0
        if let id = notice.id {
            Network.patchNotice(id: id, parameters: parameters.compactMapValues({return $0}), responseHandler: weakCompletion(of: self, onResult: NewAddNoticeViewController.postSuccess, onError: NewAddNoticeViewController.postError))
        }else{
            Network.postNotice(parameters: parameters.compactMapValues({return $0}), responseHandler: weakCompletion(of: self, onResult: NewAddNoticeViewController.postSuccess, onError: NewAddNoticeViewController.postError))
        }
    }
    
    func postSuccess(_ result: Notice){
        indicator?.removeFromSuperview()
        NotificationCenter.default.post(name: NSNotification.Name("userUpdated"), object: nil)
        delegate?.addEdit(result)
        if result.id == notice.id {
            self.navigationController?.popViewController(animated: true)
        }else{
            self.performSegue(withIdentifier: "toConfirmNotice", sender: self)
        }
//        self.navigationController?.popViewControllerWithHandler(){ self.navigationController?.topViewController?.performSegue(withIdentifier: "toConfirmNotice", sender: (Any).self)
//        }
    }
    func postError(_ error: Error){
        indicator?.removeFromSuperview()
        makeToast("Произошла ошибка при публикации объявления", position: .top)
        self.navigationItem.rightBarButtonItem?.isEnabled = true
    }
}

extension NewAddNoticeViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        
        return false
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool{
        if textField == priceTF{
            let maxLength = 8
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
}

extension NewAddNoticeViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText:String = textView.text
        let updatedText = (currentText as NSString).replacingCharacters(in: range, with: text) as NSString
        if updatedText.length < 1 {
            textView.text = textView.accessibilityHint
            textView.textColor =  UIColor.gray.withAlphaComponent(0.3)
            textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
        }else if textView.textColor ==  UIColor.gray.withAlphaComponent(0.3) && !text.isEmpty{
            textView.textColor = UIColor.black
            textView.text = text
        }
        if textView == titleTextView {
            let maxLength = 50
            return updatedText.length <= maxLength
        }
        if textView == descriptionTextView{
            let maxLength = 3000
            return updatedText.length <= maxLength
        }
        
        return true
    }
    func textViewDidChangeSelection(_ textView: UITextView) {
        if self.view.window != nil {
            if textView.textColor ==  UIColor.gray.withAlphaComponent(0.3) {
                textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
            }
        }
    }
}

extension NewAddNoticeViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

protocol AddEditNoticeDelegate{
    func addEdit(_ result: Notice)
}
