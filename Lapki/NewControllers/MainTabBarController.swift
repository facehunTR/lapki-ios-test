//
//  MainTabBarController.swift
//  Lapki
//
//  Created by Denis Dolgopolov on 25/02/2019.
//  Copyright © 2019 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import MaterialComponents.MDCBottomNavigationBar
import Toast_Swift
import StoreKit
class MainTabBarController: UITabBarController {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var Categories: Pojo!
    var notice: Notice?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        
        
        let fileURL = Bundle.main.url(forResource: "serializable_db", withExtension: "pj")
        if let data = try? Data(contentsOf: fileURL!) {
            do {
                Categories = try JSONDecoder().decode(Pojo.self, from: data)
            }catch{
                print(error)
            }
        }
        StoreReviewHelper.checkAndAskForReview()
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.notice != nil {
            let storyBoard: UIStoryboard = UIStoryboard(name: "NoticeDetailStoryBoard", bundle: nil)
            let noticeDetailViewController = storyBoard.instantiateViewController(withIdentifier: "NoticeDetailViewController") as! NewNoticeDetailViewController
            noticeDetailViewController.notice = self.notice
            UIApplication.topViewController()?.navigationController?.pushViewController(noticeDetailViewController, animated: true)
            self.notice = nil
        }
        
        
    }
}
extension MainTabBarController: UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, animationControllerForTransitionFrom fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return MyTransition(viewControllers: tabBarController.viewControllers)
    }
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if viewController.restorationIdentifier == "EmptyController" {
            if MyStaticUser.verificationPhone == true {
                let storyBoard = UIStoryboard(name: "Categories", bundle: nil)
                let categories = storyBoard.instantiateViewController(withIdentifier: "CategoriesViewController") as! NewCategoriesViewController
                categories.mode = .Add
                categories.pojo = self.Categories
               (tabBarController.selectedViewController as? UINavigationController)?.pushViewController(categories, animated: true)
            }else{
                let storyBoard = UIStoryboard(name: "Login", bundle: nil)
                let loginVC = storyBoard.instantiateViewController(withIdentifier: "LoginViewController")
                (tabBarController.selectedViewController as? UINavigationController)?.pushViewController(loginVC, animated: true)
            }
            return false
        }
        return true
    }
    
}

class MyTransition: NSObject, UIViewControllerAnimatedTransitioning {
    
    let viewControllers: [UIViewController]?
    let transitionDuration: Double = 0.4
    
    init(viewControllers: [UIViewController]?) {
        self.viewControllers = viewControllers
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return TimeInterval(transitionDuration)
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        guard
            let fromVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from),
            let fromView = fromVC.view,
            let fromIndex = fromVC.tabBarItem?.tag,
            let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to),
            let toView = toVC.view,
            let toIndex = toVC.tabBarItem?.tag
            else {
                transitionContext.completeTransition(false)
                return
        }
        
        let frame = transitionContext.initialFrame(for: fromVC)
        var fromFrameEnd = frame
        var toFrameStart = frame
        fromFrameEnd.origin.x = toIndex > fromIndex ? frame.origin.x - frame.width : frame.origin.x + frame.width
        toFrameStart.origin.x = toIndex > fromIndex ? frame.origin.x + frame.width : frame.origin.x - frame.width
        toView.frame = toFrameStart
        
        DispatchQueue.main.async {
            transitionContext.containerView.addSubview(toView)
            UIView.animate(withDuration: self.transitionDuration, animations: {
                fromView.frame = fromFrameEnd
                toView.frame = frame
            }, completion: {success in
                fromView.removeFromSuperview()
                transitionContext.completeTransition(success)
            })
        }
    }
}

