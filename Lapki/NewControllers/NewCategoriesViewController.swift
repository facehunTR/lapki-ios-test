//
//  NewCategoriesViewController.swift
//  Lapki
//
//  Created by Denis Dolgopolov on 05/03/2019.
//  Copyright © 2019 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import Crashlytics
import Firebase

class NewCategoriesViewController: UIViewController {
    var pojo: Pojo!
    var checkedType: String?
    var checkedCategory: String?
    var categoriesDelegate: CategoriesDelegate?
    var categories: [String] = [String]()
    var mode: CategoriesMode!
    var selectedPojo: Pojo?
    var currentAd: Ad?
    var ads: Ads?
    
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var footerImage: UIImageView!
    @IBOutlet weak var myTableView: UITableView!
    
    enum AdsErrors: Error{
        case NoAdsInCategoriesAndSocial
        case CoefficientsSumIsZero
        case WrongCoefficientsOrRandomCrash
        case CurrentAdIsNil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        if let first = pojo.names[0], first.contains(oneOf: ["кажите","ыберите"]) {
            self.title = first
            pojo.names.removeFirst()
            pojo.subs?.removeFirst()
        }
        let names = pojo.names.compactMap({return $0})
        categories.append(contentsOf: names)
        if mode != .Add {
            let string = (self.navigationController?.getPreviousVC() is NewCategoriesViewController ) ? "Все объявления этой категории": "Все объявления"
                categories.insert(string,at: 0)
                pojo.names.insert(nil,at: 0)
                pojo.subs?.insert(nil, at: 0)
        }
        myTableView.delegate = self
        myTableView.dataSource = self
        initFooter()
    }
    
    func initFooter(){
        if footerView.gestureRecognizers?.isEmpty ?? true {
            let gesture = UITapGestureRecognizer(target: self, action: #selector(self.footerClicked(_:)))
            gesture.delegate = self
            footerView.addGestureRecognizer(gesture)
            footerView.sizeToFit()
        }
        if ads == nil || ads!.isEmpty{
            let categ = (self.navigationController?.getPreviousVC() is NewCategoriesViewController) ? self.checkedCategory : nil
            ads = getAds(category: categ) ?? getAds(category:nil) ?? getAds(category: "Social")
            ads?.sort(by: { return $0.number > $1.number})
        }
        guard let ads = ads else {
            Crashlytics.sharedInstance().recordError(AdsErrors.NoAdsInCategoriesAndSocial)
            return
        }
        let coefSum = ads.map({return $0.coefficient}).reduce(0,+)
        guard coefSum != 0  else{
            Crashlytics.sharedInstance().recordError(AdsErrors.CoefficientsSumIsZero)
            return
        }
        let random = Int.random(in: 1...1000)
        let k = 1000/coefSum
        var coefs = [Int]()
        for (index, element) in ads.enumerated(){
            if index == 0{
                coefs.append(0)
            }
            coefs.append(coefs.last! + element.coefficient*k)
        }
        coefs[coefs.count-1] = 1000
        let less = coefs.filter({return $0 < random})
        guard let last = less.last,
            let result = coefs.firstIndex(of: last)
            else {
                Crashlytics.sharedInstance().recordError(AdsErrors.WrongCoefficientsOrRandomCrash)
                return}
        currentAd = ads[result]
        let image = currentAd!.metr_image ?? currentAd!.image ?? ""
        footerImage.kf.setImage(with: URL(string: image)){ result in
            switch result {
            case .success(let img):
                guard let currentAd = self.currentAd else {
                    Crashlytics.sharedInstance().recordError(AdsErrors.CurrentAdIsNil)
                    return
                }
                Analytics.logEvent("Advertising_\(currentAd.title ?? "without title")_\(currentAd.number)", parameters: currentAd.dictionary)
                self.footerImage.kf.setImage(with: URL(string: image),placeholder: img.image,options: [.forceRefresh])
            case .failure(let error):
                Crashlytics.sharedInstance().recordError(error)
            }
        }
    }
    
    @objc func footerClicked(_ sender: Any?){
        guard let currentAd = self.currentAd, let url = URL(string: currentAd.url)else {
            return
        }
        Analytics.logEvent("Advertising_\(currentAd.title ?? "without title")_\(currentAd.number)_click", parameters: currentAd.dictionary)
        UIApplication.shared.open(url, completionHandler: {success in
            if success {
                self.ads?.removeAll(where: {return $0.metr_image == $0.metr_image})
                self.initFooter()
            }
        })
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let next = segue.destination as? NewCategoriesViewController{
            next.pojo = selectedPojo
            next.checkedCategory = checkedCategory
            next.checkedType = checkedType
            next.mode = mode
            next.categoriesDelegate = categoriesDelegate
        }
    }
}
extension NewCategoriesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "categoriesCell", for: indexPath) as? CategoriesTableViewCell  else {
            fatalError("The dequeued cell is not an instance of CategoriesTableViewCell.")
        }
        cell.categoriesLabel.text = categories[indexPath.row]
        cell.iconImage.image = getPojoImage(name: categories[indexPath.row])
        if categories[indexPath.row] == checkedCategory || categories[indexPath.row] == checkedType {
           tableView.selectRow(at: indexPath, animated: false, scrollPosition: .middle)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let checkedString: String? = pojo.names[indexPath.row]
        if self.navigationController?.viewControllers[(self.navigationController?.viewControllers.count ?? 2)-2] is NewCategoriesViewController {
            if categoriesDelegate == nil && self.mode == .Add{
                guard var viewControllers = self.navigationController?.viewControllers else { return }
                viewControllers.removeLast()
                viewControllers.removeLast()
                let storyBoard = UIStoryboard(name: "Add&EditStoryboard", bundle: nil)
                let addVC = storyBoard.instantiateViewController(withIdentifier: "AddNoticeViewController") as! NewAddNoticeViewController
                addVC.category = self.checkedCategory
                addVC.type = checkedString
                viewControllers.append(addVC)
                self.navigationController?.setViewControllers(viewControllers, animated: true)
            }else{
                categoriesDelegate?.set(category: checkedCategory, type: checkedString)
            self.navigationController?.popToViewController(categoriesDelegate as! UIViewController, animated: true)
            }
        }else{
            checkedCategory = checkedString
            if let subPojo = pojo.subs?[indexPath.row]{
                self.selectedPojo = subPojo
                self.performSegue(withIdentifier: "NextCategories", sender: self)
            }else{
                guard let categoriesDelegate = self.categoriesDelegate else {return}
                categoriesDelegate.set(category: checkedCategory, type: nil)
                self.navigationController?.popToViewController(categoriesDelegate as! UIViewController, animated: true)
            }
        }
        
    }
}

enum CategoriesMode {
    case Home
    case Filter
    case Add
}

protocol CategoriesDelegate{
    func set(category: String?, type: String?)
}

extension NewCategoriesViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

extension UINavigationController{
    func getPreviousVC() -> UIViewController?{
        let count = self.viewControllers.count
        guard count > 1 else{return nil}
        return viewControllers[count - 2]
    }
}
