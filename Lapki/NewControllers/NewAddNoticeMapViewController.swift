//
//  NewAddNoticeMapViewController.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 10/12/2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import MapKit
import MaterialComponents.MDCMultilineTextField
import MaterialComponents.MaterialButtons
import SearchTextField



struct PrevAddress {
    var coordinates:CLLocationCoordinate2D?
    var address:String?
    
    init(lat:String? = nil,long:String? = nil,address:String? = nil){
        self.address = address
        self.coordinates = CLLocationCoordinate2D.init(latitude: Double(lat ?? "0") ?? 0.0, longitude: Double(long ?? "0") ?? 0.0)
    }
    func checkForEmpty() -> Bool{
        if self.address != nil && self.address != "" && coordinates != nil{
            return false
        }
        else{
            return true
        }
    }
}

class NewAddNoticeMapViewController: UIViewController,UITableViewDelegate {
    
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var myAddressViewHeightConst: NSLayoutConstraint!
    @IBOutlet weak var myAddressView: UIView!
    var parentView: UIViewController!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var addressCard: MDCCard!
    var lastCenter: CLLocationCoordinate2D?
    @IBOutlet weak var addressTF: UITextField!
    var userInteractedMap = true
    @IBOutlet weak var myAddressLabel: UILabel!
    var currentAddress:String = ""{
        didSet{
            myAddressLabel.text = currentAddress
            if currentAddress != ""{
                if myAddressView.isHidden == true{
                    myAddressView.isHidden = false
                    myAddressViewHeightConst.isActive = false
                }
            }
            else{
                myAddressView.isHidden = true
                myAddressViewHeightConst.isActive = true
            }
        }
    }
    var tileRenderer: MKTileOverlayRenderer!
    var mapRegion: MKCoordinateRegion!
    var seconds = 3
    var timer: Timer?
    var myTimer: Timer?
    var userAddress: MKUserLocation?
    var addresses = [String]()
    var nominatimString = ""
    
    @IBOutlet weak var autoCompletionTableView: UITableView!
    var previousConst = CGFloat()
    @IBOutlet weak var mapViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var arrowImageView: UIImageView!
    let dataSource = AutoCompleteDataSource()
    let delayValue = 2.0
    @IBOutlet weak var addressViewHeightConstraint: NSLayoutConstraint!
    var region: MKCoordinateRegion?
    var addressStruct:PrevAddress = PrevAddress()
    override func viewDidLoad() {
        super.viewDidLoad()
        myAddressView.isHidden = true
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.lapkiColors.lostOrange]
        navigationController?.isNavigationBarHidden = false
        tabBarController?.tabBar.isHidden = true
        autoCompletionTableView.dataSource = dataSource
        mapView.delegate = self
        mapView.showsUserLocation = true
        setupTileOverlay()
        previousConst = addressViewHeightConstraint.constant
        loadIndicatorSettings()
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        
    }
    func loadIndicatorSettings(){
        activityIndicator.hidesWhenStopped = true
        activityIndicator.color = UIColor.lapkiColors.lostOrange
        
    }
    
    func checkIfTherePrevAddress(){
        if addressStruct.checkForEmpty() == false{
            currentAddress = addressStruct.address!
            userInteractedMap = false
            mapView.setRegion(MKCoordinateRegion(center: addressStruct.coordinates!, latitudinalMeters: 500, longitudinalMeters: 500), animated: true)
        }
    }
    func dadataSuccess(_ result: Dadata){
        kladr = result.suggestions![0].data?.kladrId ?? ""
    }
    func reverseGeoSuccess(_ result: ReverseGeoData){
        guard let city = result.address?.city else {return}
        country = result.address?.countryCode ?? ""
        if isUserInRussia != false{
            Network.dadata(address: city, autoComplete: true, completion: weakCompletion(of: self, onResult: NewAddNoticeMapViewController.dadataSuccess, onError: NewAddNoticeMapViewController.handleResponseError))
        }


    }
    func getKladrAndSet(location: MKUserLocation){
        Network.reverseGeo(lat: location.coordinate.latitude, lng: location.coordinate.longitude, zoom: 18, responseHandler: weakCompletion(of: self, onResult: NewAddNoticeMapViewController.reverseGeoSuccess, onError: NewAddNoticeMapViewController.handleResponseError))
    }
    func changeHeightOfSearchView(height:CGFloat){
        if view!.superview!.bounds.height - 64 != height{
            self.mapViewBottomConstraint.constant = height - 3
        }
        self.addressViewHeightConstraint.constant = height
        self.previousConst = self.addressViewHeightConstraint.constant
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isUserInRussia{
            getGeoDadata(index: indexPath.row)
            
            guard let current = dataSource.dadataSourceArray[indexPath.row].editedValue else{return}
            currentAddress = current
            
        }else{
            var coordinates = CLLocationCoordinate2D.init(latitude: 0, longitude: 0)
            guard let lat = Double(dataSource.nominatimArray[indexPath.row].lat ?? "")else{return}
            coordinates.latitude = lat
            
            guard let long = Double(dataSource.nominatimArray[indexPath.row].lon ?? "")else{return}
            coordinates.longitude = long
            self.userInteractedMap = true
            self.mapView.setRegion(MKCoordinateRegion(center: coordinates, latitudinalMeters: 500, longitudinalMeters: 500), animated: true)
            self.currentAddress = dataSource.nominatimArray[indexPath.row].address?.editedValue ?? ""
        }
        changeHeightOfSearchView(height: 70)
        addressTF.endEditing(true)
    }
    @IBAction func handlePan(_ sender: UIPanGestureRecognizer) {
        let halfScreen = sender.view!.superview!.bounds.height * 0.45
        let fullScreen = sender.view!.superview!.bounds.height - 64
        let bottomScreen = 64 as CGFloat
        switch sender.state{
        case .began, .changed:
            
            let translation = sender.translation(in: self.view)
            if previousConst - translation.y < ((sender.view!.superview?.bounds.height)! - bottomScreen){
                addressViewHeightConstraint.constant = previousConst - translation.y
            }
            if addressViewHeightConstraint.constant < halfScreen{
                mapViewBottomConstraint.constant = (previousConst - translation.y) - 3
            }
            if addressViewHeightConstraint.constant <= 71{
                if arrowImageView.image != UIImage(named:"ic_more_arrow"){
                    changeAnimatedArrow(isNormalState: true)
                }
            }
            else{
                if arrowImageView.image != UIImage(named:"ic_less_arrow"){
                    changeAnimatedArrow(isNormalState: false)
                }
            }
            
            break
            
        case .ended:
            let heightArray = [bottomScreen,halfScreen,fullScreen]
            let result = heightArray.nearestElement(to: addressViewHeightConstraint.constant)
            if result == bottomScreen{
                
                changeHeightOfSearchView(height: bottomScreen)
                changeAnimatedArrow(isNormalState: true)
            }
            else if result == halfScreen{
                changeHeightOfSearchView(height: halfScreen)
            }
            else if result == fullScreen  {
                changeHeightOfSearchView(height: fullScreen)
            }
            break
        default:
            break
        }
        
    }
    func changeAnimatedArrow(isNormalState:Bool){
        var imageString = ""
        
        if isNormalState{
            imageString = "ic_more_arrow"
        }
        else{
            imageString = "ic_less_arrow"
        }
        UIView.transition(with: arrowImageView,
                          duration: 0.5,
                          options: .transitionCrossDissolve,
                          animations: { self.arrowImageView.image = UIImage(named:imageString) },
                          completion: nil)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        mapView.setCamera(mapView.camera, animated: false)
        checkIfTherePrevAddress()
    }
    @IBAction func plusClicked(_ sender: Any){
        if region == nil {
            region = mapView.region
        }
        region!.center = mapView.region.center
        region!.span.latitudeDelta /= 2.0
        region!.span.longitudeDelta /= 2.0
        mapView.setRegion(region!, animated: true)
    }
    @IBAction func minusClicked(_ sender: Any){
        if region == nil {
            region = mapView.region
        }
        region!.center = mapView.region.center
        region!.span.latitudeDelta = min(region!.span.latitudeDelta * 2.0, 180.0)
        region!.span.longitudeDelta = min(region!.span.longitudeDelta * 2.0, 180.0)
        mapView.setRegion(region!, animated: true)
    }
    @IBAction func backButtonClick(_ sender: Any){
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func acceptButtonClick(_ sender: Any){
        if !(currentAddress.contains("Укажите") ) && !(myAddressLabel?.text!.contains("Определение") ?? true) && !(myAddressLabel?.text!.isEmpty ?? true){
            let vcArray = navigationController?.viewControllers
            if let addNoticeViewController = vcArray![vcArray!.count - 2]  as? NewAddNoticeViewController{
                addNoticeViewController.lat = String(format: "%.4f", mapView.region.center.latitude)
                addNoticeViewController.lng = String(format: "%.4f", mapView.region.center.longitude)
                addNoticeViewController.location_address = currentAddress
                addNoticeViewController.addressText.text = currentAddress
                self.navigationController?.popViewController(animated: true)
            }
            
            if let filterViewController = vcArray![vcArray!.count - 2]  as? NewFilterViewController {
                filterViewController.parameters["lat"] = String(format: "%.4f", mapView.region.center.latitude)
                filterViewController.parameters["lng"] = String(format: "%.4f", mapView.region.center.longitude)
                filterViewController.addressText.text = currentAddress
                self.navigationController?.popViewController(animated: true)
            }
            
            if (vcArray![vcArray!.count - 2] as? NewSettingsViewController) != nil{
                Network.patchUserData(parameters: [
                    "def_lng":String(format: "%.4f", mapView.region.center.longitude),
                    "def_lat":String(format: "%.4f", mapView.region.center.latitude),
                    "def_address":currentAddress
                    ], responseHandler: weakCompletion(of: self, onResult: NewAddNoticeMapViewController.userPatched, onError: NewAddNoticeMapViewController.handleResponseError))
                
            }}else{
            self.view.makeToast("Дождитесь определения адреса")
        }
    }
    func userPatched(_ user: MyUser){
        userToStatic(user)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func userLocationButtonClicked(_ sender: Any){
        if userAddress != nil {
            let mapRegion = MKCoordinateRegion(center: userAddress!.coordinate, latitudinalMeters: 500, longitudinalMeters: 500)
            self.mapView.setRegion(mapRegion, animated: true)
        }else {
            switch CLLocationManager.authorizationStatus() {
            case .denied:
                let alert = createAlertController(title: "Включите GPS навигацию.", message: "Перейти в настройки для включения GPS навигации?")
                alert.addAction(MDCAlertAction(title: "Да", handler: {action in
                    guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                        return
                    }
                    if UIApplication.shared.canOpenURL(settingsUrl) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in })
                    }
                }))
                alert.addAction(MDCAlertAction(title: "Отмена", handler: {action in
                    alert.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
                break
            case .notDetermined:
                let appDel = UIApplication.shared.delegate as? AppDelegate
                appDel?.setupLocationManager()
            case .authorizedAlways,.authorizedWhenInUse:
                
                break
            default:
                break
            }
            
        }
    }
    func setupTileOverlay(){
        let overlay = MyMKTileOverlay()
        overlay.canReplaceMapContent = true
        for overlay in mapView.overlays {
            mapView.removeOverlay(overlay)
        }
        mapView.addOverlay(overlay)
        tileRenderer = MKTileOverlayRenderer(tileOverlay: overlay)
    }
    override func viewWillLayoutSubviews() {
        mapRegion = mapView.region
    }
    override func viewDidLayoutSubviews() {
        if !(mapView.region.center.latitude == mapRegion.center.latitude) || !(mapView.region.center.longitude == mapRegion.center.longitude)
        {
            mapView.setCenter(mapRegion.center, animated: true)
        }
    }
    
    func getZoom(_ mapView: MKMapView) -> Double {
        var angleCamera = mapView.camera.heading
        if angleCamera > 270 {
            angleCamera = 360 - angleCamera
        } else if angleCamera > 90 {
            angleCamera = fabs(angleCamera - 180)
        }
        let angleRad = .pi * angleCamera / 180
        let width = Double(mapView.bounds.height)
        let height = Double(mapView.bounds.height)
        let spanStraight = width * mapView.region.span.longitudeDelta / (width * cos(angleRad) + (height) * sin(angleRad))
        return log2(360 * ((width / 256) / spanStraight)) + 1;
    }
    @IBAction func textFieldDidChange(_ sender: Any) {
        timer?.invalidate()
        timer = Timer.scheduledTimer(timeInterval: delayValue, target: self, selector: #selector(self.fetchDataReload), userInfo: nil, repeats: false)
        
    }
    @IBAction func editingDidEnd(_ sender: Any) {
        
    }
    func geoDataResult(_ result: Dadata){
        if result.suggestions!.count != 0{
            var coordinates = CLLocationCoordinate2D.init(latitude: 0, longitude: 0)
            guard let lat = Double(result.suggestions?[0].data?.geoLat ?? "")else{return}
            coordinates.latitude = lat
            
            guard let long = Double(result.suggestions?[0].data?.geoLon ?? "")else{return}
            coordinates.longitude = long
            self.userInteractedMap = false
            self.mapView.setRegion(MKCoordinateRegion(center: coordinates, latitudinalMeters: 500, longitudinalMeters: 500), animated: true)
        }
    }
    func getGeoDadata(index:Int) { Network.dadata(address:dataSource.dadataSourceArray[index].value , autoComplete: false, completion: weakCompletion(of: self, onResult: NewAddNoticeMapViewController.geoDataResult, onError: NewAddNoticeMapViewController.handleResponseError))
    }
    @objc func fetchDataReload(){
        
        if isUserInRussia{
            dadataRequest(query: "", autoComplete: true)
        }
        else{
            nominatimGeocoding(address: addressTF.text ?? "")
        }
        
    }
    
    func dadataEditString(_ result: Dadata) -> Dadata{
        var result = result
        for each in result.suggestions!{
            var edited = ""
            if each.data?.street != nil{edited.append(each.data!.street!) }
            if each.data?.houseType != nil{edited.append(", " + each.data!.houseType! + " ") }
            if each.data?.house != nil{edited.append(each.data!.house!)}
            if each.data?.blockType != nil{edited.append(", " + each.data!.blockType!)}
            if each.data?.block != nil { edited.append(" " + each.data!.block!)}
            if each.data?.settlement != nil {edited.append(", " + each.data!.settlement!)}
            if each.data?.city != nil {edited.append(", " + each.data!.city!)}
            if each.data?.city != each.data?.region  ?? ""{
                if each.data?.region != nil {edited.append(", " + each.data!.region! )}
            }
            if !(each.data?.region?.contains(oneOf: ["Крым","Севастополь"]) ?? false){
            if each.data?.country != nil {edited.append(", " + each.data!.country! + "")}
            }
            if edited.prefix(2) == ", "{
                edited = String(edited.dropFirst(2))
            }
            if edited.suffix(2) == ", "{
                edited = String(edited.dropLast(2))
            }
            
            if let i = result.suggestions?.firstIndex(where: { return $0.value == each.value }){
                result.suggestions![i].editedValue = edited
            }
        
    }
        return result
    }
    
    func dadataResponse(_ result: Dadata){
        var result = dadataEditString(result)
        
        
        if result.autoComplete ?? false {
            self.dataSource.dadataSourceArray = result.suggestions!
            self.autoCompletionTableView.reloadData()
        }
        else{//request to edit reversegeo address
            if result.suggestions?.count == 0 { //dadata result 0 use nominatim
                self.currentAddress = self.nominatimString
            }
            else{
                self.currentAddress = result.suggestions![0].editedValue ?? "" //use dadata result
            }
        }
    }
    func dadataError(_ error: Error){
        self.currentAddress = self.nominatimString
        self.handleResponseError(error)
    }
    func dadataRequest(query:String,autoComplete:Bool){
        var queryString = query
        if queryString == ""{
            queryString = addressTF.text!
        }
        if queryString.count > 2{
            Network.dadata(address: queryString, autoComplete: autoComplete,completion: weakCompletion(of: self, onResult: NewAddNoticeMapViewController.dadataResponse, onError: NewAddNoticeMapViewController.dadataError))
        }
    }
    func nominatimResult(_ result: GeoDatas){
        var result = result
        for each in result{
            if let i = result.firstIndex(where: { return $0.displayName == each.displayName }){
                result[i].address!.editedValue = self.editNominatimString(result: each.address!)

            }
        }
        self.dataSource.nominatimArray = result
        self.autoCompletionTableView.reloadData()
    }
    func nominatimGeocoding (address:String){
        if address.count > 2{
            Network.geoSearch(q: address, responseHandler: weakCompletion(of: self, onResult: NewAddNoticeMapViewController.nominatimResult, onError: NewAddNoticeMapViewController.handleResponseError))
        }
    }
}
extension NewAddNoticeMapViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        changeHeightOfSearchView(height:view!.superview!.bounds.height - 64)
    }
}
extension NewAddNoticeMapViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
extension NewAddNoticeMapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        guard let tileOverlay = overlay as? MKTileOverlay else {
            return MKOverlayRenderer()
        }
        
        return MKTileOverlayRenderer(tileOverlay: tileOverlay)
    }
    func nominatimReverseGeoResult(_ result: ReverseGeoData){
        if result.address?.countryCode != country{
            country = result.address?.countryCode ?? ""
            self.dataSource.dadataSourceArray.removeAll()
            self.dataSource.nominatimArray.removeAll()
            self.autoCompletionTableView.reloadData()
            self.seconds = 1
            return
        }
        guard let nomAddress = result.address
            else{
            self.activityIndicator.stopAnimating()
            currentAddress = ""
            return
            
        }
        self.nominatimString = editNominatimString(result: nomAddress)
        self.currentAddress = self.nominatimString
        
        timer?.invalidate()
        self.activityIndicator.stopAnimating()
    }
    func nominatimReverseGeo(){
         Network.reverseGeo(lat: mapView.region.center.latitude, lng: mapView.region.center.longitude, zoom: 18, responseHandler: weakCompletion(of: self, onResult: NewAddNoticeMapViewController.nominatimReverseGeoResult, onError: NewAddNoticeMapViewController.reverseGeoError))
        
    }
    func dadataReverseGeo(){
        Network.geoDadata(lat: mapView.region.center.latitude , lon: mapView.region.center.longitude, responseHandler:weakCompletion(of: self, onResult: NewAddNoticeMapViewController.dadataReverseResult, onError: NewAddNoticeMapViewController.reverseGeoError))
        
    }
    
    func mapViewDidChangeVisibleRegion(_ mapView: MKMapView) {

        let currentCenter = mapView.region.center
        if lastCenter == nil || (lastCenter!.latitude.isEqual(to: currentCenter.latitude) && lastCenter!.longitude.isEqual(to: currentCenter.longitude)) {
            lastCenter = currentCenter
        }
        myTimer?.invalidate()
        seconds = 2
        myTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] timer in
            guard let self = self else { return }
            self.seconds -= 1
            if self.seconds == 0 {
                if self.userInteractedMap {
                    self.activityIndicator.startAnimating()
                    self.currentAddress = " "
                    print("Go!")
                    isUserInRussia = isRussia(lat:mapView.centerCoordinate.latitude , lng:mapView.centerCoordinate.longitude)
                    if isUserInRussia {
                        self.dadataReverseGeo()
                        
                    }else{
                        self.nominatimReverseGeo()
                    }
                }
                
                self.userInteractedMap = true
                
            }
        }
        
    }
    func dadataReverseResult(_ result: Dadata){
        
        let result = dadataEditString(result)
        if result.suggestions?.count != 0{
        let suggestion = result.suggestions?[0]
            currentAddress = suggestion?.editedValue ?? ""
            self.activityIndicator.stopAnimating()
        }
        else{
            nominatimReverseGeo()
        }
        print(result)
        
    }

    func reverseGeoError(_ error: Error){
        
        timer?.invalidate()
        self.activityIndicator.stopAnimating()
        self.handleResponseError(error)
    }
    func editNominatimString(result:Address?) -> String{
        //TODO: change isitcrimea to isitRussia
        if let address = result {
            var edited = ""
            if address.road != nil{edited.append(address.road!)}
            else if address.pedestrian != nil{edited.append((" ") + address.pedestrian!)}
            if address.houseNumber != nil{edited.append(" " + address.houseNumber!) }
            if address.village != nil {edited.append(", " + address.village!)}
            if address.city != nil {edited.append(", " + address.city!)}
            if address.region != nil {edited.append(", " + address.region!)}
            let crimea = ["Республіка Крим","Республика Крым","Севастополь"]
            
             if (address.state?.contains(oneOf:crimea) ?? false){
                let sevastopol = "Севастополь"
                if address.state == sevastopol{
                    if address.city != sevastopol{
                    edited.append(", " + sevastopol)
                    }
                }
                else{
                edited.append(", Республика Крым")
                }
            }
             else{
            if address.state != nil {edited.append(", " + address.state!)}
            if address.country != nil {edited.append(", " + address.country!)}
            }
           
            
            if edited.prefix(2) == ", "{
                edited = String(edited.dropFirst(2))
            }
            if edited.suffix(2) == ", "{
                edited = String(edited.dropLast(2))
            }
            return edited
        }
        else{
            return ""
        }
    }
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        if isUserInRussia{
        if kladr == ""{
            getKladrAndSet(location: userLocation)
        }
        }
        if userAddress == nil && self.addressStruct.checkForEmpty() {
            
            let mapRegion = MKCoordinateRegion(center: userLocation.coordinate, latitudinalMeters: 500, longitudinalMeters: 500)
            self.mapView.setRegion(mapRegion, animated: true)
        }
        self.userAddress = userLocation
        
    }
}


class AutoCompleteCell: UITableViewCell{
    @IBOutlet weak var autoCompleteCellLabel: UILabel!
}





