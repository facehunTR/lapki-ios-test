//
//  NewNewLoginViewController.swift
//  Lapki
//
//  Created by Denis Dolgopolov on 27/02/2019.
//  Copyright © 2019 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialCards
import MaterialComponents.MaterialButtons
import InputMask
import VK_ios_sdk
import ok_ios_sdk
import FlagKit

class NewLoginViewController: UIViewController, CountryPickerDelegate {
    
    
    @IBOutlet weak var countryButton: UIButton!
    
    @IBAction func countryClicked(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "OldMain", bundle: nil)
        let countryPickerViewController = storyBoard.instantiateViewController(withIdentifier: "CountryPickerViewController") as! CountryPickerViewController
        countryPickerViewController.countryDelegate = self
        //countryPickerViewController.selected = self.selectedCountry
        //        pickerViewController.data = subTypes
        //        pickerViewController.selected = subType
        //        pickerViewController.parentView = self
        //        pickerViewController.outPutName = "subType"
        let bottomSheet = MDCBottomSheetController(contentViewController: countryPickerViewController)
        bottomSheet.preferredContentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height/3)
        self.present(bottomSheet, animated: true, completion: nil)
    }
    func set(country: Country) {
        self.inputMaskDelegate.primaryMaskFormat = country.phoneMask
        self.countryButton.setImage(country.flag, for: .normal)
        //        let index = country.phoneMask.index(before: country.phoneMask.firstIndex(of: "["))
        //self.selectedCountry = country.countryCode
        let placeholderString = country.phoneMask.prefix(while: {return $0 != "["})
        self.phoneNumberField.placeholder = String(placeholderString)
        inputMaskDelegate.put(text: "", into: phoneNumberField)
        //        print("countryCode \(countryCode.rawValue)")
    }
    @IBOutlet weak var vkButton: MDCButton!
    @IBOutlet weak var okButton: MDCButton!
    var okSettings: OKSDKInitSettings?
    var token : String?
    @IBAction func okButtonClicked(_ sender: Any) {
        if okSettings == nil {
            okSettings = OKSDKInitSettings()
            okSettings!.appId = "1271174400"
            okSettings!.appKey = "CBAGJFMMEBABABABA"
            okSettings!.controllerHandler = {return self}
            OKSDK.initWith(okSettings)
        }
        OKSDK.clearAuth()
        OKSDK.authorize(withPermissions: ["GET_EMAIL","VALUABLE_ACCESS"], success: {response in
            OKSDK.invokeMethod("users.getCurrentUser", arguments: nil, success: {response in
                let rep = response as! NSDictionary
                let email = rep["email"] as? String
                let username = rep["uid"] as? String
                let pic3 = rep["pic_3"] as? String
                let pic2 = rep["pic_2"] as? String
                let pic1 = rep["pic_1"] as? String
                var parameters = Parameters()
                parameters["token"] = OKSDK.currentAccessToken()!
                parameters["url_avatar"] = pic3 ?? (pic2 ?? pic1)
                parameters["email"] = email
                parameters["backend"] = "odnoklassniki-oauth2"
                parameters["username"] = username
                Network.registerSocial(parameters: parameters, responseHandler: weakCompletion(of: self, onResult: NewLoginViewController.registerSocialSuccess, onError: NewLoginViewController.registerSocialFailure))
            }, error: {error in
                print("ok-auth error \(String(describing: error))")
            })
        }, error: {error in
            print("ok-auth error \(String(describing: error))")
        })
    }
    
    @IBOutlet var inputMaskDelegate: MaskedTextFieldDelegate!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var PasswordField: UITextField!
    @IBOutlet weak var LoginButton: UIButton!
    @IBOutlet weak var phoneNumberField: UITextField!
    @IBOutlet weak var bottomHeight: NSLayoutConstraint!
    
    var mainViewController: MyMainViewController!
    
    @IBOutlet weak var licenseCard: MDCCard!
    @IBOutlet weak var policyCard: MDCCard!
    override func viewDidLoad() {
        super.viewDidLoad()
        // TODO: Нужно переписать
        countryButton.layer.masksToBounds = true
        countryButton.layer.borderColor = UIColor.black.withAlphaComponent(0.26).cgColor
        countryButton.layer.borderWidth = 0.5
        countryButton.contentEdgeInsets = .zero
        countryButton.imageEdgeInsets = .zero
        countryButton.setImage(Flag(countryCode: "RU")?.image(style: .roundedRect), for: .normal)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        vkButton.layer.cornerRadius = vkButton.bounds.width/2
        vkButton.imageView?.layer.cornerRadius = vkButton.bounds.width/2
        vkButton.imageEdgeInsets = .zero
        vkButton.contentEdgeInsets = .zero
        okButton.layer.cornerRadius = okButton.bounds.width/2
        okButton.imageView?.layer.cornerRadius = okButton.bounds.width/2
        okButton.imageEdgeInsets = .zero
        okButton.contentEdgeInsets = .zero
        self.addLineToView(view: phoneNumberField, position: .LINE_POSITION_BOTTOM,color: UIColor.black,width: 0.5)
        self.addLineToView(view: PasswordField, position: .LINE_POSITION_BOTTOM,color: UIColor.black,width: 0.5)
        LoginButton.layer.cornerRadius=5
        policyCard.inkView.inkColor = UIColor.inkColors.white
        policyCard.setShadowElevation(.none, for: .normal)
        licenseCard.inkView.inkColor = UIColor.inkColors.white
        licenseCard.setShadowElevation(.none, for: .normal)
    }
    func socialRegisterCompleted(_ success: Bool ){
        if success {
            self.navigationController?.popViewController(animated: true)
            makeToast("Добро пожаловать",position: .top)
        }else{
            makeToast("Произошла ошибка при получении данных пользователя", position: .top)
        }
    }
    func socialRegisterError(_ error: Error){
        makeToast("\(error)", position: .top)
        self.handleResponseError(error)
    }
    @IBAction func vkloginClicked(_ sender: Any){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if !(appDelegate.sdkInstance!.uiDelegate is NewLoginViewController){
            appDelegate.sdkInstance!.register(self)
            appDelegate.sdkInstance!.uiDelegate = self
        }
        
        VKSdk.wakeUpSession([VK_PER_EMAIL,VK_PER_PHOTOS,VK_PER_OFFLINE], complete: { (state, error) in
            if state == VKAuthorizationState.authorized {
                var parameters = Parameters()
                parameters["token"] = VKSdk.accessToken()!.accessToken!
                parameters["backend"] = "vk-oauth2"
                Network.registerSocial(parameters: parameters, responseHandler: weakCompletion(of: self, onResult: NewLoginViewController.registerSocialSuccess, onError: NewLoginViewController.registerSocialFailure))
            }else if state == VKAuthorizationState.initialized{
                VKSdk.authorize([VK_PER_EMAIL,VK_PER_PHOTOS,VK_PER_OFFLINE])
            }else{
                VKSdk.forceLogout()
                VKSdk.authorize([VK_PER_EMAIL,VK_PER_PHOTOS,VK_PER_OFFLINE])
            }
        })
    }
    func registerSocialSuccess(_ token: AuthToken){
        do{
            try saveTokenAndId(authToken: token)
            Network.getCurrentUserData(weakCompletion(of: self, onResult: NewLoginViewController.socialRegisterCompleted, onError: NewLoginViewController.socialRegisterError))
        }catch{
            self.registerSocialFailure(error)
        }
    }
    
    func registerSocialFailure(_ error: Error){
        makeToast("Произошла ошибка при получении данных пользователя", position: .top)
        self.handleResponseError(error)
    }
    @IBAction func licenseOrPolicyClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "WebViewSegue", sender: sender)
    }
    
    func loginSuccess (_ success:Bool){
        if success {
            self.navigationController?.popViewController(animated: true)
            makeToast("Добро пожаловать",position: .top)
        }else{
            makeToast("Произошла ошибка при авторизации",position: .top)
        }
    }
    func loginError (_ error:Error){
        makeToast("\(error)",position: .top)
        self.handleResponseError(error)
    }
    
    @IBAction func LoginButtonClicked(_ sender: UIButton) {
        var text = phoneNumberField.text!
        for a in text {
            if a == "+" || a == " " || a == "(" || a == ")"{
                text.remove(at: text.firstIndex(of: a)!)
            }
        }
        Network.loginUser(username:text, password: PasswordField.text!, responseHandler: weakCompletion(of: self, onResult: NewLoginViewController.loginSuccess, onError: NewLoginViewController.loginError))
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "WebViewSegue"{
            let webview = segue.destination as! NewWebViewController
            if let senderView = sender as? UIView{
                switch(senderView.restorationIdentifier){
                case "LicenseCard":
                    webview.urlString = "https://www.lapki.com/license/"
                    webview.titleString = "Лицензионное соглашение"
                    break
                case "PolicyCard":
                    webview.urlString = "https://www.lapki.com/privacy/"
                    webview.titleString = "Политика конфиденциальности"
                    break
                default:
                    break
                }
            }
        }
    }
}

extension NewLoginViewController: VKSdkDelegate,VKSdkUIDelegate {
    func vkSdkShouldPresent(_ controller: UIViewController!) {
        if !VKSdk.vkAppMayExists(){
            print("vkSdkShouldPresent(_ controller: \(String(describing: controller)))")
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    func vkSdkUserAuthorizationFailed() -> Void {
        debugPrint("vkSdkUserAuthorizationFailed")
    }
    
    func vkSdkAccessTokenUpdated(newToken:VKAccessToken?, oldToken:VKAccessToken?) -> Void {
        debugPrint("vkSdkAccessTokenUpdated(newToken:\(String(describing: newToken)),oldToken:\(String(describing: oldToken)))")
    }
    
    func vkSdkAuthorizationStateUpdated(with result:VKAuthorizationResult) -> Void {
        if result.state == .authorized {
            if result.token.accessToken!.isEmpty {
                return
            }
            let user_id = result.token.userId
            let userRequest = VKApi.users().get(["fields":["contacts","photo_id","screen_name","photo_max","photo_max_orig"]])
            userRequest?.execute(resultBlock: {
                response in
                let user = (response!.parsedModel as! VKUsersArray).firstObject()!
                var parameters = Parameters()
                parameters["username"] = user.screen_name ?? user_id
                parameters["token"] = result.token.accessToken
                parameters["email"] = result.token.email
                parameters["url_avatar"] = user.photo_max_orig ?? user.photo_max
                parameters["backend"] = "vk-oauth2"
                
                Network.registerSocial(parameters: parameters, responseHandler: weakCompletion(of: self, onResult: NewLoginViewController.registerSocialSuccess, onError: NewLoginViewController.registerSocialFailure))
                
            }, errorBlock: {
                error in
                print(error as Any)
            })
        }
    }
    
    func vkSdkShouldPresentViewController(controller:UIViewController?) -> Void {
        if !VKSdk.vkAppMayExists(){
            self.present(controller!, animated: true, completion: nil)
        }
    }
    
    func vkSdkNeedCaptchaEnter(_ captchaError: VKError!) {
        debugPrint("vkSdkNeedCaptchaEnter(captchaError:\(String(describing: captchaError)))")
    }
    
    func vkSdkAccessAuthorizationFinished(with result: VKAuthorizationResult!) {
        debugPrint("vkSdkNeedCaptchaEnter(captchaError:\(String(describing: result)))")
    }
}



