//
//   NewNoticeDetailViewController.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 10/12/2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//

import UIKit
import ImageSlideshow
import MaterialComponents.MaterialButtons
import MessageUI

class  NewNoticeDetailViewController: UIViewController,CAAnimationDelegate, AddEditNoticeDelegate {
    
    
    var wasUplifted: Bool?
    var parentView: UIViewController!
    var myMainViewController: MyMainViewController!
    var owner: MyUser?
    
    
    
    
    @IBOutlet weak var upliftButton: MyButton!
    @IBOutlet weak var upliftCard: MDCCard!
    @IBOutlet weak var errorIcon: UIImageView!
    @IBOutlet weak var statisticCard: MDCCard!
    @IBOutlet var titleToPrice: NSLayoutConstraint!
    @IBOutlet var titleToTop: NSLayoutConstraint!
    @IBOutlet weak var subSubTypeLabel: UILabel!
    @IBOutlet weak var subSubTypeCard: MDCCard!
    @IBOutlet weak var subTypeLabel: UILabel!
    @IBOutlet weak var subTypeCard: MDCCard!
    @IBOutlet weak var descriptionCard: MDCCard!
    @IBOutlet weak var typeCard: MDCCard!
    @IBOutlet weak var categoryCard: MDCCard!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var noticeDescriptionTF: UILabel!
    @IBOutlet weak var addressCard: MDCCard!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var noticeTitleTF: UILabel!
    @IBOutlet weak var sendButton: MDCButton!
    @IBOutlet weak var callButton: MDCButton!
    @IBOutlet weak var noticePriceTF: UILabel!
    @IBOutlet var stackView: UIStackView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageSlideShow: ImageSlideshow!
    @IBOutlet var titleCard: MDCCard!
    @IBOutlet var priceToTop: NSLayoutConstraint!
    @IBOutlet weak var statusCard: MDCCard!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var rejectionReasonLabel: UILabel!
    @IBOutlet weak var ageCard: MDCCard!
    @IBOutlet weak var genderCard: MDCCard!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var viewsLabel: UILabel!
    @IBOutlet weak var favoritesLabel: UILabel!
    
    func addEdit(_ result: Notice) {
        self.notice = result
    }
    
    var notice: Notice! {
        didSet{
            //Notice Date
            if let date: Date = DateFormatter().date(fromPythonDate: self.notice.date){
                self.statisticCard.isHidden = false
                let dateFormatter = DateFormatter()
                dateFormatter.locale = Locale(identifier: "ru_RU")
                dateFormatter.dateFormat = "dd MMMM YYYY"
                self.dateLabel.text = "Дата публикации: \(dateFormatter.string(from: date))"
            }else{
                self.statisticCard.isHidden = true
            }
            //Notice Views
            if let views: Int = self.notice.views {
                self.statisticCard.isHidden = false
                self.viewsLabel.text = "Просмотров: \(views)"
            }else{
                self.statisticCard.isHidden = true
            }
            if let inFavorites = notice.inFavorites {
                self.favoritesLabel?.text = "В избранном: \(inFavorites)"
            }else{
                self.favoritesLabel.removeFromSuperview()
            }
            self.statisticCard.sizeToFit()
            //Notice Age
            if self.notice.age > 0 {
                self.ageCard.isHidden = false
                var yearString, monthString, weekString: String?
                switch (self.notice.oldYear % 10){
                case Int.min...0:
                    break
                case 1:
                    yearString = " \(self.notice.oldYear) год"
                    break
                case 2,3,4:
                    yearString = " \(self.notice.oldYear) года"
                    break
                default:
                    yearString = " \(self.notice.oldYear) лет"
                    break
                }
                switch (self.notice.oldMonth){
                case Int.min...0:
                    break
                case 1:
                    monthString = " \(self.notice.oldMonth) месяц"
                    break
                case 2,3,4:
                    monthString = " \(self.notice.oldMonth) месяца"
                    break
                default:
                    monthString = " \(self.notice.oldMonth) месяцев"
                    break
                }
                if self.notice.oldWeek > 0 {
                    if self.notice.oldWeek == 1 {
                        weekString = " \(self.notice.oldWeek) неделя"
                    }else{
                        weekString = " \(self.notice.oldWeek) недели"
                    }
                    if (self.notice.oldYear > 0 || self.notice.oldMonth > 0) {
                        weekString = " и" + weekString!
                    }
                }
                self.ageLabel.text = "Возраст: \(yearString ?? "")\(monthString ?? "")\(weekString ?? "")"
                self.ageCard.sizeToFit()
            }else {
                self.ageCard.isHidden = true
            }
            //Notice gender
            if !(self.notice.gender?.isEmpty ?? true) {
                self.genderCard.isHidden = false
                self.genderLabel.text = "Пол: \(self.notice.gender!)"
            }else{
                self.genderCard.isHidden = true
            }
            //Notice uplift button
            if notice.status.contains("active") && (MyStaticUser.id == notice.owner || (notice.category.contains(oneOf: ["Потеря","Находка"]) && notice.price < 1)){
                upliftCard.inkView.inkColor = UIColor.clear
                if notice.category.contains(oneOf: ["Потеря","Находка"]) && MyStaticUser.id != notice.owner {
                    let string = (notice.category.contains("Потеря")) ? "Помочь найти питомца": "Помочь найти хозяина"
                    self.upliftButton.setTitle(string, for: .normal)
                }
                self.upliftCard.sizeToFit()
            }else {
                self.upliftCard?.removeFromSuperview()
            }
            //Notice status
            statusCard?.inkView.inkColor = UIColor.white.withAlphaComponent(0)
            if notice.status.elementsEqual("active"){
                statusCard?.backgroundColor = UIColor.white
                statusCard?.removeFromSuperview()
                errorIcon?.removeFromSuperview()
            }else {
                errorIcon?.tintColor = UIColor.red
                statusLabel.textColor = UIColor.black
                rejectionReasonLabel?.textColor = UIColor.black
                statusCard?.backgroundColor = UIColor.red.withAlphaComponent(0.40)
                switch notice.status {
                case "rejected":
                    statusLabel.text = "Не одобрено"
                case "moderation":
                    statusLabel.text = "Ждёт одобрения"
                    errorIcon?.removeFromSuperview()
                case "deleted":
                    statusLabel.text = "В архиве"
                    errorIcon?.removeFromSuperview()
                default: break
                }
                if !(notice.reasonRejection?.isEmpty ?? true) {
                    rejectionReasonLabel?.text = notice.reasonRejection
                }else{
                    rejectionReasonLabel?.removeFromSuperview()
                }
            }
            //Notice address
            addressLabel.text = notice.locationAddress
            addressLabel.sizeToFit()
            //Notice category
            categoryCard.inkView.inkColor = UIColor.white.withAlphaComponent(0)
            if !notice.category.isEmpty{
                categoryLabel.text = notice.category
                categoryLabel.sizeToFit()
            }else {
                categoryCard.isHidden = true
            }
            //Notice type
            typeCard.inkView.inkColor = UIColor.white.withAlphaComponent(0)
            if !notice.type.isEmpty {
                typeLabel.text = notice.type
                typeLabel.sizeToFit()
            }else {
                typeCard.isHidden = true
            }
            //Call and Send buttons
            if notice.owner == MyStaticUser.id {
                callButton?.removeFromSuperview()
                sendButton?.removeFromSuperview()
            }else{
                navigationItem.rightBarButtonItems?.removeAll()
            }
            //Notice subtype
            subTypeCard.inkView.inkColor = UIColor.white.withAlphaComponent(0)
            if notice.subtype != nil && !notice.subtype!.isEmpty{
                subTypeLabel.text = notice.subtype!
                subTypeLabel.sizeToFit()
            }else {
                subTypeCard.isHidden = true
            }
            //Notice subSubType
            subSubTypeCard.inkView.inkColor = UIColor.white.withAlphaComponent(0)
            if notice.subsubtype != nil && !notice.subsubtype!.isEmpty{
                subSubTypeLabel.text = notice.subsubtype!
                subSubTypeLabel.sizeToFit()
            }else {
                subSubTypeCard.isHidden = true
            }
            //Notice Title
            titleCard.inkView.inkColor = UIColor.white.withAlphaComponent(0)
            noticeTitleTF.text = notice.title
            //Notice Price
            noticePriceTF?.isHidden = false
            if notice.price == -1 {
                noticePriceTF?.removeFromSuperview()
            }else if notice.price == 0 {
                noticePriceTF?.text = "Бесплатно"
            }else if notice.price > 0 {
                noticePriceTF?.text = "\(notice.price) Р"
            }
            noticePriceTF?.sizeToFit()
            //Notice description
            descriptionCard.inkView.inkColor = UIColor.white.withAlphaComponent(0)
            noticeDescriptionTF.text = notice.description
            noticeDescriptionTF.sizeToFit()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initScrollStackView()
        initSlideView()
        Network.getUserData(id:"\(notice!.owner)",responseHandler: weakCompletion(of: self, onResult: NewNoticeDetailViewController.setOwner, onError: NewNoticeDetailViewController.handleResponseError))
        Network.noticeViewed(id: self.notice.id)
    }
    
    func setOwner(_ owner: MyUser){
        self.owner = owner
    }
    
    @IBAction func upliftBtnClick(_ sender: AnyObject){
        // TODO: Переход на поднятие
        guard MyStaticUser.id != 0 else{
            makeToast("Доступно только авторизированным пользователям", position: .top)
            return
        }
        self.performSegue(withIdentifier: "UpliftSegue", sender: sender)
    }
    
    @IBAction func shareSocialClick(_ sender:UIButton){
        let title = notice.title
        guard let image = KingfisherSource(urlString: notice.image1!)else {return}
        let body = notice.description
        let footer = "Подробности в приложении Лапки:"
        let url = URL(string: "https://www.lapki.com/redirect/?ad=" + "\(notice.id)")
        let activityViewController: UIActivityViewController = UIActivityViewController(
            activityItems: [title,image,body,footer,url!], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = sender
        activityViewController.excludedActivityTypes = [
            UIActivity.ActivityType.postToWeibo,
            UIActivity.ActivityType.print,
            UIActivity.ActivityType.assignToContact,
            UIActivity.ActivityType.saveToCameraRoll,
            UIActivity.ActivityType.addToReadingList,
            UIActivity.ActivityType.postToFlickr,
            UIActivity.ActivityType.postToVimeo,
            UIActivity.ActivityType.postToTencentWeibo
        ]
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    
    
    @IBAction func callButtonClick(_ sender: Any){
        guard let phonenumber = self.owner?.phonenumber else{
            print("Ошибка при звонке!")
            return
        }
        guard let url = URL(string: "tel://\(phonenumber)"), UIApplication.shared.canOpenURL(url) else {
            print("Ошибка при звонке! \(phonenumber)")
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler:nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination{
        case let upliftVC as NewUpliftViewController:
            upliftVC.notice = self.notice
            self.wasUplifted = true
        case let noticeMapVC as NewNoticeDetailMapViewController:
            noticeMapVC.notice = self.notice
        case let editNoticeVC as NewAddNoticeViewController:
            editNoticeVC.delegate = self
            editNoticeVC.notice = self.notice.toEditNotice()
        default:
            break
        }
    }
    
    @IBAction func addressCardClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "NoticeDetailMapSegue", sender: self)
    }
    
    @IBAction func sendButtonClicked(_ sender: Any){
        guard let phonenumber = self.owner?.phonenumber else {return}
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = ""
            controller.recipients = [phonenumber]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    func initScrollStackView(){
        if !self.scrollView.subviews.contains(self.stackView){
            self.scrollView.addSubview(self.stackView)
            self.stackView.translatesAutoresizingMaskIntoConstraints = false
            self.stackView.leadingAnchor.constraint(equalTo: self.scrollView.leadingAnchor).isActive = true
            self.stackView.trailingAnchor.constraint(equalTo: self.scrollView.trailingAnchor).isActive = true
            self.stackView.topAnchor.constraint(equalTo: self.scrollView.topAnchor).isActive = true
            self.stackView.bottomAnchor.constraint(equalTo: self.scrollView.bottomAnchor).isActive = true
            self.stackView.widthAnchor.constraint(equalTo: self.scrollView.widthAnchor).isActive = true
        }
    }
    func initSlideView(){
        var sources = [InputSource]()
        if notice.image1 != nil && KingfisherSource(urlString: notice.image1!) != nil {
            sources.append(KingfisherSource(urlString: notice.image1!)!)
        }
        if notice.image2 != nil && KingfisherSource(urlString: notice.image2!) != nil{
            sources.append(KingfisherSource(urlString: notice.image2!)!)
        }
        if notice.image3 != nil && KingfisherSource(urlString: notice.image3!) != nil {
            sources.append(KingfisherSource(urlString: notice.image3!)!)
        }
        if notice.image4 != nil && KingfisherSource(urlString: notice.image4!) != nil {
            sources.append(KingfisherSource(urlString: notice.image4!)!)
        }
        if notice.image5 != nil && KingfisherSource(urlString: notice.image5!) != nil {
            sources.append(KingfisherSource(urlString: notice.image5!)!)
        }
        imageSlideShow.setImageInputs(sources)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(imageClicked(_:)))
        
        imageSlideShow.activityIndicator = DefaultActivityIndicator(style: .white, color: UIColor.lapkiColors.lostOrange)
        imageSlideShow.sd_setShowActivityIndicatorView(true)
        imageSlideShow.addGestureRecognizer(gesture)
        imageSlideShow.backgroundColor = UIColor.black
    }
    @objc func imageClicked(_ sender: Any){
        imageSlideShow.presentFullScreenController(from: self)
    }
}
extension  NewNoticeDetailViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
extension  NewNoticeDetailViewController: MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
    }
}
