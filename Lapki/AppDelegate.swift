//
//  AppDelegate.swift
//  Lapki
//
//  Created by Yuriy Yashchenko on 23.10.2018.
//  Copyright © 2018 MEGAKOM, OOO. All rights reserved.
//
import Siren
import UIKit
import Kingfisher
import CoreLocation
import NotificationCenter
import UserNotifications
import Firebase
import VK_ios_sdk
import ok_ios_sdk
import IQKeyboardManagerSwift
import Firebase


public var lastUserLocation: CLLocation?
var START_TIME:CFAbsoluteTime = CFAbsoluteTimeGetCurrent()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate, UNUserNotificationCenterDelegate{
    
    var window: UIWindow?
    private var myUser: MyUser?
    var sdkInstance: VKSdk?
    let VK_APP_ID = "6703641"
    var locationManager: CLLocationManager?
    private var notices: Notices!
    private var notificated: [Int]?
    var alertsavailable = false
    let center = UNUserNotificationCenter.current()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window?.makeKeyAndVisible()
        if FirebaseApp.app() == nil {
            FirebaseApp.configure()
        }
        self.window?.backgroundColor = UIColor.white
        setupLocationManager()
        let siren = Siren.shared
        siren.apiManager = APIManager(countryCode: "RU")
        siren.rulesManager = RulesManager(majorUpdateRules: .critical,
                                          minorUpdateRules: .critical,
                                          patchUpdateRules: .default,
                                          showAlertAfterCurrentVersionHasBeenReleasedForDays: 1)
        siren.presentationManager = PresentationManager(alertTintColor: UIColor.lapkiColors.orange,forceLanguageLocalization: Localization.Language.russian)
        siren.wail()
        DispatchQueue.global().async {
            print("azazaza1 \(isRussia(lat: 45.204469, lng: 36.848451))")
            getAuthToken(tokenHandler: { _ in
                Network.getCurrentUserData({ (_,_) in})
            })
            
            Network.updateAds()
            let currentVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
            let prev = UserDefaults.standard.object(forKey: "appVersion") as? String
            if !(prev?.elementsEqual(currentVersion) ?? false){
                URLCache.shared.removeAllCachedResponses()
                KingfisherManager.shared.cache.clearMemoryCache()
                KingfisherManager.shared.cache.clearDiskCache()
                KingfisherManager.shared.cache.cleanExpiredDiskCache()
            }
            UserDefaults.standard.set(currentVersion, forKey: "appVersion")
            UserDefaults.standard.synchronize()
        }
        sdkInstance = VKSdk.initialize(withAppId: self.VK_APP_ID)
        IQKeyboardManager.shared.enable = true
//        Network.getNotices(status: "active", responseHandler: weakCompletion(of: self, onResult: AppDelegate.noticesResult, onError: AppDelegate.noticesError))
        StoreReviewHelper.incrementAppOpenedCount()
        return true
    }
    
    func noticesResult(_ notices: PageResult){
        //TODO: Прогрузку объявлений до старта приложения
        dump(notices)
    }
    func noticesError(_ error: Error){
        dump(error)
    }
    @available(iOS 10.0, *)
    func application(
        _ app: UIApplication,
        open url: URL,
        options: [UIApplication.OpenURLOptionsKey : Any] = [:]
        ) -> Bool {
        
        var parameters: [String: String] = [:]
        URLComponents(url: url, resolvingAgainstBaseURL: false)?.queryItems?.forEach {
            parameters[$0.name] = $0.value
        }
        
        if url.host == "ad"{
            if let val = parameters["id"]{
                Network.getNotice(id: Int(val)!, responseHandler: weakCompletion(of: self, onResult: AppDelegate.openNotice, onError: AppDelegate.handleError))
            }
        }
        
        let app = options[.sourceApplication] as? String
        VKSdk.processOpen(url, fromApplication: app)
        OKSDK.open(url)
        return true
    }
    
    func openNotice(_ notice: Notice){
        let storyBoard = UIStoryboard.init(name: "NoticeDetailStoryBoard", bundle: nil)
        guard let vc = storyBoard.instantiateViewController(withIdentifier: "NoticeDetailViewController") as? NewNoticeDetailViewController,
        let nav = UIApplication.topViewController()?.navigationController  else {return}
        vc.notice = notice
        nav.pushViewController(vc, animated: true)
    }
    
    func handleError(_ error: Error){
        print("\(error)")
    }
    
    func setupLocationManager(){
        if locationManager != nil {
            removeLocationManager()
        }
        locationManager = CLLocationManager()
        
        locationManager?.delegate = self
        let status = CLLocationManager.authorizationStatus()
        if status != .authorizedAlways && status != .authorizedWhenInUse {
            locationManager?.requestAlwaysAuthorization()
            return
        }
        if status == .authorizedAlways {
            if !CLLocationManager.significantLocationChangeMonitoringAvailable() {return}
            locationManager?.allowsBackgroundLocationUpdates = true
            locationManager?.pausesLocationUpdatesAutomatically = false
            locationManager?.startMonitoringSignificantLocationChanges()
        }else if status == .authorizedWhenInUse {
            locationManager?.allowsBackgroundLocationUpdates = false
            locationManager?.pausesLocationUpdatesAutomatically = true
            locationManager?.distanceFilter = 250
            locationManager?.desiredAccuracy = kCLLocationAccuracyHundredMeters
            locationManager?.startUpdatingLocation()
        }
        if center.delegate == nil {
            setupNotificationCenter()
        }
    }
    func removeLocationManager(){
        locationManager?.stopMonitoringSignificantLocationChanges()
        locationManager?.stopUpdatingLocation()
        locationManager?.delegate = nil
        locationManager = nil
    }
    
    func setupNotificationCenter(){
        center.delegate = self
        center.requestAuthorization(options: [.alert,.sound,.badge], completionHandler: {(granted, error) in
            if !granted {
                self.locationManager?.stopMonitoringSignificantLocationChanges()
                self.locationManager?.stopUpdatingLocation()
                self.alertsavailable = false
            }else{
                self.center.getNotificationSettings(completionHandler: {
                    settings in
                    guard settings.authorizationStatus == .authorized else {return}
                })
            }
        })
        let noticeNotificationCategory = UNNotificationCategory(identifier: "socialNotice", actions: [], intentIdentifiers: [], options: [])
        center.setNotificationCategories([noticeNotificationCategory])
    }
    
    func application(
        _ application: UIApplication,
        open url: URL,
        sourceApplication: String?,
        annotation: Any
        ) -> Bool {
        VKSdk.processOpen(url, fromApplication: sourceApplication)
        OKSDK.open(url)
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        //        UNUserNotificationCenterDelegate
        let status = CLLocationManager.authorizationStatus()
        if status == .authorizedWhenInUse {
            removeLocationManager()
        }
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        setupLocationManager()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        let status = CLLocationManager.authorizationStatus()
        if status == .authorizedWhenInUse {
            removeLocationManager()
        }
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .denied, .notDetermined, .restricted:
            print("\(manager) CLAuthorizationStatus: \(status)")
        case .authorizedWhenInUse,.authorizedAlways:
            Network.getSocialNotices(responseHandler: weakCompletion(of: self, onResult: AppDelegate.setSocialNotices, onError: AppDelegate.handleRequestError))
        default:
            break
        }
    }
    func setSocialNotices(_ notices: Notices){
        self.notices = notices
        self.alertsavailable = true
    }
    func handleRequestError(_ error: Error){
        print("\(error)")
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        lastUserLocation = locations.last
        NotificationCenter.default.post(Notification(name: Notification.Name("locationUpdated"), object: nil))
        if alertsavailable {
            if notificated == nil {
                notificated = (UserDefaults.standard.array(forKey: "notificatedArray") as? [Int])  ?? [Int]()
            }
            for notice in self.notices{
                if !notificated!.contains(notice.id){
                    let loc = CLLocation(latitude: notice.lat, longitude: notice.lng)
                    if loc.distance(from: locations.last!) < 2000 {
                        let content = UNMutableNotificationContent()
                        content.categoryIdentifier = "socialNotice"
                        if notice.category.contains("Потеря"){
                            content.title = "В вашем районе потерялся питомец!"
                            content.body = "Возможно вы его видели"
                        }else if notice.category.contains("Находка"){
                            content.title = "В вашем районе нашелся питомец!"
                            content.body = "Возможно вы знаете его хозяина"
                        }
                        content.sound = UNNotificationSound.default
                        content.badge = 0
                        let ndata = (try? JSONEncoder().encode(notice)) ?? Data()
                        content.userInfo = ["notice":ndata]
                        let uuidString = UUID().uuidString
                        let request = UNNotificationRequest(identifier: uuidString, content: content, trigger: nil)
                        center.add(request, withCompletionHandler: nil)
                        self.notificated!.append(notice.id)
                    }
                }
            }
            UserDefaults.standard.set(self.notificated!, forKey: "notificatedArray")
            UserDefaults.standard.synchronize()
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        if let error = error as? CLError, error.code == .denied {
            removeLocationManager()
            return
        }
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let mainViewController = storyBoard.instantiateInitialViewController() as! MyMainViewController
        mainViewController.notice = try! JSONDecoder().decode(Notice.self, from:  response.notification.request.content.userInfo["notice"] as! Data)
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = mainViewController
        self.window?.makeKeyAndVisible()
    }
}

